﻿using UnityEngine;
using System.Collections;

namespace FrispGames.Social {

	public class ScreenshotSharer : MonoBehaviour {
        public GameObject canvas_3;
        public Rect windowRect;
		private static readonly ScreenshotSharer _singleton = new ScreenshotSharer ();

		private ScreenshotSharer() {}

		public static ScreenshotSharer Instance() {
			return _singleton;
		}

		public IEnumerator PostScreenshot(string title, string message) {
            
            GameObject canvas= GameObject.Find("Canvas").GetComponent<Canvas>().gameObject;
            GameObject canvas_1 = GameObject.Find("Canvas_1").GetComponent<Canvas>().gameObject;
            GameObject canvas_2 = GameObject.Find("Canvas_2").GetComponent<Canvas>().gameObject;
          //  GameObject canvas_3 = GameObject.Find("Canvas_3").GetComponent<Canvas>().gameObject;
            
            canvas_1.SetActive(false);
            canvas_2.SetActive(false);
            canvas.SetActive(false);
            canvas_3.SetActive(true);
			yield return new WaitForEndOfFrame();
            //windowRect = new Rect(0, 0, 400, 300);
            //int newx = Mathf.FloorToInt(windowRect.x * Screen.width);
            //int newy = Mathf.FloorToInt(windowRect.y * Screen.height);
            ////int neww = Mathf.FloorToInt(windowRect.width);
            ////int newh = Mathf.FloorToInt(windowRect.height);
            //int neww = Mathf.FloorToInt(Screen.width * windowRect.width);
            //int newh = Mathf.FloorToInt(Screen.height * windowRect.height);
            //Debug.Log(newx + " " + newy + " " + neww + " " + newh);
            ////newx = Mathf.FloorToInt((Screen.width*.75f)-(windowRect.width/2));
            ////newy = Mathf.FloorToInt((Screen.height / 2) - (windowRect.height / 2));
            ////Texture2D screenshot = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, true);  
            //Texture2D screenshot = new Texture2D(newh, newh, TextureFormat.RGB24, true);
            //yield return new WaitForEndOfFrame();
            ////screenshot.ReadPixels(new Rect(newx, Screen.height - newy - newh, neww, newh), 0, 0, false);  // old
            //screenshot.ReadPixels(new Rect(newx + ((neww - newh) / 2), newy, newh, newh), 0, 0, false);
            //screenshot.Apply();
            //if (Application.platform == RuntimePlatform.Android)
            //{
            //    Api.AndroidScreenshotSharer.Instance().ShareImage(title, message, screenshot);
            //}
            //else if (Application.platform == RuntimePlatform.IPhonePlayer)
            //{
            //    Api.AppleScreenshotSharer.Instance().ShareImage(message, screenshot);
            //}
            //Destroy(screenshot);

            // Create a texture the size of the screen, RGB24 format
            int width = Screen.width;
            int height = Screen.height;
            Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, false);
            // Read screen contents into the texture
            tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
            tex.Apply();

            if (Application.platform == RuntimePlatform.Android)
            {
                Api.AndroidScreenshotSharer.Instance().ShareImage(title, message, tex);
            }
            else if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                Api.AppleScreenshotSharer.Instance().ShareImage(message, tex);
            }

            //Destroy(tex);
            yield return new WaitForEndOfFrame();
            canvas.SetActive(true);
            canvas_1.SetActive(true);
            canvas_2.SetActive(true);
            canvas_3.SetActive(false);
            Debug.Log(canvas.activeSelf);
            //GameObject.Find("MyCanvas").GetComponent<Canvas>().enabled = enabled;
		}
	}
}