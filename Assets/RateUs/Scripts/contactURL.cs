﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class contactURL : MonoBehaviour
{
    GameObject SoundManagereObj;
    SoundManagerClass SoundManScript;
    void Awake()
    {

        SoundManagereObj = GameObject.FindGameObjectWithTag("SoundManager");
        SoundManScript = (SoundManagerClass)SoundManagereObj.GetComponent("SoundManagerClass");
    }
    public void SocialButton(string url)
    {
        Application.OpenURL(url);
        SoundManScript.PlaySound(SoundManScript.Botton_Clip);
    }
}
