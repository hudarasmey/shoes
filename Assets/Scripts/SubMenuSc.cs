﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public enum MaterialSelected { texture, pattern, none };
public enum MaterialType { Mat, Fabric, Gloosy, HalfGloosy, Metal, Glass };
public class SubMenuSc : MonoBehaviour
{
    public ObjMenu ObjIs;
    public int materialIndex;
    public Material TargetMaterial;
    MaterialParam MatParm;
    //MaterialParam Mat2Parm;
    //MaterialParam Mat3Parm;
    public Shose shose;
    public GameObject shosePart;
    public MaterialType matType, tempMatType;
    public GameObject colorPicker;
    public GameObject colorListPicker;
    //public ColorPickerSliderManager colorPickerManager;
    public ScrollListTest materialSlider;
    public ScrollListTest PatternList;
    // Use this for initialization
    public MaterialMenu tempMatMenu;
    //public Button mainMatBtn, colorBtn, PatternBtn;

    //public Button MatColorBtn, GlossyBtn, HalfGloosyBtn, GlassBtn,MetalBtn;
    public Vector4 MaterialPara;
    public Vector4 matMaterialPara;//, glossyMaterialPara, HalfGlossyMaterialPara, GlassMaterialPara, MetalMaterialPara;// x= metallic ,y=Smoothness ,z=occlution
    //public Vector3 matMaterialPara
    public bool matChange;
    public Color tempColor;
    public float maxTileValue = 30;
    public MaterialSelected MatSelect, tempMatSelect;
    bool itCleared;
    bool isStartCase;
    bool isMaterialStart;
    public Texture2D selectedTexteure;
    public Sprite selectedSprite;
    public Texture2D normalTexture;
    Texture2D TempNormalTexture;
    public Texture2D detailNormalMap;

    public PopUpButton Button;
    public ColorPickerSliderManager ColorManager;
    public modelControl1 ModelManager;
    public ParameterMatEffect parameterMatEffect;
    public Vector2 tileValue = new Vector2(2, 2), TemptileValue;
    float Temptile = 1;
    public MaterialMenusManager materialMenusManager;
    public BtnActivationScript BtnActivation,MatPatActivation;
    public BtnActivationScript BtnMatTypeActivation;
    public int targetMaterialIndex = 0;
    public PatternCollectionManager patternCollectionManager;
   // public GameObject SpinningLoadingBar;
    public Image progressbar;
    int patternCollectionManagerIndexTemp;
    public Vector2 FixedBmpTile = new Vector2(2f, 2f);
    float alpha;
    Color TempPickColor;
    GameObject SoundManagereObj;
    SoundManagerClass SoundManScript;
    public GameObject particalManagereObj;
    public ParticalManager particalManScript;
    ShopMan shop;
    public bool isLoadNormalImage;

    void Awake()
    {
        shop = GameObject.FindGameObjectWithTag("UnlockWin").GetComponent<ShopMan>();

        parameterMatEffect =GameObject.FindGameObjectWithTag("MatParamBank").GetComponent<ParameterMatEffect>();
        SoundManagereObj = GameObject.FindGameObjectWithTag("SoundManager");
        SoundManScript = (SoundManagerClass)SoundManagereObj.GetComponent("SoundManagerClass");
        particalManagereObj = GameObject.FindGameObjectWithTag("ParticalManager");
        particalManScript = (ParticalManager)particalManagereObj.GetComponent("ParticalManager"); 
            //  tileValue = 1;
         MatSelect = MaterialSelected.texture;
        tempMatSelect = MaterialSelected.none;
        tempColor = Color.white;
        MatParm = new MaterialParam();
        //Mat2Parm = new MaterialParam();
        //Mat3Parm = new MaterialParam();
        tempMatMenu = MaterialMenu.texture;
        matType = MaterialType.Mat;
        tempMatType = MaterialType.Mat;
        switch (ObjIs)
        {
            case ObjMenu.Face:
                shosePart = shose.faceObj;
                break;
            case ObjMenu.Base:
                shosePart = shose.baseObj;
                break;
            case ObjMenu.Heel:
                shosePart = shose.heelObj;
                break;
            case ObjMenu.none:
                break;
            default:
                break;
        }
        //colorPicker.GetComponent<ColorPicker>().getColorMaterial(shosePart,materialIndex);/////////////////////////////////dee el beta5o d el loon
        ////colorPicker.GetComponent<ColorPicker>().setColorMaterial();

        //materialSlider.transform.parent.transform.gameObject.SetActive(false);
        //tempColor = Color.black;




        //ColorManager.getColorMaterial(shosePart, materialIndex);
        matChange = true;
        materialSlider.selectedIndex = 0;
        materialSlider.IsAction = true;
        //TexureChangeFn(MaterialMenu.texture);
        //ColorControler();
        //TileTexure(tileValue);
        //MaterialTypeFn();
        MaterialPara = matMaterialPara;
        
        materialSlider.selectedIndex = 0;

        //  materialSlider.transform.parent.transform.gameObject.SetActive(false);

    }

    //IEnumerator MyCoroutine(Material Mat)
    //{
    //    isStartHighlight = true;
    //    Color StartColor = Mat.color;
    //    Color TempStart = StartColor;
    //    highlightColor = new Color(
    //        StartColor.r * 1.5f,
    //        StartColor.g * 1.5f,
    //        StartColor.b * 1.5f
    //    );
    //    Mat.color = highlightColor;
    //    yield return new WaitForSeconds(.5f);
    //    Mat.color = StartColor;
    //    isStartHighlight = false;

    //    // print("Reached the target.");

    //}
    void OnEnable()
    {
        //materialSlider.gameObject.GetComponent<Image>().enabled = false;
        //colorListPicker.GetComponent<Image>().enabled = false;
        // SelectedPattrernMenuIndex
        //if (targetMaterialIndex >= 0 )
        //{
      //  savedIndex,tempClickedIndex
       // SpinningLoadingBar.SetActive(false);
        BtnMatTypeActivation.activateBtn(materialMenusManager.materialType[targetMaterialIndex]);

        //   Debug.Log(materialMenusManager.TextureSelectedIndex[targetMaterialIndex] + "   " + materialMenusManager.PatternSelectedIndex[targetMaterialIndex]);
        //    PatternList = patternCollectionManager.PatternsScrollList[materialMenusManager.SelectedPattrernMenuIndex[targetMaterialIndex]];
        if (materialMenusManager.PatternSelectedIndex[targetMaterialIndex] != -1)
        {
            //Debug.Log("kkggkk");
            patternCollectionManager.PatternsSelectScroll.gameObject.SetActive(true);
            colorListPicker.transform.parent.gameObject.SetActive(false);
            materialSlider.gameObject.transform.parent.transform.gameObject.SetActive(false);

            BtnActivation.activateBtn(1);
            //ButtonClick(materialMenusManager.materialSelected[targetMaterialIndex]);
            ButtonClick(2);
            tempMatMenu = MaterialMenu.pattern;

            //MatSelect = MaterialSelected.pattern;
            //patternCollectionManager.gameObject.SetActive(true);
            //PatternList.gameObject.transform.parent.gameObject.SetActive(true);
            //patternCollectionManager.enablePatternCollection(true);
            //patternCollectionManager.resetScrollesExcept(materialMenusManager.SelectedPattrernMenuIndex[targetMaterialIndex]);
            patternCollectionManager.clearselector();//materialMenusManager.SelectedPattrernMenuIndex[targetMaterialIndex]);

            //materialSlider.cells[materialMenusManager.SelectedPattrernMenuIndex[targetMaterialIndex]].SetMarker(true);
            if (materialSlider.cells.Count >= 1)
            {
                materialSlider.cells[0].SetMarker(true);

            }
            //PatternList.myScrollRect.horizontalNormalizedPosition = materialMenusManager.colorTexHorizontalNormalizedPosition[targetMaterialIndex];
            //PatternList.gameObject.transform.parent.gameObject.SetActive(true);
            //PatternList.gameObject.GetComponent<Image>().enabled = true;
            //materialSlider.gameObject.GetComponent<Image>().enabled = false;
            //colorListPicker.gameObject.GetComponent<Image>().enabled = false;

        }
        else
        {
            //patternCollectionManager.gameObject.SetActive(false);
            //PatternList.gameObject.transform.parent.gameObject.SetActive(false);
            //patternCollectionManager.enablePatternCollection(false);
            tempMatMenu = MaterialMenu.texture;
            BtnActivation.activateBtn(0);
            ButtonClick(1);
            patternCollectionManager.PatternsSelectScroll.gameObject.SetActive(false);
            //MatSelect = MaterialSelected.texture;
            patternCollectionManager.resetScrollesExcept(-1);
            //PatternList.myScrollRect.horizontalNormalizedPosition = 0;
            //PatternList.gameObject.transform.parent.gameObject.SetActive(false);
            //PatternList.gameObject.GetComponent<Image>().enabled = false;
            //materialSlider.gameObject.GetComponent<Image>().enabled = true;
            //colorListPicker.gameObject.GetComponent<Image>().enabled = true;
            colorListPicker.transform.parent.gameObject.SetActive(true);
            materialSlider.gameObject.transform.parent.transform.gameObject.SetActive(true);


            if (materialMenusManager.TextureSelectedIndex[targetMaterialIndex] == -1)
            {

                //  materialSlider.cells[0].clickit();
                patternCollectionManager.resetScrollesExcept(-1);
                if (materialSlider.IsVerticalList)
                {
                    materialSlider.myScrollRect.verticalNormalizedPosition = 1;
                }
                else
                {
                    materialSlider.myScrollRect.horizontalNormalizedPosition = 0;
                }


                materialSlider.tempIndex = materialSlider.selectedIndex = -1;



                // materialSlider.IsAction = true;
                if (materialSlider.cells.Count >= 1)
                {
                   // Debug.Log("kkkjkkk");
                    materialSlider.cells[0].SetMarker(true);
                    materialSlider.cells[0].relatedList.selectedIndex = 0;
                    materialSlider.cells[0].relatedList.IsAction = true;
                    ////////////////////////materialSlider.cells[0].clickit();
                    
                }
                
               // Debug.Log("kkkjkkk");
            }
            else
            {
                //Debug.Log("jjjkkk");
                if (materialSlider.IsVerticalList)
                {
                    materialSlider.myScrollRect.verticalNormalizedPosition = materialMenusManager.BWTexHorizontalNormalizedPosition[targetMaterialIndex];
                }
                else
                {
                    materialSlider.myScrollRect.horizontalNormalizedPosition = materialMenusManager.BWTexHorizontalNormalizedPosition[targetMaterialIndex];
                }
                // materialSlider.myScrollRect.horizontalNormalizedPosition = materialMenusManager.BWTexHorizontalNormalizedPosition[targetMaterialIndex];
                materialSlider.cells[materialMenusManager.TextureSelectedIndex[targetMaterialIndex]].SetMarker(true);
             //   materialSlider.MarkAgain(materialMenusManager.TextureSelectedIndex[targetMaterialIndex]);
                // targetMaterialIndex
                materialSlider.savedIndex = materialSlider.tempClickedIndex = materialMenusManager.TextureSelectedIndex[targetMaterialIndex];
            }
        }
        //     Debug.Log("dfdf"+materialMenusManager.ColorSelectedIndex[targetMaterialIndex] );//+ "  " + colorListPicker.GetComponent<ScrollListTest>().cells[materialMenusManager.ColorSelectedIndex[targetMaterialIndex]]);
        if (materialMenusManager.ColorSelectedIndex[targetMaterialIndex] == -1)
        {
            colorListPicker.GetComponent<ScrollListTest>().tempIndex = colorListPicker.GetComponent<ScrollListTest>().selectedIndex = 0;

            if (colorListPicker.GetComponent<ScrollListTest>().cells.Count >= 0)
            {
                colorListPicker.GetComponent<ScrollListTest>().cells[0].SetMarker(true);
            }
            if (colorListPicker.GetComponent<ScrollListTest>().IsVerticalList)
            {
                colorListPicker.GetComponent<ScrollListTest>().myScrollRect.verticalNormalizedPosition = 1;
            }
            else
            {
                colorListPicker.GetComponent<ScrollListTest>().myScrollRect.horizontalNormalizedPosition = 0;
            }
            colorListPicker.GetComponent<ScrollListTest>().myScrollRect.horizontalNormalizedPosition = 0;
            tempColor = new Color(0, 0, 0.0001f);


        }
        else
        {
            if (colorListPicker.GetComponent<ScrollListTest>().IsVerticalList)
            {
                colorListPicker.GetComponent<ScrollListTest>().myScrollRect.verticalNormalizedPosition = materialMenusManager.colorHorizontalNormalizedPosition[targetMaterialIndex];
            }
            else
            {
                colorListPicker.GetComponent<ScrollListTest>().myScrollRect.horizontalNormalizedPosition = materialMenusManager.colorHorizontalNormalizedPosition[targetMaterialIndex];
            }
            colorListPicker.GetComponent<ScrollListTest>().tempIndex=colorListPicker.GetComponent<ScrollListTest>().savedIndex = colorListPicker.GetComponent<ScrollListTest>().tempClickedIndex = materialMenusManager.ColorSelectedIndex[targetMaterialIndex];
            colorListPicker.GetComponent<ScrollListTest>().cells[materialMenusManager.ColorSelectedIndex[targetMaterialIndex]].SetMarker(true);
        }
        //materialSlider.gameObject.GetComponent<Image>().enabled = true;
        //colorListPicker.GetComponent<Image>().enabled = true; 
        //}
        setMatPar(materialMenusManager.materialType[targetMaterialIndex]);

    }
    public void EnableFun(int index)
    {
      //  tempMatSelect = MaterialSelected.none;
        //SpinningLoadingBar.SetActive(false);
        BtnMatTypeActivation.activateBtn(materialMenusManager.materialType[index]);
        //patternCollectionManager.clearselector();
        patternCollectionManager.MaterialSelectIndex = index;
        if (materialMenusManager.PatternSelectedIndex[index] != -1)
        {
            Debug.Log("0.1");

            patternCollectionManager.PatternsSelectScroll.gameObject.SetActive(true);
           // patternCollectionManager.PatternsSelectScroll.gameObject.transform.parent.transform.gameObject.SetActive(true);
           // patternCollectionManager.xx(materialMenusManager.SelectedPattrernMenuIndex[index]);
            patternCollectionManager.PatternsScrollList[materialMenusManager.SelectedPattrernMenuIndex[index]].gameObject.SetActive(true);
            Debug.Log(materialMenusManager.SelectedPattrernMenuIndex[index]+"  "+index);
            patternCollectionManager.xx(materialMenusManager.SelectedPattrernMenuIndex[index]);
            colorListPicker.transform.parent.gameObject.SetActive(false);
            materialSlider.gameObject.transform.parent.transform.gameObject.SetActive(false);

            BtnActivation.activateBtn(1);
            //ButtonClick(materialMenusManager.materialSelected[targetMaterialIndex]);
            //ButtonClick(2);
            tempMatMenu = MaterialMenu.pattern;

            //MatSelect = MaterialSelected.pattern;
            //patternCollectionManager.gameObject.SetActive(true);
            //PatternList.gameObject.transform.parent.gameObject.SetActive(true);
            //patternCollectionManager.enablePatternCollection(true);
            //patternCollectionManager.resetScrollesExcept(materialMenusManager.SelectedPattrernMenuIndex[targetMaterialIndex]);
            patternCollectionManager.clearselector();//materialMenusManager.SelectedPattrernMenuIndex[targetMaterialIndex]);
            
            //materialSlider.cells[materialMenusManager.SelectedPattrernMenuIndex[targetMaterialIndex]].SetMarker(true);
            if (materialSlider.cells.Count >= 1)
            {
                materialSlider.cells[0].SetMarker(true);
                Debug.Log("kkkjkkk");
            }
            //PatternList.myScrollRect.horizontalNormalizedPosition = materialMenusManager.colorTexHorizontalNormalizedPosition[targetMaterialIndex];
            //PatternList.gameObject.transform.parent.gameObject.SetActive(true);
            //PatternList.gameObject.GetComponent<Image>().enabled = true;
            //materialSlider.gameObject.GetComponent<Image>().enabled = false;
            //colorListPicker.gameObject.GetComponent<Image>().enabled = false;
           // MatSelect = MaterialSelected.pattern;
        }
        else
        {
            Debug.Log("0.2");
            //patternCollectionManager.gameObject.SetActive(false);
            //PatternList.gameObject.transform.parent.gameObject.SetActive(false);
            //patternCollectionManager.enablePatternCollection(false);
            tempMatMenu = MaterialMenu.texture;
            BtnActivation.activateBtn(0);
            //ButtonClick(1);
            patternCollectionManager.PatternsSelectScroll.gameObject.SetActive(false);
            //MatSelect = MaterialSelected.texture;
            patternCollectionManager.resetScrollesExcept(-1);
            //PatternList.myScrollRect.horizontalNormalizedPosition = 0;
            //PatternList.gameObject.transform.parent.gameObject.SetActive(false);
            //PatternList.gameObject.GetComponent<Image>().enabled = false;
            //materialSlider.gameObject.GetComponent<Image>().enabled = true;
            //colorListPicker.gameObject.GetComponent<Image>().enabled = true;
            colorListPicker.transform.parent.gameObject.SetActive(true);
            materialSlider.gameObject.transform.parent.transform.gameObject.SetActive(true);


            if (materialMenusManager.TextureSelectedIndex[index] == -1)
            {
                Debug.Log("1");
                //  materialSlider.cells[0].clickit();
                patternCollectionManager.resetScrollesExcept(-1);
                if (materialSlider.IsVerticalList)
                {
                    materialSlider.myScrollRect.verticalNormalizedPosition = 1;
                }
                else
                {
                    materialSlider.myScrollRect.horizontalNormalizedPosition = 0;
                }


                materialSlider.tempIndex = materialSlider.selectedIndex = -1;


                //Debug.Log(materialSlider.cells.Count + " " + index + "sss");
                // materialSlider.IsAction = true;
                if (materialSlider.cells.Count >= 1)
                {
                    Debug.Log("kkkjkkk");
                   // Debug.Log(materialSlider.cells.Count + " " + index + "sss");
                    materialSlider.cells[0].relatedList.selectedIndex = 0;
                    materialSlider.cells[0].SetMarker(true);
                    //materialMenusManager.
                         materialMenusManager.index[index]=0 ;
                       //  materialMenusManager.targetMat[index] = targetMaterialIndex;
                      //   materialMenusManager.ObjIs[index]=ObjIs ;   ///////// jkjhkjhjkhkjh
                    // mainMaterialMenu.materialSlider.GetComponent<ScrollListTest>().tileValueSlider.value = TilesValue[_index].x;
                         materialMenusManager.Texture[index]=selectedTexteure ;
                         materialMenusManager.TextureSelectedIndex[index]=0;
                         materialSlider.MarkAgain(materialMenusManager.TextureSelectedIndex[0]);
                         materialMenusManager.materialFolder[index] = materialSlider.Bank.textureFileName.ToString();
                    //materialSlider.cells[0].relatedList.IsAction = true;
                    ////////////////////////materialSlider.cells[0].clickit();
                }
               // MatSelect = tempMatSelect = 0;
               // MatSelect = MaterialSelected.texture;
               // materialSlider.IsAction = true;
                 
            }
            else
            {
                Debug.Log("2");
                //Debug.Log("jjjkkk");
                if (materialSlider.IsVerticalList)
                {
                    materialSlider.myScrollRect.verticalNormalizedPosition = materialMenusManager.BWTexHorizontalNormalizedPosition[index];
                }
                else
                {
                    materialSlider.myScrollRect.horizontalNormalizedPosition = materialMenusManager.BWTexHorizontalNormalizedPosition[index];
                }
                // materialSlider.myScrollRect.horizontalNormalizedPosition = materialMenusManager.BWTexHorizontalNormalizedPosition[targetMaterialIndex];
                materialSlider.cells[materialMenusManager.TextureSelectedIndex[index]].SetMarker(true); //1
             //   cells[materialMenusManager.TextureSelectedIndex[targetMaterialIndex]]
                   //  materialSlider.MarkAgain(materialMenusManager.TextureSelectedIndex[index]);
            }
        }
        //     Debug.Log("dfdf"+materialMenusManager.ColorSelectedIndex[targetMaterialIndex] );//+ "  " + colorListPicker.GetComponent<ScrollListTest>().cells[materialMenusManager.ColorSelectedIndex[targetMaterialIndex]]);
        if (materialMenusManager.ColorSelectedIndex[index] == -1)
        {
            Debug.Log("2.1");
            colorListPicker.GetComponent<ScrollListTest>().tempIndex = colorListPicker.GetComponent<ScrollListTest>().selectedIndex = 0;

            if (colorListPicker.GetComponent<ScrollListTest>().cells.Count >= 0)
            {
                colorListPicker.GetComponent<ScrollListTest>().cells[0].SetMarker(true);
            }
            if (colorListPicker.GetComponent<ScrollListTest>().IsVerticalList)
            {
                colorListPicker.GetComponent<ScrollListTest>().myScrollRect.verticalNormalizedPosition = 1;
            }
            else
            {
                colorListPicker.GetComponent<ScrollListTest>().myScrollRect.horizontalNormalizedPosition = 0;
            }
            materialMenusManager.ColorSelectedIndex[index] = 0;
            colorListPicker.GetComponent<ScrollListTest>().myScrollRect.horizontalNormalizedPosition = 0;
            tempColor = new Color(0, 0, 0.0001f);


        }
        else
        {
            Debug.Log("2.3");
            if (colorListPicker.GetComponent<ScrollListTest>().IsVerticalList)
            {
                colorListPicker.GetComponent<ScrollListTest>().myScrollRect.verticalNormalizedPosition = materialMenusManager.colorHorizontalNormalizedPosition[index];
            }
            else
            {
                colorListPicker.GetComponent<ScrollListTest>().myScrollRect.horizontalNormalizedPosition = materialMenusManager.colorHorizontalNormalizedPosition[index];
            }

            colorListPicker.GetComponent<ScrollListTest>().cells[materialMenusManager.ColorSelectedIndex[index]].SetMarker(true);//color mark

        }

        targetMaterialIndex = index;
        //materialSlider.gameObject.GetComponent<Image>().enabled = true;
        //colorListPicker.GetComponent<Image>().enabled = true; 
        //}
     //  setMatPar(materialMenusManager.materialType[index]);
       // SetMaterialManagerIndex(index);
      //  MatSelect = (MaterialSelected)0;
        
    }
    void OnDisable()
    {
        //materialSlider.gameObject.GetComponent<Image>().enabled = false;
        //colorListPicker.GetComponent<Image>().enabled = false;
    }
    void Start()
    {

        //tempColor = Color.white;
        //isMaterialStart = false;
        //if (materialSlider.)
        //{

        //}



        // materialSlider.cells[0].SetMarker(true);
        //matChange = true;
        //materialSlider.selectedIndex = 0;
        //materialSlider.IsAction = true;
        //materialSlider.SetMarker(true);
        //materialSlider.clearMarker(index);

        //MatSelect = MaterialSelected.none;
        //tempMatSelect=MaterialSelected.none;
        //tempColor = Color.white;
        //MatParm = new MaterialParam();
        //tempMatMenu = MaterialMenu.color;
        //matType = MaterialType.Mat;
        //switch (ObjIs)
        //{
        //    case ObjMenu.Face:
        //        shosePart = shose.faceObj;
        //        break;
        //    case ObjMenu.Base:
        //        shosePart = shose.baseObj;
        //        break;
        //    case ObjMenu.Heel:
        //        shosePart = shose.heelObj;
        //        break;
        //    case ObjMenu.none:
        //        break;
        //    default:
        //        break;
        //}
        //colorPicker.GetComponent<ColorPicker>().getColorMaterial(shosePart);
        //colorPicker.GetComponent<ColorPicker>().setColorMaterial();
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.J))
        //{
        //     materialSlider.myScrollRect.horizontalNormalizedPosition = 0;
        //     PatternList.myScrollRect.horizontalNormalizedPosition = 0;
        //}
        if (patternCollectionManager != null)
        {
            if (patternCollectionManagerIndexTemp != patternCollectionManager.PatternsSelectIndex)
            {
                PatternList = patternCollectionManager.selectedScrolllist;
                patternCollectionManagerIndexTemp = patternCollectionManager.PatternsSelectIndex;
            }

        }

        if (isLoadNormalImage == true)
        {
            isLoadNormalImage = false;
            StartCoroutine(WaitForLoadingBar(0.25f));
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
             StartCoroutine(LoadNormalImage(selectedTexteure));
        }
        //if (materialSlider.gameObject.activeSelf == true && !isMaterialStart)
        //{

        //    for (int i = 0; i < materialSlider.cells.Count; i++)
        //    {
        //        materialSlider.cells[i].gameObject.GetComponent<Image>().color = tempColor;

        //    }
        //    isMaterialStart = true;
        //}
        //if (materialSlider.isUpdate == false && !isStartCase && materialSlider.cells[1] != null)
        //{
        //    // Debug.Log(materialSlider.cells[0].);
        //    isStartCase = true;
        //    materialSlider.cells[1].clickit();
        //}
        //switch (tempMatMenu)
        //{
        //    case MaterialMenu.color:
        ////        Debug.Log("s");

        //        break;
        //    case MaterialMenu.texture:
        //        if (materialSlider.selectedIndex >= 0 && MatSelect== MaterialSelected.texture)
        //        {
        // Debug.Log( "  " + Temptile + "  " + materialSlider.tileValueSlider.value);
        if (materialSlider.tileValueSlider.value != Temptile)
        {
            if (ObjIs == ObjMenu.Heel)
            {
                if (materialSlider.selectedIndex > -1)
                {
                    tileValue = new Vector2(materialSlider.Bank.startTileValuePerTexture[materialSlider.selectedIndex] + materialSlider.tileValueSlider.value * maxTileValue, ModelManager.baseHeightFront.value * 2 * materialSlider.Bank.startTileValuePerTexture[materialSlider.selectedIndex] + materialSlider.tileValueSlider.value * maxTileValue);
                }
                else if (PatternList.selectedIndex > -1)
                {
                    tileValue = new Vector2(PatternList.Bank.startTileValuePerTexture[PatternList.selectedIndex] + PatternList.tileValueSlider.value * maxTileValue, ModelManager.baseHeightFront.value * 2 * PatternList.Bank.startTileValuePerTexture[PatternList.selectedIndex] + PatternList.tileValueSlider.value * maxTileValue);
                }
            }
            else
            {
                Debug.Log(PatternList.selectedIndex + " kARAAAAM " + materialSlider.selectedIndex);
                if (materialSlider.selectedIndex > -1)
                {
                    tileValue = new Vector2(materialSlider.Bank.startTileValuePerTexture[materialSlider.selectedIndex] + materialSlider.tileValueSlider.value * maxTileValue, materialSlider.Bank.startTileValuePerTexture[materialSlider.selectedIndex] + materialSlider.tileValueSlider.value * maxTileValue);/////sjdlksjdl
                }
                else if (PatternList.selectedIndex > -1)
                {
                    tileValue = new Vector2(PatternList.Bank.startTileValuePerTexture[PatternList.selectedIndex] + PatternList.tileValueSlider.value * maxTileValue, PatternList.Bank.startTileValuePerTexture[PatternList.selectedIndex] + PatternList.tileValueSlider.value * maxTileValue);/////sjdlksjdl
                }
                else
                {

                }

            }
            Temptile = materialSlider.tileValueSlider.value;
            if (materialSlider.selectedIndex > -1)
            {
                PatternList.tileValueSlider.value = materialSlider.tileValueSlider.value;
            }
            else
            {
                materialSlider.tileValueSlider.value = PatternList.tileValueSlider.value;
            }

        }
        //}

        //      //  materialSlider.tileValueSlider.value = PatternList.tileValueSlider.value;
        //        break;
        //    case MaterialMenu.pattern:
        //        if (PatternList.selectedIndex >= 0 && MatSelect == MaterialSelected.pattern)
        //        {
        //            if (ObjIs == ObjMenu.Heel)
        //            {

        //                tileValue = new Vector2(PatternList.Bank.startTileValuePerTexture[PatternList.selectedIndex] + PatternList.tileValueSlider.value * maxTileValue, ModelManager.baseHeightFront.value * 2 * PatternList.Bank.startTileValuePerTexture[PatternList.selectedIndex] + PatternList.tileValueSlider.value * maxTileValue );
        //            }else
        //            {

        //                tileValue = new Vector2(PatternList.Bank.startTileValuePerTexture[PatternList.selectedIndex] + PatternList.tileValueSlider.value * maxTileValue, PatternList.Bank.startTileValuePerTexture[PatternList.selectedIndex] + PatternList.tileValueSlider.value * maxTileValue);
        //            }
        //        }
        //        materialSlider.tileValueSlider.value = PatternList.tileValueSlider.value;
        //        //PatternList.tileValueSlider.value = materialSlider.tileValueSlider.value;


        //        break;
        //    case MaterialMenu.none:
        //        break;
        //    default:
        //        break;
        //}
        if (MatSelect != tempMatSelect)
        {

            switch (MatSelect)
            {
                case MaterialSelected.texture:
                    //   Debug.Log("DDDD");
                    patternCollectionManager.resetScrollesExcept(-1);
                    //   patternCollectionManager.clearselector(-1);
                    PatternList.clearMarker(-1);
                    tempMatSelect = MatSelect;
                    materialMenusManager.materialSelected[targetMaterialIndex] = MatSelect;
                    break;
                case MaterialSelected.pattern:
                    materialSlider.clearMarker(-1);
                    tempMatSelect = MatSelect;
                    materialMenusManager.materialSelected[targetMaterialIndex] = MatSelect;
                    break;
                case MaterialSelected.none:
                    break;
                default:
                    break;
            }

        }


        ColorControler();
        TileTexure(tileValue);
        TexureChangeFn(tempMatMenu);
        MaterialTypeFn();
        //Debug.Log("KOKO" + materialSlider.selectedIndex + "  " + materialSlider.Bank.spritesArr.Length);

    }

    void ColorControler()
    {
        if (ColorManager.InCustomMode)
        {
            //Debug.Log("PP1");
            if (MatSelect == MaterialSelected.texture)
            {
                //    Debug.Log("PP" + ColorManager.tempColor);
                TargetMaterial.color = ColorManager.tempColor;
            }
            if (!ColorManager.isOpenWindow)
            {
                ColorManager.InCustomMode = false;
            }
            //==> shosePart.GetComponent<SkinnedMeshRenderer>().materials[materialIndex].color = ColorManager.tempColor;//.GetComponent<ScrollListTest>().Bank.ColorsBank[colorListPicker.GetComponent<ScrollListTest>().selectedIndex];
        }
        else
        {
            if (colorListPicker != null)
            {
                if (colorListPicker.GetComponent<ScrollListTest>().selectedIndex != -1)
                {
                    // if (colorPicker.GetComponent<ColorPicker>().CurrentColor != tempColor)
                    if (colorListPicker.GetComponent<ScrollListTest>().Bank.ColorsBank[colorListPicker.GetComponent<ScrollListTest>().selectedIndex] != tempColor)
                    {

                        //for (int i = 0; i < materialSlider.cells.Count; i++)
                        //{
                        //    materialSlider.cells[i].gameObject.GetComponent<Image>().color = colorListPicker.GetComponent<ScrollListTest>().Bank.ColorsBank[colorListPicker.GetComponent<ScrollListTest>().selectedIndex];//colorPicker.GetComponent<ColorPicker>().CurrentColor;
                        //}


                        if (MatSelect == MaterialSelected.texture)
                        {

                            //shosePart.GetComponent<Renderer>().materials[materialIndex].color = colorPicker.GetComponent<ColorPicker>().CurrentColor;
                            //tempColor = colorPicker.GetComponent<ColorPicker>().CurrentColor;
                            if (colorListPicker != null)
                            {
                                if (colorListPicker.GetComponent<ScrollListTest>().selectedIndex != -1)
                                {
                                    tempColor = colorListPicker.GetComponent<ScrollListTest>().Bank.ColorsBank[colorListPicker.GetComponent<ScrollListTest>().selectedIndex];
                                    //==> shosePart.GetComponent<SkinnedMeshRenderer>().materials[materialIndex].color = colorListPicker.GetComponent<ScrollListTest>().Bank.ColorsBank[colorListPicker.GetComponent<ScrollListTest>().selectedIndex];
                                    TargetMaterial.color = colorListPicker.GetComponent<ScrollListTest>().Bank.ColorsBank[colorListPicker.GetComponent<ScrollListTest>().selectedIndex];


                                }
                            }

                        }
                        materialMenusManager.ColorSelectedIndex[targetMaterialIndex] = colorListPicker.GetComponent<ScrollListTest>().selectedIndex;
                        materialMenusManager.color[targetMaterialIndex] = tempColor;
                        if (colorListPicker.GetComponent<ScrollListTest>().IsVerticalList)
                        {
                            //colorListPicker.GetComponent<ScrollListTest>().myScrollRect.verticalNormalizedPosition = materialMenusManager.colorHorizontalNormalizedPosition[targetMaterialIndex];
                            if (colorListPicker.GetComponent<ScrollListTest>().myScrollRect.verticalNormalizedPosition < 0)
                            {
                                materialMenusManager.colorHorizontalNormalizedPosition[targetMaterialIndex] = 0;
                            }
                            else if (colorListPicker.GetComponent<ScrollListTest>().myScrollRect.verticalNormalizedPosition > 1)
                            {
                                materialMenusManager.colorHorizontalNormalizedPosition[targetMaterialIndex] = 1;
                            }
                            else
                            {
                                materialMenusManager.colorHorizontalNormalizedPosition[targetMaterialIndex] = colorListPicker.GetComponent<ScrollListTest>().myScrollRect.verticalNormalizedPosition;
                            }
                        }
                        else
                        {
                            if (colorListPicker.GetComponent<ScrollListTest>().myScrollRect.horizontalNormalizedPosition < 0)
                            {
                                materialMenusManager.colorHorizontalNormalizedPosition[targetMaterialIndex] = 0;
                            }
                            else if (colorListPicker.GetComponent<ScrollListTest>().myScrollRect.horizontalNormalizedPosition > 1)
                            {
                                materialMenusManager.colorHorizontalNormalizedPosition[targetMaterialIndex] = 1;
                            }
                            else
                            {
                                materialMenusManager.colorHorizontalNormalizedPosition[targetMaterialIndex] = colorListPicker.GetComponent<ScrollListTest>().myScrollRect.horizontalNormalizedPosition;
                            }
                        }


                    }
                }


            }
        }

        if (MatSelect == MaterialSelected.pattern)
        {
            //shosePart.GetComponent<Renderer>().materials[materialIndex].color = Color.white;
            tempColor = Color.white;

        }

        //if (tempMatMenu == MaterialMenu.color)
        //{
        //    MatParm.SetColor(colorPicker.GetComponent<ColorPicker>().CurrentColor);
        //}
        //color

        // changeActiveMenu(colorPicker, materialSliderFace);

    }

    void TileTexure(Vector2 value)
    {
        // Debug.Log(shosePart.GetComponent<Renderer>().material.GetTextureScale("_MainTex")+"  "+ tileValue+"  "+ TemptileValue);

        //material tile
        if (TemptileValue != value)
        {
            if (matType != MaterialType.Fabric)
            {

            }
            MatParm.setTileValue(value);
            TemptileValue = value;
            if (ObjIs == ObjMenu.Heel)
            {
                //==> shosePart.GetComponent<Renderer>().materials[materialIndex].mainTextureScale = value;//new Vector2(ModelManager.baseHeightFront.value * 2 * value, value)
                //TargetMaterial.mainTextureScale = value;
                //TargetMaterial.SetTextureScale("_MainTex", value);
                //TargetMaterial.SetTextureScale("_BumpMap", value);
                //TargetMaterial.SetTextureScale("_OcclusionMap", value);
                // UpdateTileMat(TargetMaterial, new Vector2(value.x, ModelManager.baseHeightFront.value * 2 * value.y));
                UpdateTileMat(TargetMaterial, value);
            }
            else
            {
                if ((ObjIs == ObjMenu.Base && materialIndex == 1))
                {
                    Debug.Log("2");
                    // shose.heelObj.GetComponent<Renderer>().materials[0].mainTextureScale = new Vector2(value.x, ModelManager.baseHeightFront.value * 2 * value.y);
                    // shose.heelIn.mainTextureScale = new Vector2(value.x, ModelManager.baseHeightFront.value * 2 * value.y);
                    UpdateTileMat(shose.heelIn, new Vector2(value.x, ModelManager.baseHeightFront.value * 2 * value.y));
                    // shosePart.GetComponent<Renderer>().materials[0].SetTextureScale("_MainTex", new Vector2(ModelManager.baseHeightFront.value * 2 * value.x , value.y));
                    //==>  shosePart.GetComponent<Renderer>().materials[materialIndex].mainTextureScale= new Vector2(2 * value.x, 2 * value.y);
                    //TargetMaterial.mainTextureScale = new Vector2(2 * value.x, 2 * value.y);
                    //TargetMaterial.SetTextureScale("_MainTex", new Vector2(2 * value.x, 2 * value.y));
                    //TargetMaterial.SetTextureScale("_BumpMap", new Vector2(2 * value.x, 2 * value.y));
                    //TargetMaterial.SetTextureScale("_OcclusionMap", new Vector2(2 * value.x, 2 * value.y));
                    UpdateTileMat(TargetMaterial, new Vector2(2 * value.x, 2 * value.y));
                   // UpdateTileMat(TargetMaterial, new Vector2( value.x,  value.y));
                }
                else
                {
                    if ((ObjIs == ObjMenu.Base && materialIndex == 2))
                    {
                        Debug.Log("3");
                        // shosePart.GetComponent<Renderer>().materials[2].mainTextureScale= new Vector2(value.x, (1 - ModelManager.baseHeightFront.value * 2) * value.y);
                        //if (ModelManager.baseHeightFront.value>0.55f)
                        //{
                        // shosePart.GetComponent<Renderer>().materials[2].mainTextureScale = new Vector2(value.x, -1 * (1 - (ModelManager.baseHeightFront.value * 2)) * value.y);
                        ///////////////////////////////////////////////      shosePart.GetComponent<Renderer>().materials[2].mainTextureScale = new Vector2(value.x,  ((ModelManager.baseHeightFront.value * 2)) * value.x * 0.5f);//wegdite
                        //}
                        //shose.baseMiddle.mainTextureScale =  new Vector2(value.x,  ((ModelManager.baseHeightFront.value * 2)) * value.x * 0.5f);//wegdite
                        //shose.baseMiddle.SetTextureScale("_MainTex", new Vector2(value.x,  ((ModelManager.baseHeightFront.value * 2)) * value.x * 0.5f));
                        //shose.baseMiddle.SetTextureScale("_BumpMap", new Vector2(value.x, ((ModelManager.baseHeightFront.value * 2)) * value.x * 0.5f));
                        //shose.baseMiddle.SetTextureScale("_OcclusionMap", new Vector2(value.x, ((ModelManager.baseHeightFront.value * 2)) * value.x * 0.5f));
                        UpdateTileMat(shose.baseMiddle, new Vector2(value.x, ((ModelManager.baseHeightFront.value * 2)) * value.x * 0.5f));
                    }
                    else
                    {
                        Debug.Log("4");
                        //TargetMaterial.mainTextureScale = value;
                        //TargetMaterial.SetTextureScale("_MainTex", value);
                        //TargetMaterial.SetTextureScale("_BumpMap", value);
                        //TargetMaterial.SetTextureScale("_OcclusionMap", value);
                        UpdateTileMat(TargetMaterial, value);
                        //==>    shosePart.GetComponent<Renderer>().materials[materialIndex].mainTextureScale= value;//new Vector2(value, value)
                    }
                }

            }
            materialMenusManager.TilesValue[targetMaterialIndex] = TargetMaterial.mainTextureScale;
            materialMenusManager.TilesSliderValue[targetMaterialIndex] = materialSlider.tileValueSlider.value = PatternList.tileValueSlider.value;

            //glow

            //float scaleX = Mathf.Cos(Time.time) * 0.5F + 1;
            //float scaleY = Mathf.Sin(Time.time) * 0.5F + 1;
            //shose.faceObj.GetComponent<Renderer>().material.mainTextureScale = new Vector2(scaleX, scaleY);
            //shose.faceObj.GetComponent<Renderer>().material.mainTextureScale = new Vector2(tileValue, tileValue);
        }


    }

    public void SubMenuIs(MaterialMenu subMenuType)
    {
        if (subMenuType != tempMatMenu)
        {
            switch (subMenuType)
            {
                case MaterialMenu.color:
                    //  ColorControler();
                    // tempMatMenu = MaterialMenu.color;

                    //  materialSlider.gameObject.transform.parent. transform.gameObject.SetActive(false);
                    ////  colorPicker.gameObject.SetActive(true);
                    //  colorListPicker.transform.parent.gameObject.SetActive(true);
                    //  PatternList.gameObject.transform.parent.gameObject.SetActive(false);


                    break;
                case MaterialMenu.texture:
                    //color
                    //s    Debug.Log("KKKKKKKK:"+tempMatMenu);    //materialSlider.gameObject.transform.parent. transform.gameObject.SetActive(false);

                    //colorListPicker.gameObject.GetComponent<Image>().enabled = true;
                    //materialSlider.gameObject.GetComponent<Image>().enabled = true;
                    //PatternList.gameObject.GetComponent<Image>().enabled = false;
                    // colorPicker.gameObject.SetActive(true);
                    //patternCollectionManager.xx(-1);
                    colorListPicker.transform.parent.gameObject.SetActive(true);
                    PatternList.gameObject.transform.parent.gameObject.SetActive(false);
                    //patternCollectionManager.xx(-1);

                    //  tempMatMenu = MaterialMenu.texture;
                    materialSlider.gameObject.transform.parent.transform.gameObject.SetActive(true);

                    patternCollectionManager.enablePatternCollection(false);
                    patternCollectionManager.gameObject.SetActive(false);
                    //   colorPicker.gameObject.SetActive(false);
                    // colorListPicker.transform.parent.gameObject.SetActive(false);
                    // PatternList.gameObject.transform.parent.gameObject.SetActive(false);

                    break;
                case MaterialMenu.pattern:
                    //   tempMatMenu = MaterialMenu.pattern;
                    //colorListPicker.gameObject.GetComponent<Image>().enabled = false;
                    //materialSlider.gameObject.GetComponent<Image>().enabled = false;
                    //PatternList.gameObject.GetComponent<Image>().enabled = true;
                    materialSlider.gameObject.transform.parent.transform.gameObject.SetActive(false);
                    //  colorPicker.gameObject.SetActive(false);
                    colorListPicker.transform.parent.gameObject.SetActive(false);
                    // PatternList.gameObject.SetActive(true);
                    patternCollectionManager.gameObject.SetActive(true);
                   ////// PatternList.gameObject.transform.parent.gameObject.SetActive(true);
                    patternCollectionManager.enablePatternCollection(true);

                    break;
                case MaterialMenu.none:
                    //materialSlider.gameObject.GetComponent<Image>().enabled = false; 
                    //materialSlider.gameObject.GetComponent<Image>().enabled = false;
                    //PatternList.gameObject.GetComponent<Image>().enabled = false;
                    // materialSlider.gameObject.transform.parent.transform.gameObject.SetActive(false);
                    //// colorPicker.gameObject.SetActive(false);
                    // colorListPicker.transform.parent.gameObject.SetActive(false);
                    // PatternList.gameObject.transform.parent.gameObject.SetActive(false);

                    break;
                default:
                    break;
            }
        }
        tempMatMenu = subMenuType;
    }

    public void ButtonClick(int index)
    {
      //  Debug.Log("OOO");
       // SoundManScript.PlaySound(SoundManScript.Botton_Clip);
       
        switch (MatSelect)
        {
            case MaterialSelected.texture:
               // Debug.Log("OOt");
                //if (!materialSlider.isParchaseWin)
                //{
                //    Debug.Log("OOtv");
                    SubMenuIs((MaterialMenu)index);
                    shop.unlocksureWindow.SetActive(false);
                    BtnActivation.activateBtn(index - 1);
                bool noSelect = true;
                for (int i = 0; i < patternCollectionManager.PatternsScrollList.Length; i++)
                {
                    if (patternCollectionManager.SelectedIndTexforList[i]> -1)
                    {
                        noSelect = false;
                        patternCollectionManager.PatternsScrollList[i].transform.parent.gameObject.SetActive(true);
                    }
                    else
                    {

                        patternCollectionManager.PatternsScrollList[i].transform.parent.gameObject.SetActive(false);
                    }
                    
                }
                if (noSelect)
                {
                    if (patternCollectionManager.PatternsSelectIndex != -1)
                    {
                        patternCollectionManager.PatternsScrollList[patternCollectionManager.PatternsSelectIndex].transform.parent.gameObject.SetActive(true);
                    }else
                    {
                        patternCollectionManager.PatternsScrollList[0].transform.parent.gameObject.SetActive(true);
                    }
                   
                    noSelect = false;
                }
                
                //}
                //else
                //{
                //    //   Debug.Log("OOts");
                //    shop.unlocksureWindow.SetActive(false);
                //    shop.unlocksureWindow.SetActive(true);
                    
                //}
                break;
            case MaterialSelected.pattern:
                // Debug.Log("OOO");
                //if (!PatternList.isParchaseWin)
                //{
                //    Debug.Log("OOe");
                bool noselect = true;
                for (int i = 0; i < patternCollectionManager.PatternsScrollList.Length; i++)
                {
                    if (patternCollectionManager.SelectedIndTexforList[i] > -1)
                    {
                        noselect = false;
                        patternCollectionManager.PatternsScrollList[i].transform.parent.gameObject.SetActive(true);
                    }
                    else
                    {

                        patternCollectionManager.PatternsScrollList[i].transform.parent.gameObject.SetActive(false);
                    }

                }
                
                if (noselect)
                {
                    if (patternCollectionManager.PatternsSelectIndex != -1)
                    {
                        patternCollectionManager.PatternsScrollList[patternCollectionManager.PatternsSelectIndex].transform.parent.gameObject.SetActive(true);
                    }
                    else
                    {
                        patternCollectionManager.PatternsScrollList[0].transform.parent.gameObject.SetActive(true);
                    }

                    noselect = false;
                }
                SubMenuIs((MaterialMenu)index);
                    shop.unlocksureWindow.SetActive(false);
                    BtnActivation.activateBtn(index - 1);
                //}
                //else
                //{
                //    //  Debug.Log("OO3");
                //    shop.unlocksureWindow.SetActive(false);
                //    shop.unlocksureWindow.SetActive(true);
                   
                //}
                break;
            case MaterialSelected.none:
                break;
            default:
                break;
        }
        //if (!materialSlider.isParchaseWin)
        //{
        //    SubMenuIs((MaterialMenu)index);
        //    shop.unlocksureWindow.SetActive(false);
        //    BtnActivation.activateBtn(index - 1);
        //}
        //else
        //{
           
        //    shop.unlocksureWindow.SetActive(true);
        //    //if (index==1)
        //    //{
        //    //    BtnActivation.activateBtn(1);
        //    //}
        //    //else
        //    //{
        //    //    BtnActivation.activateBtn(0);
        //    //}
           
        ////   BtnActivation.activateBtn(index - 1);
        //}
        //if (index == 1)
        //{
        //    mainMatBtn.gameObject.SetActive(false);
        //    MatBtn.gameObject.SetActive(true);

        //   // colorBtn.gameObject.SetActive(true);
        //}
        //else if (index == 2)
        //{
        //    mainMatBtn.gameObject.SetActive(true);
        //    MatBtn.gameObject.SetActive(false);
        //   // colorBtn.gameObject.SetActive(false);
        //}
    }

    public void setMatPar(int index)/////////////////////==================>
    {
        //SoundManScript.PlaySound(SoundManScript.Botton_Clip);
        MatParm.MaterialParamter(TargetMaterial, MaterialPara);
        matType = (MaterialType)index;
        MatParm.GetMatParams(TargetMaterial);
        switch (matType)
        {
            case MaterialType.Mat:
                // MaterialPara = matMaterialPara;
                //  Debug.Log("OP");
                MaterialPara = parameterMatEffect.ParamValue[0];
                //normalTexture = materialSlider.Bank.defaultNormal;
                //normalTexture = (Texture2D)parameterMatEffect.normals[0];
                if (selectedTexteure != null)
                {
                    if (materialSlider.selectedIndex >= 0 && (MatSelect == MaterialSelected.texture || MatSelect == MaterialSelected.pattern))
                    {
                        normalTexture = materialSlider.Bank.defaultNormal;
                        MatParm._BumpMap = normalTexture;
                    }
                    else
                    {
                        if (tempMatType != MaterialType.Fabric)
                        {
                            
                          //  Debug.Log("GGG:1");
                           StartCoroutine(LoadNormalImage(selectedTexteure)); //1

                            //normalTexture = materialSlider.Bank.NormalMapGen(selectedTexteure, 1, 1);
                            MatParm._BumpMap = normalTexture;
                        }
                    }

                }
                tempMatType = MaterialType.Mat;
                //TemptileValue = new Vector2(.5f, .5f);
                materialMenusManager.materialType[targetMaterialIndex] = (int)MaterialType.Mat;

                break;
            case MaterialType.Fabric:
                //  Debug.Log("iii3");
                MaterialPara = parameterMatEffect.ParamValue[1];
                //normalTexture = (Texture2D) parameterMatEffect.normals[0];
                //TemptileValue = new Vector2(.5f, .5f);


                normalTexture = (Texture2D)parameterMatEffect.normals[0];
                MatParm._TileBumpValue = FixedBmpTile;
                // TargetMaterial.SetTextureScale("_BumpMap", new Vector2(2, 2));
                MatParm._BumpMap = normalTexture;
                //TargetMaterial.SetTexture("_BumpMap", (Texture2D)parameterMatEffect.normals[0]);

                materialMenusManager.materialType[targetMaterialIndex] = (int)MaterialType.Fabric;

                break;
            case MaterialType.Glass:
                switch (MatSelect)
                {
                    case MaterialSelected.texture:
                        // MaterialPara = GlassMaterialPara;
                        MaterialPara = parameterMatEffect.ParamValue[1];
                        // detailNormalMap = (Texture2D)parameterMatEffect.normals[1];
                        break;
                    case MaterialSelected.pattern:
                        MaterialPara = new Vector4(0, 1, 0, 0);
                        //  detailNormalMap = (Texture2D)parameterMatEffect.normals[1];

                        break;

                }
                materialMenusManager.materialType[targetMaterialIndex] = (int)MaterialType.Glass;
                break;
            case MaterialType.Gloosy:
               // Debug.Log("iii" + tempMatType);
                // MaterialPara = HalfGlossyMaterialPara;
                MaterialPara = parameterMatEffect.ParamValue[2];
                if (selectedTexteure != null)
                {
                    if (materialSlider.selectedIndex >= 0 && (MatSelect == MaterialSelected.texture || MatSelect == MaterialSelected.pattern))
                    {
                        normalTexture = materialSlider.Bank.defaultNormal;
                        MatParm._BumpMap = normalTexture;
                    }
                    else
                    {
                        if (tempMatType == MaterialType.Fabric)
                        {
                            Debug.Log("ii");
                            Debug.Log("GGG:2");
                            StartCoroutine(LoadNormalImage(selectedTexteure));//2

                            //  normalTexture = materialSlider.Bank.NormalMapGen(selectedTexteure, 1, 1);
                            MatParm._BumpMap = normalTexture;
                        }
                    }

                }
                tempMatType = MaterialType.Gloosy;
                materialMenusManager.materialType[targetMaterialIndex] = (int)MaterialType.Gloosy;
                //detailNormalMap = (Texture2D)parameterMatEffect.normals[2];
                break;
            case MaterialType.HalfGloosy:
                //Debug.Log("i9i");
                // MaterialPara = glossyMaterialPara;
                MaterialPara = parameterMatEffect.ParamValue[3];
                if (selectedTexteure != null)
                {
                    if (materialSlider.selectedIndex >= 0 && (MatSelect == MaterialSelected.texture || MatSelect == MaterialSelected.pattern))
                    {
                        normalTexture = materialSlider.Bank.defaultNormal;
                        MatParm._BumpMap = normalTexture;
                    }
                    else
                    {
                        if (tempMatType == MaterialType.Fabric)
                        {
                            Debug.Log("GGG:3");
                            StartCoroutine(LoadNormalImage(selectedTexteure));//3

                            //normalTexture = materialSlider.Bank.NormalMapGen(selectedTexteure, 1, 1);
                            MatParm._BumpMap = normalTexture;
                        }
                    }

                }
                tempMatType = MaterialType.HalfGloosy;
                materialMenusManager.materialType[targetMaterialIndex] = (int)MaterialType.HalfGloosy;
                // detailNormalMap = (Texture2D)parameterMatEffect.normals[3];
                break;
            case MaterialType.Metal:
                // MaterialPara = MetalMaterialPara;
                MaterialPara = parameterMatEffect.ParamValue[4];
                if (selectedTexteure != null)
                {
                    if (materialSlider.selectedIndex >= 0 && (MatSelect == MaterialSelected.texture || MatSelect == MaterialSelected.pattern))
                    {
                        normalTexture = materialSlider.Bank.defaultNormal;
                        MatParm._BumpMap = normalTexture;
                    }
                    else
                    {
                        if (tempMatType == MaterialType.Fabric)
                        {
                          //  Debug.Log("GGG:4");
                            StartCoroutine(LoadNormalImage(selectedTexteure));//4

                            // normalTexture = materialSlider.Bank.NormalMapGen(selectedTexteure, 1, 1);
                            MatParm._BumpMap = normalTexture;
                        }
                    }

                }
                tempMatType = MaterialType.Metal;
                materialMenusManager.materialType[targetMaterialIndex] = (int)MaterialType.Metal;
                //  detailNormalMap = (Texture2D)parameterMatEffect.normals[4];
                break;
            default:
                break;
        }
        matChange = true;

        //  MaterialPara = H;
    }

    void TexureChangeFn(MaterialMenu _tempMatMenu)
    {
        switch (_tempMatMenu)
        {
            //case MaterialMenu.color:
            //    if (MatSelect == MaterialSelected.texture)
            //    {
            //        //MatParm.SetColor(colorPicker.GetComponent<ColorPicker>().CurrentColor);
            //       // shosePart.GetComponent<Renderer>().materials[materialIndex].color = colorPicker.GetComponent<ColorPicker>().CurrentColor;
            //        //tempColor = colorPicker.GetComponent<ColorPicker>().CurrentColor;
            //        if (colorListPicker != null)
            //        {
            //          //  Debug.Log("GGG:");
            //            if (!ColorManager.InCustomMode)
            //            {
            //                if (colorListPicker.GetComponent<ScrollListTest>().selectedIndex != -1)
            //                {
            //                    TempPickColor=colorListPicker.GetComponent<ScrollListTest>().Bank.ColorsBank[colorListPicker.GetComponent<ScrollListTest>().selectedIndex];

            //                    if (matType == MaterialType.Glass)
            //                    {
            //                        alpha=0.5f;
            //                    }
            //                    else
            //                    {
            //                        alpha=1f;
            //                    }

            //                    }
            //                    if (ColorManager.tempColor != tempColor)
            //                    {

            //                     tempColor= new Color(TempPickColor.r,TempPickColor.g,TempPickColor.b,alpha);
            //                 //   Debug.Log("GGGs:");
            //                    if ((ObjIs == ObjMenu.Base && materialIndex == 1))
            //                    {
            //                        shose.heelIn.color = new Color(TempPickColor.r, TempPickColor.g, TempPickColor.b, alpha);
            //                        //shose.heelObj.GetComponent<Renderer>().materials[0].color =  new Color(TempPickColor.r,TempPickColor.g,TempPickColor.b,alpha);
            //                        //shose.heel2Obj.GetComponent<Renderer>().materials[0].color =  new Color(TempPickColor.r,TempPickColor.g,TempPickColor.b,alpha);
            //                        MatParm.SetColor(new Color(TempPickColor.r,TempPickColor.g,TempPickColor.b,alpha));//colorListPicker.GetComponent<ScrollListTest>().Bank.ColorsBank[colorListPicker.GetComponent<ScrollListTest>().selectedIndex]);

            //                        ColorManager.tempColor = tempColor;
            //                    }
            //                    if (colorListPicker.GetComponent<ScrollListTest>().selectedIndex >= 0)
            //                    {
            //                        TargetMaterial.color = colorListPicker.GetComponent<ScrollListTest>().Bank.ColorsBank[colorListPicker.GetComponent<ScrollListTest>().selectedIndex];
            //                        //==>  shosePart.GetComponent<Renderer>().materials[materialIndex].color = colorListPicker.GetComponent<ScrollListTest>().Bank.ColorsBank[colorListPicker.GetComponent<ScrollListTest>().selectedIndex];
            //                        // Debug.Log(tempColor);
            //                        MatParm.SetColor(colorListPicker.GetComponent<ScrollListTest>().Bank.ColorsBank[colorListPicker.GetComponent<ScrollListTest>().selectedIndex]);

            //                        //if (matType == MaterialType.Glass)
            //                        //{
            //                        //    Debug.Log(ColorManager.tempColor+" "+tempColor);
            //                        //    tempColor= new Color(tempColor.r,tempColor.g,tempColor.b,0.5f);
            //                        //    ColorManager.tempColor = tempColor;
            //                        //}
            //                        //else
            //                        //{
            //                        ColorManager.tempColor = tempColor;
            //                        //}

            //                        // } 
            //                    }

            //                }
            //            }
            //        }
            //    }

            //    break;
            case MaterialMenu.texture:

                //color control
               // Debug.Log("GGG:");
                //MatParm.SetColor(colorPicker.GetComponent<ColorPicker>().CurrentColor);
                // shosePart.GetComponent<Renderer>().materials[materialIndex].color = colorPicker.GetComponent<ColorPicker>().CurrentColor;
                //tempColor = colorPicker.GetComponent<ColorPicker>().CurrentColor;\
                // Debug.Log(colorListPicker.GetComponent<ScrollListTest>().IsAction);
                if (materialSlider.gameObject.activeSelf && materialSlider.selectedIndex >= -1 && (materialSlider.IsAction || colorListPicker.GetComponent<ScrollListTest>().IsAction))
                {
                    materialSlider.IsAction=false;
                    colorListPicker.GetComponent<ScrollListTest>().IsAction = false;
                  //  SoundManScript.PlaySound(SoundManScript.Botton_Clip);
                    // Debug.Log(")"+tempMatMenu);
                    patternCollectionManager.xx(-1);
                    colorListPicker.GetComponent<ScrollListTest>().IsAction = false;
                    if (colorListPicker != null)
                    {
                        //  Debug.Log("GGG:");
                        if (!ColorManager.InCustomMode)
                        {
                            if (colorListPicker.GetComponent<ScrollListTest>().selectedIndex != -1)
                            {
                                TempPickColor = colorListPicker.GetComponent<ScrollListTest>().Bank.ColorsBank[colorListPicker.GetComponent<ScrollListTest>().selectedIndex];

                                if (matType == MaterialType.Glass)
                                {
                                    alpha = 0.5f;
                                }
                                else
                                {
                                    alpha = 1f;
                                }
                                MatSelect = MaterialSelected.texture;
                            }
                            //    Debug.Log(ColorManager.tempColor +"  "+ tempColor);
                            if (ColorManager.tempColor != tempColor)
                            {

                                //  tempColor = new Color(TempPickColor.r, TempPickColor.g, TempPickColor.b, alpha);
                                if (materialSlider.selectedIndex == 0 )
                                {
                                    //  Debug.Log("GGGs:");
                                    materialSlider.cells[0].SetMarker(true);

                                }
                                if ((ObjIs == ObjMenu.Base && materialIndex == 1))
                                {
                                    shose.heelIn.color = new Color(TempPickColor.r, TempPickColor.g, TempPickColor.b, alpha);
                                    //shose.heelObj.GetComponent<Renderer>().materials[0].color =  new Color(TempPickColor.r,TempPickColor.g,TempPickColor.b,alpha);
                                    //shose.heel2Obj.GetComponent<Renderer>().materials[0].color =  new Color(TempPickColor.r,TempPickColor.g,TempPickColor.b,alpha);
                                    MatParm.SetColor(new Color(TempPickColor.r, TempPickColor.g, TempPickColor.b, alpha));//colorListPicker.GetComponent<ScrollListTest>().Bank.ColorsBank[colorListPicker.GetComponent<ScrollListTest>().selectedIndex]);

                                    ColorManager.tempColor = tempColor;
                                }
                                if (colorListPicker.GetComponent<ScrollListTest>().selectedIndex >= 0)
                                {

                                    TargetMaterial.color = colorListPicker.GetComponent<ScrollListTest>().Bank.ColorsBank[colorListPicker.GetComponent<ScrollListTest>().selectedIndex];
                                    //==>  shosePart.GetComponent<Renderer>().materials[materialIndex].color = colorListPicker.GetComponent<ScrollListTest>().Bank.ColorsBank[colorListPicker.GetComponent<ScrollListTest>().selectedIndex];
                                    // Debug.Log(tempColor);
                                    MatParm.SetColor(colorListPicker.GetComponent<ScrollListTest>().Bank.ColorsBank[colorListPicker.GetComponent<ScrollListTest>().selectedIndex]);

                                    //if (matType == MaterialType.Glass)
                                    //{
                                    //    Debug.Log(ColorManager.tempColor+" "+tempColor);
                                    //    tempColor= new Color(tempColor.r,tempColor.g,tempColor.b,0.5f);
                                    //    ColorManager.tempColor = tempColor;
                                    //}
                                    //else
                                    //{
                                    ColorManager.tempColor = tempColor;
                                    //}

                                    // } 
                                }

                            }
                        }
                    }
                    //end of color control

                    //if (colorListPicker.GetComponent<ScrollListTest>().selectedIndex != -1)
                    //{
                    //    tempColor = colorListPicker.GetComponent<ScrollListTest>().Bank.ColorsBank[colorListPicker.GetComponent<ScrollListTest>().selectedIndex];
                    //}
                    //  TileTexure(tileValue);
                    //   TemptileValue = tileValue;
                    if (matType == MaterialType.Glass)
                    {
                        tempColor = new Color(tempColor.r, tempColor.g, tempColor.b, 0.5f);
                        selectedTexteure = PatternList.Bank.defaultTexture;
                        //StartCoroutine(LoadNormalImage(selectedTexteure));
                        //LoadNormalImage(selectedTexteure);  /// Glass
                        //if (isLoadNormalImage)
                        //{
                        //    Debug.Log("material");
                        //    isLoadNormalImage = false;
                        //    StartCoroutine(LoadNormalImage(selectedTexteure)); 
                        //}
                        Debug.Log("GGG:5");
                        isLoadNormalImage = false;
                        StartCoroutine(LoadNormalImage(selectedTexteure));//5
                       // StartCoroutine(LoadNormalImage(selectedTexteure));                                  /// 
                        ///////////////////////////////////////////particalManScript.runParticalMaterial(ObjIs);
                        //    normalTexture = materialSlider.Bank.NormalMapGen(selectedTexteure, 1, 1);
                        //  normalTexture = materialSlider.Bank.LoadTextureFromResourse(materialSlider.Bank.normaltextureFileName, materialSlider.Bank.namesArr[materialSlider.selectedIndex]);
                        //selectedTexteure = materialSlider.Bank.LoadTextureFromResourse(materialSlider.Bank.normaltextureFileName, materialSlider.Bank.namesArr[materialSlider.selectedIndex]); 
                        //MaterialPara = GlassMaterialPara;
                        MaterialPara = parameterMatEffect.ParamValue[1];
                        MatParm.MaterialParamter(TargetMaterial, MaterialPara);
                        //==>     MatParm.MaterialParamter(shosePart.GetComponent<SkinnedMeshRenderer>().materials[materialIndex], MaterialPara);
                    }
                    else
                    {
                        tempColor = new Color(tempColor.r, tempColor.g, tempColor.b, 1f);
                    }
                    // Debug.Log("KOKO" + materialSlider.selectedIndex + "  " + materialSlider.Bank.spritesArr.Length + "  " + materialSlider.Bank.name + "  " + "  " + materialSlider.Bank.Finishload + "  " + materialSlider.Bank.namesArr[materialSlider.selectedIndex]);
                    // Debug.Log(materialSlider.selectedIndex);
                    // materialSlider.Bank.namesArr[materialSlider.selectedIndex]
                    if (materialSlider.Bank.namesArr.Length > 0)
                    {
                        //     Debug.Log("PPK");
                        if (materialSlider.selectedIndex >= 0)
                        {
                            selectedTexteure = materialSlider.Bank.LoadTextureFromResourse(materialSlider.Bank.textureFileName, materialSlider.Bank.namesArr[materialSlider.selectedIndex]);
                        }
                        else
                        {
                            materialSlider.selectedIndex = 0;
                            selectedTexteure = materialSlider.Bank.LoadTextureFromResourse(materialSlider.Bank.textureFileName, materialSlider.Bank.namesArr[0]);
                        }
                       
                        // Debug.Log("material");
                        materialMenusManager.materialFolder[targetMaterialIndex] = materialSlider.Bank.textureFileName.ToString();
                        //StartCoroutine(LoadNormalImage(selectedTexteure));
                        // LoadNormalImage(selectedTexteure);
                        switch (matType)
                        {
                            case MaterialType.Fabric:
                             //   Debug.Log("yalahwy");
                                normalTexture = (Texture2D)parameterMatEffect.normals[0];
                                TargetMaterial.SetTextureScale("_BumpMap", FixedBmpTile);
                                StartCoroutine(settextureit());
                                particalManScript.runParticalMaterial(ObjIs);
                                StartCoroutine(WaitForLoadingBar(0.25f));
                                // tileValue = new Vector2(2, 2);
                                break;
                            case MaterialType.Mat:
                            case MaterialType.Glass:
                            case MaterialType.Gloosy:
                            case MaterialType.HalfGloosy:
                            case MaterialType.Metal:
                                //  LoadNormalImage(selectedTexteure);
                               //s Debug.Log("GGG:6");
                                StartCoroutine(LoadNormalImage(selectedTexteure));//6
                                break;
                            default:
                                break;

                        }
                     ///////////////////////////   particalManScript.runParticalMaterial(ObjIs);
                        // normalTexture = materialSlider.Bank.NormalMapGen(selectedTexteure, 1, 1);
                        //normalTexture = materialSlider.Bank.LoadTextureFromResourse(materialSlider.Bank.normaltextureFileName, materialSlider.Bank.namesArr[materialSlider.selectedIndex]);

                    }


                }
                break;
            case MaterialMenu.pattern:
                if (PatternList.gameObject.activeSelf && PatternList.selectedIndex >= 0 && PatternList.IsAction)
                {
                   // materialSlider.selectedIndex = -1;
                    PatternList.IsAction = false;
               //     SoundManScript.PlaySound(SoundManScript.Botton_Clip);
                    //Texture Xs = PatternList.Bank.texture[PatternList.selectedIndex];
                    //Xs.wrapMode = TextureWrapMode.Repeat;

                    // TileTexure(tileValue);
                    //    TemptileValue = tileValue;
                    // Texture Xs;
                    if (matType == MaterialType.Glass)
                    {

                        tempColor = new Color(1, 1, 1, 0.5f);
                        selectedTexteure = PatternList.Bank.defaultTexture; //(Texture2D)PatternList.Bank.normal[PatternList.selectedIndex];
                        //  StartCoroutine(LoadNormalImage(PatternList.Bank.LoadTextureFromResourse(PatternList.Bank.textureFileName, PatternList.Bank.namesArr[PatternList.selectedIndex])));
                        switch (matType)
                        {

                            case MaterialType.Fabric:
                                normalTexture = (Texture2D)parameterMatEffect.normals[0];
                                TargetMaterial.SetTextureScale("_BumpMap", FixedBmpTile);
                                StartCoroutine(settextureit());
                                
                                particalManScript.runParticalMaterial(ObjIs);
                                StartCoroutine(WaitForLoadingBar(0.25f));
                                // tileValue = new Vector2(2, 2);
                                break;
                            case MaterialType.Mat:
                            case MaterialType.Glass:
                            case MaterialType.Gloosy:
                            case MaterialType.HalfGloosy:
                            case MaterialType.Metal:
                                Debug.Log("GGG:7");
                                StartCoroutine(LoadNormalImage(PatternList.Bank.LoadTextureFromResourse(PatternList.Bank.textureFileName, PatternList.Bank.namesArr[PatternList.selectedIndex])));//7

                                //LoadNormalImage(PatternList.Bank.LoadTextureFromResourse(PatternList.Bank.textureFileName, PatternList.Bank.namesArr[PatternList.selectedIndex]));
                                break;
                            default:
                                break;

                        }
                        // LoadNormalImage(PatternList.Bank.LoadTextureFromResourse(PatternList.Bank.textureFileName, PatternList.Bank.namesArr[PatternList.selectedIndex]));
                        //  normalTexture = PatternList.Bank.NormalMapGen(PatternList.Bank.LoadTextureFromResourse(PatternList.Bank.textureFileName, PatternList.Bank.namesArr[PatternList.selectedIndex]), 1, 1);
                        // normalTexture = PatternList.Bank.LoadTextureFromResourse(PatternList.Bank.normaltextureFileName, PatternList.Bank.namesArr[PatternList.selectedIndex]);
                        MaterialPara = new Vector4(0, 1, 0, 0);
                        MatParm.MaterialParamter(TargetMaterial, MaterialPara);
                       // particalManScript.runParticalMaterial(ObjIs);
                        //==>   MatParm.MaterialParamter(shosePart.GetComponent<SkinnedMeshRenderer>().materials[materialIndex], MaterialPara);

                    }
                    else
                    {

                        tempColor = new Color(1, 1, 1, 1);
                        // Xs = PatternList.Bank.texture[PatternList.selectedIndex];//.bank.texture;
                        // Xs = PatternList.Bank.LoadTextureFromResourse(PatternList.Bank.textureFileName, PatternList.Bank.namesArr[PatternList.selectedIndex]);
                        selectedTexteure = PatternList.Bank.LoadTextureFromResourse(PatternList.Bank.textureFileName, PatternList.Bank.namesArr[PatternList.selectedIndex]);
                        //Debug.Log("Pattrn");
                        materialMenusManager.materialFolder[targetMaterialIndex] = PatternList.Bank.textureFileName.ToString();
                        // StartCoroutine(LoadNormalImage(selectedTexteure));
                        // LoadNormalImage(selectedTexteure);
                        switch (matType)
                        {


                            case MaterialType.Fabric:
                                normalTexture = (Texture2D)parameterMatEffect.normals[0];
                                TargetMaterial.SetTextureScale("_BumpMap", FixedBmpTile);
                                TargetMaterial.SetColor("_Color", new Color(1, 1, 1, 1));
                                StartCoroutine(settextureit());
                                particalManScript.runParticalMaterial(ObjIs);
                                StartCoroutine(WaitForLoadingBar(0.25f));
                                // tileValue = new Vector2(2, 2);
                                break;
                            case MaterialType.Mat:
                            case MaterialType.Glass:
                            case MaterialType.Gloosy:
                            case MaterialType.HalfGloosy:
                            case MaterialType.Metal:
                                //LoadNormalImage(selectedTexteure);
                                Debug.Log("GGG:8");
                                StartCoroutine(LoadNormalImage(selectedTexteure));//8
                                TargetMaterial.SetColor("_Color", new Color(1, 1, 1, 1));
                                break;
                            default:
                                break;

                        }
                        //  normalTexture = PatternList.Bank.NormalMapGen(selectedTexteure, 1, 1);
                        // normalTexture = PatternList.Bank.LoadTextureFromResourse(PatternList.Bank.normaltextureFileName, PatternList.Bank.namesArr[PatternList.selectedIndex]);
                        selectedSprite = PatternList.Bank.spritesArr[PatternList.selectedIndex];
                       ////////////////////////////// particalManScript.runParticalMaterial(ObjIs);
                    }
                    //        Debug.Log(PatternList.selectedIndex);
                    materialMenusManager.PatternSelectedIndex[targetMaterialIndex] = PatternList.selectedIndex;
                    materialSlider.selectedIndex = -1;
                    //particalManScript.runParticalMaterial(ObjIs);
                    //       Debug.Log(PatternList.selectedIndex);



                }

                break;
            case MaterialMenu.none:
                break;
            default:
                break;
        }

    }
    IEnumerator settextureit()
    {

        yield return new WaitForSeconds(0);
        switch (tempMatMenu)
        {
            case MaterialMenu.color:
                break;
            case MaterialMenu.texture:
                if (materialSlider.selectedIndex > -1)
                {

                    selectedSprite = materialSlider.Bank.spritesArr[materialSlider.selectedIndex];

                    if ((ObjIs == ObjMenu.Base && materialIndex == 1))
                    {
                        if (materialSlider.Bank.defaultTexture != null)
                        {
                            //  Debug.Log(materialSlider.Bank.defaultTexture.name);
                        }

                        //  Debug.Log("OLO" + tempColor);
                        //selectedTexteure = (Texture2D)materialSlider.Bank.texture[materialSlider.selectedIndex];
                        // Debug.Log("KOKO" + materialSlider.selectedIndex + "  " + materialSlider.Bank.spritesArr.Length + "  " + materialSlider.Bank.name + "  " + materialSlider.Bank.spritesArr[materialSlider.selectedIndex].name + "  " + materialSlider.Bank.Finishload);

                        //   ===========================>
                        //     MatParm.ChangeTexture(materialSlider.Bank.defaultTexture, materialSlider.Bank.normal[materialSlider.selectedIndex], materialSlider.Bank.texture[materialSlider.selectedIndex], tempColor, new Vector2( tileValue.y * 0.5f,ModelManager.baseHeightFront.value * 2 * tileValue.x * 0.5f));//ll  tileValue * 0.5fnew Vector2( ModelManager.baseHeightFront.value * 2 *tileValue.x*0.5f,tileValue.y*0.5f)
                        MatParm.ChangeTexture(materialSlider.Bank.defaultTexture, normalTexture, selectedTexteure, detailNormalMap, tempColor, new Vector2(tileValue.x, ModelManager.baseHeightFront.value * 2 * tileValue.y));//ll  tileValue * 0.5fnew Vector2( ModelManager.baseHeightFront.value * 2 *tileValue.x*0.5f,tileValue.y*0.5f)
                        if (matType == MaterialType.Fabric)
                        {

                            MatParm._TileBumpValue = FixedBmpTile;
                        }
                        else
                        {
                            MatParm._TileBumpValue = MatParm._TileValue;
                        }

                        MatParm.SetMatParams(shose.heelIn);
                        //  MatParm.SetMatParams(shose.heelIn);
                        MatParm.ChangeTexture(materialSlider.Bank.defaultTexture, normalTexture, selectedTexteure, detailNormalMap, tempColor, new Vector2(tileValue.x , tileValue.y ));//ll  tileValue * 0.5fnew Vector2( ModelManager.baseHeightFront.value * 2 *tileValue.x*0.5f,tileValue.y*0.5f)
                        //if (matType == MaterialType.Fabric)
                        //{

                        //    MatParm._TileBumpValue = FixedBmpTile;
                        //}
                        //else
                        //{
                        //    MatParm._TileBumpValue = MatParm._TileValue;
                        //}

                        MatParm.SetMatParams(TargetMaterial);

                        //==>    MatParm.SetMatParams(shosePart.GetComponent<SkinnedMeshRenderer>().materials[materialIndex]);
                    }
                    else
                    {

                        if ((ObjIs == ObjMenu.Base && materialIndex == 2))
                        {
                            // selectedTexteure = (Texture2D)materialSlider.Bank.texture[materialSlider.selectedIndex];
                            //selectedTexteure = materialSlider.Bank.LoadTextureFromResourse(materialSlider.Bank.textureFileName, materialSlider.Bank.namesArr[materialSlider.selectedIndex]);
                            //normalTexture
                            // MatParm.ChangeTexture(materialSlider.Bank.defaultTexture, normalTexture, selectedTexteure, tempColor, new Vector2(tileValue.x, -1*(1 - (ModelManager.baseHeightFront.value * 2)) * tileValue.y));//ll  tileValue * 0.5fnew Vector2( ModelManager.baseHeightFront.value * 2 *tileValue.x*0.5f,tileValue.y*0.5f)

                            MatParm.ChangeTexture(materialSlider.Bank.defaultTexture, normalTexture, selectedTexteure, detailNormalMap, tempColor, new Vector2(tileValue.x, ((ModelManager.baseHeightFront.value * 2)) * tileValue.y * 0.5f));//wegdite

                            if (materialSlider.Bank.defaultTexture != null)
                            {
                              //  Debug.Log(materialSlider.Bank.defaultTexture.name);
                            }

                        }
                        else
                        {
                            //  Debug.Log("OLO2" + tempColor);
                            //   selectedTexteure = (Texture2D)materialSlider.Bank.texture[materialSlider.selectedIndex];// materialSlider.Bank.normal[materialSlider.selectedIndex];//oooooooooooooooo
                            //  selectedTexteure = materialSlider.Bank.LoadTextureFromResourse(materialSlider.Bank.textureFileName, materialSlider.Bank.namesArr[materialSlider.selectedIndex]);
                            //     Debug.Log(materialSlider.Bank.defaultTexture + " X " + materialSlider.Bank.normal[materialSlider.selectedIndex].name + " X " + materialSlider.Bank.texture[materialSlider.selectedIndex].name + " x " + tempColor + " X " + tileValue);
                            MatParm.ChangeTexture(materialSlider.Bank.defaultTexture, normalTexture, selectedTexteure, detailNormalMap, tempColor, tileValue);
                            if (materialSlider.Bank.defaultTexture != null)
                            {
                                //          Debug.Log(materialSlider.Bank.defaultTexture.name);
                            }

                        }
                        if (matType == MaterialType.Fabric)
                        {
                            //    Debug.Log("OFOFO");
                            MatParm._TileBumpValue = FixedBmpTile;
                        }
                        else
                        {
                            MatParm._TileBumpValue = MatParm._TileValue;
                        }

                        if (materialSlider.Bank.defaultTexture != null)
                        {
                            //       Debug.Log(materialSlider.Bank.defaultTexture.name);
                        }
                        MatParm.SetMatParams(TargetMaterial);
                        //==>      MatParm.SetMatParams(shosePart.GetComponent<SkinnedMeshRenderer>().materials[materialIndex]);
                    }
                    // TileTexure(tileValue);

                    materialSlider.IsAction = false;

                    materialMenusManager.Texture[targetMaterialIndex] = selectedTexteure;
                    materialMenusManager.Normals[targetMaterialIndex] = normalTexture;
                    materialMenusManager.Sprites[targetMaterialIndex] = selectedSprite;
                    materialMenusManager.TextureSelectedIndex[targetMaterialIndex] = materialSlider.selectedIndex;
                    // Debug.Log("1"+tempMatMenu);
                    materialMenusManager.PatternSelectedIndex[targetMaterialIndex] = -1;   /////////////////////////////////heya dee
                    if (materialSlider.GetComponent<ScrollListTest>().IsVerticalList)
                    {
                        materialMenusManager.BWTexHorizontalNormalizedPosition[targetMaterialIndex] = materialSlider.GetComponent<ScrollListTest>().myScrollRect.verticalNormalizedPosition;
                    }
                    else
                    {
                        materialMenusManager.BWTexHorizontalNormalizedPosition[targetMaterialIndex] = materialSlider.GetComponent<ScrollListTest>().myScrollRect.horizontalNormalizedPosition;
                    }

                }
                break;
            case MaterialMenu.pattern:
                tempColor = Color.white;
                if ((ObjIs == ObjMenu.Base && materialIndex == 1))
                {
                    Debug.Log("pattern");
                     tempColor = Color.white;
                    //tempColor = new Color(1, 1, 1, 1);
                    // selectedTexteure = (Texture2D)Xs;
                    // MatParm.ChangeTexture(Xs, PatternList.Bank.defaultNormal, PatternList.Bank.DefaultOccTexture, tempColor, new Vector2( tileValue.y * 0.5f,ModelManager.baseHeightFront.value * 2 * tileValue.x * 0.5f));
                    //MatParm.ChangeTexture(selectedTexteure, PatternList.Bank.defaultNormal, PatternList.Bank.DefaultOccTexture, tempColor, new Vector2(tileValue.x, ModelManager.baseHeightFront.value * 2 * tileValue.y));
                    MatParm.ChangeTexture(selectedTexteure, normalTexture, PatternList.Bank.DefaultOccTexture, detailNormalMap, tempColor, new Vector2(tileValue.x, ModelManager.baseHeightFront.value * 2 * tileValue.y));
                    if (matType == MaterialType.Fabric)
                    {

                        MatParm._TileBumpValue = FixedBmpTile;
                    }
                    else
                    {
                        MatParm._TileBumpValue = MatParm._TileValue;
                    }
                    MatParm.SetMatParams(shose.heelIn);
                    //MatParm.SetMatParams(shose.heel2Obj.GetComponent<SkinnedMeshRenderer>().materials[0]);

                }
                if ((ObjIs == ObjMenu.Base && materialIndex == 2))
                {
                    //selectedTexteure = (Texture2D)Xs;
                    // MatParm.ChangeTexture(selectedTexteure, PatternList.Bank.defaultNormal, PatternList.Bank.DefaultOccTexture, tempColor, new Vector2(tileValue.x, (1 - ModelManager.baseHeightFront.value * 2) * tileValue.y));
                    // MatParm.ChangeTexture(materialSlider.Bank.defaultTexture, normalTexture, selectedTexteure, tempColor, new Vector2(tileValue.x, -1 * (1 - (ModelManager.baseHeightFront.value * 2)) * tileValue.y));

                    MatParm.ChangeTexture(selectedTexteure, normalTexture, PatternList.Bank.DefaultOccTexture, detailNormalMap, tempColor, new Vector2(tileValue.x, ((ModelManager.baseHeightFront.value * 2)) * tileValue.y * 0.5f));//wegdite



                    //  MatParm.ChangeTexture(selectedTexteure, normalTexture, PatternList.Bank.DefaultOccTexture, tempColor, new Vector2(tileValue.x, ModelManager.baseHeightFront.value * 2 * tileValue.y));


                }
                else
                {
                    // selectedTexteure = (Texture2D)Xs;
                    //Xs.wrapMode = TextureWrapMode.Repeat;

                    // MatParm.ChangeTexture(selectedTexteure, PatternList.Bank.defaultNormal, PatternList.Bank.DefaultOccTexture, tempColor, tileValue);
                    MatParm.ChangeTexture(selectedTexteure, normalTexture, PatternList.Bank.DefaultOccTexture, detailNormalMap, tempColor, tileValue);

                    //ChangeTexture(shose.baseObj, Xs, null, 1f);
                }
                if (matType == MaterialType.Fabric)
                {
                    //   Debug.Log("OFOsFO");
                    MatParm._TileBumpValue = FixedBmpTile;
                }
                else
                {
                    MatParm._TileBumpValue = MatParm._TileValue;
                }
                MatParm.SetMatParams(TargetMaterial);
                //==>   MatParm.SetMatParams(shosePart.GetComponent<SkinnedMeshRenderer>().materials[materialIndex]);
                PatternList.IsAction = false;
                MatSelect = MaterialSelected.pattern;
                materialMenusManager.Texture[targetMaterialIndex] = selectedTexteure;
                materialMenusManager.Normals[targetMaterialIndex] = normalTexture;
                materialMenusManager.Sprites[targetMaterialIndex] = selectedSprite;
                //Debug.Log(PatternList.selectedIndex);
                //materialMenusManager.PatternSelectedIndex[targetMaterialIndex] = PatternList.selectedIndex;
                //Debug.Log(PatternList.selectedIndex);
                //   Debug.Log(patternCollectionManager.PatternsSelectIndex + "  " + PatternList.selectedIndex);
                patternCollectionManager.xx1(patternCollectionManager.PatternsSelectIndex);
                materialMenusManager.SelectedPattrernMenuIndex[targetMaterialIndex] = patternCollectionManager.PatternsSelectIndex;
                //patternCollectionManager.PatternsSelectTextureIndex = PatternList.selectedIndex;
                for (int i = 0; i < patternCollectionManager.SelectedIndTexforList.Length; i++)
                {
                    if (patternCollectionManager.PatternsSelectIndex == i)
                    {
                        patternCollectionManager.SelectedIndTexforList[i] = PatternList.selectedIndex;
                    }
                    else
                    {
                        patternCollectionManager.SelectedIndTexforList[i] = -1; /////ioiio
                    }

                }

                materialMenusManager.TextureSelectedIndex[targetMaterialIndex] = -1;
                if (PatternList.GetComponent<ScrollListTest>().IsVerticalList)
                {
                    if (PatternList.GetComponent<ScrollListTest>().myScrollRect.verticalNormalizedPosition < 0)
                    {
                        materialMenusManager.colorTexHorizontalNormalizedPosition[targetMaterialIndex] = 0;
                    }
                    else if (PatternList.GetComponent<ScrollListTest>().myScrollRect.verticalNormalizedPosition > 1)
                    {
                        materialMenusManager.colorTexHorizontalNormalizedPosition[targetMaterialIndex] = 1;
                    }
                    else
                    {
                        materialMenusManager.colorTexHorizontalNormalizedPosition[targetMaterialIndex] = PatternList.GetComponent<ScrollListTest>().myScrollRect.verticalNormalizedPosition;
                    }
                }
                else
                {
                    if (PatternList.GetComponent<ScrollListTest>().myScrollRect.horizontalNormalizedPosition < 0)
                    {
                        materialMenusManager.colorTexHorizontalNormalizedPosition[targetMaterialIndex] = 0;
                    }
                    else if (PatternList.GetComponent<ScrollListTest>().myScrollRect.horizontalNormalizedPosition > 1)
                    {
                        materialMenusManager.colorTexHorizontalNormalizedPosition[targetMaterialIndex] = 1;
                    }
                    else
                    {
                        materialMenusManager.colorTexHorizontalNormalizedPosition[targetMaterialIndex] = PatternList.GetComponent<ScrollListTest>().myScrollRect.horizontalNormalizedPosition;
                    }
                }


                break;
            case MaterialMenu.none:
                break;
            default:
                break;
        }
        yield return new WaitForSeconds(0.5f);
        //SpinningLoadingBar.SetActive(false);

    }

    void UpdateTileMat(Material _TargetMaterial, Vector2 value)
    {

        switch (matType)
        {
            //case MaterialType.Mat:

            //    _TargetMaterial.SetTextureScale("_MainTex", value);

            //    _TargetMaterial.SetTextureScale("_OcclusionMap", value);

            //    break;
            case MaterialType.Fabric:
                //  Debug.Log("iii");
                _TargetMaterial.SetTextureScale("_MainTex", value);
                _TargetMaterial.SetTextureScale("_OcclusionMap", value);
                normalTexture = (Texture2D)parameterMatEffect.normals[0];
                _TargetMaterial.SetTexture("_BumpMap", normalTexture);
                _TargetMaterial.SetTextureScale("_BumpMap", FixedBmpTile);
                break;
            case MaterialType.Mat:
            case MaterialType.Glass:
            case MaterialType.Gloosy:
            case MaterialType.HalfGloosy:
            case MaterialType.Metal:
                _TargetMaterial.SetTextureScale("_MainTex", value);
                _TargetMaterial.SetTextureScale("_BumpMap", value);
                _TargetMaterial.SetTextureScale("_OcclusionMap", value);
                break;
            default:
                break;
        }

    }
    void MaterialTypeFn()
    {

        if (matChange)
        {

            //switch (tempMatMenu)
            //{
            //    case MaterialMenu.color:
            //        break;
            //    case MaterialMenu.texture:
            //        materialSlider.IsAction = true;
            //        break;
            //    case MaterialMenu.pattern:
            //        PatternList.IsAction = true;
            //        break;
            //    case MaterialMenu.none:
            //        break;
            //    default:
            //        break;
            //}
            // SkinnedMeshRenderer renderer = shosePart.GetComponent<SkinnedMeshRenderer>();
            Material TempMat = TargetMaterial;
            if (matType == MaterialType.Glass)
            {
                // Debug.Log(renderer.materials[1]);
                // renderer.materials[materialIndex] = shose.faceTransparentMaterial;
                // renderer.materials = new Material[] { shose.faceTransparentMaterial, shose.faceTransparentMaterial, shose.faceTransparentMaterial };

                MatParm.MaterialParamter(TargetMaterial, MaterialPara);
                TempMat.EnableKeyword("_NORMALMAP");
                TempMat.SetFloat("_Mode", 3);
                //TempMat.EnableKeyword("_NORMALMAP");
                TempMat.SetFloat("_Glossiness", 1);
                TempMat.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                TempMat.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                TempMat.SetInt("_ZWrite", 0);
                TempMat.DisableKeyword("_ALPHATEST_ON");
                TempMat.EnableKeyword("_ALPHABLEND_ON");
                TempMat.EnableKeyword("_ALPHAPREMULTIPLY_ON");

                TempMat.EnableKeyword("_NORMALMAP");
                TempMat.EnableKeyword("_DETAIL_MULX2");
                TempMat.EnableKeyword("_SPECCGLOSSMAP");
                TempMat.EnableKeyword("_METALLICGLOSSMAP");
                TempMat.renderQueue = 300;
                //   renderer.materials[materialIndex].renderQueue = 3000;
                //Material[] Temp = renderer.materials;//[materialIndex];
                //renderer.materials = Temp;
                //renderer.materials[1].
                //  Material material = renderer.material;
                //  MaterialParam(shose.faceObj.GetComponent<SkinnedMeshRenderer>().material, menu.MaterialPara);
                // shose.faceParm.MaterialParamter(shose.faceTransparentMaterial, MaterialPara);
                //switch (MatSelect)
                //{
                //    case MaterialSelected.texture:
                //        Debug.Log("texture");
                //        MaterialPara = GlassMaterialPara;
                //        MatParm.MaterialParamter(shosePart.GetComponent<SkinnedMeshRenderer>().material, MaterialPara);
                //        break;
                //    case MaterialSelected.pattern:
                //        Debug.Log("pattern");
                //        MaterialPara = new Vector4(0, 1, 0, 0);
                //        MatParm.MaterialParamter(shosePart.GetComponent<SkinnedMeshRenderer>().material, MaterialPara);

                //        break;

                //}

                //==>    MatParm.MaterialParamter(shosePart.GetComponent<SkinnedMeshRenderer>().materials[materialIndex], MaterialPara);
                MatParm.MaterialParamter(TargetMaterial, MaterialPara);

                //MatParm.SetMatParams(shosePart.GetComponent<SkinnedMeshRenderer>().material);
            }
            else
            {
                //    Debug.Log("renderer");

                //  renderer.materials[materialIndex] = shose.faceOpaqueMaterial;

                TempMat.SetFloat("_Mode", 0);
                TempMat.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                TempMat.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                TempMat.SetInt("_ZWrite", 1);

                TempMat.DisableKeyword("_ALPHATEST_ON");
                TempMat.DisableKeyword("_ALPHABLEND_ON");
                TempMat.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                TempMat.EnableKeyword("_NORMALMAP");
                TempMat.EnableKeyword("_SPECCGLOSSMAP");
                TempMat.DisableKeyword("_METALLICGLOSSMAP");
                TempMat.renderQueue = 1000;
                //Material[] Temp = renderer.materials;//[materialIndex];
                //renderer.materials = Temp;


                //renderer.material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcColor);
                //renderer.material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusDstColor);
                //renderer.material.SetInt("_ZWrite", 1);
                //renderer.material.DisableKeyword("_ALPHATEST_OFF");
                //renderer.material.EnableKeyword("_ALPHABLEND_OFF");
                //renderer.material.DisableKeyword("_ALPHAPREMULTIPLY_OFF");
                //renderer.material.renderQueue = 3000;
                //   Material material = renderer.material;
                // MaterialParam(shose.faceObj.GetComponent<SkinnedMeshRenderer>().material, menu.MaterialPara);
                //  shose.faceParm.MaterialParamter(shose.faceOpaqueMaterial, MaterialPara);
                MatParm.MaterialParamter(TargetMaterial, MaterialPara);
                //==>      MatParm.MaterialParamter(shosePart.GetComponent<SkinnedMeshRenderer>().materials[materialIndex], MaterialPara);
                //material.renderQueue = 10000;
                //  MatParm.SetMatParams(shosePart.GetComponent<SkinnedMeshRenderer>().material);
            }
            //////////////////////////////////////////////////////////////////////
            switch (MatSelect)
            {
                case MaterialSelected.texture:

                    materialSlider.IsAction = true;
                    TexureChangeFn(MaterialMenu.texture);
                    break;
                case MaterialSelected.pattern:
                    PatternList.IsAction = true;
                    TexureChangeFn(MaterialMenu.pattern);
                    break;
                case MaterialSelected.none:
                    break;
                default:
                    break;
            }
            ////////////////////////////////////////////////////////////////////////////
            MatParm.SetMatParams(TargetMaterial);
            //   MatParm.SetMatParams(shosePart.GetComponent<SkinnedMeshRenderer>().materials[materialIndex]);
            materialMenusManager.MatParam[targetMaterialIndex] = MaterialPara;
            matChange = false;
        }
    }

    public static void SetupMaterialWithBlendMode(Material material, int blendMode)
    {
        switch (blendMode)
        {
            case 0://BlendMode.Opaque:
                material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                material.SetInt("_ZWrite", 1);
                material.DisableKeyword("_ALPHATEST_ON");
                material.DisableKeyword("_ALPHABLEND_ON");
                material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                material.renderQueue = -1;
                break;
            case 1://BlendMode.Cutout:
                material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                material.SetInt("_ZWrite", 1);
                material.EnableKeyword("_ALPHATEST_ON");
                material.DisableKeyword("_ALPHABLEND_ON");
                material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                material.renderQueue = 2450;
                break;
            case 2://BlendMode.Fade:
                material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                material.SetInt("_ZWrite", 0);
                material.DisableKeyword("_ALPHATEST_ON");
                material.EnableKeyword("_ALPHABLEND_ON");
                material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                material.renderQueue = 3000;
                break;
            case 3://BlendMode.Transparent:
                material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                material.SetInt("_ZWrite", 0);
                material.DisableKeyword("_ALPHATEST_ON");
                material.DisableKeyword("_ALPHABLEND_ON");
                material.EnableKeyword("_ALPHAPREMULTIPLY_ON");
                material.renderQueue = 3000;
                break;
        }
    }
    IEnumerator WaitForLoadingBar(float seconds)
    {
        float currentTime = seconds;
        progressbar.transform.parent.transform.gameObject.SetActive(true);
        // shop.loadingImage.sprite
        if (tempMatMenu == MaterialMenu.texture)
        {
            shop.loadingImage.color = TempPickColor;
            if (materialSlider.selectedIndex >= 0)
            shop.loadingImage.sprite = materialSlider.Bank.spritesArr[materialSlider.selectedIndex];
            else
            {
                shop.loadingImage.sprite = materialSlider.Bank.spritesArr[0];
            }
            
        }
        else
        {
            shop.loadingImage.color = Color.white;
            shop.loadingImage.sprite = PatternList.Bank.spritesArr[PatternList.selectedIndex];
        }
        
        while (currentTime > 0)
        {
           // Debug.Log(currentTime);
            progressbar.fillAmount = 1- currentTime / seconds;
            currentTime -= Time.deltaTime;
            
            yield return null;
        }
        progressbar.transform.parent.transform.gameObject.SetActive(false);
        progressbar.fillAmount = 0;
        isLoadNormalImage = false;
       // Debug.Log(seconds + " seconds have passed! And I'm done waiting!");
    }
    IEnumerator LoadNormalImage(Texture2D selectTexture)
    {

        //normalTexture = null;
        isLoadNormalImage = true;
       // Debug.Log("screenShotPathKllllllllllllllllllllll  "+ isLoadNormalImage);
       // SpinningLoadingBar.SetActive(true);
        
        yield return new WaitForSeconds(Random.Range(0.15f,0.25f));
        //StartCoroutine(StartLoading());

        string normalPath = Application.persistentDataPath + "/" + selectTexture.name + ".png";
        Debug.Log(normalPath);
#if UNITY_IPHONE || UNITY_ANDROID
        //  Debug.Log("mobile" + normalPath);
        // WWW w = new WWW("file://'" + screenShotPath);
        WWW w = new WWW("file:///" + normalPath);
        yield return w;
        if (w.error == null)
        {

            normalTexture = w.texture;
            //  Debug.Log("screenShotPathK" + normalTexture.name);
            particalManScript.runParticalMaterial(ObjIs);
        }
        else
        {
           // print(w.error);
            normalTexture = materialSlider.Bank.NormalMapGen(selectedTexteure, 1, 1); //print(w.error);
            particalManScript.runParticalMaterial(ObjIs);
            //    Debug.Log("screenShotPathCreat");
        }
        StartCoroutine(settextureit());

#endif


    }

    IEnumerator StartLoading()
    {
        yield return new WaitForSeconds(0);
       // SpinningLoadingBar.SetActive(true);
    }


    //    void LoadNormalImage(Texture2D selectTexture)
    //    {
    //        Debug.Log(selectTexture.name);
    //        string normalPath = Application.persistentDataPath + selectTexture.name + ".png";
    //#if UNITY_IPHONE || UNITY_ANDROID

    //        Debug.Log("mobile");
    //        WWW w = new WWW("file://" + normalPath);
    //        //yield return w;

    //#endif
    //        if (w != null)
    //        {
    //            normalTexture = w.texture;
    //        }
    //        else
    //        {
    //            normalTexture = materialSlider.Bank.NormalMapGen(selectedTexteure, 1, 1); //print(w.error);
    //        }
    //    }
    public void SetMaterialManagerIndex(int index)
    {
        targetMaterialIndex = index;
    }
    void SetFolderName()
    {
    }
}
