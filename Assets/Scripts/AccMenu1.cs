﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
public class AccMenu1 : MonoBehaviour
{
    //public List<GameObject> AccerroiesModels;
    public List<ScrollListTest> AccSliders;
    public List<Button> SliderButtons;
    int AccSliderIndex = -1;
    public int selectedIndex;
    public InfoBank Bank;
    public GameObject Model;
    public GameObject NextBackBtn, AccBtnCollection, backBtn, InfoCollection;
    public Decal GeneratManager;
    public ColorPicker colorPicker;
    //public GameObject colorPicker;
    public GameObject colorListPicker;
    public ColorPickerSliderManager ColorManager;
    public Color tempColor;
    public GameObject AccObjectMenu, ColorPicker, EachItemControl;
    GameObject SoundManagereObj;
    SoundManagerClass SoundManScript;
    public modelControl1 manager;
    public ProgressBar proBar;
    void Awake()
    {
        SoundManagereObj = GameObject.FindGameObjectWithTag("SoundManager");
        SoundManScript = (SoundManagerClass)SoundManagereObj.GetComponent("SoundManagerClass");
    }
    // Use this for initialization
    void Start()
    {
        ColorManager.gameObject.SetActive(false);
        tempColor = Color.white;
        for (int i = 0; i < AccSliders.Count; i++)
        {
            AccSliders[i].clearMarker(-1);
            AccSliders[i].IsAction = false;
            AccSliders[i].gameObject.SetActive(false);

        }

    }
    //void ColorControler()
    //{


    //    if (colorPicker.CurrentColor != tempColor)
    //    {
    //        tempColor = colorPicker.CurrentColor;

    //        //colorPicker.getColorMaterial(shosePart);
    //        //colorPicker.setColorMaterial();

    //    }
    //    if ( GeneratManager.color != tempColor)
    //    {
    //        GeneratManager.color = tempColor;
    //    }


    //}
    void ColorControler()
    {

        GeneratManager.inColorCustom = ColorManager.InCustomMode;
        if (ColorManager.InCustomMode)
        {

            if (ColorManager.isOpenWindow)
            {
                if (backBtn.activeSelf == true)
                {
                    backBtn.SetActive(false);
                    AccSliders[AccSliderIndex].gameObject.SetActive(false);
                    EachItemControl.gameObject.SetActive(false);
                }
                //tempColor=
                GeneratManager.color= ColorManager.tempColor;

            }
            else
            {
                if (backBtn.activeSelf == false)
                {
                    ColorManager.InCustomMode = false;
                    backBtn.SetActive(true);
                    Debug.Log(AccSliders[AccSliderIndex].gameObject.activeSelf);
                    AccSliders[AccSliderIndex].gameObject.SetActive(true);
                    EachItemControl.gameObject.SetActive(true);



                }
            }

        }
        else
        {

        }
        //{
        //  Debug.Log(colorPicker.CurrentColor + "  " + tempColor);
        if (colorPicker.CurrentColor != tempColor)
        {
            Debug.Log("KOD");
            if (ColorManager.InCustomMode)
            {

                Debug.Log("InCustomMode");
                GeneratManager.color = ColorManager.tempColor;

                //shosePart.GetComponent<SkinnedMeshRenderer>().materials[materialIndex].color = ColorManager.tempColor;//.GetComponent<ScrollListTest>().Bank.ColorsBank[colorListPicker.GetComponent<ScrollListTest>().selectedIndex];
            }
            else
            {
                Debug.Log("!InCustomMode");
                GeneratManager.color = colorPicker.CurrentColor;
            }
            tempColor = GeneratManager.color;
        }


    }



    // Update is called once per frame
    void Update()
    {

        //if (GeneratManager.accessoriesList.Count > 0)
        //{
        //    if (EachItemControl.activeSelf == false)
        //    {
        //        EachItemControl.SetActive(true);
        //    }

        //}
        //else
        //{
        //    if (EachItemControl.activeSelf == true)
        //    {
        //        EachItemControl.SetActive(false);
        //    }

        //}
        if (!ColorManager.InCustomMode)
        {
            if (AccSliderIndex >= 0)
            {
                if (GeneratManager.accessoriesList.Count > 0)
                {
                    if (EachItemControl.activeSelf == false)
                    {
                        EachItemControl.SetActive(true);
                    }

                }
                else
                {
                    if (EachItemControl.activeSelf == true)
                    {
                        EachItemControl.SetActive(false);
                    }

                }


            }
            else
            {
                if (GeneratManager.StartDecalAction)
                {
                    if (EachItemControl.activeSelf == true)
                    {
                        EachItemControl.SetActive(false);
                    }
                    GeneratManager.StartDecalAction = false;
                    if (GeneratManager.selectedItemIndex >= 0)
                    {
                        markerActivate(GeneratManager.selectedItemIndex, false);
                    }

                }
            }
        }

        ColorControler();
        if (AccSliderIndex >= 0)
        {

            if (AccSliders[AccSliderIndex].IsAction)
            {
                // Debug.Log("llloko");
                Bank = AccSliders[AccSliderIndex].Bank;// gameObject.GetComponent<InfoBank>();
                selectedIndex = AccSliders[AccSliderIndex].selectedIndex;
                SelectModel(Bank, selectedIndex);
                clearselector(AccSliderIndex);
                GeneratManager.AccItemObj = Model;
                AccSliders[AccSliderIndex].IsAction = false;
                if (!GeneratManager.StartDecalAction)
                {
                    Debug.Log(GeneratManager.StartDecalAction);
                    GeneratManager.StartDecalAction = true;
                    if (GeneratManager.selectedItemIndex >= 0)
                    {
                        markerActivate(GeneratManager.selectedItemIndex, true);
                    }

                }
            }

        }
    }
    void markerActivate(int index, bool _bool)
    {

        MeshRenderer[] allChildren = GeneratManager.accessoriesList[index].GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer child in allChildren)
        {
            //   Debug.Log(child.name+"dPP");

            if (child.gameObject.CompareTag("ItemMarker"))
                child.enabled = _bool;
        }
    }
    public void SelectModel(InfoBank Bank, int index)
    {
        // Debug.Log(index);
        Model = Bank.Models[index];
        //colorPicker.getColorMeshMaterial(Model); /// if get color
        //colorPicker.setColorMaterial();
    }
    public void ButtonClick(int index)
    {
        manager.changemorphe = true;
        //resetcollider = false;
        manager.resetcollider = true;
        proBar.ProgbarAnim(proBar.ProgressbarAnim.status, AminationStatus.back);
        SoundManScript.PlaySound(SoundManScript.ListSelectBotton_Clip);
        SubMenuIs(index);
    }
    public void BackClick()
    {

        proBar.ProgbarAnim(proBar.ProgressbarAnim.status, AminationStatus.forward);
        AccBtnCollection.SetActive(true);
        NextBackBtn.SetActive(true);
        InfoCollection.SetActive(true);

        backBtn.SetActive(false);
        ColorManager.InCustomMode = false;
        ColorManager.gameObject.SetActive(false);
        AccSliderIndex = -1;
        for (int i = 0; i < AccSliders.Count; i++)
        {

            // AccSliders[i].clearMarker(-1);
            AccSliders[i].IsAction = false;
            AccSliders[i].gameObject.SetActive(false);

        }
        manager.isInnerMenuAcc = false;
        EachItemControl.gameObject.SetActive(false);
        manager.Camera.GetComponent<WowCamera>().OnClickRight(1, manager.Camera.GetComponent<WowCamera>().cameraRotationAngle);
    }
    //public void MainMenuBtnClick(int index)
    //{
    //    MainMenuIs(index);
    //}
    public void clearselector(int accSliderIndex)
    {
        // Debug.Log("jj");
        for (int i = 0; i < AccSliders.Count; i++)
        {
            if (AccSliderIndex != i)
            {
                AccSliders[i].clearMarker(-1);
                AccSliders[i].IsAction = false;

            }

        }

    }
    public void SubMenuIs(int subMenuIndex)
    {
        manager.isInnerMenuAcc = true;
        NextBackBtn.SetActive(false);
        InfoCollection.SetActive(false);
        AccBtnCollection.SetActive(false);
        backBtn.SetActive(true);
        ColorManager.gameObject.SetActive(true);
        if (AccSliderIndex != subMenuIndex)
        {
            for (int i = 0; i < AccSliders.Count; i++)
            {
                if (i == subMenuIndex)
                {
                    AccSliders[i].gameObject.SetActive(true);
                    // clearselector();
                    AccSliderIndex = subMenuIndex;
                }
                else
                {
                    AccSliders[i].gameObject.SetActive(false);
                }

            }
        }


    }
    //void MainMenuIs(int index)
    //{
    //    switch (index)
    //    {
    //        case 1:
    //            AccObjectMenu.SetActive(true); ColorManager.gameObject.SetActive(false);
    //            break;
    //        case 0:
    //            AccObjectMenu.SetActive(false); ColorManager.gameObject.SetActive(true);
    //            break;
    //        //default:
    //        //    break;
    //    }
    //}

}
