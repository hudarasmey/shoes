﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;


public class AccMenu : MonoBehaviour {


    public List<GameObject> AccerroiesModels;
    public ObjMenu ObjIs;
    MaterialParam MatParm;
    public Shose shose;
    public GameObject shosePart;
    public MaterialType matType;
    public GameObject colorPicker;
    public ScrollListTest materialSlider;
    public ScrollListTest PatternList;
    // Use this for initialization
    public MaterialMenu tempMatMenu;
    public Button MatBtn, colorBtn, PatternBtn;
    public Button MatColorBtn, GlossyBtn, HalfGloosyBtn, GlassBtn, MetalBtn;
    public Vector4 MaterialPara;
    public Vector4 matMaterialPara, glossyMaterialPara, HalfGlossyMaterialPara, GlassMaterialPara, MetalMaterialPara;// x= metallic ,y=Smoothness ,z=occlution
    //public Vector3 matMaterialPara
    public bool matChange;
    public Color tempColor;
    public Vector2 tileValue = new Vector2(1, 1), TemptileValue = new Vector2(0, 0);
    public MaterialSelected MatSelect, tempMatSelect;
    bool itCleared;
    bool isStartCase;
    bool isMaterialStart;
    public Texture2D detailNormalMap;
    void Awake()
    {
      //  tileValue = 1;
        MatSelect = MaterialSelected.texture;
        tempMatSelect = MaterialSelected.none;
        tempColor = Color.white;
        MatParm = new MaterialParam();
        tempMatMenu = MaterialMenu.texture;
        matType = MaterialType.Mat;
        switch (ObjIs)
        {
            case ObjMenu.Face:
                shosePart = shose.faceObj;
                break;
            case ObjMenu.Base:
                shosePart = shose.baseObj;
                break;
            case ObjMenu.Heel:
                shosePart = shose.heelObj;
                break;
            case ObjMenu.none:
                break;
            default:
                break;
        }
        colorPicker.GetComponent<ColorPicker>().getColorMaterial(shosePart,0);
        colorPicker.GetComponent<ColorPicker>().setColorMaterial();



        matChange = true;
        materialSlider.selectedIndex = 0;

        materialSlider.IsAction = true;
        //TexureChangeFn(MaterialMenu.texture);
        //materialSlider.transform.parent.transform.gameObject.SetActive(false);
        tempColor = Color.black;
        ColorControler();
        //TileTexure(tileValue);
        MaterialPara = matMaterialPara;
       // MaterialTypeFn();
       // materialSlider.selectedIndex = 0;

        //  materialSlider.transform.parent.transform.gameObject.SetActive(false);

    }
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (materialSlider.gameObject.activeSelf == true && !isMaterialStart)
        {

            for (int i = 0; i < materialSlider.cells.Count; i++)
            {
                materialSlider.cells[i].gameObject.GetComponent<Image>().color = tempColor;
            }
            isMaterialStart = true;
        }

        switch (tempMatMenu)
        {
            case MaterialMenu.color:
                Debug.Log("s");

                break;
            case MaterialMenu.texture:
                if (materialSlider.selectedIndex >= 0 && MatSelect == MaterialSelected.texture)
                {
                   // tileValue = materialSlider.Bank.startTileValuePerTexture[materialSlider.selectedIndex] + materialSlider.tileValueSlider.value * 10;

                }
                PatternList.tileValueSlider.value = materialSlider.tileValueSlider.value;
                break;
            case MaterialMenu.pattern:
                if (PatternList.selectedIndex >= 0 && MatSelect == MaterialSelected.pattern)
                {
                   // tileValue = PatternList.Bank.startTileValuePerTexture[PatternList.selectedIndex] + PatternList.tileValueSlider.value * 10;

                }
                materialSlider.tileValueSlider.value = PatternList.tileValueSlider.value;
                break;
            case MaterialMenu.none:
                break;
            default:
                break;
        }
        if (MatSelect != tempMatSelect)
        {

            switch (MatSelect)
            {
                case MaterialSelected.texture:
                   
                    PatternList.clearMarker(-1);
                    tempMatSelect = MatSelect;
                    break;
                case MaterialSelected.pattern:
                    materialSlider.clearMarker(-1);
                    tempMatSelect = MatSelect;
                    break;
                case MaterialSelected.none:
                    break;
                default:
                    break;
            }

        }


        ColorControler();
        //TileTexure(tileValue);
       // TexureChangeFn(tempMatMenu);
        //MaterialTypeFn();


    }

    void ColorControler()
    {


        if (colorPicker.GetComponent<ColorPicker>().CurrentColor != tempColor)
        {

            for (int i = 0; i < materialSlider.cells.Count; i++)
            {
                materialSlider.cells[i].gameObject.GetComponent<Image>().color = colorPicker.GetComponent<ColorPicker>().CurrentColor;
            }


            if (MatSelect == MaterialSelected.texture)
            {

                shosePart.GetComponent<Renderer>().material.color = colorPicker.GetComponent<ColorPicker>().CurrentColor;
                tempColor = colorPicker.GetComponent<ColorPicker>().CurrentColor;
            }

        }


        if (MatSelect == MaterialSelected.pattern)
        {
            shosePart.GetComponent<Renderer>().material.color = Color.white;
            tempColor = Color.white;

        }

        //if (tempMatMenu == MaterialMenu.color)
        //{
        //    MatParm.SetColor(colorPicker.GetComponent<ColorPicker>().CurrentColor);
        //}
        //color

        // changeActiveMenu(colorPicker, materialSliderFace);

    }

    void TileTexure(Vector2 value)
    {
        // Debug.Log(shosePart.GetComponent<Renderer>().material.GetTextureScale("_MainTex")+"  "+ tileValue+"  "+ TemptileValue);

        //material tile
        if (TemptileValue != value)
        {
            MatParm.setTileValue(value);
            TemptileValue = value;
            shosePart.GetComponent<Renderer>().material.SetTextureScale("_MainTex", value);
            //glow

            //float scaleX = Mathf.Cos(Time.time) * 0.5F + 1;
            //float scaleY = Mathf.Sin(Time.time) * 0.5F + 1;
            //shose.faceObj.GetComponent<Renderer>().material.mainTextureScale = new Vector2(scaleX, scaleY);
            //shose.faceObj.GetComponent<Renderer>().material.mainTextureScale = new Vector2(tileValue, tileValue);
        }


    }

    public void SubMenuIs(MaterialMenu subMenuType)
    {
        
        if (subMenuType != tempMatMenu)
        {
            switch (subMenuType)
            {
                case MaterialMenu.color:
                    //  ColorControler();
                    // tempMatMenu = MaterialMenu.color;

                    materialSlider.gameObject.transform.parent.transform.gameObject.SetActive(false);
                    colorPicker.gameObject.SetActive(true);
                    PatternList.gameObject.transform.parent.gameObject.SetActive(false);


                    break;
                case MaterialMenu.texture:
                    //  tempMatMenu = MaterialMenu.texture;
                    materialSlider.gameObject.transform.parent.transform.gameObject.SetActive(true);
                    colorPicker.gameObject.SetActive(false);
                    PatternList.gameObject.transform.parent.gameObject.SetActive(false);

                    break;
                case MaterialMenu.pattern:
                    //   tempMatMenu = MaterialMenu.pattern;
                    materialSlider.gameObject.transform.parent.transform.gameObject.SetActive(false);
                    colorPicker.gameObject.SetActive(false);
                    PatternList.gameObject.transform.parent.gameObject.SetActive(true);

                    break;
                case MaterialMenu.none:

                    materialSlider.gameObject.transform.parent.transform.gameObject.SetActive(false);
                    colorPicker.gameObject.SetActive(false);
                    PatternList.gameObject.transform.parent.gameObject.SetActive(false);

                    break;
                default:
                    break;
            }
        }
        tempMatMenu = subMenuType;
    }

    public void ButtonClick(int index)
    {
        SubMenuIs((MaterialMenu)index);
    }

    public void setMatPar(int index)
    {
        matType = (MaterialType)index;
        switch (matType)
        {
            case MaterialType.Mat:
                MaterialPara = matMaterialPara;
                break;
            case MaterialType.Glass:
                switch (MatSelect)
                {
                    case MaterialSelected.texture:
                        MaterialPara = GlassMaterialPara;
                        break;
                    case MaterialSelected.pattern:
                        MaterialPara = new Vector4(0, 1, 0, 0);

                        break;

                }

                break;
            case MaterialType.Gloosy:
                MaterialPara = HalfGlossyMaterialPara;
                break;
            case MaterialType.HalfGloosy:
                MaterialPara = glossyMaterialPara;
                break;
            case MaterialType.Metal:
                MaterialPara = MetalMaterialPara;
                break;
            default:
                break;
        }
        matChange = true;

        //  MaterialPara = H;
    }

    void TexureChangeFn(MaterialMenu _tempMatMenu)
    {
        switch (_tempMatMenu)
        {
            case MaterialMenu.color:
                if (MatSelect == MaterialSelected.texture)
                {
                    MatParm.SetColor(colorPicker.GetComponent<ColorPicker>().CurrentColor);
                    shosePart.GetComponent<Renderer>().material.color = colorPicker.GetComponent<ColorPicker>().CurrentColor;
                    //tempColor = colorPicker.GetComponent<ColorPicker>().CurrentColor;
                }

                break;
            case MaterialMenu.texture:
                if (materialSlider.gameObject.activeSelf && materialSlider.selectedIndex >= 0 && materialSlider.IsAction)
                {
                    //  TileTexure(tileValue);
                    //   TemptileValue = tileValue;
                    if (matType == MaterialType.Glass)
                    {
                        Debug.Log("texture");
                        MaterialPara = GlassMaterialPara;
                        MatParm.MaterialParamter(shosePart.GetComponent<SkinnedMeshRenderer>().material, MaterialPara);
                    }
                    //     Debug.Log(materialSlider.Bank.defaultTexture + " X " + materialSlider.Bank.normal[materialSlider.selectedIndex].name + " X " + materialSlider.Bank.texture[materialSlider.selectedIndex].name + " x " + tempColor + " X " + tileValue);
                    MatParm.ChangeTexture(materialSlider.Bank.defaultTexture, materialSlider.Bank.normal[materialSlider.selectedIndex], materialSlider.Bank.texture[materialSlider.selectedIndex],detailNormalMap, tempColor, tileValue);
                    // TileTexure(tileValue);
                    MatParm.SetMatParams(shosePart.GetComponent<SkinnedMeshRenderer>().material);
                    materialSlider.IsAction = false;
                    MatSelect = MaterialSelected.texture;


                }
                break;
            case MaterialMenu.pattern:
                if (PatternList.gameObject.activeSelf && PatternList.selectedIndex >= 0 && PatternList.IsAction)
                {
                    //Texture Xs = PatternList.Bank.texture[PatternList.selectedIndex];
                    //Xs.wrapMode = TextureWrapMode.Repeat;

                    // TileTexure(tileValue);
                    //    TemptileValue = tileValue;
                    Texture Xs;
                    if (matType == MaterialType.Glass)
                    {
                        Xs = PatternList.Bank.normal[PatternList.selectedIndex];

                        MaterialPara = new Vector4(0, 1, 0, 0);
                        MatParm.MaterialParamter(shosePart.GetComponent<SkinnedMeshRenderer>().material, MaterialPara);

                    }
                    else
                    {
                        Xs = PatternList.Bank.texture[PatternList.selectedIndex];//.bank.texture;
                    }


                    //Xs.wrapMode = TextureWrapMode.Repeat;

                    MatParm.ChangeTexture(Xs, PatternList.Bank.defaultNormal, PatternList.Bank.DefaultOccTexture,detailNormalMap, tempColor, tileValue);
                    //ChangeTexture(shose.baseObj, Xs, null, 1f);

                    MatParm.SetMatParams(shosePart.GetComponent<SkinnedMeshRenderer>().material);
                    PatternList.IsAction = false;
                    MatSelect = MaterialSelected.pattern;
                }

                break;
            case MaterialMenu.none:
                break;
            default:
                break;
        }

    }

    void MaterialTypeFn()
    {

        if (matChange)
        {

            //switch (tempMatMenu)
            //{
            //    case MaterialMenu.color:
            //        break;
            //    case MaterialMenu.texture:
            //        materialSlider.IsAction = true;
            //        break;
            //    case MaterialMenu.pattern:
            //        PatternList.IsAction = true;
            //        break;
            //    case MaterialMenu.none:
            //        break;
            //    default:
            //        break;
            //}
            SkinnedMeshRenderer renderer = shosePart.GetComponent<SkinnedMeshRenderer>();

            if (matType == MaterialType.Glass)
            {

                ////renderer.material = shose.faceTransparentMaterial;
                ////Material material = renderer.material;
                //  MaterialParam(shose.faceObj.GetComponent<SkinnedMeshRenderer>().material, menu.MaterialPara);
                // shose.faceParm.MaterialParamter(shose.faceTransparentMaterial, MaterialPara);
                //switch (MatSelect)
                //{
                //    case MaterialSelected.texture:
                //        Debug.Log("texture");
                //        MaterialPara = GlassMaterialPara;
                //        MatParm.MaterialParamter(shosePart.GetComponent<SkinnedMeshRenderer>().material, MaterialPara);
                //        break;
                //    case MaterialSelected.pattern:
                //        Debug.Log("pattern");
                //        MaterialPara = new Vector4(0, 1, 0, 0);
                //        MatParm.MaterialParamter(shosePart.GetComponent<SkinnedMeshRenderer>().material, MaterialPara);

                //        break;

                //}
                MatParm.MaterialParamter(shosePart.GetComponent<SkinnedMeshRenderer>().material, MaterialPara);


                //MatParm.SetMatParams(shosePart.GetComponent<SkinnedMeshRenderer>().material);
            }
            else
            {
                ////renderer.material = shose.faceOpaqueMaterial;
                ////Material material = renderer.material;
                // MaterialParam(shose.faceObj.GetComponent<SkinnedMeshRenderer>().material, menu.MaterialPara);
                //  shose.faceParm.MaterialParamter(shose.faceOpaqueMaterial, MaterialPara);

                MatParm.MaterialParamter(shosePart.GetComponent<SkinnedMeshRenderer>().material, MaterialPara);
                //material.renderQueue = 10000;
                //  MatParm.SetMatParams(shosePart.GetComponent<SkinnedMeshRenderer>().material);
            }
            switch (MatSelect)
            {
                case MaterialSelected.texture:

                    materialSlider.IsAction = true;
                    TexureChangeFn(MaterialMenu.texture);
                    break;
                case MaterialSelected.pattern:
                    PatternList.IsAction = true;
                    TexureChangeFn(MaterialMenu.pattern);
                    break;
                case MaterialSelected.none:
                    break;
                default:
                    break;
            }

            //Debug.Log("PPPsssssP");
            MatParm.SetMatParams(shosePart.GetComponent<SkinnedMeshRenderer>().material);

            matChange = false;
        }
    }
}