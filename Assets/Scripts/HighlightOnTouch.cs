﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighlightOnTouch : MonoBehaviour
{
    private Material material;
    private Color normalColor;
    private Color highlightColor;
    private bool touching;
    private float interval;

    void Start()
    {
        material = GetComponent<MeshRenderer>().material;

        normalColor = material.color;
        highlightColor = new Color(
            normalColor.r * 1.5f,
            normalColor.g * 1.5f,
            normalColor.b * 1.5f
        );
        StartCoroutine(MyCoroutine(normalColor));
    }

    void Update()
    {
        if (touching)
        {
            interval += Time.deltaTime;
            material.color = Color.Lerp(normalColor, highlightColor, interval);
        }

        if (Input.GetMouseButtonUp(0) && touching)
        {
            touching = false;
            material.color = normalColor;
        }
    }

    void OnMouseDown()
    {
        touching = true;
        interval = 0f;
    }
    void highlight()
    {
        if (touching)
        {
            interval += Time.deltaTime;
            material.color = Color.Lerp(normalColor, highlightColor, interval);
        }
    }
    IEnumerator MyCoroutine(Color StartColor)
    {
       Color TempStart = StartColor;
       highlightColor = new Color(
           StartColor.r * 1.5f,
           StartColor.g * 1.5f,
           StartColor.b * 1.5f
       );
        //float timeToStart = Time.time;
        //while (TempStart != highlightColor) // This is your target size of object.
        //{
        //    Debug.Log("PPP " + normalColor + "  " + highlightColor);
        //    TempStart = Color.Lerp(normalColor, highlightColor, (Time.time - timeToStart) * 2f);//Here speed is the 1 or any number which decides the how fast it reach to one to other end.
        //    material.color = TempStart;
        //    yield return null;
        //}
       yield return new WaitForSeconds(1);
       material.color = highlightColor;
        yield return new WaitForSeconds(1);

        material.color = StartColor;

        print("Reached the target.");

    }
}