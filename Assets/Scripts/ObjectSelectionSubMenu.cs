﻿using UnityEngine;
using System.Collections;

public class ObjectSelectionSubMenu : MonoBehaviour
{


    public ObjMenu ObjIs;
    ObjMenu ObjIsTemp = ObjMenu.none;
    public ScrollListTest FaceModelScroller;
    public ScrollListTest BaseModelScroller;
    public ScrollListTest HeelModelScroller;
    public ScrollListTest FrontFModelScroller;
    public ScrollListTest BackGModelScroller;

    public GameObject BtnCollection, backNextBtn, backBtn, InfoCollection;
    public ScrollListTest selectedScroll;
    public Shose shose;
    public GameObject shosePart;
    public GameObject baseModel, baseLabel, WidgeLabel;
    public modelControl1 modelCont;
    public int widgetIndex;

    public SkinnedMeshRenderer backFaceSkinnedMesh, frontFaceSkinnedMesh, faceSkinnedMesh, baseSkinnedMesh, heelSkinnedMesh;
    public SkinnedMeshRenderer backFaceSkinnedMeshTemp, frontFaceSkinnedMeshTemp, faceSkinnedMeshTemp, baseSkinnedMeshTemp, heelSkinnedMeshTemp;
    public ProgressBar proBar;
    public modelControl1 manager;
    public ShopMan shop;

    WowCamera cam;
    bool startColorlerp;
    float duration = .15f; // This will be your time in seconds.
    float smoothness = 0.01f;
    public Color color_1, color_2, tempcolor, targetColor;

    //Speed at which lerp should occur
    public float speed = 1.0f;

    //A and B vectors to which lerp should occur
    //Vector3 positionA =new Vector3(0,0,0);
    //Vector3 positionB=new Vector3(0,10,0);

    //Lerp Direction
    bool LerpedUp = false;
    //Lerp time value
    float LerpTime = 1.0f;
    public float LerpNum = 0.5f;
    public bool startLerp = true, startit;


    GameObject SoundManagereObj;
    SoundManagerClass SoundManScript;
    public GameObject particalManagereObj;
    public ParticalManager particalManScript;
    SkinnedMeshRenderer skMesh;
    void Awake()
    {
        //Bank = GameObject.FindGameObjectWithTag("SaveInfoBank").GetComponent<InfoBank>();
        SoundManagereObj = GameObject.FindGameObjectWithTag("SoundManager");
        SoundManScript = (SoundManagerClass)SoundManagereObj.GetComponent("SoundManagerClass");
        backFaceSkinnedMeshTemp=backFaceSkinnedMesh;
        frontFaceSkinnedMeshTemp=frontFaceSkinnedMesh;
        faceSkinnedMeshTemp=faceSkinnedMesh;
        baseSkinnedMeshTemp=baseSkinnedMesh;
        heelSkinnedMeshTemp=heelSkinnedMesh;
    }
    void Start()
    {

        particalManagereObj = GameObject.FindGameObjectWithTag("ParticalManager");
        particalManScript = (ParticalManager)particalManagereObj.GetComponent("ParticalManager");
        cam = (WowCamera)GameObject.FindObjectOfType<WowCamera>();

        modelCont = GameObject.FindObjectOfType<modelControl1>().GetComponent<modelControl1>();
        FaceModelScroller.transform.gameObject.SetActive(false);
        // BaseModelScroller.transform.gameObject.SetActive(false);
        HeelModelScroller.transform.gameObject.SetActive(false);

        //selectedScroll.selectedIndex = 0;

        //selectedScroll.IsAction = true;


    }
    void FnRelatedSelection()
    {

    }
    void high()
    {
        if (true)
        {

        }
        // Debug.Log(selectedScroll.Bank.Models[1].name);
        //selectedScroll.Bank.Models[1].GetComponent<EachObjControl>().HighlightObj();

    }
    void ModelChange(ScrollListTest modelScroll)
    {
        
        if (modelScroll != null)
        {
            if (modelScroll == HeelModelScroller)
            {

                if (modelScroll.gameObject.activeSelf && modelScroll.selectedIndex >= 0 && modelScroll.IsAction)
                {
                    // int tempModelIndex = 0;
                    Debug.Log("sdsdsd " + modelScroll.selectedIndex + "  " + widgetIndex);
                    for (int i = 0; i < modelScroll.Bank.spritesArr.Length; i++)
                    {
                        if (i == modelScroll.selectedIndex)
                        {
                            // Debug.Log(i);
                            if (i == widgetIndex)//modelScroll.Bank.modelIndex[i] == widgetIndex)
                            {
                                baseModel.SetActive(false);
                                manager.wedgeImg.SetActive(true);
                                manager.baseImg.SetActive(false);
                                baseLabel.SetActive(false);
                                WidgeLabel.SetActive(true);
                                shose.transform.position = new Vector3(0, 0.8f, 0);
                            }
                            else
                            {
                                baseModel.SetActive(true);
                                baseLabel.SetActive(true);
                                manager.wedgeImg.SetActive(false);
                                manager.baseImg.SetActive(true);
                                WidgeLabel.SetActive(false);
                                shose.transform.position = new Vector3(0, 0, 0);
                            }

                            modelScroll.Bank.Models[i].GetComponent<SkinnedMeshRenderer>().enabled = true;
                            particalManScript.runParticalOnce(particalManScript.p1, modelScroll.Bank.Models[i].GetComponent<SkinnedMeshRenderer>());
                            skMesh = modelScroll.Bank.Models[i].GetComponent<SkinnedMeshRenderer>();
                            setSkinnedMesh(skMesh);
                            //Debug.Log("KDKD");
                            if (modelScroll.Bank.Models[i].GetComponent<EachObjControl>().subObj != null)
                            {
                                modelScroll.Bank.Models[i].GetComponent<EachObjControl>().subObj.GetComponent<SkinnedMeshRenderer>().enabled = true;
                            }
                            // modelScroll.Bank.Models[i].GetComponent<EachObjControl>().HighlightObj();
                            if (modelScroll.Bank.Models[i].GetComponent<MeshCollider>() != null)
                            {
                                modelScroll.Bank.Models[i].GetComponent<MeshCollider>().enabled = true;

                            }
                            if (modelScroll.Bank.Models[i].GetComponent<EachObjControl>().subObj != null)
                            {
                                modelScroll.Bank.Models[i].GetComponent<EachObjControl>().subObj.GetComponent<MeshCollider>().enabled = true;
                            }
                            manager.setValueForheelThickness(1);
                            //  modelScroll.setBtnSprit(i);
                        }
                        else
                        {
                            modelScroll.Bank.Models[i].GetComponent<SkinnedMeshRenderer>().enabled = false;
                            if (modelScroll.Bank.Models[i].GetComponent<EachObjControl>().subObj != null)
                            {
                                modelScroll.Bank.Models[i].GetComponent<EachObjControl>().subObj.GetComponent<SkinnedMeshRenderer>().enabled = false;
                            }
                            if (modelScroll.Bank.Models[i].GetComponent<MeshCollider>() != null)
                            {
                                modelScroll.Bank.Models[i].GetComponent<MeshCollider>().enabled = false;

                            }
                            if (modelScroll.Bank.Models[i].GetComponent<EachObjControl>().subObj != null)
                            {
                                modelScroll.Bank.Models[i].GetComponent<EachObjControl>().subObj.GetComponent<MeshCollider>().enabled = false;
                            }
                        }
                        //{
                        //    //if (i<3)
                        //    //{
                        //    //    BaseModelScroller.Bank.Models[0].SetActive(true);
                        //    //}
                        //    //else
                        //    //{
                        //    //    BaseModelScroller.Bank.Models[0].SetActive(false);
                        //    //}
                        //    for (int f = 0; f < modelScroll.Bank.Models.Length; f++)
                        //    {
                        //        //modelScroll.Bank.Models[f].SetActive(false);
                        //        modelScroll.Bank.Models[f].GetComponent<SkinnedMeshRenderer>().enabled = false;
                        //    }
                        //    for (int j = 0; j < modelScroll.Bank.Models.Length; j++)
                        //    {
                        //        //Debug.Log("pp");

                        //        if (modelScroll.Bank.modelIndex[i]== j)
                        //        {
                        //            tempModelIndex = modelScroll.Bank.modelIndex[i];

                        //            ////////////////////modelScroll.Bank.Models[modelScroll.Bank.modelIndex[i]].SetActive(true);

                        //            modelCont.heelthickIndex = modelScroll.Bank.morphIndex[i];
                        //            //Debug.Log(modelScroll.Bank.modelIndex[i] + "  " + widgetIndex);
                        //            if (modelScroll.Bank.modelIndex[i] == widgetIndex)
                        //            {
                        //                baseModel.SetActive(false);

                        //            }
                        //            else
                        //            {
                        //                baseModel.SetActive(true);
                        //            }
                        //            //modelScroll.Bank.Models[modelScroll.Bank.modelIndex[i]].GetComponent<SkinnedMeshRenderer>().enabled = true;
                        //            modelScroll.Bank.Models[j].GetComponent<SkinnedMeshRenderer>().enabled = true;

                        //        }
                        //        //else
                        //        //{
                        //        //    modelScroll.Bank.Models[j].SetActive(false);
                        //        //}
                        //    }

                        //  //  modelScroll.Bank.Models[modelScroll.Bank.modelIndex[i]].SetActive(true);

                        //}
                        //else
                        //{
                        //    //if (modelScroll.Bank.modelIndex[i] != tempModelIndex)
                        //    //{
                        //    //    modelScroll.Bank.Models[modelScroll.Bank.modelIndex[i]].SetActive(false);
                        //    //}
                        //   // modelScroll.Bank.Models[modelScroll.Bank.modelIndex[i]].SetActive(true);
                        //   // modelScroll.Bank.Models[i].SetActive(false);
                        //}

                    }
                    modelScroll.IsAction = false;
                }

            }
            else
            //Debug.Log(modelScroll);
            //   if (modelScroll == FrontFModelScroller)
            {

                if (modelScroll.gameObject.activeSelf && modelScroll.selectedIndex >= 0 && modelScroll.IsAction)
                {
                    // int tempModelIndex = 0;
                    for (int i = 0; i < modelScroll.Bank.spritesArr.Length; i++)
                    {
                        if (i == modelScroll.selectedIndex)
                        {
                            // Debug.Log("OKJ");                    
                            modelScroll.Bank.Models[i].GetComponent<SkinnedMeshRenderer>().enabled = true;
                            particalManScript.runParticalOnce(particalManScript.p1, modelScroll.Bank.Models[i].GetComponent<SkinnedMeshRenderer>());
                            skMesh = modelScroll.Bank.Models[i].GetComponent<SkinnedMeshRenderer>();
                            setSkinnedMesh(skMesh);
                            Debug.Log("LG" + modelScroll.Bank.Models[i].GetComponent<SkinnedMeshRenderer>().name);
                            //particalManScript.runParticalOnce(particalManScript.p2);
                            if (modelScroll.Bank.Models[i].GetComponent<EachObjControl>().subObj != null)
                            {
                                modelScroll.Bank.Models[i].GetComponent<EachObjControl>().subObj.GetComponent<SkinnedMeshRenderer>().enabled = true;
                            }


                            //    modelScroll.Bank.Models[i].GetComponent<EachObjControl>().HighlightObj();// = true;
                            if (modelScroll.Bank.Models[i].GetComponent<MeshCollider>() != null)
                            {
                                // Debug.Log("OK");
                                modelScroll.Bank.Models[i].GetComponent<MeshCollider>().enabled = true;

                            }
                            if (modelScroll.Bank.Models[i].GetComponent<EachObjControl>().subObj != null)
                            {
                                modelScroll.Bank.Models[i].GetComponent<EachObjControl>().subObj.GetComponent<MeshCollider>().enabled = true;
                            }
                            //  modelScroll.setBtnSprit(i);
                        }
                        else
                        {
                            // Debug.Log("Osj");
                            modelScroll.Bank.Models[i].GetComponent<SkinnedMeshRenderer>().enabled = false;
                            if (modelScroll.Bank.Models[i].GetComponent<EachObjControl>().subObj != null)
                            {
                                modelScroll.Bank.Models[i].GetComponent<EachObjControl>().subObj.GetComponent<SkinnedMeshRenderer>().enabled = false;
                            }
                            if (modelScroll.Bank.Models[i].GetComponent<MeshCollider>() != null)
                            {
                                //  Debug.Log("Os");

                                modelScroll.Bank.Models[i].GetComponent<MeshCollider>().enabled = false;

                            }
                            if (modelScroll.Bank.Models[i].GetComponent<EachObjControl>().subObj != null)
                            {
                                modelScroll.Bank.Models[i].GetComponent<EachObjControl>().subObj.GetComponent<MeshCollider>().enabled = false;
                            }
                        }

                    }
                    modelScroll.IsAction = false;
                    
                }
            }
            

        }
    }
    void SubMenuIs(int index)
    {
        modelCont.isInnerMenuObj = true;
       // Debug.Log("afashtak");
        switch (index)
        {
            case 0:
                
                ObjIs = ObjMenu.Face;
                selectedScroll = FaceModelScroller;
                ObjIsTemp = ObjIs;
                FaceModelScroller.transform.gameObject.SetActive(true);
                //BaseModelScroller.transform.gameObject.SetActive(false);
                HeelModelScroller.transform.gameObject.SetActive(false);
                FrontFModelScroller.transform.gameObject.SetActive(false);
                BackGModelScroller.transform.gameObject.SetActive(false);
                // Debug.Log("kOk");
                selectedScroll.Bank.Models[selectedScroll.selectedIndex].GetComponent<EachObjControl>().HighlightObj();
                break;

            case 1:
                ObjIs = ObjMenu.Heel;
                selectedScroll = HeelModelScroller;
                ObjIsTemp = ObjIs;
                FaceModelScroller.transform.gameObject.SetActive(false);
                //BaseModelScroller.transform.gameObject.SetActive(false);
                HeelModelScroller.transform.gameObject.SetActive(true);
                FrontFModelScroller.transform.gameObject.SetActive(false);
                BackGModelScroller.transform.gameObject.SetActive(false);
                //  Debug.Log(selectedScroll.gameObject.name);
                selectedScroll.Bank.Models[selectedScroll.selectedIndex].GetComponent<EachObjControl>().HighlightObj();
                break;
            case 2:
                ObjIs = ObjMenu.FaceFront;
                selectedScroll = FrontFModelScroller;
                ObjIsTemp = ObjIs;
                FrontFModelScroller.transform.gameObject.SetActive(true);
                FaceModelScroller.transform.gameObject.SetActive(false);
                //BaseModelScroller.transform.gameObject.SetActive(false);
                HeelModelScroller.transform.gameObject.SetActive(false);
                BackGModelScroller.transform.gameObject.SetActive(false);
                selectedScroll.Bank.Models[selectedScroll.selectedIndex].GetComponent<EachObjControl>().HighlightObj();
                break;
            case 3:
                ObjIs = ObjMenu.FaceBack;
                selectedScroll = BackGModelScroller;
                ObjIsTemp = ObjIs;
                BackGModelScroller.transform.gameObject.SetActive(true);
                //BaseModelScroller.transform.gameObject.SetActive(false);
                //  BackGModelScroller.transform.gameObject.SetActive(false);
                FrontFModelScroller.transform.gameObject.SetActive(false);
                FaceModelScroller.transform.gameObject.SetActive(false);
                //BaseModelScroller.transform.gameObject.SetActive(false);
                HeelModelScroller.transform.gameObject.SetActive(false);
                selectedScroll.Bank.Models[selectedScroll.selectedIndex].GetComponent<EachObjControl>().HighlightObj();
                break;
            //case 5:
            //    ObjIs = ObjMenu.Base;
            //    selectedScroll = BaseModelScroller;
            //    ObjIsTemp = ObjIs;
            //    FaceModelScroller.transform.gameObject.SetActive(false);
            //    //BaseModelScroller.transform.gameObject.SetActive(true);
            //    HeelModelScroller.transform.gameObject.SetActive(false);
            //    break;
            case 6:
                ObjIs = ObjMenu.none;
                BackGModelScroller.transform.gameObject.SetActive(false);
                FrontFModelScroller.transform.gameObject.SetActive(false);
                FaceModelScroller.transform.gameObject.SetActive(false);
                //BaseModelScroller.transform.gameObject.SetActive(true);
                HeelModelScroller.transform.gameObject.SetActive(false);
                break;
            default:
                break;
        }


    }
    // Update is called once per frame
    void Update()
    {
        if (tempcolor != targetColor && startColorlerp)
        {
            startColorlerp = false;
            StartCoroutine(LerpColor(tempcolor, targetColor));
            //Color i = Color.Lerp(color_1, color_2, 5f);
            //Manager.mainScene.GetComponent<MeshRenderer>().material.SetColor("_Tint", i );//Color.Lerp(color_1, color_2, 2);

        }
        ModelChange(selectedScroll);
        if (startit)
        {

            //  lerpit(float value);


        }


    }

    public void lerpit(float value)
    {
        //If HoldOn Right Mousebutton,Move from Point A to B  
        if (startLerp && !LerpedUp)
        {
            if (!LerpedUp)
            {
                //Reset LerpTime
                LerpTime = 0.0f;
                //State Lerping Up(A to B)
                LerpedUp = true;
            }
            else if (LerpTime < 1.1f)
            {
                value = Mathf.Lerp(0.5f, 1f, LerpTime);
                LerpTime += Time.deltaTime * speed;
            }

        }
        else //if(LerpedUp) //If released Right Mousebutton,Move from Point B to A
        {
            if (LerpedUp)
            {
                startLerp = false;
                //Reset LerpTime
                LerpTime = 0.0f;
                //State Lerping Down(B to A)
                LerpedUp = false;
            }
            else if (LerpTime < 1.1f)
            {


                value = Mathf.Lerp(1f, 0.5f, LerpTime);
                LerpTime += Time.deltaTime * speed;

            }
            else
            {
                if (value <= 0.5f)
                {
                    value = 0.5f;
                    startit = false;
                    startLerp = true;
                    startLerp = true;
                }
            }

        }

    }
    public void DeactiveAll()
    {
        BackGModelScroller.transform.gameObject.SetActive(false);
        FrontFModelScroller.transform.gameObject.SetActive(false);
        FaceModelScroller.transform.gameObject.SetActive(false);
        HeelModelScroller.transform.gameObject.SetActive(false);
    }

    public void ButtonClick(int index)
    {
        //  Debug.Log("PPP");

        proBar.ProgbarAnim(proBar.ProgressbarAnim.status , AminationStatus.back);
        SubMenuIs(index);
        backNextBtn.SetActive(false);
        BtnCollection.SetActive(false);
        InfoCollection.SetActive(false);
        backBtn.SetActive(true);
        cam.OnClickRight(index, cam.GetComponent<WowCamera>().selectObjCamRotAngle);
        high();
        SoundManScript.PlaySound(SoundManScript.ListSelectBotton_Clip);
        targetColor = color_2;
        startColorlerp = true;
    }

    public void BackBtn()
    {
        if (!selectedScroll.isParchaseWin)
        {
            proBar.ProgbarAnim(proBar.ProgressbarAnim.status, AminationStatus.forward);
            backNextBtn.SetActive(true);
            BtnCollection.SetActive(true);
            InfoCollection.SetActive(true);
            backBtn.SetActive(false);
            SubMenuIs(6);
            cam.OnClickRight(0, cam.GetComponent<WowCamera>().selectObjCamRotAngle);
            SoundManScript.PlaySound(SoundManScript.Botton_Clip);
            modelCont.isInnerMenuObj = false;
        }
        else
        {
            Debug.Log("al7a26");
            shop.window.SetActive(false);
            shop.unlocksureWindow2.SetActive(true);
            //shop.unlocksureWindow.SetActive(false);
            //shop.unlocksureWindow.SetActive(true);
        }
        targetColor = color_1;
        startColorlerp = true;
    }

    public void resetmodelSelectionLists()
    {

        BackGModelScroller.resetScroller();
        activeModel(BackGModelScroller, 0);
        FrontFModelScroller.resetScroller();
        activeModel(FrontFModelScroller, 0);
        FaceModelScroller.resetScroller();
        activeModel(FaceModelScroller, 0);
        HeelModelScroller.resetScroller();
        activeModel(HeelModelScroller, 0);
        //BaseModelScroller.transform.gameObject.SetActive(false);
    }
    IEnumerator LerpColor(Color startColor, Color endColor)
    {
        float progress = 0; //This float will serve as the 3rd parameter of the lerp function.
        float increment = smoothness / duration; //The amount of change to apply.
        while (progress < 1)
        {
            Color currentColor = Color.Lerp(startColor, endColor, progress);
            manager.mainScene.GetComponent<MeshRenderer>().material.SetColor("_Tint", currentColor);
            progress += increment;
            tempcolor = manager.mainScene.GetComponent<MeshRenderer>().material.GetColor("_Tint");
            yield return new WaitForSeconds(smoothness);
        }
    }
    public void activeModel(ScrollListTest scroller, int index)
    {
        if (scroller.Bank != null)
        {

            if (scroller.Bank.Models.Length > 0)
            {
                for (int i = 0; i < scroller.Bank.Models.Length; i++)
                {
                    if (index == i)
                    {
                        //Debug.Log(scroller.name + "  " + scroller.Bank.name + "   " + scroller.Bank.Models.Length + "  " + i);
                        scroller.Bank.Models[i].GetComponent<SkinnedMeshRenderer>().enabled = true;
                        scroller.savedIndex = i;
                        scroller.tempClickedIndex = i;
                        scroller.myScrollRect.verticalNormalizedPosition = 1;
                       // scroller.myScrollRect.horizontalNormalizedPosition = 0;
                        //Debug.Log("SDS");
                        if (i >= 0 && particalManScript != null)
                        {
                            // Debug.Log("sSDS");
                            particalManScript.runParticalOnce(particalManScript.p1, scroller.Bank.Models[i].GetComponent<SkinnedMeshRenderer>());
                            skMesh = scroller.Bank.Models[i].GetComponent<SkinnedMeshRenderer>();
                            setSkinnedMesh(skMesh);
                            // Debug.Log("L2G" + scroller.Bank.Models[i].GetComponent<SkinnedMeshRenderer>().name);
                            //particalManScript.runParticalOnce(particalManScript.p1, scroller.Bank.Models[i].GetComponent<SkinnedMeshRenderer>());
                            //Debug.Log("LGGLGL"+scroller.Bank.Models[i].GetComponent<SkinnedMeshRenderer>().name);
                        }

                        if (scroller.Bank.Models[i].GetComponent<EachObjControl>().subObj != null)
                        {
                            scroller.Bank.Models[i].GetComponent<EachObjControl>().subObj.GetComponent<SkinnedMeshRenderer>().enabled = true;
                        }
                        //Debug.Log(scroller.name + "  " + HeelModelScroller.name);
                        //if (scroller.name == HeelModelScroller.name)
                        //{

                        //}
                        //startit = true;
                    }
                    else
                    {
                        if (scroller.Bank.Models[i].GetComponent<EachObjControl>().subObj != null)
                        {
                            scroller.Bank.Models[i].GetComponent<EachObjControl>().subObj.GetComponent<SkinnedMeshRenderer>().enabled = false;
                        }
                        scroller.Bank.Models[i].GetComponent<SkinnedMeshRenderer>().enabled = false;
                    }

                }
            }
        }

    }
    void setSkinnedMesh(SkinnedMeshRenderer sMesh)
    {
        switch (ObjIsTemp)
        {
            case ObjMenu.Face:
                faceSkinnedMesh=sMesh;
                break;
            case ObjMenu.Base:
                baseSkinnedMesh=sMesh;
                break;
            case ObjMenu.Heel:
                heelSkinnedMesh=sMesh;
                break;
            case ObjMenu.FaceFront:
               // frontFaceSkinnedMesh=sMesh;
                faceSkinnedMesh = sMesh;
                break;
            case ObjMenu.FaceBack:
                backFaceSkinnedMesh=sMesh;
                break;
            case ObjMenu.none:
                break;
            default:
                break;
        }
    }
    public void resetSkinnedMesh()
    {
        if (backFaceSkinnedMeshTemp != null)
        {
            backFaceSkinnedMesh = backFaceSkinnedMeshTemp;
        }
        if (frontFaceSkinnedMeshTemp != null)
        {
            frontFaceSkinnedMesh = frontFaceSkinnedMeshTemp;
        }
        if (faceSkinnedMeshTemp != null)
        {
            faceSkinnedMesh = faceSkinnedMeshTemp;
        }
        if (baseSkinnedMeshTemp != null)
        {
            baseSkinnedMesh = baseSkinnedMeshTemp;
        }
        if (heelSkinnedMeshTemp != null)
        {
            heelSkinnedMesh = heelSkinnedMeshTemp;
        }
        
    }
}