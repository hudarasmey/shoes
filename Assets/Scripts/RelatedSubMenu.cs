﻿using UnityEngine;
using System.Collections;

public class RelatedSubMenu : MonoBehaviour {
    public bool isActive;
    public ListPositionCtrl mainSlider;
    public RelatedSubMenu[] RelatedMenus;
    int tempContentID;
    public bool ActONChild;
	// Use this for initialization
    void OnEnable()
    {
    //    print("script was enabled");
        tempContentID = -1;
        if (!isActive)
        {
            for (int i = 0; i < RelatedMenus.Length; i++)
            {
         //       Debug.Log(RelatedMenus[i].name);
                //if (RelatedMenus[i].gameObject.activeSelf)
                //{
                //Debug.Log("Oy");
                RelatedMenus[i].gameObject.SetActive(false);
                RelatedMenus[i].isActive = false;
                //}
            }
        }
    }
	void Start () {
        //tempContentID = mainSlider.CenteredObjNum;
        //for (int i = 0; i < RelatedMenus.Length; i++)
        //{
        //    Debug.Log(RelatedMenus[i].name);
        //    //if (RelatedMenus[i].gameObject.activeSelf)
        //    //{
        //        //Debug.Log("Oy");
        //        //RelatedMenus[i].gameObject.SetActive(false);
        //        //RelatedMenus[i].isActive = false;
        //    //}
        //}
	}
	
	// Update is called once per frame
	void Update () {
        if (mainSlider != null)
        {

            if (tempContentID != mainSlider.CenteredObjNum)
            {
                tempContentID = mainSlider.CenteredObjNum;
                Fn(tempContentID, RelatedMenus);
            }

        }
     
	}
    void Fn(int ID, RelatedSubMenu[] RelatedMenus)
    {
        if (RelatedMenus.Length > 0)
        {


            if (ID < RelatedMenus.Length)
            {
                for (int i = 0; i < RelatedMenus.Length; i++)
                {

                    if (ID == i)
                    {
                        //if (RelatedMenus[i] != null)
                        //{
                        if (!RelatedMenus[i].gameObject.activeSelf)
                        {
                            RelatedMenus[i].gameObject.SetActive(true);
                            RelatedMenus[i].isActive = true;
                            if (ActONChild)
                            {
                                //if (  RelatedMenus[i].GetComponent<RelatedSubMenu>()!= null)
                                //    {
                                RelatedSubMenu[] SubMenus = RelatedMenus[i].GetComponent<RelatedSubMenu>().RelatedMenus;
                                for (int j = 0; j < SubMenus.Length; j++)
                                {
                                    if (!SubMenus[j].gameObject.activeSelf && SubMenus[j].isActive == true)
                                    {

                                        SubMenus[j].gameObject.SetActive(true);

                                    }
                                }

                                //}



                            }
                        }
                        //}
                        //else
                        //{

                        //}
                    }
                    else
                    {
                        // if (RelatedMenus[i] != null)
                        if (RelatedMenus[i].gameObject.activeSelf)
                        {
                            if (ActONChild)
                            {
                                //if (RelatedMenus[i].GetComponent<RelatedSubMenu>() != null)
                                //{
                                RelatedSubMenu[] SubMenus = RelatedMenus[i].GetComponent<RelatedSubMenu>().RelatedMenus;
                                for (int j = 0; j < SubMenus.Length; j++)
                                {
                                    if (SubMenus[j].gameObject.activeSelf)
                                    {
                                        SubMenus[j].gameObject.SetActive(false);
                                        // SubMenus[j].isActive = false;
                                    }
                                }

                                //}
                            }
                            RelatedMenus[i].gameObject.SetActive(false);
                            RelatedMenus[i].isActive = false;
                        }

                    }
                }
            }
            else
            {
                //Debug.Log("LLLLLLLLL");
                for (int i = 0; i < RelatedMenus.Length; i++)
                {

                    if (RelatedMenus[i].gameObject.activeSelf)
                    {
                        RelatedMenus[i].gameObject.SetActive(false);
                        RelatedMenus[i].isActive = false;
                    }
                }
            }
        }
        else
        {
        }
        
    }
}
