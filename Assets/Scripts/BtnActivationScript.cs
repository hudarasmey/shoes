﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class BtnActivationScript : MonoBehaviour {
    public List<Button> ButtonList;
    public ScrollRect scroll;
    ShopMan shop;

    public SubMenuSc mainman;  

	// Use this for initialization
	void Start () {
        shop = GameObject.FindGameObjectWithTag("UnlockWin").GetComponent<ShopMan>();
       // ButtonList[0].interactable = false;
	}
    void OnEnable()
    {
        if (scroll != null)
        {
            scroll.verticalNormalizedPosition = 1;
        }
        
    }
	// Update is called once per frame
	void Update () {
	
	}
    public void CheckActivation(GameObject obj)
    {
      //  Debug.Log("0CheckActivation2" + obj.name);
        for (int i = 0; i < ButtonList.Count; i++)
        {
            if (ButtonList[i].gameObject.name == obj.name)
            {
                ButtonList[i].interactable = false;
            }
            else
            {
                ButtonList[i].interactable = true;
            }
        }
    }
    public void deActivation(int index)
    {
        //Debug.Log("T" + ButtonList[index].gameObject.name);
        ButtonList[index].interactable = true;
    }
    public void activateBtn(int index)
    {
       // Debug.Log("false" + ButtonList[index].gameObject.name);
        ButtonList[index].interactable = false;
        CheckActivation(ButtonList[index].gameObject);
        //if (index == 0)
        //{
        //    if (!mainman.materialSlider.isParchaseWin)
        //    {
        //        ButtonList[index].interactable = false;
        //        CheckActivation(ButtonList[index].gameObject);
        //    }

        //}
        //else if (index == 1)
        //{
        //    if (!mainman.PatternList.isParchaseWin)
        //    {
        //        ButtonList[index].interactable = false;
        //        CheckActivation(ButtonList[index].gameObject);
        //    }
        //}
        

    }
}
