﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PopButtonsManager : MonoBehaviour
{
    public Camera cam;
    public Color color_1, color_2, tempcolor, targetColor;
    public GameObject PopupCollection;
    public GameObject BackButton;
    public GameObject nextbutton, infoCollection, MatrialEffectSlider, MaterialTileSlider;
    public List<PopUpButton> popButton;
    bool nextAction;
    //public GameObject o;
    public bool mustFadeBack = false;
    int materialMenuIndex;
    int tempCase;
    //public List<SubMenuSc> PopUpButtonRelatedMenu;// FaceSubMenu, FaceSubMenuIn, BaseSubMenu, BaseSubMenuUp, BaseSubMenuDown, HeelSubMenu;
    bool isHide, isStartHighlight;
    Color highlightColor;
    public SubMenuSc MainMaterialMenu;
    public MaterialMenusManager materialMenusManager;
    public Sprite defaultSprite;
    GameObject SoundManagereObj;
    SoundManagerClass SoundManScript;
    public modelControl1 Manager;
    public ProgressBar proBar;
    public ShopMan shopManager;
    bool startColorlerp;
    float duration = .12f; // This will be your time in seconds.
    float smoothness = 0.01f; // This will determine the smoothness of the lerp. Smaller values are smoother. Really it's the time between updates.
    void Awake()
    {

        SoundManagereObj = GameObject.FindGameObjectWithTag("SoundManager");
        SoundManScript = (SoundManagerClass)SoundManagereObj.GetComponent("SoundManagerClass");
    }
    // Use this for initialization
    void Start()
    {
        BackBottonMaterial();
        ButtonImageUpdateAll();
    }
    IEnumerator LerpColor(Color startColor, Color endColor)
    {
        float progress = 0; //This float will serve as the 3rd parameter of the lerp function.
        float increment = smoothness / duration; //The amount of change to apply.
        while (progress < 1)
        {
            Color currentColor = Color.Lerp(startColor, endColor, progress);
            Manager.mainScene.GetComponent<MeshRenderer>().material.SetColor("_Tint", currentColor);
            progress += increment;
            tempcolor = Manager.mainScene.GetComponent<MeshRenderer>().material.GetColor("_Tint");
            yield return new WaitForSeconds(smoothness);
        }
    }
    // Update is called once per frame
    void Update()
    {
        //Debug.Log(Manager.mainScene.GetComponent<MeshRenderer>().material.name);
        if (tempcolor != targetColor && startColorlerp)
        {
            startColorlerp = false;
            StartCoroutine(LerpColor(tempcolor, targetColor));
            //Color i = Color.Lerp(color_1, color_2, 5f);
            //Manager.mainScene.GetComponent<MeshRenderer>().material.SetColor("_Tint", i );//Color.Lerp(color_1, color_2, 2);
            
        }
        
        if (Input.GetKeyDown(KeyCode.G))
        {
            resetAllPopup();
        }
        // Debug.Log("kolo" + cam.GetComponent<WowCamera>().finish + nextAction);
        if (cam.GetComponent<WowCamera>().finish == true && nextAction == true)// && cam.GetComponent<WowCamera>().IsActive == false)
        {

            nextAction = false;
            MainMaterialMenu.gameObject.SetActive(true);
            //for (int i = 0; i < PopUpButtonRelatedMenu.Count; i++)
            //{
            //    if (materialMenuIndex == i)
            //    {
            //        PopUpButtonRelatedMenu[i].gameObject.SetActive(true);

            //    }
            //    else
            //    {
            //        PopUpButtonRelatedMenu[i].gameObject.SetActive(false);
            //    }
            //}
            MatrialEffectSlider.gameObject.SetActive(true);
            MaterialTileSlider.gameObject.SetActive(true);

            BackButton.SetActive(true);


        }

        //for (int i = 0; i < popButton.Count; i++)
        //{

        //    // Debug.Log("PPP");
        //    RaycastHit hit;
        //    if (Physics.Raycast(cam.transform.position, popButton[i].gameObject.transform.position - cam.transform.position, out hit))
        //    {
        //        //Debug.Log(hit.collider.gameObject.name);
        //        if (hit.collider.gameObject.layer == LayerMask.NameToLayer("CanHide"))
        //        {
        //            mustFadeBack = true;

        //            isHide = true;
        //            FadeDown(popButton[i]);


        //        }
        //        else
        //        {

        //            if (mustFadeBack)
        //            {
        //                isHide = false;
        //                mustFadeBack = false;
        //                FadeUp(popButton[i]);
        //                // ButtonImageUpdate(i);

        //            }

        //        }
        //    }
        //}

    }

    void FadeUp(PopUpButton f)
    {
        //   SetTexture(Texture2D imagemat)
        f.isVisiable = true;


    }

    void FadeDown(PopUpButton f)
    {
        f.isVisiable = false;
    }

    public Sprite SetTexture(Texture2D imagemat)
    {
        Sprite imageContent;
        imageContent = Sprite.Create(imagemat, new Rect(0, 0, imagemat.width, imagemat.height), new Vector2(0.5f, 0.5f), 1);
        // GetComponent<Image>().sprite = imageContent;
        return imageContent;

    }
    public void MaterialMenuController(int MaterialMenuIndex)
    {
        startColorlerp = true;
         targetColor = color_2;
        Manager.isInnerMenuMat = true;
        proBar.ProgbarAnim(proBar.ProgressbarAnim.status, AminationStatus.back);
        SoundManScript.PlaySound(SoundManScript.ListSelectBotton_Clip);
        cam.GetComponent<WowCamera>().OnClickRight(MaterialMenuIndex + 2, cam.GetComponent<WowCamera>().cameraRotationAngle);
        MainMaterialMenu.SetMaterialManagerIndex(MaterialMenuIndex);
        HighlightObj(materialMenusManager.targetMat[MaterialMenuIndex]);
        //HighlightObj(PopUpButtonRelatedMenu[MaterialMenuIndex].TargetMaterial);
        PopupCollection.SetActive(false);
        nextbutton.SetActive(false);
        //MatrialEffectSlider.gameObject.SetActive(false);
        //MaterialTileSlider.gameObject.SetActive(false);
        infoCollection.SetActive(false);
        nextAction = true;
       
         ////////////// materialMenuIndex = MaterialMenuIndex;
         materialMenuIndex = 1;
        //if (cam.GetComponent<WowCamera>().finish == true)// && cam.GetComponent<WowCamera>().IsActive == false)
        //{
        //    for (int i = 0; i < PopUpButtonRelatedMenu.Count; i++)
        //    {
        //        if (MaterialMenuIndex == i)
        //        {
        //            PopUpButtonRelatedMenu[i].gameObject.SetActive(true);

        //        }
        //        else
        //        {
        //            PopUpButtonRelatedMenu[i].gameObject.SetActive(false);
        //        }
        //    }


        //    BackButton.SetActive(true);


        //}
        //else
        //{

        //    //MaterialMenuController(MaterialMenuIndex);

        //}

    }
    public void ClearAllPopUp()
    {
        for (int i = 0; i < popButton.Count; i++)
        {
            popButton[i].isVisiable = false;
            // popButton[i].gameObject.SetActive(false);

            popButton[i].VisabiltyAction(false);
        }
        PopupCollection.SetActive(true);
    }
    void ButtonImageUpdate(int i)// update not stop
    {
        //Debug.Log(PopUpButtonRelatedMenu[i].selectedTexteure.name);
        //if (popButton[i].tempPool)//popButton[i].buttonImage.sprite.name)
        // {
        Debug.Log(popButton[i].name + "ttt");
        // popButton[i]=PopUpButtonRelatedMenu[i]
        popButton[i].SetTexture(materialMenusManager.Sprites[i], materialMenusManager.color[i]);
        //  popButton[i].SetTexture(PopUpButtonRelatedMenu[i].selectedSprite, PopUpButtonRelatedMenu[i].tempColor);
        //  }
    }
    public void ButtonImageUpdateAll()
    {

        for (int i = 0; i < popButton.Count; i++)
        {
            //ButtonImageUpdate(i);
            popButton[i].isRefreshButtonMatreial = true;
            popButton[i].sprite = materialMenusManager.Sprites[i];// PopUpButtonRelatedMenu[i].selectedSprite;
            popButton[i]._color = materialMenusManager.color[i];// PopUpButtonRelatedMenu[i].tempColor;

        }
    }
    public void BackBottonMaterial()
    {
        //for (int i = 0; i < PopUpButtonRelatedMenu.Count; i++)
        //{

        //    PopUpButtonRelatedMenu[i].gameObject.SetActive(false);

        //}
        
        Debug.Log(shopManager.window.activeSelf);
        switch (MainMaterialMenu.tempMatMenu)
        {

            case MaterialMenu.color:
                break;
            case MaterialMenu.texture:
               // if (!MainMaterialMenu.materialSlider.GetComponent<ScrollListTest>().isParchaseWin && !MainMaterialMenu.PatternList.GetComponent<ScrollListTest>().isParchaseWin)
               if (shopManager.window.activeSelf ==false)

                    {
                        //  Debug.Log("al7a2");
                        Manager.isInnerMenuMat = false;
                    proBar.ProgbarAnim(proBar.ProgressbarAnim.status, AminationStatus.forward);
                    SoundManScript.PlaySound(SoundManScript.Botton_Clip);
                    MainMaterialMenu.BtnMatTypeActivation.deActivation((int)MainMaterialMenu.matType);
                    MatrialEffectSlider.gameObject.SetActive(false);
                    MaterialTileSlider.gameObject.SetActive(false);
                    MainMaterialMenu.materialSlider.resetScroller1();
                    MainMaterialMenu.ColorManager.colorSlider.resetScroller1();
                    MainMaterialMenu.gameObject.SetActive(false);
                    PopupCollection.SetActive(true);
                    BackButton.SetActive(false);
                    nextbutton.SetActive(true);
                    infoCollection.SetActive(true);
                    cam.GetComponent<WowCamera>().OnClickRight(0, cam.GetComponent<WowCamera>().cameraRotationAngle);
                    // MainMaterialMenu.materialIndex = -1;

                    MainMaterialMenu.matType = MaterialType.Mat;
                }
                else
                {
                    Debug.Log("al7a2");
                    shopManager.window.SetActive(false);
                    shopManager.unlocksureWindow2.SetActive(true);
                    //shopManager.unlocksureWindow.SetActive(false);
                    //shopManager.unlocksureWindow.SetActive(true);
                    //   ButtonImageUpdateAll();
                    //  MainMaterialMenu.materialSlider.GetComponent<ScrollListTest>().isParchaseWin = false;
                    // MainMaterialMenu.materialSlider.GetComponent<ScrollListTest>().cells[MainMaterialMenu.materialSlider.GetComponent<ScrollListTest>().tempClickedIndex].lockManager.GetComponent<ShopMan>().unlocksureWindow.SetActive(true);//unlocksureWindow.SetActive(true);
                    //   MainMaterialMenu.materialSlider.GetComponent<ScrollListTest>().cells[ MainMaterialMenu.materialSlider.GetComponent<ScrollListTest>().tempClickedIndex].clickitBack();
                    //clickitBack()
                }
                break;
            case MaterialMenu.pattern:
               // if (!MainMaterialMenu.materialSlider.GetComponent<ScrollListTest>().isParchaseWin && !MainMaterialMenu.PatternList.GetComponent<ScrollListTest>().isParchaseWin)
                    if (shopManager.window.activeSelf == false)
                {
               //     Debug.Log("al7a2");
                    Manager.isInnerMenuMat = false;
                    proBar.ProgbarAnim(proBar.ProgressbarAnim.status, AminationStatus.forward);
                    SoundManScript.PlaySound(SoundManScript.Botton_Clip);
                    MainMaterialMenu.BtnMatTypeActivation.deActivation((int)MainMaterialMenu.matType);
                    MatrialEffectSlider.gameObject.SetActive(false);
                    MaterialTileSlider.gameObject.SetActive(false);
                    MainMaterialMenu.PatternList.resetScroller1();
                    MainMaterialMenu.ColorManager.colorSlider.resetScroller1();
                    MainMaterialMenu.gameObject.SetActive(false);
                    PopupCollection.SetActive(true);
                    BackButton.SetActive(false);
                    nextbutton.SetActive(true);
                    infoCollection.SetActive(true);
                    cam.GetComponent<WowCamera>().OnClickRight(0, cam.GetComponent<WowCamera>().cameraRotationAngle);
                    // MainMaterialMenu.materialIndex = -1;

                    MainMaterialMenu.matType = MaterialType.Mat;
                }
                else
                {
                    Debug.Log("al7a25");
                    shopManager.window.SetActive(false);
                    shopManager.unlocksureWindow2.SetActive(true);
                    //shopManager.unlocksureWindow.SetActive(false);
                    //shopManager.unlocksureWindow.SetActive(true);
                    //   ButtonImageUpdateAll();
                    //  MainMaterialMenu.materialSlider.GetComponent<ScrollListTest>().isParchaseWin = false;
                    // MainMaterialMenu.materialSlider.GetComponent<ScrollListTest>().cells[MainMaterialMenu.materialSlider.GetComponent<ScrollListTest>().tempClickedIndex].lockManager.GetComponent<ShopMan>().unlocksureWindow.SetActive(true);//unlocksureWindow.SetActive(true);
                    //   MainMaterialMenu.materialSlider.GetComponent<ScrollListTest>().cells[ MainMaterialMenu.materialSlider.GetComponent<ScrollListTest>().tempClickedIndex].clickitBack();
                    //clickitBack()
                }
                break;
            case MaterialMenu.none:
                break;
            default:
                break;
        }


        targetColor = color_1;
        startColorlerp = true;
    }
    public void BackWithoutScave()
    {
        switch (MainMaterialMenu.tempMatMenu)
        {
            case MaterialMenu.color:
                break;
            case MaterialMenu.texture:
                MainMaterialMenu.materialSlider.GetComponent<ScrollListTest>().isParchaseWin = false;
                MainMaterialMenu.materialSlider.GetComponent<ScrollListTest>().cells[MainMaterialMenu.materialSlider.GetComponent<ScrollListTest>().tempClickedIndex].clickitBack();
                break;
            case MaterialMenu.pattern:
                MainMaterialMenu.PatternList.GetComponent<ScrollListTest>().isParchaseWin = false;
                MainMaterialMenu.PatternList.GetComponent<ScrollListTest>().cells[MainMaterialMenu.materialSlider.GetComponent<ScrollListTest>().tempClickedIndex].clickitBack();
                break;
            case MaterialMenu.none:
                break;
            default:
                break;
        }
        //MainMaterialMenu.materialSlider.GetComponent<ScrollListTest>().isParchaseWin = false;
        //MainMaterialMenu.materialSlider.GetComponent<ScrollListTest>().cells[MainMaterialMenu.materialSlider.GetComponent<ScrollListTest>().tempClickedIndex].clickitBack();
      //  Manager.isInnerMenuMat = false;
        //proBar.ProgbarAnim(proBar.ProgressbarAnim.status, AminationStatus.forward);
        //SoundManScript.PlaySound(SoundManScript.Botton_Clip);
        //MainMaterialMenu.BtnMatTypeActivation.deActivation((int)MainMaterialMenu.matType);
        //MatrialEffectSlider.gameObject.SetActive(false);
        //MaterialTileSlider.gameObject.SetActive(false);
        //MainMaterialMenu.materialSlider.resetScroller1();
        //MainMaterialMenu.ColorManager.colorSlider.resetScroller1();
        //MainMaterialMenu.gameObject.SetActive(false);
        //PopupCollection.SetActive(true);
        //BackButton.SetActive(false);
        //nextbutton.SetActive(true);
        //infoCollection.SetActive(true);
        //cam.GetComponent<WowCamera>().OnClickRight(0, cam.GetComponent<WowCamera>().cameraRotationAngle);
        
        //BackBottonMaterial();
      //  ButtonImageUpdateAll();
    }
    public void kkkk(int x)
    {
      //  Manager.isInnerMenuMat = false;
 //       proBar.ProgbarAnim(proBar.ProgressbarAnim.status, AminationStatus.forward);
       // SoundManScript.PlaySound(SoundManScript.Botton_Clip);
       // MainMaterialMenu.BtnMatTypeActivation.deActivation((int)MainMaterialMenu.matType);
        //ButtonImageUpdateAll();
       // MainMaterialMenu.targetMaterialIndex = x;
        MainMaterialMenu.materialSlider.IsAction = false;
        //MainMaterialMenu.targetMaterialIndex = x;
       // MainMaterialMenu.targetMaterialIndex = x;
        MainMaterialMenu.materialSlider.resetScroller1();
        

        MainMaterialMenu.ColorManager.colorSlider.resetScroller1();
        
        MainMaterialMenu.EnableFun(x);
       //MaterialMenuController(x);
        HighlightObj(materialMenusManager.targetMat[x]);
        cam.GetComponent<WowCamera>().OnClickRight(x+1, cam.GetComponent<WowCamera>().cameraRotationAngle);

    }
    //void OnDrawGizmos()
    //{
    //    for (int i = 0; i < player.Count; i++)
    //    {
    //        Gizmos.color = Color.red;
    //        Gizmos.DrawRay(cam.transform.position, player[i].gameObject.transform.position - cam.transform.position);
    //    }
    //}
    public void HighlightObj(Material mat)
    {
        //mats = gameObject.GetComponent<SkinnedMeshRenderer>().material;
        //  Debug.Log("OPOKI");
        StartCoroutine(MyCoroutine(mat));


    }
    IEnumerator MyCoroutine(Material Mat)
    {
        isStartHighlight = true;
        Color StartColor = Mat.color;
        Color TempStart = StartColor;
        highlightColor = new Color(1f, 0.65f, 1, 1);
        //highlightColor = new Color(
        //    StartColor.r * 3f,
        //    StartColor.g * 3f,
        //    StartColor.b * 3f
        //);
        //float timeToStart = Time.time;
        //while (TempStart != highlightColor) // This is your target size of object.
        //{
        //    Debug.Log("PPP " + normalColor + "  " + highlightColor);
        //    TempStart = Color.Lerp(normalColor, highlightColor, (Time.time - timeToStart) * 2f);//Here speed is the 1 or any number which decides the how fast it reach to one to other end.
        //    material.color = TempStart;
        //    yield return null;
        //}
        yield return new WaitForSeconds(.5f);
        Mat.color = highlightColor;
        yield return new WaitForSeconds(.75f);

        Mat.color = StartColor;
        isStartHighlight = false;

        // print("Reached the target.");

    }
    public void resetAllPopup()
    {
        for (int i = 0; i < popButton.Count; i++)
        {
            popButton[i].buttonImage.sprite = defaultSprite;
            popButton[i].buttonImage.color = Color.white;
        }

    }
}
