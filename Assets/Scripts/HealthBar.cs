﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Canvas c; // The healthbar prefab (that is assumed to have a child object used for filling)
    public float verticalOffset; // The amount of pixels the health bar should be shifted up/down
    public float HorizintalOffset; 
    public Camera myCamera;
    public Color highHealth = new Color(10.0f / 255.0f, 255.0f / 255.0f, 10.0f / 255.0f); // green
    public Color mediumHealth = new Color(255.0f / 255.0f, 249.0f / 255.0f, 11.0f / 255.0f); // yellow
    public Color lowHealth = new Color(255.0f / 255.0f, 67.0f / 255.0f, 11.0f / 255.0f); // red
    
    public GameObject creep; // The creep that this health bar belongs to
    public Image background;
    private Image foreground;

    private void Start()
    {
        // Store a reference to the associated creep
        //creep = GetComponent<Creep>();

        // Create a new game object with the image prefab
    //    Canvas c = Instantiate<Canvas>(healthBarPrefab);

        // Change the canvas' parent to the creep prefab (so it's automatically destroyed when the creep is)
     //   c.transform.SetParent(transform);

        // Store a reference to the background to easily move the health bar
       // background = HelperScripts.singleton.recursFindTransByName(c.transform, "Background").GetComponent<Image>();

        // Store a reference to the foreground to easily modify the color and fill
     //   foreground = HelperScripts.singleton.recursFindTransByName(c.transform, "Foreground").GetComponent<Image>();
    }
    void OnBecameInvisible()
    {
        Debug.Log("PPPLOOO");
        enabled = false;
    }
    void OnBecameVisible()
    {
        Debug.Log("PPPO");
        enabled = true;
    }
    //private void OnGUI()
    //{
    //    // Create a vector representing the top of the creep's head
    //    Vector3 topOfCreep = Camera.main.WorldToScreenPoint(new Vector3(creep.transform.position.x, creep.transform.position.y , creep.transform.position.z));
    //    Vector3 worldToScreen = myCamera.WorldToScreenPoint(creep.transform.position);
    //    topOfCreep.y += verticalOffset;
    //    topOfCreep.x += HorizintalOffset;
    //    background.transform.position = topOfCreep;
    //    //// Move the bar to the top of the creep's head
    //    //background.transform.position = topOfCreep;

    //    //// TEMP:
    //    //foreground.transform.position = topOfCreep;

    //    // Resize the foreground based on health remaining
    //  //  foreground.fillAmount = creep.healthPercent;

    //    // Color the foreground based on health remaining
    //    //if (creep.healthPercent > 0.50f)
    //    //    foreground.color = Color.Lerp(mediumHealth, highHealth, (creep.healthPercent - 0.50f) / 0.50f); // want 1.00 to be 100%, want .50 to be 0%
    //    //else
    //    //    foreground.color = Color.Lerp(lowHealth, mediumHealth, creep.healthPercent / 0.50f); // want .50 to be 100%, want 0.00 to be 0%
    //}
}