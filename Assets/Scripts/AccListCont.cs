﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
public class AccListCont : MonoBehaviour {
    public List<GameObject> AccerroiesModels;
    public List<ScrollListTest> AccSliders;
    public ObjMenu ObjIs;
    MaterialParam MatParm;
    public Shose shose;
    public GameObject shosePart;
    public MaterialType matType;
    public GameObject colorPicker;
    public ScrollListTest materialSlider;
    public ScrollListTest PatternList;
    // Use this for initialization
    public MaterialMenu tempMatMenu;
    public Button MatBtn, colorBtn, PatternBtn;
    
    
    public bool matChange;
    Color tempColor;
    public MaterialSelected MatSelect, tempMatSelect;
    bool itCleared;
    bool isStartCase;
    bool isMaterialStart;
    void Awake()
    {
        
        MatSelect = MaterialSelected.texture;
        tempMatSelect = MaterialSelected.none;
        tempColor = Color.white;
        MatParm = new MaterialParam();
        tempMatMenu = MaterialMenu.texture;
        matType = MaterialType.Mat;
        switch (ObjIs)
        {
            case ObjMenu.Face:
                shosePart = shose.faceObj;
                break;
            case ObjMenu.Base:
                shosePart = shose.baseObj;
                break;
            case ObjMenu.Heel:
                shosePart = shose.heelObj;
                break;
            case ObjMenu.none:
                break;
            default:
                break;
        }
        colorPicker.GetComponent<ColorPicker>().getColorMaterial(shosePart,0);
        colorPicker.GetComponent<ColorPicker>().setColorMaterial();



        matChange = true;
        materialSlider.selectedIndex = 0;

        materialSlider.IsAction = true;
       
        tempColor = Color.black;
        ColorControler();  
    }
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (materialSlider.gameObject.activeSelf == true && !isMaterialStart)
        {

            for (int i = 0; i < materialSlider.cells.Count; i++)
            {
                materialSlider.cells[i].gameObject.GetComponent<Image>().color = tempColor;
            }
            isMaterialStart = true;
        }

        switch (tempMatMenu)
        {
            case MaterialMenu.color:
                Debug.Log("s");

                break;
            case MaterialMenu.texture:
                if (materialSlider.selectedIndex >= 0 && MatSelect == MaterialSelected.texture)
                {
                   // tileValue = materialSlider.Bank.startTileValuePerTexture[materialSlider.selectedIndex] + materialSlider.tileValueSlider.value * 10;

                }
                PatternList.tileValueSlider.value = materialSlider.tileValueSlider.value;
                break;
            case MaterialMenu.pattern:
                if (PatternList.selectedIndex >= 0 && MatSelect == MaterialSelected.pattern)
                {
                   // tileValue = PatternList.Bank.startTileValuePerTexture[PatternList.selectedIndex] + PatternList.tileValueSlider.value * 10;

                }
                materialSlider.tileValueSlider.value = PatternList.tileValueSlider.value;
                break;
            case MaterialMenu.none:
                break;
            default:
                break;
        }
        if (MatSelect != tempMatSelect)
        {

            switch (MatSelect)
            {
                case MaterialSelected.texture:
                   
                    PatternList.clearMarker(-1);
                    tempMatSelect = MatSelect;
                    break;
                case MaterialSelected.pattern:
                    materialSlider.clearMarker(-1);
                    tempMatSelect = MatSelect;
                    break;
                case MaterialSelected.none:
                    break;
                default:
                    break;
            }

        }


        ColorControler();
        //TileTexure(tileValue);
       // TexureChangeFn(tempMatMenu);
        //MaterialTypeFn();


    }

    void ColorControler()
    {


        if (colorPicker.GetComponent<ColorPicker>().CurrentColor != tempColor)
        {

            for (int i = 0; i < materialSlider.cells.Count; i++)
            {
                materialSlider.cells[i].gameObject.GetComponent<Image>().color = colorPicker.GetComponent<ColorPicker>().CurrentColor;
            }


            if (MatSelect == MaterialSelected.texture)
            {

                shosePart.GetComponent<Renderer>().material.color = colorPicker.GetComponent<ColorPicker>().CurrentColor;
                tempColor = colorPicker.GetComponent<ColorPicker>().CurrentColor;
            }

        }


        if (MatSelect == MaterialSelected.pattern)
        {
            shosePart.GetComponent<Renderer>().material.color = Color.white;
            tempColor = Color.white;

        }

       
    }


    public void SubMenuIs(MaterialMenu subMenuType)
    {
        
        if (subMenuType != tempMatMenu)
        {
            switch (subMenuType)
            {
                case MaterialMenu.color:
                    //  ColorControler();
                    // tempMatMenu = MaterialMenu.color;

                    materialSlider.gameObject.transform.parent.transform.gameObject.SetActive(false);
                    colorPicker.gameObject.SetActive(true);
                    PatternList.gameObject.transform.parent.gameObject.SetActive(false);


                    break;
                case MaterialMenu.texture:
                    //  tempMatMenu = MaterialMenu.texture;
                    materialSlider.gameObject.transform.parent.transform.gameObject.SetActive(true);
                    colorPicker.gameObject.SetActive(false);
                    PatternList.gameObject.transform.parent.gameObject.SetActive(false);

                    break;
                case MaterialMenu.pattern:
                    //   tempMatMenu = MaterialMenu.pattern;
                    materialSlider.gameObject.transform.parent.transform.gameObject.SetActive(false);
                    colorPicker.gameObject.SetActive(false);
                    PatternList.gameObject.transform.parent.gameObject.SetActive(true);

                    break;
                case MaterialMenu.none:

                    materialSlider.gameObject.transform.parent.transform.gameObject.SetActive(false);
                    colorPicker.gameObject.SetActive(false);
                    PatternList.gameObject.transform.parent.gameObject.SetActive(false);

                    break;
                default:
                    break;
            }
        }
        tempMatMenu = subMenuType;
    }

    public void ButtonClick(int index)
    {
        SubMenuIs((MaterialMenu)index);
    }

    void TexureChangeFn(MaterialMenu _tempMatMenu)
    {
        switch (_tempMatMenu)
        {
            case MaterialMenu.color:
                if (MatSelect == MaterialSelected.texture)
                {
                    MatParm.SetColor(colorPicker.GetComponent<ColorPicker>().CurrentColor);
                    shosePart.GetComponent<Renderer>().material.color = colorPicker.GetComponent<ColorPicker>().CurrentColor;
                }

                break;
            case MaterialMenu.texture:
                if (materialSlider.gameObject.activeSelf && materialSlider.selectedIndex >= 0 && materialSlider.IsAction)
                {
                   materialSlider.IsAction = false;
                    MatSelect = MaterialSelected.texture;


                }
                break;
            case MaterialMenu.pattern:
                if (PatternList.gameObject.activeSelf && PatternList.selectedIndex >= 0 && PatternList.IsAction)
                {
                    //Texture Xs = PatternList.Bank.texture[PatternList.selectedIndex];
                    //Xs.wrapMode = TextureWrapMode.Repeat;

                    // TileTexure(tileValue);
                    //    TemptileValue = tileValue;
                    Texture Xs;
                    
                      Xs = PatternList.Bank.texture[PatternList.selectedIndex];//.bank.texture;
                   
                    PatternList.IsAction = false;
                    MatSelect = MaterialSelected.pattern;
                }

                break;
            case MaterialMenu.none:
                break;
            default:
                break;
        }

    }

    void MaterialTypeFn()
    {

        if (matChange)
        {
            //SkinnedMeshRenderer renderer = shosePart.GetComponent<SkinnedMeshRenderer>();

            
                //renderer.material = shose.faceOpaqueMaterial;
                //Material material = renderer.material;
             //+++++++++++++++++++++++++++++++++++>>>>>>>>>
               
            switch (MatSelect)
            {
                case MaterialSelected.texture:

                    materialSlider.IsAction = true;
                    TexureChangeFn(MaterialMenu.texture);
                    break;
                case MaterialSelected.pattern:
                    PatternList.IsAction = true;
                    TexureChangeFn(MaterialMenu.pattern);
                    break;
                case MaterialSelected.none:
                    break;
                default:
                    break;
            }

            MatParm.SetMatParams(shosePart.GetComponent<SkinnedMeshRenderer>().material);

            matChange = false;
        }
    }
}
