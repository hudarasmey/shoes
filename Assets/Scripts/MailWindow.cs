﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MailWindow : MonoBehaviour {

	public InputField Email;
	public InputField Subject;
	public InputField Body;
    public GameObject settingMenu;
    public GameManager Manager;
    public void Awake()
    {
        if (GameObject.FindObjectOfType<GameManager>() != null)
        {
            Manager = GameObject.FindObjectOfType<GameManager>();
        }
        
    }
    private void OnEnable()
    {

        Subject.text = "";
        Body.text = "";
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            CloseMailWindow();

        }
    }
        public void SendMail () {

        if (Manager != null)
        {
            //Manager.
                Debug.Log(PlayerPrefs.GetString("playerID"));
            Mail.SendMail("mudplay.studio@gmail.com", Subject.text + " [" + PlayerPrefs.GetString("playerID") + "]", Body.text);
        }
        else
        {
            Mail.SendMail("mudplay.studio@gmail.com", Subject.text, Body.text);
        }
		

    }
    public void CloseMailWindow()
    {
        gameObject.SetActive(false);
    }
    public void OpenMailWindow()
    {
    settingMenu.SetActive(false); 
    gameObject.SetActive(true);
    }
}
