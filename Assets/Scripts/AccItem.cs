﻿using UnityEngine;
using System.Collections;

public class AccItem : MonoBehaviour {
    public int index;
    public Vector3 itemPosition;
    public GameObject innerObj;
    public Color color;
    public Material mat;
    public float rotationRatio, scaleRatio;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
