﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum ObjMenu { Face, Base, Heel, Widge, FaceFront, FaceBack, Label, Cone, none };
public enum MaterialMenu { color, texture, pattern, none };
public class modelControl1 : MonoBehaviour
{
    public int MaterialMenuIndex;
    public int mainIndex;
    public int heelthickIndex = 2, tempHeelthickIndex = 0;
    public int mainIndexTemp = -1;
    int tempMainIndex;
    public ObjectSelectionSubMenu objSelectMenu;
    public GameObject ObjectSelectGroup;
    public Button FaceBtn, HeelBtn, BaseBtn;
    //public SubMenuSc FaceSubMenu, FaceSubMenuIn, BaseSubMenu, BaseSubMenuUp, BaseSubMenuDown, HeelSubMenu;
    public Shose shose;
    public  Color c1 ,c2;
    public Slider baseHeightFront, heelThickness, heelThicknessDump, heelThickness_1, heelThicknessDump_1, facethick;
    public float  heelThicknessValue, facethickValue;
    public int faceIndex, baseIndex, heelIndex, frontFaceIndex, backFaceIndex;
    [HideInInspector]
    public float baseHeightFrontTemp = -1, heelThicknessTemp = -1, heelThicknessDumpTemp = -1, facethickTemp = -1;
    public WowCamera Camera;
    public Camera GUICamera;
    int tempCase = -1;
    //public MaterialMenu matMenu, tempMatMenu;
    public ObjMenu objType;
    public zzVerticalRuler Ruler;
    public GameObject RulerText;
    public float front;
    public GameObject StartMenuColection, SelectStyleWin, MorphMenuCollection, MaterialMenuCollection, AccMenu, nextBackCollBtn, MaterialPopUpMenu, PopUpMenuControler, BankContainer, LoadMenu, settingWindow, shopWindow,unlockWindow, unlockWindow2, profileImage, profileImageShade , progressBar;
    public Decal DecalScript;
    public PopButtonsManager popButtonsManager;
    public AccMenu1 accMenu;
    //public ObjectSelectionSubMenu
    //public SubMenuSc heelMaterialManager, baseMaterialManager, baseMaterialManagerMiddel;
    public int maxIndex = 4;
    public GameObject nextBtn, backBtn, SaveBtn;
    public GameObject finalCamera;
    public bool resetcollider, changemorphe;
    public GameObject frontModelSel, backModelSel, faceModelSel, frontAndback3dModel, face3dModel, backFaceMat,heelMat, TwoFaceMatSelImg, OneFaceMatSelImg,wedgeImg,baseImg;//, frontAndbackMatSel, faceMatSel;
    public int ChooseTypeIndex;
    public Image fadIt;
    public bool isOutOfRange, isInOfRange, startGame;
    public InfoBank LoadBank;
    public GameObject contineBtnGroup, withoutContineBtnGroup, LoadBtn;
    public GameObject[] listofInfo;
    public bool isMorphPhase;
    GameObject SoundManagereObj;
    SoundManagerClass SoundManScript;
    public GameObject particalManagereObj;
    public ParticalManager particalManScript;
    public ProgressBar porgressBar;
    public GameObject closeWin;
    public bool isInnerMenuMat, isInnerMenuObj, isInnerMenuAcc, resetall; 
    public Vector3 startScale=new Vector3(0.9f,1,1), EndScale;
    public ScrollListTest[] listOfScrolls;
    public ShopMan shop;
    public GameObject mainScene, DarkScene,morphBlock, morphBlockWin;
    public Decal _decal;
    public bool escapeClicked;
    //public ObjectSelectionSubMenu heelObjControl;
    void Awake()
    {
        morphBlockWin.SetActive(false);
        SoundManagereObj = GameObject.FindGameObjectWithTag("SoundManager");
        SoundManScript = (SoundManagerClass)SoundManagereObj.GetComponent("SoundManagerClass");
        particalManagereObj = GameObject.FindGameObjectWithTag("ParticalManager");
        particalManScript = (ParticalManager)particalManagereObj.GetComponent("ParticalManager");
        LoadBank = GameObject.FindGameObjectWithTag("SaveInfoBank").GetComponent<InfoBank>();
        for (int i = 0; i < listOfScrolls.Length; i++)
        {
            listOfScrolls[i].Bank= BankValueSet(listOfScrolls[i].targetBank);
        }
    }
    InfoBank BankValueSet(TagCollection targetBank)
    {
        InfoBank Bank;
        switch (targetBank)
        {

            case TagCollection.none:
                Bank = null;
                break;
            case TagCollection.SaveInfoBank:
                Bank = GameObject.FindGameObjectWithTag("SaveInfoBank").GetComponent<InfoBank>();
                break;
            case TagCollection.MatColorTexInfoBank:
                Bank = GameObject.FindGameObjectWithTag("MatColorTexInfoBank").GetComponent<InfoBank>();
                break;
            case TagCollection.MatBWTexInfoBank:
                Bank = GameObject.FindGameObjectWithTag("MatBWTexInfoBank").GetComponent<InfoBank>();
                break;
            case TagCollection.ColorInfoBank:
                Bank = GameObject.FindGameObjectWithTag("ColorInfoBank").GetComponent<InfoBank>();
                break;
            case TagCollection.AccsInfoBank:
                Bank = GameObject.FindGameObjectWithTag("AccsInfoBank").GetComponent<InfoBank>();
                break;
            case TagCollection.coolBank:
                Bank = GameObject.FindGameObjectWithTag("coolBank").GetComponent<InfoBank>();
                break;
            case TagCollection.flowerBank:
                Bank = GameObject.FindGameObjectWithTag("flowerBank").GetComponent<InfoBank>();
                break;
            case TagCollection.foodBank:
                Bank = GameObject.FindGameObjectWithTag("foodBank").GetComponent<InfoBank>();
                break;
            case TagCollection.leatherBank:
                Bank = GameObject.FindGameObjectWithTag("leatherBank").GetComponent<InfoBank>();
                break;
            case TagCollection.FaceModelBank:
                Bank = GameObject.FindGameObjectWithTag("FaceModelBank").GetComponent<InfoBank>();
                break;
            case TagCollection.FrontFaceModelBank:
                Bank = GameObject.FindGameObjectWithTag("FrontFaceModelBank").GetComponent<InfoBank>();
                break;
            case TagCollection.BackFaceModelBank:
                Bank = GameObject.FindGameObjectWithTag("BackFaceModelBank").GetComponent<InfoBank>();
                break;
            case TagCollection.HeelModelBank:
                Bank = GameObject.FindGameObjectWithTag("HeelModelBank").GetComponent<InfoBank>();
                break;
            case TagCollection.PatternSelectBank:
                Bank = GameObject.FindGameObjectWithTag("PatternSelectBank").GetComponent<InfoBank>();
                break;
            default:
                Bank = null;
                break;
        }
        return Bank;
    }
    void OnEnable()
    {

    }
    void Start()
    {

        //FaceSubMenu.gameObject.SetActive(false);
        //HeelSubMenu.gameObject.SetActive(false);
        //BaseSubMenu.gameObject.SetActive(false);
        //FaceSubMenuIn.gameObject.SetActive(false);
        //BaseSubMenuUp.gameObject.SetActive(false);
        //BaseSubMenuDown.gameObject.SetActive(false);
        //PopUpMenuControler.gameObject.SetActive(false);
        MaterialMenuCollection.gameObject.SetActive(false);
        StartMenuColection.gameObject.SetActive(false);
        MorphMenuCollection.gameObject.SetActive(false);
        MaterialMenuCollection.gameObject.SetActive(false);
        AccMenu.gameObject.SetActive(false);
        nextBackCollBtn.SetActive(false);
        progressBar.SetActive(false);
        Ruler.transform.parent.gameObject.SetActive(false);

    }
    // Update is called once per frame
    void Update()
    {

        
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            escapeClicked = true;
        }
        if (escapeClicked && (mainIndex == 1 || mainIndex == 0 || mainIndex == 8 || mainIndex == 9))
        {
            escapeClicked = false;
            if (mainIndex == 1)
            {
               
                    CloseExitWin(true);
                
                
            }
            else
            {
                mainIndex = 1;
            }
            
        }
        else if (escapeClicked && mainIndex != 1)
        {
            
            escapeClicked = false;
            if (isInnerMenuMat)
            {
                if (unlockWindow.activeSelf == true)
                {
                    //unlockWindow.SetActive(false);
                    shop.Close(unlockWindow);
                }
                else if(unlockWindow2.activeSelf == true)
                {
                    shop.Close2(unlockWindow);
                    popButtonsManager.BackBottonMaterial();
                }
                else
                {
                    popButtonsManager.BackBottonMaterial();
                }

            }
            else if (isInnerMenuObj)
            {
                if (unlockWindow.activeSelf == true)
                {
                    //unlockWindow.SetActive(false);
                    shop.Close(unlockWindow);
                }
                else if (unlockWindow2.activeSelf == true)
                {
                    shop.Close2(unlockWindow);
                }
                
                else
                {
                    objSelectMenu.BackBtn();
                }
            }

            else if (isInnerMenuAcc)
            {
                if (unlockWindow.activeSelf == true)
                {
                    unlockWindow.SetActive(false);
                }
                else if (unlockWindow2.activeSelf == true)
                {
                    shop.Close2(unlockWindow);
                }
                else
                {
                    accMenu.BackClick();
                }


            }
            else
            {
                MenuIndexControl(-1);
            }
        }

        //if (Input.GetKeyDown(KeyCode.F))
        //{
        //   // GameManager.Instance.AddSoftCurrency(50);
        // //   PlayfabManager.Instance.AddCurrency("SC", 50);
        //}
        ShoeScale(baseHeightFront.value);
        //  baseHeightFrontValue = baseHeightFront.value; heelThicknessValue = heelThicknessDump.value; facethickValue = facethick.value;
        if (mainIndexTemp != mainIndex)
        {
            //Debug.Log(mainIndex);
            switch (mainIndex)
            {
                case 1:// Start Menu
                       // porgressBar.resetProgressBar();
                    Camera.GetComponent<WowCamera>().SetObliqueness(0.25f, 0);
                    progressBar.SetActive(false);
                    Camera.gameObject.GetComponent<Camera>().enabled = false;//.SetActive(false);
                    GUICamera.gameObject.GetComponent<Camera>().enabled = true;
                    Camera.gameObject.SetActive(false);
                    if (startGame)
                    {
                        contineBtnGroup.SetActive(true);
                        withoutContineBtnGroup.SetActive(false);
                    }
                    else
                    {
                        contineBtnGroup.SetActive(false);
                        withoutContineBtnGroup.SetActive(true);
                    }
                    shose.gameObject.transform.localPosition = new Vector3(0, shose.transform.localPosition.y, 0);// Vector3.zero;
                    shose.gameObject.transform.localRotation = Quaternion.identity;
                    LoadMenu.gameObject.SetActive(false);
                    SelectStyleWin.gameObject.SetActive(false);
                    StartMenuColection.gameObject.SetActive(true);
                   // finalCamera.SetActive(false);
                    MorphMenuCollection.gameObject.SetActive(false);
                    MaterialMenuCollection.gameObject.SetActive(false);
                    AccMenu.gameObject.SetActive(false);
                    nextBackCollBtn.SetActive(false);
                    objSelectMenu.gameObject.SetActive(false);
                    ObjectSelectGroup.SetActive(false);
                    DecalScript.StartDecalAction = false;
                    MaterialPopUpMenu.gameObject.SetActive(false);
                    Ruler.transform.parent.gameObject.SetActive(false);
                    RulerText.SetActive(false);
                    SaveBtn.gameObject.SetActive(false);
                    settingWindow.SetActive(false);
                    shopWindow.SetActive(false);
                    profileImage.SetActive(false);
                    profileImageShade.SetActive(false);
                    mainScene.SetActive(true);
                    DarkScene.SetActive(false);
                    SelectInfo(-1);
                    break;
                case 2:// Start Menu
                    Camera.GetComponent<WowCamera>().SetObliqueness(0.25f, 0);
                   
                    porgressBar.resetProgressBar();
                    progressBar.SetActive(true);
                    objSelectMenu.DeactiveAll();
                //    shose.gameObject.transform.localPosition = Vector3.zero;
                    shose.gameObject.transform.localRotation = Quaternion.identity;
                    Camera.gameObject.GetComponent<Camera>().enabled = false;//);
                    GUICamera.gameObject.GetComponent<Camera>().enabled = true;
                    Camera.gameObject.SetActive(false);
                    LoadMenu.gameObject.SetActive(false);
                    //StartMenuColection.GetComponent<GUIAnimationGroup>().DeactivateAll();
                    StartMenuColection.gameObject.SetActive(false);
                    SelectStyleWin.gameObject.SetActive(true);
                   // finalCamera.SetActive(false);
                    nextBtn.gameObject.SetActive(false);
                    MorphMenuCollection.gameObject.SetActive(false);
                    MaterialMenuCollection.gameObject.SetActive(false);
                    AccMenu.gameObject.SetActive(false);
                    nextBackCollBtn.SetActive(true);
                    objSelectMenu.gameObject.SetActive(false);
                    ObjectSelectGroup.SetActive(false);
                    DecalScript.StartDecalAction = false;
                    MaterialPopUpMenu.gameObject.SetActive(false);
                    Ruler.transform.parent.gameObject.SetActive(false);
                    RulerText.SetActive(false);
                    SaveBtn.gameObject.SetActive(false);
                    settingWindow.SetActive(false);
                    shopWindow.SetActive(false);
                    profileImage.SetActive(false);
                    profileImageShade.SetActive(false);
                    mainScene.SetActive(true);
                    DarkScene.SetActive(false);
                    SelectInfo(1);
                    break;
                case 3:  //Morph Menu
                    Camera.GetComponent<WowCamera>().SetObliqueness(0.25f, 0);
                    shose.gameObject.transform.localPosition = new Vector3(0, shose.transform.localPosition.y, 0);//Vector3.zero;
                    shose.gameObject.transform.localRotation = Quaternion.identity;
                    progressBar.SetActive(true);
                    nextBtn.gameObject.SetActive(true);
                    SelectStyleWin.gameObject.SetActive(false);
                    Camera.gameObject.GetComponent<Camera>().enabled = true;//);
                    GUICamera.gameObject.GetComponent<Camera>().enabled = false;
                    Camera.gameObject.SetActive(true);
                    LoadMenu.gameObject.SetActive(false);
                   // BankContainer.SetActive(true);
                   // finalCamera.SetActive(false);
                    Camera.GetComponent<WowCamera>().OnClickRight(1, Camera.GetComponent<WowCamera>().cameraRotationAngle);
                    MorphMenuCollection.gameObject.SetActive(false);
                    StartMenuColection.gameObject.SetActive(false);
                    mainScene.SetActive(true);
                    DarkScene.SetActive(false);
                    nextBackCollBtn.SetActive(true);

                    MaterialMenuCollection.gameObject.SetActive(false);
                    //PopUpMenuControler.gameObject.SetActive(false);
                    objSelectMenu.gameObject.SetActive(true);
                    ObjectSelectGroup.SetActive(true);
                    AccMenu.gameObject.SetActive(false);
                    MaterialPopUpMenu.gameObject.SetActive(false);
                    DecalScript.StartDecalAction = false;
                    Ruler.transform.parent.gameObject.SetActive(false);
                    RulerText.SetActive(false);
                    SaveBtn.gameObject.SetActive(false);
                    isMorphPhase = false;
                    profileImage.SetActive(true);
                    profileImageShade.SetActive(true);
                    mainScene.SetActive(true);
                    DarkScene.SetActive(false);
                    // nextBtn.SetActive(true);
                    //  popButtonsManager.ClearAllPopUp();
                    SelectInfo(2);
                    break;
                case 4:  //Morph Menu
                    Camera.GetComponent<WowCamera>().SetObliqueness(0.25f, 0);
                    shose.gameObject.transform.localPosition = new Vector3(0, shose.transform.localPosition.y, 0);//Vector3.zero;
                    shose.gameObject.transform.localRotation = Quaternion.identity;
                    isMorphPhase = true;
                    progressBar.SetActive(true);
                    objSelectMenu.DeactiveAll();
                    LoadMenu.gameObject.SetActive(false);
                  //  finalCamera.SetActive(false);
                    Camera.GetComponent<WowCamera>().OnClickRight(1, Camera.GetComponent<WowCamera>().cameraRotationAngle);
                    MorphMenuCollection.gameObject.SetActive(true);
                    StartMenuColection.gameObject.SetActive(false);
                    nextBackCollBtn.SetActive(true);
                    Camera.gameObject.GetComponent<Camera>().enabled = true;
                    GUICamera.gameObject.GetComponent<Camera>().enabled = false;
                    MaterialMenuCollection.gameObject.SetActive(false);
                    //PopUpMenuControler.gameObject.SetActive(false);
                    objSelectMenu.gameObject.SetActive(false);
                    ObjectSelectGroup.SetActive(false);
                    AccMenu.gameObject.SetActive(false);
                    MaterialPopUpMenu.gameObject.SetActive(false);
                    DecalScript.StartDecalAction = false;
                    Ruler.transform.parent.gameObject.SetActive(true);
                    
                    RulerText.SetActive(true);
                    SaveBtn.gameObject.SetActive(false);
                    profileImage.SetActive(true);
                    profileImageShade.SetActive(true);
                    nextBtn.SetActive(true);
                    mainScene.SetActive(true);
                    DarkScene.SetActive(false);
                    //  popButtonsManager.ClearAllPopUp();
                    SelectInfo(3);
                    break;
                case 5://Material Menu
                    //popButtonsManager.ClearAllPopUp();
                    Camera.GetComponent<WowCamera>().SetObliqueness(0.25f, 0);
                    shose.gameObject.transform.localPosition = new Vector3(0, shose.transform.localPosition.y, 0);// Vector3.zero;
                    shose.gameObject.transform.localRotation = Quaternion.identity;
                    isMorphPhase = false;
                    progressBar.SetActive(true);
                    LoadMenu.gameObject.SetActive(false);
                  //  finalCamera.SetActive(false);
                    Camera.gameObject.GetComponent<Camera>().enabled = true;
                    GUICamera.gameObject.GetComponent<Camera>().enabled = false;
                    Camera.GetComponent<WowCamera>().OnClickRight(0, Camera.GetComponent<WowCamera>().cameraRotationAngle);
                    Debug.Log(objSelectMenu.heelSkinnedMesh.gameObject.GetComponent<EachObjControl>().objType);
                    if (objSelectMenu.heelSkinnedMesh.gameObject.GetComponent<EachObjControl>().objType == ObjMenu.Widge)
                    {
                        heelMat.SetActive(false);
                    }
                    else
                    {
                        heelMat.SetActive(true);
                    }

                    MaterialMenuCollection.gameObject.SetActive(true);
                    StartMenuColection.gameObject.SetActive(false);
                    MorphMenuCollection.gameObject.SetActive(false);
                    PopUpMenuControler.gameObject.SetActive(true);
                    MaterialPopUpMenu.gameObject.SetActive(true);
                    objSelectMenu.gameObject.SetActive(false);
                    ObjectSelectGroup.SetActive(false);
                    AccMenu.gameObject.SetActive(false);
                    DecalScript.StartDecalAction = false;
                    Ruler.transform.parent.gameObject.SetActive(false);
                    RulerText.SetActive(false);
                    changemorphe = true;
                    resetcollider = false;
                    SaveBtn.gameObject.SetActive(false);
                    profileImage.SetActive(true);
                    profileImageShade.SetActive(true);
                    nextBtn.SetActive(true);
                    mainScene.SetActive(true);
                    DarkScene.SetActive(false);
                    AccMenu.gameObject.GetComponent<AccMenu1>().clearselector(-1);
                    SelectInfo(4);
                    break;
                case 6:// Acc Menu
                    changemorphe = true;
                    resetcollider = false;
                    Camera.GetComponent<WowCamera>().SetObliqueness(0.25f, 0);
               
                    progressBar.SetActive(true);
                    LoadMenu.gameObject.SetActive(false);
                    Camera.gameObject.GetComponent<Camera>().enabled = true;
                    GUICamera.gameObject.GetComponent<Camera>().enabled = false;
                   // Camera.gameObject.SetActive(true);
                 //   finalCamera.SetActive(false);
                    Camera.GetComponent<WowCamera>().OnClickRight(1, Camera.GetComponent<WowCamera>().cameraRotationAngle);
                    AccMenu.gameObject.SetActive(true);
                    StartMenuColection.gameObject.SetActive(false);
                    MorphMenuCollection.gameObject.SetActive(false);
                    MaterialMenuCollection.gameObject.SetActive(false);
                    objSelectMenu.gameObject.SetActive(false);
                    ObjectSelectGroup.SetActive(false);
                    MaterialPopUpMenu.gameObject.SetActive(false);
                    ///////////////////         DecalScript.StartDecalAction = true;
                    DecalScript.shose = shose;
                    //changemorphe = true;
                    ////resetcollider = false;
                    //resetcollider = true;
                    Debug.Log("resetcollider" + resetcollider);
                    nextBtn.SetActive(true);
                    //DecalScript.ResetCollider(shose.faceObj);
                    //DecalScript.ResetCollider(shose.baseObj);
                    //DecalScript.ResetCollider(shose.heelObj);
                    Ruler.transform.parent.gameObject.SetActive(false);
                    RulerText.SetActive(false);
                    SaveBtn.gameObject.SetActive(false);
                    profileImage.SetActive(true);
                    profileImageShade.SetActive(true);
                    SelectInfo(5);
                    mainScene.SetActive(true);
                    DarkScene.SetActive(false);
                    //    Debug.Log("acc");
                    shose.gameObject.transform.localPosition = new Vector3(0, shose.transform.localPosition.y, 0);// Vector3.zero;
                    shose.gameObject.transform.localRotation = Quaternion.identity;
                    // popButtonsManager.ClearAllPopUp();
                    break;
                case 7:// Acc Menu
                    // Camera.GetComponent<WowCamera>().OnClickRight(1);
                    Camera.GetComponent<WowCamera>().SetObliqueness(0.05f, 0);
                    progressBar.SetActive(true);
                    LoadMenu.gameObject.SetActive(false);
                   // Camera.gameObject.GetComponent<Camera>().enabled = false;
                    Camera.gameObject.GetComponent<Camera>().enabled = true;
                    //GUICamera.gameObject.GetComponent<Camera>().enabled = true;
                    GUICamera.gameObject.GetComponent<Camera>().enabled = false;
                    Camera.GetComponent<WowCamera>().OnClickRight(9, Camera.GetComponent<WowCamera>().cameraRotationAngle);
                    // Camera.gameObject.SetActive(false);
                    nextBtn.SetActive(false);
                 //   finalCamera.SetActive(true);
                    AccMenu.gameObject.SetActive(false);
                    StartMenuColection.gameObject.SetActive(false);
                    MorphMenuCollection.gameObject.SetActive(false);
                    MaterialMenuCollection.gameObject.SetActive(false);
                    objSelectMenu.gameObject.SetActive(false);
                    ObjectSelectGroup.SetActive(false);
                    MaterialPopUpMenu.gameObject.SetActive(false);
                    DecalScript.StartDecalAction = false;
                    DecalScript.shose = shose;
                    resetcollider = false;
                    AccMenu.gameObject.GetComponent<AccMenu1>().clearselector(-1);
                    Ruler.transform.parent.gameObject.SetActive(false);
                //    RulerText.SetActive(true);
                    SaveBtn.gameObject.SetActive(true);
                    profileImage.SetActive(false);
                    profileImageShade.SetActive(false);
                    // popButtonsManager.ClearAllPopUp();
                    SelectInfo(6);
                    break;
                case 0:// Load.se
                    progressBar.SetActive(false);
                    LoadMenu.gameObject.SetActive(true);
                    if (LoadBank.spritesArr.Length > 0)
                    {
                        LoadBtn.SetActive(false);
                    }
                    else
                    {
                        LoadBtn.SetActive(true);
                    }
                    StartMenuColection.gameObject.SetActive(false);
                //    finalCamera.SetActive(false);
                    MorphMenuCollection.gameObject.SetActive(false);
                    MaterialMenuCollection.gameObject.SetActive(false);
                    AccMenu.gameObject.SetActive(false);
                    nextBackCollBtn.SetActive(false);
                    objSelectMenu.gameObject.SetActive(false);
                    ObjectSelectGroup.SetActive(false);
                    DecalScript.StartDecalAction = false;
                    MaterialPopUpMenu.gameObject.SetActive(false);
                    Ruler.transform.parent.gameObject.SetActive(false);
                    SaveBtn.gameObject.SetActive(false);
                    settingWindow.SetActive(false);
                    profileImage.SetActive(false);
                    profileImageShade.SetActive(false);
                    break;

                case 8:// Load.setting
                    progressBar.SetActive(false);
                    LoadMenu.gameObject.SetActive(false);
                    StartMenuColection.gameObject.SetActive(true);
                 //   finalCamera.SetActive(false);
                    MorphMenuCollection.gameObject.SetActive(false);
                    MaterialMenuCollection.gameObject.SetActive(false);
                    AccMenu.gameObject.SetActive(false);
                    nextBackCollBtn.SetActive(false);
                   objSelectMenu.gameObject.SetActive(false);
                    ObjectSelectGroup.SetActive(false);
                    DecalScript.StartDecalAction = false;
                    MaterialPopUpMenu.gameObject.SetActive(false);
                    Ruler.transform.parent.gameObject.SetActive(false);
                    SaveBtn.gameObject.SetActive(false);
                    profileImage.SetActive(false);
                    profileImageShade.SetActive(false);
                    settingWindow.SetActive(true);
                    mainScene.SetActive(true);
                    DarkScene.SetActive(false);
                    break;
                case 9:// Load.setting
                    progressBar.SetActive(false);
                    LoadMenu.gameObject.SetActive(false);
                    //StartMenuColection.gameObject.SetActive(false);
                //    finalCamera.SetActive(false);
                    MorphMenuCollection.gameObject.SetActive(false);
                    MaterialMenuCollection.gameObject.SetActive(false);
                    AccMenu.gameObject.SetActive(false);
                    nextBackCollBtn.SetActive(false);
                    objSelectMenu.gameObject.SetActive(false);
                    ObjectSelectGroup.SetActive(false);
                    DecalScript.StartDecalAction = false;
                    MaterialPopUpMenu.gameObject.SetActive(false);
                    Ruler.transform.parent.gameObject.SetActive(false);
                    SaveBtn.gameObject.SetActive(false);
                    settingWindow.SetActive(false);
                    profileImage.SetActive(true);
                    profileImageShade.SetActive(false);
                    shopWindow.SetActive(true);
                    mainScene.SetActive(true);
                    DarkScene.SetActive(false);
                    break;
                default:
                    break;
            }
            mainIndexTemp = mainIndex;
        }


        //shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(2, front * 100);
        //shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(2, front * 100);
        //shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(2, front * 100);

        //MaterialMenuController();

        // MorphFn();
        RulerScale(Ruler, baseHeightFront.value, 12.5f, 17);


    }
    void SelectInfo(int index)
    {
        for (int i = 0; i < listofInfo.Length; i++)
        {
            if (i == index)
            {
                listofInfo[i].SetActive(true);
            }
            else
            {
                listofInfo[i].SetActive(false);
            }
        }
    }
    public void ChooseType(int index)
    {
        //   Debug.Log("dsds");
        switch (index)
        {

            case 0:
                if (index != ChooseTypeIndex || resetall)
                {
                    resetall = false;
                    _decal.resetAcc();
                }
                frontModelSel.gameObject.SetActive(false);
                backModelSel.gameObject.SetActive(false);
                faceModelSel.gameObject.SetActive(true);

                frontAndback3dModel.SetActive(false);

                face3dModel.SetActive(true);
                backFaceMat.SetActive(false);
                TwoFaceMatSelImg.SetActive(true); OneFaceMatSelImg.SetActive(false);
               
                ChooseTypeIndex = 0;
                //frontAndbackMatSel.gameObject.SetActive(false);
                //faceMatSel.gameObject.SetActive(true);
                break;
            case 1:
                if (index != ChooseTypeIndex || resetall)
                {
                    resetall = false;
                    _decal.resetAcc();
                }
                frontModelSel.gameObject.SetActive(true);
                backModelSel.gameObject.SetActive(true);
                faceModelSel.gameObject.SetActive(false);
                frontAndback3dModel.SetActive(true);
                face3dModel.SetActive(false);
                backFaceMat.SetActive(true);
                TwoFaceMatSelImg.SetActive(false); OneFaceMatSelImg.SetActive(true);
                //Debug.Log(objSelectMenu.heelSkinnedMesh.gameObject.GetComponent<EachObjControl>().objType);
                //if (objSelectMenu.heelSkinnedMesh.gameObject.GetComponent<EachObjControl>().objType == ObjMenu.Widge)
                //{
                //    heelMat.SetActive(false);
                //}
                ChooseTypeIndex = 1;
                //frontAndbackMatSel.gameObject.SetActive(true);
                //faceMatSel.gameObject.SetActive(false);
                break;
            default:
                break;
        }
    }
    public void FadeOut(Image _image)
    {
        StartCoroutine(FadeOutCR(_image));
    }

    private IEnumerator FadeOutCR(Image _image)
    {

        for (int i = 0; i < 5; i++)
        {
            float duration = 0.3f; //0.5 secs
            float currentTime = 0f;
            while (currentTime < duration)
            {

                _image.color = Color.Lerp(c1, c2, currentTime / duration);
                // _image.color = new Color(_image.color.r, _image.color.g, _image.color.b, alpha);
                currentTime += Time.deltaTime;

                yield return null;
            }
            _image.color = c1;
            yield return null;// new WaitForSeconds();
            // yield break;
        }

    }
    public void changeInSlider()
    {
        heelThickness.value = heelThickness_1.value;

        heelThicknessDump.value = heelThicknessDump_1.value;
    }
    public void changeInMorphSlider()
    {
        heelThickness_1.value = heelThickness.value;

        heelThicknessDump_1.value = heelThicknessDump.value;
    }
    public void setValueForBaseHeightFront(int value)
    {
        //BaseHeightFront, heelThickness, heelThicknessDump, heelThickness_1, heelThicknessDump_1, facethick

        baseHeightFront.value = value;
        heelThickness.value = ((float)System.Math.Round(baseHeightFront.value, 3) * 4);
        if (heelThickness.value==0)
        {
            heelThicknessDump.value = heelThickness.value;
        }
        
        //changeInSlider();
        //changeInMorphSlider();
       
    }
    public void MorphBlock()
    {
        morphBlockWin.SetActive(true);
    }
    public void MorphBlockWin(bool flag)
    {
        if (flag)
        {
            morphBlockWin.SetActive(false);
            resetall = false;
            _decal.resetAcc();
            morphBlock.SetActive(false);
        }
        else
        {
            morphBlockWin.SetActive(false);
            morphBlock.SetActive(true);
        }
    }
    public void setValueForheelThickness(int value)
    {
        //BaseHeightFront, heelThickness, heelThicknessDump, heelThickness_1, heelThicknessDump_1, facethick
        if (value > 0)
        {
            heelThicknessDump.value = heelThickness.value;
            //changeInSlider();
            //changeInMorphSlider();
        }
        else
        {
            heelThicknessDump.value = 0;
        }
        //heelThicknessDump.value = value;
       // changeInSlider();
       // changeInMorphSlider();
    }
    public void setValueForfacethick(int value)
    {
        //BaseHeightFront, heelThickness, heelThicknessDump, heelThickness_1, heelThicknessDump_1, facethick

        facethick.value = value;

    }
    
    //void MaterialMenuController()
    //{
    //    if (tempCase != MaterialMenuIndex)
    //    {
    //        switch (MaterialMenuIndex)
    //        {
    //            case 1:  //face out
    //                if (tempCase != 1)
    //                {
    //                    // objType = ObjMenu.none;
    //                    //matMenu = MaterialMenu.none;
    //                    //changeActiveMenu(colorPicker, materialSliderFace);

    //                   // shose.faceParm.GetMatParams(shose.faceObj.GetComponent<SkinnedMeshRenderer>().materials[1]);
    //                    objType = ObjMenu.Face;
    //                    //colorPicker.getColorMaterial(shose.faceObj);
    //                    //colorPicker.setColorMaterial();
    //                    //FaceSubMenu.colorPicker.GetComponent<ColorPicker>().getColorMaterial(shose.faceObj);
    //                    //FaceSubMenu.colorPicker.GetComponent<ColorPicker>().setColorMaterial();

    //                    FaceSubMenu.gameObject.SetActive(true);
    //                    HeelSubMenu.gameObject.SetActive(false);
    //                    BaseSubMenu.gameObject.SetActive(false);

    //                    FaceSubMenuIn.gameObject.SetActive(false);
    //                    BaseSubMenuUp.gameObject.SetActive(false);
    //                    BaseSubMenuDown.gameObject.SetActive(false);
    //                    //FaceSubMenu.colorPicker.GetComponent<ColorPicker>().getColorMaterial(shose.faceObj);
    //                    //FaceSubMenu.colorPicker.GetComponent<ColorPicker>().setColorMaterial();

    //                    ////objType = ObjMenu.Face;
    //                    //changeActiveMenu(colorPicker, materialSliderFace);



    //                    tempCase = MaterialMenuIndex;
    //                    MaterialMenuIndex = 0;


    //                }

    //                break;
    //            case 2:  //facein
    //                if (tempCase != 2)
    //                {
    //                    //objType = ObjMenu.none;
    //                    //matMenu = MaterialMenu.none;
    //                    //changeActiveMenu(colorPicker, materialSliderFace);

    //                    objType = ObjMenu.Face;
    //                    //HeelSubMenu.colorPicker.GetComponent<ColorPicker>().getColorMaterial(shose.heelObj);
    //                    //HeelSubMenu.colorPicker.GetComponent<ColorPicker>().setColorMaterial();


    //                    FaceSubMenu.gameObject.SetActive(false);
    //                    HeelSubMenu.gameObject.SetActive(false);
    //                    BaseSubMenu.gameObject.SetActive(false);
    //                    FaceSubMenuIn.gameObject.SetActive(true);
    //                    BaseSubMenuUp.gameObject.SetActive(false);
    //                    BaseSubMenuDown.gameObject.SetActive(false);

    //                    //HeelSubMenu.colorPicker.GetComponent<ColorPicker>().getColorMaterial(shose.heelObj);
    //                    //HeelSubMenu.colorPicker.GetComponent<ColorPicker>().setColorMaterial();



    //                    tempCase = MaterialMenuIndex;
    //                    MaterialMenuIndex = 0;
    //                }

    //                break;
    //            case 3:  //BaseUp

    //                if (tempCase != 3)
    //                {
    //                    //objType = ObjMenu.none;
    //                    //matMenu = MaterialMenu.none;
    //                    //changeActiveMenu(colorPicker, materialSliderFace);

    //                    objType = ObjMenu.Base;

    //                    FaceSubMenu.gameObject.SetActive(false);
    //                    HeelSubMenu.gameObject.SetActive(false);
    //                    BaseSubMenu.gameObject.SetActive(true);
    //                    FaceSubMenuIn.gameObject.SetActive(false);
    //                    BaseSubMenuUp.gameObject.SetActive(false);
    //                    BaseSubMenuDown.gameObject.SetActive(false);


    //                    //BaseSubMenu.colorPicker.GetComponent<ColorPicker>().getColorMaterial(shose.baseObj);
    //                    //BaseSubMenu.colorPicker.GetComponent<ColorPicker>().setColorMaterial();



    //                    tempCase = MaterialMenuIndex;
    //                    MaterialMenuIndex = 0;
    //                }

    //                break;
    //            case 4:  //baseMiddel
    //                if (tempCase != 4)
    //                {
    //                    //objType = ObjMenu.none;
    //                    //matMenu = MaterialMenu.none;
    //                    //changeActiveMenu(colorPicker, materialSliderFace);

    //                    objType = ObjMenu.Base;
    //                    //HeelSubMenu.colorPicker.GetComponent<ColorPicker>().getColorMaterial(shose.heelObj);
    //                    //HeelSubMenu.colorPicker.GetComponent<ColorPicker>().setColorMaterial();


    //                    FaceSubMenu.gameObject.SetActive(false);
    //                    HeelSubMenu.gameObject.SetActive(false);
    //                    BaseSubMenu.gameObject.SetActive(false);
    //                    FaceSubMenuIn.gameObject.SetActive(false);
    //                    BaseSubMenuUp.gameObject.SetActive(true);
    //                    BaseSubMenuDown.gameObject.SetActive(false);

    //                    //HeelSubMenu.colorPicker.GetComponent<ColorPicker>().getColorMaterial(shose.heelObj);
    //                    //HeelSubMenu.colorPicker.GetComponent<ColorPicker>().setColorMaterial();



    //                    tempCase = MaterialMenuIndex;
    //                    MaterialMenuIndex = 0;
    //                }

    //                break;



    //            case 5:  //heel
    //                if (tempCase != 5)
    //                {
    //                    //objType = ObjMenu.none;
    //                    //matMenu = MaterialMenu.none;
    //                    //changeActiveMenu(colorPicker, materialSliderFace);

    //                    objType = ObjMenu.Base;
    //                    //HeelSubMenu.colorPicker.GetComponent<ColorPicker>().getColorMaterial(shose.heelObj);
    //                    //HeelSubMenu.colorPicker.GetComponent<ColorPicker>().setColorMaterial();


    //                    FaceSubMenu.gameObject.SetActive(false);
    //                    HeelSubMenu.gameObject.SetActive(false);
    //                    BaseSubMenu.gameObject.SetActive(false);
    //                    FaceSubMenuIn.gameObject.SetActive(false);
    //                    BaseSubMenuUp.gameObject.SetActive(false);
    //                    BaseSubMenuDown.gameObject.SetActive(true);

    //                    //HeelSubMenu.colorPicker.GetComponent<ColorPicker>().getColorMaterial(shose.heelObj);
    //                    //HeelSubMenu.colorPicker.GetComponent<ColorPicker>().setColorMaterial();



    //                    tempCase = MaterialMenuIndex;
    //                    MaterialMenuIndex = 0;
    //                }

    //                break;
    //            case 6:  //heel
    //                if (tempCase != 6)
    //                {
    //                    //objType = ObjMenu.none;
    //                    //matMenu = MaterialMenu.none;
    //                    //changeActiveMenu(colorPicker, materialSliderFace);

    //                    objType = ObjMenu.Heel;
    //                    //HeelSubMenu.colorPicker.GetComponent<ColorPicker>().getColorMaterial(shose.heelObj);
    //                    //HeelSubMenu.colorPicker.GetComponent<ColorPicker>().setColorMaterial();


    //                    FaceSubMenu.gameObject.SetActive(false);
    //                    HeelSubMenu.gameObject.SetActive(true);
    //                    BaseSubMenu.gameObject.SetActive(false);
    //                    FaceSubMenuIn.gameObject.SetActive(false);
    //                    BaseSubMenuUp.gameObject.SetActive(false);
    //                    BaseSubMenuDown.gameObject.SetActive(false);
    //                    //HeelSubMenu.colorPicker.GetComponent<ColorPicker>().getColorMaterial(shose.heelObj);
    //                    //HeelSubMenu.colorPicker.GetComponent<ColorPicker>().setColorMaterial();



    //                    tempCase = MaterialMenuIndex;
    //                    MaterialMenuIndex = 0;
    //                }

    //                break;
    //            default:
    //                break;
    //        }
    //        tempCase = MaterialMenuIndex;
    //    }
    //}
    //public void BackBottonMaterial()
    //{

    //                    FaceSubMenu.gameObject.SetActive(false);
    //                    HeelSubMenu.gameObject.SetActive(false);
    //                    BaseSubMenu.gameObject.SetActive(false);
    //                    FaceSubMenuIn.gameObject.SetActive(false);
    //                    BaseSubMenuUp.gameObject.SetActive(false);
    //                    BaseSubMenuDown.gameObject.SetActive(false);

    //}


    //public void changeActiveMenu(ColorPicker colorpicker, ScrollListTest materialSlider)
    //{



    //    if (tempMatMenu != matMenu)
    //    {
    //        switch (matMenu)
    //        {
    //            case MaterialMenu.color:

    //                tempMatMenu = MaterialMenu.color;
    //                materialSlider.transform.parent.gameObject.SetActive(false);
    //                colorpicker.gameObject.SetActive(true);
    //                break;
    //            case MaterialMenu.texture:
    //                tempMatMenu = MaterialMenu.texture;
    //                materialSlider.transform.parent.gameObject.SetActive(true);
    //                colorpicker.gameObject.SetActive(false);
    //                break;
    //            case MaterialMenu.pattern:
    //                tempMatMenu = MaterialMenu.pattern;
    //                materialSlider.transform.parent.gameObject.SetActive(false);
    //                colorpicker.gameObject.SetActive(false);
    //                break;
    //            case MaterialMenu.none:
    //                Debug.Log("lllll");
    //                materialSlider.transform.parent.gameObject.SetActive(false);
    //                colorpicker.gameObject.SetActive(false);
    //                break;
    //            default:
    //                break;
    //        }


    //    }
    //}
    public void ButtonClick(int index)
    {
        MaterialMenuIndex = index;
    }
    void ShoeScale(float heelHeightRange) //heelHeight range 0->1
    {
        
        if (heelHeightRange <= 0.5)
        {
            shose.transform.localScale = new Vector3(startScale.x +(0.13f-(heelHeightRange*0.13f)/0.5f), startScale.y, startScale.z);
           // rulerHeight = heelHeightRange * HeightInHalfWAy * 2;
        }
        else
        {
            shose.transform.localScale = startScale;
            //rulerHeight = (heelHeightRange * 2 * HeightInFullWAy) / HeightInHalfWAy;
        }
    }
            void RulerScale(zzVerticalRuler Ruler, float heelHeightRange, float HeightInHalfWAy, float HeightInFullWAy) //heelHeight range 0->1
    {
        Ruler.scrollervalue = heelHeightRange;
        float rulerHeight;
        if (heelHeightRange <= 0.5)
        {

            rulerHeight = heelHeightRange * HeightInHalfWAy * 2;
        }
        else
        {

            //rulerHeight = (heelHeightRange * 2 * HeightInFullWAy) / HeightInHalfWAy;
            rulerHeight = HeightInHalfWAy + (HeightInFullWAy - HeightInHalfWAy) * ((heelHeightRange * 2) - 1);
        }
        Ruler.gameObject.transform.localScale = new Vector3(Ruler.gameObject.transform.localScale.x, rulerHeight, 0);
        //  Ruler.height = System.Math.Round((heelHeightRange * 6), 2).ToString() + " inch";
        double tempHeight = System.Math.Round((6 * rulerHeight) / HeightInFullWAy, 1);
        Ruler.heightValue = (float)tempHeight;
        if (tempHeight > 0)
        {
            Ruler.height = string.Format("{0:0.0}", tempHeight) + " inch"; //tempHeight.ToString() + " inch";
        }
        else
        {
            Ruler.height = "";
        }

    }
    public void resetObjPos()
    {
        Debug.Log("resetPos");
        shose.gameObject.transform.localPosition = Vector3.zero;
        shose.gameObject.transform.localRotation = Quaternion.identity;
        shose.baseObj.SetActive(true);
    }
    //void MorphFn()
    //{
    //    if (heelthickIndex != tempHeelthickIndex)
    //    {
    //        for (int i = 2; i < shose.heelObjBlendShapeLength; i++)
    //        {
    //            if (i == heelthickIndex)
    //            {

    //                //tempHeelthickIndex = heelthickIndex;
    //            }
    //            else
    //            {
    //                shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(i, 0);
    //            }

    //        }
    //        for (int i = 2; i < shose.heel2ObjBlendShapeLength; i++)
    //        {
    //            if (i == heelthickIndex)
    //            {


    //            }
    //            else
    //            {
    //                shose.heel2Obj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(i, 0);
    //            }

    //        }
    //        if (heelThicknessDump.value == 1)
    //        {
    //            heelThicknessDump.value = 0.995f;
    //        }

    //        tempHeelthickIndex = heelthickIndex;

    //    }
    //  //  Debug.Log(baseHeightFrontTemp +"  "+ (float)System.Math.Round(baseHeightFront.value, 3));
    //    if (baseHeightFrontTemp != (float)System.Math.Round(baseHeightFront.value, 3) || heelThicknessDump.value != heelThicknessDumpTemp)
    //    {
    //        if (baseHeightFrontTemp != baseHeightFront.value)
    //        {


    //        ////1
    //        if ((float)System.Math.Round(baseHeightFront.value, 3) < 0.5f)
    //        {
    //           Debug.Log("h");
    //            shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, 100 - ((float)System.Math.Round(baseHeightFront.value, 3) * 2 * 100));
    //            shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, 100 - ((float)System.Math.Round(baseHeightFront.value, 3) * 2 * 100));
    //            shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, 100 - ((float)System.Math.Round(baseHeightFront.value, 3) * 2 * 100));
    //            shose.heel2Obj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, 100 - ((float)System.Math.Round(baseHeightFront.value, 3) * 2 * 100));
    //            shose.widgedObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, 100 - ((float)System.Math.Round(baseHeightFront.value, 3) * 2 * 100));//Wedget
    //            shose.labelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, 100 - ((float)System.Math.Round(baseHeightFront.value, 3) * 2 * 100));//Lable
    //            shose.coneHeelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, 100 - ((float)System.Math.Round(baseHeightFront.value, 3) * 2 * 100));//coneheel
    //            shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 0);
    //            shose.heel2Obj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 0);
    //            shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 0);
    //            shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 0);
    //            shose.coneHeelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 0);
    //            shose.widgedObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 0);
    //            shose.labelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 0);
    //            MaterialTileUpdate(heelMaterialManager.tileValue);
    //            MaterialTileUpdate2(baseMaterialManager.tileValue);
    //            MaterialTileUpdate3(baseMaterialManagerMiddel.tileValue);
    //           // baseHeightFrontTemp = (float)System.Math.Round(baseHeightFront.value, 3);
    //            baseHeightFrontTemp = baseHeightFront.value;
    //        }

    //        else if ((float)System.Math.Round(baseHeightFront.value, 3) > 0.5f)
    //        {
    //       Debug.Log("h2");
    //            shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, 0);
    //            shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, 0);
    //            shose.heel2Obj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, 0);
    //            shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, 0);
    //            shose.coneHeelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, 0);
    //            shose.widgedObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, 0);

    //            shose.labelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, 0);


    //            //shose.heelObj.transform.localScale = new Vector3(1, 1+(((baseHeightFront.value-0.5f)*0.43f)/0.5f), 1);  //////////////////////scle Height
    //            shose.heelObj.transform.localScale = new Vector3(shose.heelObj.transform.localScale.x, 1 + (((baseHeightFront.value - 0.5f) * 0.45f) / 0.5f), shose.heelObj.transform.localScale.z);
    //           // shose.heel2Obj.transform.localScale = new Vector3(1, 1 + (((baseHeightFront.value - 0.5f) * 0.465f) / 0.5f), 1);/////////////////
    //            shose.heel2Obj.transform.localScale = new Vector3(shose.heel2Obj.transform.localScale.x, 1 + (((baseHeightFront.value - 0.5f) * 0.46f) / 0.5f), shose.heel2Obj.transform.localScale.z);

    //            shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, ((float)System.Math.Round(baseHeightFront.value, 3) - 0.5f) * 2 * 100);
    //            shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, ((float)System.Math.Round(baseHeightFront.value, 3) - 0.5f) * 2 * 100);
    //            shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, ((float)System.Math.Round(baseHeightFront.value, 3) - 0.5f) * 2 * 100);//////////////////////////
    //            shose.heel2Obj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, ((float)System.Math.Round(baseHeightFront.value, 3) - 0.5f) * 2 * 100);

    //            shose.widgedObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, ((float)System.Math.Round(baseHeightFront.value, 3) - 0.5f) * 2 * 100);//wedget
    //            shose.labelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, ((float)System.Math.Round(baseHeightFront.value, 3) - 0.5f) * 2 * 100);//Label
    //            shose.coneHeelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, ((float)System.Math.Round(baseHeightFront.value, 3) - 0.5f) * 2 * 100);//cone heel
    //          //  baseHeightFrontTemp = (float)System.Math.Round(baseHeightFront.value, 3) ;
    //            baseHeightFrontTemp = baseHeightFront.value;
    //            MaterialTileUpdate(heelMaterialManager.tileValue);
    //            MaterialTileUpdate2(baseMaterialManager.tileValue);
    //            MaterialTileUpdate3(baseMaterialManagerMiddel.tileValue);
    //        }

    //        }
    //        //===============================================================================================================================================
    //        //if (shose.faceObj.GetComponent<SkinnedMeshRenderer>().GetBlendShapeWeight(1) > ((float)System.Math.Round(baseHeightFront.value, 3) * 3 * 100))//o
    //        //{
    //        //    //shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, ((float)System.Math.Round(baseHeightFront.value, 3) * 3 * 100));//o
    //        //    //shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, ((float)System.Math.Round(baseHeightFront.value, 3) * 3 * 100));//o
    //        //    shose.heel2Obj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, ((float)System.Math.Round(baseHeightFront.value, 3) * 3 * 100));//o
    //        //    //shose.heel2Obj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, ((float)System.Math.Round(baseHeightFront.value, 3) * 4 * 100));
    //        //    heelThicknessDump.value = (float)System.Math.Round((float)System.Math.Round(baseHeightFront.value, 3) * 3, 3);//o
    //        //    baseHeightFrontTemp = (float)System.Math.Round(baseHeightFront.value, 3);
    //        //    heelThicknessDumpTemp = heelThicknessDump.value;

    //        //}
    //        //else
    //        //{

    //        //    if (heelThicknessDump.value < heelThickness.value)
    //        //    {
    //        //        //shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, heelThicknessDump.value * 100);
    //        //        //shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, heelThicknessDump.value * 100);
    //        //        shose.heel2Obj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, heelThicknessDump.value * 100);
    //        //        //shose.heel2Obj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, ((float)System.Math.Round(baseHeightFront.value, 3) * 4 * 100));

    //        //    }
    //        //    else
    //        //    {
    //        //        //heelThicknessDump.value =(float )System.Math.Round(heelThickness.value,2);
    //        //        heelThicknessDump.value = heelThickness.value;
    //        //        heelThicknessDumpTemp = heelThicknessDump.value;
    //        //    }


    //        //}
    //        ////Debug.Log("YOYO");

    //        //heelThickness.value = ((float)System.Math.Round(baseHeightFront.value, 3) * 3);// +0.001f;//o

    //            //==============================================================================================================================
    //        //////2
    //        //   Debug.Log(shose.faceObj.GetComponent<SkinnedMeshRenderer>().GetBlendShapeWeight(1)+" "+ ((float)System.Math.Round(baseHeightFront.value, 3) * 4 * 100));
    //        if (shose.faceObj.GetComponent<SkinnedMeshRenderer>().GetBlendShapeWeight(1) > ((float)System.Math.Round(baseHeightFront.value, 5) * 4 * 100))//o
    //        {
    //            //   //   shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, ((float)System.Math.Round(baseHeightFront.value, 3) * 3 * 100));//o
    //            //      //shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(2, ((float)System.Math.Round(baseHeightFront.value, 3) * 3* 100));//o

    //                  if (heelthickIndex < shose.heelObjBlendShapeLength)
    //                  {
    //            shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(heelthickIndex, ((float)System.Math.Round(baseHeightFront.value, 3) * 4 * 100));
    //                  }
    //                  if (heelthickIndex < shose.heel2ObjBlendShapeLength)
    //                  {
    //            shose.heel2Obj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(heelthickIndex, ((float)System.Math.Round(baseHeightFront.value, 3) * 4 * 100));
    //                  }
    //            shose.coneHeelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(2, ((float)System.Math.Round(baseHeightFront.value, 3) * 4 * 100));//o//cone heel
    //            shose.widgedObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(3, ((float)System.Math.Round(baseHeightFront.value, 3) * 4 * 100));
    //            heelThicknessDump.value = (float)System.Math.Round((float)System.Math.Round(baseHeightFront.value, 3) * 4, 3);//o
    //            //      baseHeightFrontTemp = (float)System.Math.Round(baseHeightFront.value, 3);
    //            heelThicknessDumpTemp = heelThicknessDump.value;
    //            Debug.Log("ppp");

    //        }
    //        else
    //        {

    //            if (heelThicknessDump.value < heelThickness.value)
    //            {
    //                // Debug.Log("mkk");
    //                //shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, heelThicknessDump.value * 100);
    //                //shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(2, heelThicknessDump.value * 100);
    //                if (heelthickIndex < shose.heelObjBlendShapeLength)
    //                {
    //                shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(heelthickIndex, heelThicknessDump.value * 100);
    //                }
    //                if (heelthickIndex < shose.heel2ObjBlendShapeLength)
    //                {
    //                shose.heel2Obj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(heelthickIndex, heelThicknessDump.value * 100);
    //                }
    //                shose.coneHeelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(2, heelThicknessDump.value * 100);//cone heel
    //                shose.widgedObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(3, heelThicknessDump.value * 100);
    //                heelThicknessDumpTemp = heelThicknessDump.value;
    //                //heelThicknessDumpTemp = heelThicknessDump.value;
    //            }
    //            else
    //            {
    //                // Debug.Log("mk");

    //                //shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(heelthickIndex, heelThicknessDump.value * 100);
    //                ////}
    //                ////if (heelthickIndex < shose.heel2ObjBlendShapeLength)
    //                ////{
    //                //shose.heel2Obj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(heelthickIndex, heelThicknessDump.value * 100);
    //                ////}
    //                //shose.coneHeelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(2, heelThicknessDump.value * 100);//cone heel
    //                //shose.widgedObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(3, heelThicknessDump.value * 100);
    //                //heelThicknessDump.value =(float )System.Math.Round(heelThickness.value,10);
    //                heelThicknessDump.value = heelThickness.value;
    //                heelThicknessDumpTemp = heelThicknessDump.value;
    //                //heelThickness.value = ((float)System.Math.Round(baseHeightFront.value, 3) * 3);
    //            }


    //        }
    //        ////Debug.Log("YOYO");
    //        //shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(heelthickIndex, heelThicknessDump.value * 100);
    //        ////}
    //        ////if (heelthickIndex < shose.heel2ObjBlendShapeLength)
    //        ////{
    //        //shose.heel2Obj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(heelthickIndex, heelThicknessDump.value * 100);
    //        ////}
    //        //shose.coneHeelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(2, heelThicknessDump.value * 100);//cone heel
    //        //shose.widgedObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(3, heelThicknessDump.value * 100);
    //        heelThickness.value = ((float)System.Math.Round(baseHeightFront.value, 3) * 4);// +0.001f;//o

    //    }

    //    if (facethickTemp != facethick.value)
    //    {

    //        //////////3
    //        if (facethick.value >= 0.2)
    //        {
    //            shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(2, (facethick.value - 0.2f) * 50);
    //            shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(2, (facethick.value - 0.2f) * 50);
    //            shose.widgedObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(2, (facethick.value - 0.2f) * 50);//W
    //            //shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(2, (facethick.value - 0.2f) * 50);
    //            //shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(4, 0);
    //            shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(3, 0);
    //        }
    //        else
    //        {
    //            shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(2, 0);
    //            shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(3, 100 - (facethick.value * 2 * 100) / 2);
    //        }
    //        //shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(5, (facethick.value * 100) / 2);
    //        //shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(5, (facethick.value * 100) / 2);
    //        //shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(5, (facethick.value * 100) / 2);

    //        //shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(6, (facethick.value * 100) / 2);

    //        facethickTemp = facethick.value;
    //    }



    //    //shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(6, faceNode.value * 100);
    //    //shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(6, faceNode.value * 100);
    //    //shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(6, faceNode.value * 100);
    //}

    public void MenuIndexNum(int menuNumber)
    {
        mainIndex = menuNumber;
        porgressBar.sss(menuNumber-3);
    }
    public void MenuIndexControl(int AddedNum)
    {
        tempMainIndex = mainIndex;
        SoundManScript.PlaySound(SoundManScript.Botton_Clip);
        
        if (mainIndex == 1)
        {
            if (AddedNum==2)
            {
                porgressBar.ActivateStage(1);
            }
            if (startGame == true)
            {
                if (tempMainIndex != 0)
                {
                    startGame = true;
                }

            }
            else
            {

                startGame = false;

            }
            //_decal.resetAcc();
        }
        else if (mainIndex == 3)
        {
            startGame = true;
        }
        else if (mainIndex == 7)
        {
            startGame = false;
        }
        //else if (mainIndex == 0)
        //{
        //    startGame = false;
        //}
        if (Mathf.Sign(AddedNum) == -1)
        {
            //if (mainIndex > 3)
            //{
                porgressBar.DeactivateStage(1);
            //}
            particalManScript.runParticalOnce(particalManScript.p2);
            if (mainIndex > 0)
            {
                mainIndex += AddedNum;
                if (mainIndex > 1)
                {
                    backBtn.SetActive(true);
                    nextBtn.SetActive(true);
                }
            }

            else
            {

                backBtn.SetActive(false);


            }

        }
        else
        {
           
           
            if (mainIndex < maxIndex)
            {
                if (mainIndex >= 2)
                {
                    porgressBar.ActivateStage(1);
                }
                mainIndex += AddedNum;
            }
            if (mainIndex != maxIndex)
            {
                particalManScript.runParticalOnce(particalManScript.p2);

                nextBtn.SetActive(true);
                backBtn.SetActive(true);
            }
            else
            {

                nextBtn.SetActive(false);
            }
        }


    }
    public void GoToMenu(int index)
    {
        SoundManScript.PlaySound(SoundManScript.Botton_Clip);
        particalManScript.runParticalOnce(particalManScript.p2);
        if (mainIndex == 1)
        {
            if (startGame == true)
            {
                if (tempMainIndex != 0)
                {
                    startGame = true;
                }

            }
            else
            {

                startGame = false;

            }


        }
        else if (mainIndex == 2)
        {
            startGame = true;
        }
        else if (mainIndex == 7)
        {
            startGame = false;
        }

        mainIndex = index;

    }
    void MaterialTileUpdate(Vector2 value)
    {

        // shose.heelObj.GetComponent<Renderer>().materials[1].mainTextureScale = new Vector2(value.x, baseHeightFront.value * 2 * value.x);
        // shose.heelObj.GetComponent<Renderer>().materials[1].mainTextureScale = new Vector2(value.x, baseHeightFront.value * 2 * value.x);
        shose.heelObj.GetComponent<Renderer>().materials[1].SetTextureScale("_MainTex", new Vector2(value.x, baseHeightFront.value * 2 * value.x));
        shose.heelObj.GetComponent<Renderer>().materials[1].SetTextureScale("_BumpMap", new Vector2(value.x, baseHeightFront.value * 2 * value.x));
        shose.heelObj.GetComponent<Renderer>().materials[1].SetTextureScale("_OcclusionMap", new Vector2(value.x, baseHeightFront.value * 2 * value.x));
        //new Vector2(ModelManager.baseHeightFront.value * 2 * value, value)
        //shose.heel2Obj.GetComponent<Renderer>().materials[1].mainTextureScale = new Vector2(value.x, baseHeightFront.value * 2 * value.x);

    }
    void MaterialTileUpdate2(Vector2 value)
    {
        //shose.heelObj.GetComponent<Renderer>().materials[0].mainTextureScale = new Vector2(value.x, baseHeightFront.value * 2 * value.x);//new Vector2(ModelManager.baseHeightFront.value * 2 * value, value)
        //shose.heel2Obj.GetComponent<Renderer>().materials[0].mainTextureScale = new Vector2(value.x, baseHeightFront.value * 2 * value.x);

        shose.heelObj.GetComponent<Renderer>().materials[0].SetTextureScale("_MainTex", new Vector2(value.x, baseHeightFront.value * 2 * value.x));
        shose.heelObj.GetComponent<Renderer>().materials[0].SetTextureScale("_BumpMap", new Vector2(value.x, baseHeightFront.value * 2 * value.x));
        shose.heelObj.GetComponent<Renderer>().materials[0].SetTextureScale("_OcclusionMap", new Vector2(value.x, baseHeightFront.value * 2 * value.x));
        //shose.heelObj.GetComponent<SkinnedMeshRenderer>().materials[0].SetTextureScale("_MainTex", new Vector2(value.x, baseHeightFront.value * 2 * value.x));//new Vector2(ModelManager.baseHeightFront.value * 2 * value, value)
        //shose.heel2Obj.GetComponent<SkinnedMeshRenderer>().materials[0].SetTextureScale("_MainTex", new Vector2(value.x, baseHeightFront.value * 2 * value.x));
    }
    void MaterialTileUpdate3(Vector2 value)
    {

        if (baseHeightFront.value >= .5f)
        {
           
            shose.baseMiddle.SetTextureScale("_MainTex", new Vector2(value.x, ((baseHeightFront.value * 2)) * value.x * 0.5f));
            shose.baseMiddle.SetTextureScale("_BumpMap", new Vector2(value.x, ((baseHeightFront.value * 2)) * value.x * 0.5f));
            shose.baseMiddle.SetTextureScale("_OcclusionMap", new Vector2(value.x, ((baseHeightFront.value * 2)) * value.x * 0.5f));
            
        }
        //new Vector2(ModelManager.baseHeightFront.value * 2 * value, value)

    }
    public void resetMorphValues()
    {
        resetObjPos();
        resetall = true;
        baseHeightFront.value = 0.5f;
        //  heelThickness.value=0.5f;
        heelThicknessDump.value = 0.5f;// 
        facethick.value = 0.5f;
    }
    public void QuiteApplication()
    {

        Application.Quit();
    }
    public void  CloseExitWin(bool status)
    {
        closeWin.gameObject.SetActive(status);
    }
}


//void MorphFn()
//  {
//    //  Debug.Log(baseHeightFrontTemp +"  "+ (float)System.Math.Round(baseHeightFront.value, 3));
//      if (baseHeightFrontTemp != (float)System.Math.Round(baseHeightFront.value, 3) || heelThicknessDump.value != heelThicknessDumpTemp)
//      {

//          ////1
//          if ((float)System.Math.Round(baseHeightFront.value, 3) < 0.5f)
//          {

//              shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(3, 100 - ((float)System.Math.Round(baseHeightFront.value, 3) * 2 * 100));
//              shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(3, 100 - ((float)System.Math.Round(baseHeightFront.value, 3) * 2 * 100));
//              shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(3, 100 - ((float)System.Math.Round(baseHeightFront.value, 3) * 2 * 100));
//              shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 0);
//              shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 0);
//              shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 0);

//              baseHeightFrontTemp = (float)System.Math.Round(baseHeightFront.value, 3);
//          }

//          else if ((float)System.Math.Round(baseHeightFront.value, 3) > 0.5f)
//          {
//              shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(3, 0);
//              shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(3, 0);
//              shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(3, 0);


//              shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, ((float)System.Math.Round(baseHeightFront.value, 3) - 0.5f) * 2 * 100);
//              shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, ((float)System.Math.Round(baseHeightFront.value, 3) - 0.5f) * 2 * 100);
//              shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, ((float)System.Math.Round(baseHeightFront.value, 3) - 0.5f) * 2 * 100);
//              baseHeightFrontTemp = (float)System.Math.Round(baseHeightFront.value, 3) ;

//          }

//          //////2
//          if (shose.faceObj.GetComponent<SkinnedMeshRenderer>().GetBlendShapeWeight(1) > ((float)System.Math.Round(baseHeightFront.value, 3) * 3 * 100))//o
//          {
//              shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, ((float)System.Math.Round(baseHeightFront.value, 3) * 3 * 100));//o
//              shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, ((float)System.Math.Round(baseHeightFront.value, 3) * 3* 100));//o
//              shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, ((float)System.Math.Round(baseHeightFront.value, 3) * 3 * 100));//o
//              heelThicknessDump.value = (float)System.Math.Round((float)System.Math.Round(baseHeightFront.value, 3) * 3, 3);//o
//              baseHeightFrontTemp = (float)System.Math.Round(baseHeightFront.value, 3);
//              heelThicknessDumpTemp = heelThicknessDump.value;

//          }
//          else
//          {

//              if (heelThicknessDump.value < heelThickness.value)
//              {
//                  shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, heelThicknessDump.value * 100);
//                  shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, heelThicknessDump.value * 100);
//                  shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, heelThicknessDump.value * 100);

//              }
//              else
//              {
//                  //heelThicknessDump.value =(float )System.Math.Round(heelThickness.value,2);
//                  heelThicknessDump.value = heelThickness.value;
//                  heelThicknessDumpTemp = heelThicknessDump.value;
//              }


//          }
//          //Debug.Log("YOYO");

//          heelThickness.value = ((float)System.Math.Round(baseHeightFront.value, 3) * 3);// +0.001f;//o

//      }

//      if (facethickTemp != facethick.value)
//      {

//          //////////3
//          if (facethick.value >= 0.2)
//          {
//              shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(5, (facethick.value - 0.2f) * 50);
//              shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(5, (facethick.value - 0.2f) * 50);
//              shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(5, (facethick.value - 0.2f) * 50);
//              shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(6, 0);

//          }
//          else
//          {
//              shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(6, 100 - (facethick.value * 2 * 100) / 2);
//          }
//          //shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(5, (facethick.value * 100) / 2);
//          //shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(5, (facethick.value * 100) / 2);
//          //shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(5, (facethick.value * 100) / 2);

//          //shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(6, (facethick.value * 100) / 2);

//          facethickTemp = facethick.value;
//      }



//      //shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(6, faceNode.value * 100);
//      //shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(6, faceNode.value * 100);
//      //shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(6, faceNode.value * 100);
//  }


