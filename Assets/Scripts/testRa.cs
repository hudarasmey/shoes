﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testRa : MonoBehaviour {

    public Camera cam;

    void Start()
    {
        cam = GetComponent<Camera>();
    }

    void Update()
    {
        RaycastHit hit;
       
        //Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        //if (Physics.Raycast(ray, out hit))//, Mathf.Infinity))
        //{
        //    Debug.Log(hit.collider.name);
        //}
        if (!Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit))
            return;
        
        MeshCollider meshCollider = hit.collider as MeshCollider;
        Renderer renderer = hit.collider.GetComponent<Renderer>();
        Debug.Log(meshCollider);
        if (meshCollider == null || meshCollider.sharedMesh == null)
            return;

        //Mesh mesh = meshCollider.sharedMesh;
        //Vector3[] vertices = mesh.vertices;
        //int[] triangles = mesh.triangles;
        //Vector3 p0 = vertices[triangles[hit.triangleIndex * 3 + 0]];
        //Vector3 p1 = vertices[triangles[hit.triangleIndex * 3 + 1]];
        //Vector3 p2 = vertices[triangles[hit.triangleIndex * 3 + 2]];
        //Transform hitTransform = hit.collider.transform;
        //p0 = hitTransform.TransformPoint(p0);
        //p1 = hitTransform.TransformPoint(p1);
        //p2 = hitTransform.TransformPoint(p2);
        //Debug.DrawLine(p0, p1);
        //Debug.DrawLine(p1, p2);
        //Debug.DrawLine(p2, p0);

        int materialIdx  = -1;
 
Mesh mesh  = meshCollider.sharedMesh;
int triangleIdx = hit.triangleIndex;
int lookupIdx1 = mesh.triangles[triangleIdx * 3];
int lookupIdx2 = mesh.triangles[triangleIdx * 3 + 1];
int lookupIdx3  = mesh.triangles[triangleIdx * 3 + 2];

        int subMeshesNr = mesh.subMeshCount;
        for (int i = 0; i<subMeshesNr; i++)
			{
			 var tr = mesh.GetTriangles(i);
             for (int j = 0; j < tr.Length; j += 3)
             {
                if (tr[j] == lookupIdx1 && tr[j+1] == lookupIdx2 && tr[j+2] == lookupIdx3) {
                    materialIdx = i;
                    break;
                }
			}
        
            if (materialIdx != -1) break;
        }
if (materialIdx != -1) {
   Debug.Log("-------------------- I'm using " + renderer.materials[materialIdx].name + " material(s)");
    //Debug.Log("-------------------- I'm using " +materialIdx+ " material(s)");
}
    }
}
