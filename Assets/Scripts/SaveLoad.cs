﻿using UnityEngine;
using System.Collections;
using System.IO;

public class SaveLoad : MonoBehaviour {
    public string ScreenshotName;
    public GameObject obj;
    Texture2D img;
	// Use this for initialization
	void Start () {
        //if (Application.platform == RuntimePlatform.Android)
        //{

        //    _FileLocation = Application.persistentDataPath;
        //}
        //else
        //{
        //    _FileLocation = Application.dataPath + "/save_screen";
        //}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    IEnumerator TakeScreenshot()
    {
        yield return new WaitForEndOfFrame();

        var width = Screen.width;
        var height = Screen.height;
        var tex = new Texture2D(width, height, TextureFormat.RGB24, false);
        // Read screen contents into the texture
        tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        tex.Apply();

        // save screen shot to device
        string screenShotPath = Application.persistentDataPath + "/T/" + ScreenshotName + ".png";
        byte[] screenshot = tex.EncodeToPNG(); //tex.EncodeToJPG();//
        File.WriteAllBytes(screenShotPath, screenshot);

        Debug.Log("KKKK");
    }

    //IEnumerator LoadImage()
    //{
    //    string screenShotPath = Application.persistentDataPath + "/T/" + ScreenshotName + ".png";
    //    #if UNITY_IPHONE || UNITY_ANDROID
    //    Debug.Log("mobile");
    //    WWW w = new WWW("file://" + screenShotPath);
    //    yield return w;
    //    if (w.error == null)
    //    {
    //        img = w.texture;
    //    }
    //    else
    //        print(w.error);
    //    #endif
    //  //s  return null;
    //}
    public void takeOne()
    {
       StartCoroutine( TakeScreenshot());
    }
    //public void loadOne()
    //{
    //     StartCoroutine( LoadImage());
    //    obj.GetComponent<Renderer>().material.mainTexture=img;
    //}

    public Texture2D NormalMapGen(Texture2D source, float strength, float distortion)
    {
        strength = 1f;
        distortion = 15;
        strength = Mathf.Clamp(strength, 0.0F, 55.0F);
        Texture2D normalTexture;
        float xLeft;
        float xRight;
        float yUp;
        float yDown;
        float yDelta;
        float xDelta;
        normalTexture = new Texture2D(source.width, source.height, TextureFormat.ARGB32, true);
        for (int y = 0; y < normalTexture.height; y++)
        {
            for (int x = 0; x < normalTexture.width; x++)
            {
                xLeft = source.GetPixel(x - 1, y).grayscale * strength;
                xRight = source.GetPixel(x + 1, y).grayscale * strength;
                yUp = source.GetPixel(x, y - 1).grayscale * strength;
                yDown = source.GetPixel(x, y + 1).grayscale * strength;
                xDelta = ((xLeft - (xRight)) + 1) * 0.5f;
                yDelta = ((yUp - yDown) + 1) * 0.5f;
                normalTexture.SetPixel(x, y, new Color(Mathf.Clamp01(sCurve(xDelta, distortion)), Mathf.Clamp01(sCurve(yDelta, distortion)), 1, Mathf.Clamp01(sCurve(yDelta, distortion))));
                //normalTexture.SetPixel(x,y,new Color(xDelta,yDelta,1.0f,yDelta));

            }


        }
        normalTexture.Apply();
        //Code for exporting the image to assets folder 
        //System.IO.File.WriteAllBytes("Assets/Normals/"+source.name+".png", normalTexture.EncodeToPNG());
        //#if UNITY_IPHONE || UNITY_ANDROID
         string screenShotPath = Application.persistentDataPath;
        //#endif
        // +"/T/" + ScreenshotName + ".png";
        System.IO.File.WriteAllBytes(screenShotPath + source.name + ".png", normalTexture.EncodeToPNG()); 
       // byte[] screenshot = tex.EncodeToPNG(); //tex.EncodeToJPG();//
       // File.WriteAllBytes(screenShotPath, screenshot);
        return normalTexture;
    }
    public static float sCurve(float x, float distortion)
    {
        return 1f / (1f + Mathf.Exp(-Mathf.Lerp(5, 10, distortion) * (x - 0.5f)));
    }
}
/*
 using UnityEngine;
 using System.Collections;
 
 public class Capture : MonoBehaviour 
 {
     // Store more screenshots...
     private int Screen_Shot_Count = 0;
     // Screenshot taking by touch the button.
     public GUITexture Capture_Model;
     // Check the Shot Taken/Not.
     private bool Shot_Taken = false;
     // Name of the File.
     private string Screen_Shot_File_Name;
 
     void Update()
     {
         if (Input.touches.Length > 0)       
         // Finger hit the button position.
         if(Capture_Model.HitTest (Input.GetTouch(0).position))
         {
             if (Input.GetTouch(0).phase == TouchPhase.Began)
             {
                 // Increament the screenshot count.
                 Screen_Shot_Count++;
                 // Save the screenshot name as Screenshot_1.png, Screenshot_2.png, with date format...
                 Screen_Shot_File_Name = "Screenshot__" + Screen_Shot_Count + System.DateTime.Now.ToString("__yyyy-MM-dd") + ".png";
                 Application.CaptureScreenshot(Screen_Shot_File_Name);
                 Shot_Taken = true;
             }
         }
         if(Shot_Taken == true)
         {
             string Origin_Path = System.IO.Path.Combine(Application.persistentDataPath, Screen_Shot_File_Name);
             // This is the path of my folder.
             string Path = "/mnt/sdcard/DCIM/Inde/" + Screen_Shot_File_Name;
             if(System.IO.File.Exists(Origin_Path))
             {
                 System.IO.File.Move(Origin_Path, Path);
                 Shot_Taken = false;
             }
         }
     }
 }



*/