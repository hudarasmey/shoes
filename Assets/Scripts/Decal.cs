﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Decal : MonoBehaviour {
    public Camera cam;

    float maxDist, floatOnTheWall=1;
    public GameObject decalHitWall, AccItemObj, bulletSpawn;
    public AccItem ItemBase,movableitem;
    public float waitTilNextFire, targetXRotation, targetYRotation, shootAngelRandmizationNotAim = 5, shootAngelRandmizationAim = .1f, ratioHoldHip;
    RaycastHit hit;
    private Plane drivingplane = new Plane(Vector3.up, Vector3.zero);
    string selectedObjName, selectedObjTag;
   public  Shose shose;
   public List<AccItem> accessoriesList;
  // public List<Vector2> accRotateAndScale;
   public int selectedItemIndex;
   public Slider sizeSlider, rotationSlider;
   public float rotateSliderValue, scaleSliderValue;
   public bool isActionActive;
   public bool StartDecalAction=false;
   public Material AccMaterial;
   public Color color= Color.white;
   public GameObject ParentObject;
   public Material selectedObjMaterial;
   public bool inColorCustom;
    public bool isclick,isdown;

	// Use this for initialization
	void Start () {
        color = Color.white;
	}
    //void LateUpdate()
    //{
    //    //update the mesh collider
        
    //}
    void ScaleAndRotateAcc()
    {
        // Debug.Log(shosePart.GetComponent<Renderer>().material.GetTextureScale("_MainTex")+"  "+ tileValue+"  "+ TemptileValue);

        //material tile
        if (accessoriesList.Count> 0)
        {

            if (accessoriesList[selectedItemIndex].scaleRatio != scaleSliderValue)
            //if (accRotateAndScale[selectedItemIndex].x != scaleSliderValue)
        {

            accessoriesList[selectedItemIndex].transform.localScale = new Vector3(1f + scaleSliderValue, 1f + scaleSliderValue, 1f + scaleSliderValue);
            
        //  accessoriesList[selectedItemIndex].transform.localScale = 
        }
            if (accessoriesList[selectedItemIndex].rotationRatio != rotateSliderValue)
        {
            ClampRotation(0, 360, rotateSliderValue*360);
        }
            //accessoriesList[selectedItemIndex] = new Vector2(scaleSliderValue, rotateSliderValue);
            accessoriesList[selectedItemIndex].rotationRatio = rotateSliderValue;
            accessoriesList[selectedItemIndex].scaleRatio = scaleSliderValue;
        }
        

    }
    //public void ResetCollider(GameObject Obj)
    //{
    ////    if (Input.GetKeyDown(KeyCode.R))
    ////    {
    //        Mesh baked = new Mesh();
    //        MeshCollider mcol = Obj.GetComponent<MeshCollider>();
    //        SkinnedMeshRenderer skin = Obj.GetComponent<SkinnedMeshRenderer>();

    //        skin.BakeMesh(baked);

    //        mcol.sharedMesh = null;
    //        mcol.sharedMesh = baked;

    //        //DestroyImmediate(shose.GetComponent<MeshCollider>());
    //        //shose.AddComponent<MeshCollider>();
    //        //Debug.Log("Rebuilt mesh");
    //        //SkinnedMeshRenderer skinnedMeshRenderer = shose.GetComponent<SkinnedMeshRenderer>();
    //        //shose.GetComponent<MeshCollider>().sharedMesh = null;
    //        //shose.GetComponent<MeshCollider>().sharedMesh = skinnedMeshRenderer.sharedMesh;

    //    //}
    //}

	// Update is called once per frame
	void Update () {
        if (StartDecalAction)
        {


            scaleSliderValue = sizeSlider.value;
            rotateSliderValue = rotationSlider.value;
            ScaleAndRotateAcc();
            RayOutFromCamera();

            if (accessoriesList.Count > 0)
            {
                if (selectedItemIndex >= 0)
                {
                    accessoriesList[selectedItemIndex].color = color;
                    selectedObjMaterial.color = color;

                }

            }

            if (Input.GetButtonUp("Fire1") )//&& (isclick == true || isdown == true))
            {
                isclick = false;
                isdown = false;
                isdown = false;
                movableitem = null;
                isActionActive = false;
                Debug.Log("clear");
            }
            if (Input.GetButtonDown("Fire1") && isActionActive && isclick == false)
            {
                Debug.Log("Down");
                // isActionActive = false;
                //if (Application.isEditor)
                //{
                //#if UNITY_EDITOR
                cam.GetComponent<WowCamera>().isMoveTouch = false;
                Debug.Log("Unity Editor   " + cam.GetComponent<WowCamera>().isMoveTouch + " " + cam.GetComponent<WowCamera>().mouseDown + "  " + !EventSystem.current.IsPointerOverGameObject());
                // Debug.Log(EventSystem.current.IsPointerOverGameObject() + " OP " + cam.GetComponent<WowCamera>().isMoveTouch + " " + cam.GetComponent<WowCamera>().mouseDown + " " + AccItemObj);
                if (!cam.GetComponent<WowCamera>().isMoveTouch && AccItemObj != null && !EventSystem.current.IsPointerOverGameObject())// &&cam.GetComponent<WowCamera>().mouseDown &&
                {
                    //       Debug.Log("OPa");
                    
                    if (decalHitWall.activeSelf == true)
                    {
                        decalHitWall.SetActive(false);
                    }
                    cam.GetComponent<WowCamera>().accessVis = true;
                    cam.GetComponent<WowCamera>().isMoveTouch = false;
                    isclick = true;
                    InstNewItem(AccItemObj, decalHitWall.transform.position, decalHitWall.transform.rotation, Vector3.one, accessoriesList, ParentObject);

                }
                else
                {
                    isclick = false;
                    Debug.Log("fOPa");
                    if (decalHitWall.activeSelf == true)
                    {
                        decalHitWall.SetActive(false);
                    }
                    cam.GetComponent<WowCamera>().accessVis = false;
                    cam.GetComponent<WowCamera>().isMoveTouch = false;
                }

//#endif

                //#if UNITY_ANDROID

                //#endif

//#if !UNITY_EDITOR && (UNITY_IOS || UNITY_ANDROID)
//     //  Debug.Log("ANDROID");
//                // Debug.Log(hit.collider + "  " + inColorCustom + "  " + hit.collider.gameObject.tag);
//                Debug.Log(EventSystem.current.IsPointerOverGameObject() + " OP " + cam.GetComponent<WowCamera>().isMoveTouch + " " + cam.GetComponent<WowCamera>().mouseDown + " " + AccItemObj);
//                if (!cam.GetComponent<WowCamera>().isMoveTouch && cam.GetComponent<WowCamera>().mouseDown && AccItemObj != null && !EventSystem.current.IsPointerOverGameObject())//!cam.GetComponent<WowCamera>().isMoveTouch &&cam.GetComponent<WowCamera>().mouseDown &&
//                {
//                       //    Debug.Log("OPa");
//                    InstNewItem(AccItemObj, decalHitWall.transform.position, decalHitWall.transform.rotation, Vector3.one, accessoriesList, ParentObject);
//                if (decalHitWall.activeSelf == true)
//                    {
//                        decalHitWall.SetActive(false);
//                    }
//                }
//                else
//                {
                 
//                     //  Debug.Log("fOPa");
//                    cam.GetComponent<WowCamera>().isMoveTouch = false;
//                if (decalHitWall.activeSelf == true)
//                    {
//                        decalHitWall.SetActive(false);
//                    }
//                }
//#endif

                // It can also be just
                //#if !UNITY_EDITOR && UNITY_IOS
                //    ...
                //#elif !UNITY_EDITOR && UNITY_ANDROID
                //    ...
                //#endif


            }
           
            if (Input.GetMouseButton(0) && isActionActive && isclick == true)
            {

                if (movableitem != null )
                {
                    cam.GetComponent<WowCamera>().accessVis = true;
                    Debug.Log("UP");
                    movableitem.transform.rotation = decalHitWall.transform.rotation;
                    movableitem.transform.position = decalHitWall.transform.position;
                    //isclick = false;
                    isdown = true;
                   
                        cam.GetComponent<WowCamera>().accBool = true;
                   
                    
                }

            }
          


        }
        //if (Input.GetKeyDown(KeyCode.A))
        //{
        //    incSelectedItem();
        //}
        //if (Input.GetKeyDown(KeyCode.S))
        //{
        //    decSelectedItem();
        //}

        //MeshCollider mc = shose.GetComponent<MeshCollider>();
        //if (mc == null)
        //    mc = (MeshCollider)shose.AddComponent(typeof(MeshCollider));
        //else
        //{
        //    mc.sharedMesh = null;
        //    mc.sharedMesh = mesh;
        //}
       // Vector2 mousePosition = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
       // Ray mseRay = Camera.main.ScreenPointToRay(mousePosition);
       // Debug.DrawRay(mseRay.origin, Input.mousePosition, Color.green);
       //// Debug.DrawRay(transform.position, (Input.mousePosition), Color.red, 10);
       // if (Physics.Raycast(transform.position, (Input.mousePosition), out hit, maxDist))
       // {
       //         if(hit.transform.tag=="Wall")
       //         {
       //             if(decalHitWall)
       //                 {
                        
                        
       //                  //   Instantiate(decalHitWall,hit.point+(hit.normal*floatOnTheWall),Quaternion.LookRotation(hit.normal));
       //                     //Destroy(gameObject);
       //                 }
       //         }
		
       // }
 
	}
    public void RemoveItemFn()
    {
        DestroyItem(selectedItemIndex);
    }
    void RayOutFromCamera()
    {
        //  RaycastHit hit;

        //Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        //if (Physics.Raycast(ray, out hit))//, Mathf.Infinity))
        //{
        //    Debug.Log(hit.collider.name);
        //}
        if (!Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit) || inColorCustom )
       {
              if (decalHitWall.activeSelf == true)
                {
                    decalHitWall.SetActive(false);
                }
            cam.GetComponent<WowCamera>().accessVis = false;
            selectedObjName = "";
                isActionActive = false;

          //  Debug.Log("PLPLPmm");
            return;
        }


        MeshCollider meshCollider = hit.collider as MeshCollider;
        Renderer renderer = hit.collider.GetComponent<Renderer>();
        // Debug.Log(meshCollider);
        if (meshCollider == null || meshCollider.sharedMesh == null)
            return;

        int materialIdx = -1;
        if (hit.collider.gameObject.tag != "AbleAcc")
        {
            //   Debug.Log("!AbleAcc");
            return;
        }
        Mesh mesh = meshCollider.sharedMesh;


        int subMeshesNr = mesh.subMeshCount;
        Debug.Log("!AbleAcc" + subMeshesNr);
        for (int i = 0; i < subMeshesNr; i++)
        {
            var tr = mesh.GetTriangles(i);
            int triangleIdx = hit.triangleIndex;
            int lookupIdx1 = mesh.triangles[triangleIdx * 3];
            int lookupIdx2 = mesh.triangles[triangleIdx * 3 + 1];
            int lookupIdx3 = mesh.triangles[triangleIdx * 3 + 2];
            for (int j = 0; j < tr.Length; j += 3)
            {

                if (tr[j] == lookupIdx1 && tr[j + 1] == lookupIdx2 && tr[j + 2] == lookupIdx3)
                {
                    materialIdx = i;
                    break;
                }
            }

            if (materialIdx != -1) break;
        }
        if (materialIdx != -1)
        {
            if (hit.collider != null && !inColorCustom)
            {
                isActionActive = true;
                Debug.Log(hit.collider.gameObject.tag);
                if (hit.collider.gameObject.tag == "AbleAcc" && (renderer.materials[materialIdx].name == "face_out_mat" || renderer.materials[materialIdx].name == "face_out_mat (Instance)" || renderer.materials[materialIdx].name == "base_side_mat" || renderer.materials[materialIdx].name == "base_side_mat (Instance)" || renderer.materials[materialIdx].name == "heel_back_mat" || renderer.materials[materialIdx].name == "heel_back_mat (Instance)" || renderer.materials[materialIdx].name == "Marker_Texture"))//|| renderer.materials[materialIdx].name == "" /// (hit.collider.name == shose.faceObj.transform.name || hit.collider.name == shose.heelObj.transform.name)//|| hit.collider.name == shose.baseObj.transform.name)
                {
                    cam.GetComponent<WowCamera>().accessVis = true;
                    Debug.Log(hit.collider.name);
                    //#if !UNITY_EDITOR && (UNITY_IOS || UNITY_ANDROID)
                    //                                if (decalHitWall.activeSelf == true)
                    //                                {
                    //                                    decalHitWall.SetActive(false);
                    //                                }

                    //#endif
#if UNITY_EDITOR
                    //if (!cam.GetComponent<WowCamera>().isMoveTouch)//decalHitWall.activeSelf != true && 
                    //{
                    Debug.Log(hit.collider.gameObject.tag + "true");
                    decalHitWall.SetActive(true);
                    //cam.GetComponent<WowCamera>().tarhget = hit.collider.gameObject.transform;
                    //cam.GetComponent<WowCamera>().accBool = true;
                    //}
                    //else
                    //{
                    //    Debug.Log(hit.collider.gameObject.tag + "false");
                    //    decalHitWall.SetActive(false);
                    //}
#endif

                    decalHitWall.transform.position = hit.point;// +(hit.normal * floatOnTheWall);
                    decalHitWall.transform.rotation = Quaternion.LookRotation(-hit.normal);
                    //   goPos(hit.point);
                }
                //else
                //{
                //    if (decalHitWall.activeSelf == true)
                //    {
                //        Debug.Log(hit.collider.gameObject.tag + "false2");
                //        decalHitWall.SetActive(false);
                //        cam.GetComponent<WowCamera>().accessVis = false;
                //    }
                //    cam.GetComponent<WowCamera>().accessVis = false;
                //    selectedObjName = "";
                //    isActionActive = false;
                //}
                //                // Debug.Log("-------------------- I'm using " + renderer.materials[materialIdx].name + " material(s)");
                //                //Debug.Log("-------------------- I'm using " +materialIdx+ " material(s)");
            }
         
               
            if (hit.collider.gameObject.GetComponent<EachObjControl>() != null && hit.collider.gameObject.GetComponent<SkinnedMeshRenderer>().materials != hit.collider.gameObject.GetComponent<EachObjControl>().mats)
            {
                Debug.Log("-------------------");
                hit.collider.gameObject.GetComponent<SkinnedMeshRenderer>().materials = hit.collider.gameObject.GetComponent<EachObjControl>().mats;
            }
               
            
        }
    }
    //void goPos(Vector3 pos)
    //{
    //    Vector3 targetPos = cam.GetComponent<WowCamera>().targetPos;
    //    //find the vector pointing from our position to the target
    //    //  Vector3 _direction = (new Vector3(targetPos.x, tarhget.position.y, targetPos.z) - tarhget.position).normalized;
    //    Vector3 _direction = (new Vector3(targetPos.x, pos.y, targetPos.z) - pos).normalized;
    //    //create the rotation we need to be in to look at the target
    //    Quaternion _lookRotation = Quaternion.LookRotation(_direction);

    //    //    float angle = Vector3.RotateTowards(relativePos, Vector3.forward);
    //    // Debug.Log(_lookRotation);
    //    //if (new Quaternion(Mathf.Abs(transform.rotation.x), Mathf.Abs(transform.rotation.y), Mathf.Abs(transform.rotation.z), Mathf.Abs(transform.rotation.w)) != new Quaternion(Mathf.Abs(_lookRotation.x), Mathf.Abs(_lookRotation.y), Mathf.Abs(_lookRotation.z), Mathf.Abs(_lookRotation.w)))
    //    //{

    //    //cam.GetComponent<WowCamera>().transform.rotation = Quaternion.Lerp(cam.GetComponent<WowCamera>().transform.rotation, _lookRotation, Time.deltaTime * 2f);
    //        // Debug.Log("kodfkod" + transform.rotation + "  " + _lookRotation);
    //        if (pos.y < cam.GetComponent<WowCamera>().maxHeight)
    //        {
    //            targetPos = Vector3.Lerp(targetPos, new Vector3(targetPos.x, pos.y, targetPos.z), Time.deltaTime * 2f);
    //        }
    //        else
    //        {
    //            targetPos = Vector3.Lerp(targetPos, new Vector3(targetPos.x, cam.GetComponent<WowCamera>().maxHeight, targetPos.z), Time.deltaTime * 2f);
    //        }
    //        //cam.GetComponent<WowCamera>().distance = 50;
    //        cam.GetComponent<WowCamera>().transform.position = targetPos - (cam.GetComponent<WowCamera>().transform.rotation * Vector3.forward * (cam.GetComponent<WowCamera>().desiredDistance));//* NumberTo(desiredDistance, deafultAccDis, Speed)
    //        cam.GetComponent<WowCamera>().SideRotation();

    //    //}
    //}
    //void RayOutFromCamera()
    //{
    //    //Ray ray = cameras[Index].camera.ScreenPointToRay(Input.mousePosition);

    //        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
    //        float ent = 1.0f;

    //        //if (drivingplane.Raycast(ray, out ent))
    //        //{
    //        //    Vector3 hitPoint = ray.GetPoint(ent);
    //        //    Debug.DrawRay(ray.origin, ray.direction * ent, Color.green);
    //        //    //Debug.DrawRay (transform.position, hitpoint);
    //        //}
    //        //else
    //        //{
    //        //    Debug.DrawRay(ray.origin, ray.direction * 10, Color.red);

    //        //}
    //        if (Physics.Raycast(ray, out hit))//, Mathf.Infinity))
    //        {

    //            //Instantiate(decalHitWall, hit.point + (hit.normal * floatOnTheWall), Quaternion.LookRotation(hit.normal));

    //            if (hit.collider != null && !inColorCustom)
    //            {
    //                isActionActive = true;
    //                if (hit.collider.gameObject.tag=="AbleAcc") /// (hit.collider.name == shose.faceObj.transform.name || hit.collider.name == shose.heelObj.transform.name)//|| hit.collider.name == shose.baseObj.transform.name)
    //                {

    //                   // Debug.Log(hit.collider.name);
    //                    if (decalHitWall.activeSelf != true)
    //                    {
    //                        decalHitWall.SetActive(true);
    //                    }

    //                    decalHitWall.transform.position = hit.point;// +(hit.normal * floatOnTheWall);
    //                    decalHitWall.transform.rotation = Quaternion.LookRotation(-hit.normal);
    //                }
    //                //else
    //                //{
    //                //    Debug.Log(decalHitWall.activeSelf);
    //                //    if (decalHitWall.activeSelf == true)
    //                //    {
    //                //        decalHitWall.SetActive(false);
    //                //    }

    //                //}

    //                //selectedObjName = hit.collider.name;
    //                //selectedObjTag = hit.collider.tag.ToString();
    //                //			if(Input.GetMouseButtonDown(0))
    //                //		{
    //                //			Debug.Log(hit.collider.name);
    //                //			//hit.collider.gameObject.renderer.material.color =Color.yellow;
    //                //			if(hit.collider.name == "horus statue"){
    //                //								if(info != null) Destroy(info);
    //                //								
    //                //								info2camera = transform.position - hit.transform.position;
    //                //								Quaternion infoRotation = Quaternion.LookRotation(info2camera);//, Vector3.Cross(info2camera, -transform.right));
    //                //							
    //                //								info = (GameObject)Instantiate(textPrefab, 
    //                //									new Vector3(hit.transform.position.x + 3.5f, 2.3f, hit.transform.position.z), infoRotation);
    //                //								infoAppear = true;
    //                //								
    //                //								
    //                //								//info.transform.rotation = infoRotation;
    //                //								Debug.Log(transform.position);
    //                //							//	transform.position += Time.deltaTime * 100 * new Vector3(infoRotation.x, infoRotation.y, infoRotation.z);
    //                //								Debug.Log(transform.position);
    //                //								//Quaternion.Euler(transform.rotation.x + 180, transform.rotation.z + 180, transform.rotation.y + 180));	
    //                //								//Debug.Log("cameras: "+transform.rotation.eulerAngles + "plane: "+ info.transform.rotation.eulerAngles);
    //                //							}		
    //                //		}
    //            }
    //            //else
    //            //{

    //            //}
    //        }
    //        else
    //        {
    //           // Debug.Log(decalHitWall.activeSelf);
    //            if (decalHitWall.activeSelf == true)
    //            {
    //                decalHitWall.SetActive(false);
    //            }
    //            selectedObjName = "";
    //            isActionActive = false;
    //        }



    //}
    public void DestroyItem(int indexAccItem)//destroy hero and check if you win or lose 
    {
        if (accessoriesList.Count > 0)
        {
            Destroy(accessoriesList[indexAccItem].gameObject);
            accessoriesList.Remove(accessoriesList[indexAccItem]);
           
          //  accRotateAndScale.Remove(accRotateAndScale[indexAccItem]);
            if (selectedItemIndex > 0)
            {
                selectedItemIndex = indexAccItem - 1;
            }
            else
            {

                selectedItemIndex = accessoriesList.Count - 1;


            }
            MarkSelectedItem();
        }
       
    }
    public void InstNewItem(GameObject heroPrefab, Vector3 position,Quaternion rotate,Vector3 scale,  List<AccItem> List,GameObject parentObj) // inst new hero
    {

       // AccItem acc = new AccItem();

        AccItem newItem = Instantiate(ItemBase, position, rotate) as AccItem;
        movableitem = newItem;
        GameObject internalItem = Instantiate(heroPrefab,Vector3.zero, Quaternion.identity) as GameObject;//new Vector3(sizeSlider.value ,sizeSlider.value,sizeSlider.value)
        
        AccMaterial = internalItem.GetComponent<MeshRenderer>().material;
        Material msat = new Material(AccMaterial);
        msat.color = color;
        internalItem.GetComponent<MeshRenderer>().material = msat;
        selectedObjMaterial = internalItem.GetComponent<MeshRenderer>().material;

        Transform[] allChildren = newItem.GetComponentsInChildren<Transform>();
        foreach (Transform child in allChildren)
        {
            //   Debug.Log(child.name+"dPP");

            if (child.gameObject.CompareTag("ItemMarker"))
            {

                internalItem.transform.parent = child.transform;
            }
               
        }
        //internalItem.transform.parent = newItem.transform;
        internalItem.transform.localPosition = Vector3.zero;
        internalItem.transform.localRotation = Quaternion.Euler(new Vector3(-90,0,0));
        newItem.transform.localScale = scale;
            //newHero.GetComponent<HeroScript>().Game = this;
            //newHero.GetComponent<HeroScript>().Speed = heroSpeed;
            //newHero.GetComponent<HeroScript>().heroType = type;

        //accRotateAndScale.Add(new Vector2(1, 1));
        /// Acc VAr
        newItem.innerObj = internalItem;
        newItem.itemPosition = newItem.transform.position;

        List.Add(newItem);

            selectedItemIndex = accessoriesList.Count - 1;
            MarkSelectedItem();

            newItem.transform.parent = parentObj.transform;
        cam.GetComponent<WowCamera>().tarhget = newItem.gameObject.transform;
        //cam.GetComponent<WowCamera>().accBool = true;
    }
    public void SetScale(GameObject obj, Vector3 scale)
    {
        obj.transform.localScale = scale;
    }
    public void incSelectedItem()
    {
        //Debug.Log("P");
        if (selectedItemIndex < accessoriesList.Count-1)
        {
            selectedItemIndex = selectedItemIndex + 1;
        }
        else
        {
            selectedItemIndex = 0;
        }

        rotationSlider.value = accessoriesList[selectedItemIndex].rotationRatio;
        sizeSlider.value = accessoriesList[selectedItemIndex].scaleRatio;
        cam.GetComponent<WowCamera>().tarhget =accessoriesList[selectedItemIndex].gameObject.transform;
        cam.GetComponent<WowCamera>().accBool = true;
        //rotationSlider.value  = accRotateAndScale[selectedItemIndex].y;
        //sizeSlider.value  = accRotateAndScale[selectedItemIndex].x;
        MarkSelectedItem();
    }
    public void decSelectedItem()
    {
        if (selectedItemIndex > 0)
        {
            selectedItemIndex = selectedItemIndex - 1;
        }
        else
        {
            selectedItemIndex = accessoriesList.Count - 1;
        }
        //rotationSlider.value  = accRotateAndScale[selectedItemIndex].y;
        //sizeSlider.value  = accRotateAndScale[selectedItemIndex].x;
        rotationSlider.value = accessoriesList[selectedItemIndex].rotationRatio;
        sizeSlider.value = accessoriesList[selectedItemIndex].scaleRatio;
        cam.GetComponent<WowCamera>().tarhget = accessoriesList[selectedItemIndex].gameObject.transform;
        cam.GetComponent<WowCamera>().accBool = true;
        MarkSelectedItem();
    }

    void ClampRotation(float minAngle, float maxAngle, float clampAroundAngle = 0)
    {
        //clampAroundAngle is the angle you want the clamp to originate from
        //For example a value of 90, with a min=-45 and max=45, will let the angle go 45 degrees away from 90

        ////Adjust to make 0 be right side up
        //clampAroundAngle += 180;

        ////Get the angle of the z axis and rotate it up side down
        //float y = accessoriesList[selectedItemIndex].transform.rotation.eulerAngles.y - clampAroundAngle;

        //y = WrapAngle(y);

        ////Move range to [-180, 180]
        //y -= 180;

        ////Clamp to desired range
        //y = Mathf.Clamp(y, minAngle, maxAngle);

        ////Move range back to [0, 360]
        //y += 180;

        //Set the angle back to the transform and rotate it back to right side up
        Transform[] allChildren = accessoriesList[selectedItemIndex].GetComponentsInChildren<Transform>();
        //Debug.Log("dPP");
        foreach (Transform child in allChildren)
        {
            //   Debug.Log(child.name+"dPP");
            
            if (child.gameObject.CompareTag("ItemMarker"))
                child.transform.localRotation = Quaternion.Euler(child.transform.localRotation.eulerAngles.x, child.transform.localRotation.eulerAngles.y, rotationSlider.value * 360);
        }
        //accessoriesList[selectedItemIndex].transform.localRotation = Quaternion.Euler(accessoriesList[selectedItemIndex].transform.rotation.eulerAngles.x, accessoriesList[selectedItemIndex].transform.rotation.eulerAngles.y, rotationSlider.value * 360);
    }

    //Make sure angle is within 0,360 range
    float WrapAngle(float angle)
    {
        //If its negative rotate until its positive
        while (angle < 0)
            angle += 360;

        //If its to positive rotate until within range
        return Mathf.Repeat(angle, 360);
    }

    void MarkSelectedItem()
    {
       
        for (int i = 0; i < accessoriesList.Count; i++)
        {

            if (i==selectedItemIndex)
                {
                    MeshRenderer[] allChildren = accessoriesList[i].GetComponentsInChildren<MeshRenderer>();
                  //  Debug.Log("PPP");
                    //gos2 = GetChildGameObjects(other);

                    foreach (MeshRenderer child in allChildren)
                    {
                        if (child.gameObject.CompareTag("ItemMarker"))
                            child.enabled = true;
                    }
                    
                }
            else
	            {
                    MeshRenderer[] allChildren = accessoriesList[i].GetComponentsInChildren<MeshRenderer>();
                    //Debug.Log("dPP");
                    foreach (MeshRenderer child in allChildren)
                    {
                     //   Debug.Log(child.name+"dPP");

                        if (child.gameObject.CompareTag("ItemMarker"))
                            child.enabled = false;
                    }

	            }
            
        }
    }
    public void resetAcc()
    {
       
        if (accessoriesList.Count > 0)
        {
            for (int i = 0; i < accessoriesList.Count; i++)
            {
                Destroy(accessoriesList[i].gameObject);
                //accessoriesList.Remove(accessoriesList[i]);
            }
        }
        accessoriesList.Clear();
        

       selectedItemIndex = -1;

    }
}
//public class UILineRenderer : MaskableGraphic
//{
//    public float LineThikness = 2;
//    public bool UseMargins;
//    public Vector2 Margin;
//    public Vector2[] Points;

//    protected override void OnFillVBO(List vbo)
//    {
//        if (Points == null || Points.Length < 2)
//            Points = new[] { new Vector2(0, 0), new Vector2(1, 1) };

//        var sizeX = rectTransform.rect.width;
//        var sizeY = rectTransform.rect.height;
//        var offsetX = -rectTransform.pivot.x * rectTransform.rect.width;
//        var offsetY = -rectTransform.pivot.y * rectTransform.rect.height;

//        if (UseMargins)
//        {
//            sizeX -= Margin.x;
//            sizeY -= Margin.y;
//            offsetX += Margin.x / 2f;
//            offsetY += Margin.y / 2f;
//        }

//        vbo.Clear();

//        Vector2 prevV1 = Vector2.zero;
//        Vector2 prevV2 = Vector2.zero;

//        for (int i = 1; i < Points.Length; i++)
//        {
//            var prev = Points[i - 1];
//            var cur = Points;
//            prev = new Vector2(prev.x * sizeX + offsetX, prev.y * sizeY + offsetY);
//            cur = new Vector2(cur.x * sizeX + offsetX, cur.y * sizeY + offsetY);

//            float angle = Mathf.Atan2(cur.y - prev.y, cur.x - prev.x) * 180f / Mathf.PI;

//            var v1 = prev + new Vector2(0, -LineThikness / 2);
//            var v2 = prev + new Vector2(0, +LineThikness / 2);
//            var v3 = cur + new Vector2(0, +LineThikness / 2);
//            var v4 = cur + new Vector2(0, -LineThikness / 2);

//            v1 = RotatePointAroundPivot(v1, prev, new Vector3(0, 0, angle));
//            v2 = RotatePointAroundPivot(v2, prev, new Vector3(0, 0, angle));
//            v3 = RotatePointAroundPivot(v3, cur, new Vector3(0, 0, angle));
//            v4 = RotatePointAroundPivot(v4, cur, new Vector3(0, 0, angle));

//            if (i > 1)
//                SetVbo(vbo, new[] { prevV1, prevV2, v1, v2 });

//            SetVbo(vbo, new[] { v1, v2, v3, v4 });

//            prevV1 = v3;
//            prevV2 = v4;
//        }
//    }

//    protected void SetVbo(List vbo, Vector2[] vertices)
//    {
//        for (int i = 0; i < vertices.Length; i++)
//        {
//            var vert = UIVertex.simpleVert;
//            vert.color = color;
//            vert.position = vertices;
//            vbo.Add(vert);
//        }
//    }

//    public Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles)
//    {
//        Vector3 dir = point - pivot; // get point direction relative to pivot
//        dir = Quaternion.Euler(angles) * dir; // rotate it
//        point = dir + pivot; // calculate rotated point
//        return point; // return it
//    }
//}