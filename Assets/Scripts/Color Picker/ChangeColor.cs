﻿using UnityEngine;
using System.Collections;

public class ChangeColor : MonoBehaviour {

	public Renderer r;

	void Awake()
	{
		r = GetComponent<Renderer> ();
	}

	public void SetColor ( Color col) 
	{
		r.material.color = col;
	}

	public Color GetColor()
	{
		return r.material.color;
	}

}
