using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class ScrollCellTest : MonoBehaviour
{
    public TextMeshProUGUI cellLabel;
    public Text  DateTime;
    public Texture2D image;
    public Texture2D dafaultImage;
    public Sprite imageContent;
    public ScrollListTest relatedList;
    public int index;
    public Material Mat, kource;
    public Shader shader;
    public Image imageContainer;
    public Rect rect;
    public Image marker;
    public Color color;
    public bool notSetMarker;
    public int price;

    public SelectedMenuType menuType;// = SelectedMenuType.Texture;
    public UnlockElement lockManager;
    // public bool deleteit;
    //public Sprite sprite;
    GameObject SoundManagereObj;
    SoundManagerClass SoundManScript;

    GameObject particalManagereObj;
    ParticalManager particalManScript;
    
    void Awake()
    {

        SoundManagereObj = GameObject.FindGameObjectWithTag("SoundManager");
        particalManagereObj = GameObject.FindGameObjectWithTag("ParticalManager");
        SoundManScript = (SoundManagerClass)SoundManagereObj.GetComponent("SoundManagerClass");
        particalManScript = (ParticalManager)particalManagereObj.GetComponent("ParticalManager");
    }
    void Start()
    {
        color = Color.white;
        menuType = relatedList.menuType;
        lockManager = gameObject.GetComponent<UnlockElement>();
        //       Debug.Log(GameObject.FindGameObjectWithTag("UnlockWin").name);

        if (relatedList.Bank.isSetPrice && relatedList.menuType == SelectedMenuType.Texture)
        {
            //lockManager.unlockWindow = lockManager.shop.window;// GameObject.FindGameObjectWithTag("UnlockWin");
            price = getPrice(index);
        }
        else
        {
            if (lockManager != null)
            {
                lockManager.notPriced = true;

            }

            //   lockManager.gameObject.SetActive(false);
        }
        //rect = gameObject.GetComponent<RectTransform>().rect;
        ////GetComponent<Image>().sprite
        //imageContent = Sprite.Create(image, new Rect(0, 0, image.width, image.height), new Vector2(0.5f, 0.5f), 1);

        //imageContent.sprite = sprite;
        //if (index == 0 )
        //{
        //  // clickit();
        //   relatedList.selectedIndex = index;
        //SetMarker(true);
        //relatedList.clearMarker(index);
        //}


    }

    void Update()
    {

        //if (Input.GetKeyDown(KeyCode.L))
        //{
        //    SetSpriteit();
        //}
        //if (index == 1 && Input.GetKeyDown(KeyCode.P))
        //{
        //    clickit();
        //}

    }
    public void SetSpriteColor(Color _color)
    {
        // imageContent = Sprite.Create(dafaultImage, new Rect(0, 0, dafaultImage.width, dafaultImage.height), new Vector2(0.5f, 0.5f), 1);
        imageContainer.sprite = imageContent;
        color = imageContainer.color = new Color(_color.r, _color.g, _color.b, 1);
        //GetComponent<Image>().sprite = imageContent;
        //color= GetComponent<Image>().color = new Color(_color.r,_color.g,_color.b,1) ;
    }

    public void SetSprite(Sprite imageContent)
    {
        //GetComponent<Image>().sprite = Sprite.Create(image, rect, new Vector2(0.5f, 0.5f), 100);
        imageContainer.sprite = imageContent;
        //GetComponent<Image>().sprite = imageContent;
    }
    public void SetSpriteit()
    {
        //GetComponent<Image>().sprite = Sprite.Create(image, rect, new Vector2(0.5f, 0.5f), 100);
        imageContainer.sprite = imageContent;
        //GetComponent<Image>().sprite = imageContent;
    }
    public void SetMarker(bool _bool)
    {
        //  Debug.Log("PPOO");

        if (!notSetMarker)
        {
            //      Debug.Log(index + " PPOO" + _bool);
            marker.gameObject.SetActive(_bool);
           // marker.rectTransform.sizeDelta = relatedList.grid.cellSize;//.cellSize;// gameObject.GetComponent<RectTransform>().sizeDelta; 
        }
       

    }
    public void SetTexture(Sprite imagemat)
    {
        // imageContent = Sprite.Create(imagemat, new Rect(0, 0, imagemat.width, imagemat.height), new Vector2(0.5f, 0.5f), 1);
        imageContent = imagemat;
        SetSpriteit();
        //     price=getPrice(index);

        //if (shader != null)
        //{
        //   // Mat = new Material(kource);
        //    //Image img = GetComponent<Image>();
        //    Material Mati = Instantiate(GetComponent<Image>().material);
        //    //img.material = Mati;
        //    Mati.mainTexture = imagemat;
        //    GetComponent<Image>().material = Mat;
        //       Material material = new Material (Shader.Find("Diffuse"));              // Common names are: "Diffuse", "Bumped Diffuse",
        //                                                                        // "VertexLit", "Transparent/Diffuse" etc.
        //material.SetTextureScale("_MainTex", new Vector2(100,0));               // "_MainTex" is the main diffuse texture.
        //                                                                        // This can also be accessed via mainTextureScale property. 
        //                                                                        // "_BumpMap" is the normal map. 
        //                                                                        // "_Cube" is the reflection cubemap.
        //material.mainTexture = Resources.Load("Floor") as Texture2D;            // Texture resource name
        //AssetDatabase.CreateAsset(material,("Assets/Resources/material.mat"));  // material path in assets folder
        //floor.renderer.material = material;
        //// http://answers.unity3d.com/questions/980924/ui-mask-with-shader.html
        //Mati.mainTexture = imagemat;
        //GetComponent<Image>().material = Mat;
        //}
    }
    public int getPrice(int _index)
    {
        return relatedList.Bank.priceArr[_index];
    }
    public void clickit()
    {
        //lockManager.unlockWindow = lockManager.shop.window;
        if (lockManager != null)
        {
            Debug.Log("isclicked" + lockManager.isLocked);
            if (relatedList.tempClickedIndex != index || relatedList.selectedIndex == -1)
            {
                Debug.Log(relatedList.menuType);

                if (lockManager.isLocked)
                {
                    //relatedList.savedIndex = relatedList.selectedIndex;

                    SoundManScript.PlaySound(SoundManScript.Botton_Clip);
                    relatedList.selectedIndex = index;
                    relatedList.IsAction = true;
                    relatedList.tempClickedIndex = relatedList.selectedIndex;
                    SoundManScript.PlaySound(SoundManScript.lockedListBotton_Clip);
                    // SoundManScript.PlaySound(SoundManScript.ListBotton_Clip);
                    lockManager.unlockWindowSc();
                    Debug.Log("clearMarker0 " + index);
                    //  SetMarker(true);
                    //relatedList.clearMarker(index);
                    relatedList.MarkAgain(index);
                    relatedList.Texture2d = imageContent;
                    relatedList.isParchaseWin = true;
                    lockManager.shop.window.SetActive(true);
                    lockManager.shop.unlocksureWindow.SetActive(false);
                }
                else
                {
                    if (relatedList.menuType == SelectedMenuType.Color)
                    {
                        // SoundManScript.PlaySound(SoundManScript.lockedListBotton_Clip);
                        SoundManScript.PlaySound(SoundManScript.Botton_Clip);
                        //SoundManScript.PlaySound(SoundManScript.ListBotton_Clip);
                        //particalManScript.runParticalOnce(particalManScript.p1);
                        //particalManScript.runParticalOnce(particalManScript.p2);
                        relatedList.selectedIndex = index;
                        relatedList.tempClickedIndex = relatedList.selectedIndex;
                        relatedList.savedIndex = relatedList.selectedIndex;
                        relatedList.IsAction = true;
                        SetMarker(true);
                        Debug.Log("clearMarker2 " + index);
                        //Debug.Log("clearMarker " + index);
                        relatedList.MarkAgain(index);
                        relatedList.Texture2d = imageContent;
                        

                    }
                    else if (relatedList.menuType == SelectedMenuType.Select)
                    {

                        // SoundManScript.PlaySound(SoundManScript.lockedListBotton_Clip);
                        SoundManScript.PlaySound(SoundManScript.Botton_Clip);
                        //SoundManScript.PlaySound(SoundManScript.ListBotton_Clip);
                        //particalManScript.runParticalOnce(particalManScript.p1);
                        //particalManScript.runParticalOnce(particalManScript.p2);

                        relatedList.selectedIndex = index;
                        relatedList.tempClickedIndex = relatedList.selectedIndex;
                        relatedList.savedIndex = relatedList.selectedIndex;
                        relatedList.IsAction = true;
                        SetMarker(true);
                         Debug.Log("select " + index);
                        relatedList.MarkAgain(index);
                      
                      
                      
                    }
                    else
                    {
                        // SoundManScript.PlaySound(SoundManScript.lockedListBotton_Clip);
                        SoundManScript.PlaySound(SoundManScript.Botton_Clip);
                        //SoundManScript.PlaySound(SoundManScript.ListBotton_Clip);
                        //particalManScript.runParticalOnce(particalManScript.p1);
                        //particalManScript.runParticalOnce(particalManScript.p2);

                        relatedList.selectedIndex = index;
                        relatedList.tempClickedIndex = relatedList.selectedIndex;
                        relatedList.savedIndex = relatedList.selectedIndex;
                        relatedList.IsAction = true;
                        SetMarker(true);
                       // Debug.Log("clearMarker1 " + index);
                        relatedList.MarkAgain(index);
                        relatedList.Texture2d = imageContent;
                        if (lockManager != null && relatedList.selectedIndex != -1)
                        {
                            lockManager.shop.window.SetActive(false);
                            lockManager.shop.unlocksureWindow.SetActive(false);
                        }
                        relatedList.isParchaseWin = false;
                    }
                }
            }
        }
        else
        {
            if (relatedList.menuType==SelectedMenuType.Color)
            {
                Debug.Log("isclickedcolor");
                //SoundManScript.PlaySound(SoundManScript.ListBotton_Clip);
                SoundManScript.PlaySound(SoundManScript.Botton_Clip);
                relatedList.selectedIndex = index;
                relatedList.tempClickedIndex = relatedList.selectedIndex;
                relatedList.IsAction = true;
                Debug.Log("clearMarker3 " + index);
                SetMarker(true);
                relatedList.clearMarker(index);
                relatedList.Texture2d = imageContent;
                
            }
            else
            {
                Debug.Log("isclicked");
                //SoundManScript.PlaySound(SoundManScript.ListBotton_Clip);
                SoundManScript.PlaySound(SoundManScript.Botton_Clip);
                relatedList.selectedIndex = index;
                relatedList.tempClickedIndex = relatedList.selectedIndex;
                relatedList.IsAction = true;
                Debug.Log("clearMarker4 " + index);
                SetMarker(true);
                relatedList.clearMarker(index);
                relatedList.Texture2d = imageContent;
                relatedList.isParchaseWin = false;
            }
            
        }

        //   Debug.Log("isclicked" + gameObject.name);
    }
    public void clickitBack()//bool backToMain)
    {

            Debug.Log("isclickedBack");
            //SoundManScript.PlaySound(SoundManScript.ListBotton_Clip);
            SoundManScript.PlaySound(SoundManScript.Botton_Clip);
            relatedList.selectedIndex = relatedList.savedIndex;
            relatedList.tempClickedIndex = relatedList.selectedIndex;
            
            //SetMarker(false);
            relatedList.clearMarker(relatedList.selectedIndex);
            relatedList.MarkAgain(relatedList.selectedIndex);
            relatedList.IsAction = true;
            relatedList.Texture2d = imageContent;
            relatedList.isParchaseWin = false;
            lockManager.unlockWindow.SetActive (false);
            lockManager.shop.unlocksureWindow.SetActive(false);
            //if (backToMain)
            //{
                
            //}
    }
    public void DeleteClick()
    {
        SoundManScript.PlaySound(SoundManScript.deletListBotton_Clip);
        relatedList.selectedIndex = index;
        relatedList.IsAction = true;
        relatedList.deleteAction = true;
        Destroy(gameObject);
    }
    //    public Image ImageContent
    //    {
    //        get { return imageContent; }
    //        set { imageContent = GetComponent<Image>(); }
    //    }
}

