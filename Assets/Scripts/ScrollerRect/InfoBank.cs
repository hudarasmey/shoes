﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Linq;
public class InfoBank : MonoBehaviour {

    public bool elementISImageBase = true;
    public int selectedIndex = -1;
    public bool isSetPrice =true;
    private static bool created = false;

    //public static ListBank Instance;
    void Awake()
    {
        if (!created)
        {
            DontDestroyOnLoad(this.gameObject);
            created = true;
          //  Debug.Log("Awake: " + this.gameObject);
        }
    }
    public string[] contents = {
		"jj", "jj", "3kk", "jj", "jj", "jj", "jj7","jj 8", "jj9","jj10"
	};
    //public Sprite[] Images;

    public bool isLoadTextureFromResouse;
    public string textureFileName;
    public Texture2D[] texture;
    public float[] startTileValuePerTexture;

    public bool isLoadNormalFromResouse;
    public string normaltextureFileName;
    public Texture[] normal;


    public GameObject[] Models;
    public Texture2D defaultTexture,defaultNormal,DefaultOccTexture;
    public int[] modelIndex;
    public int[] morphIndex;
    public Color[] ColorsBank;

    public Sprite[] spritesArr;
    public string[] namesArr;
    public string[] SavedModel;
    public string[] dateTime;

    public bool Finishload;
    public int[] priceArr;
    public bool isColorBank;
    //public Texture[] baseTexture;
    //public Texture[] baseNormal;
    //public Texture[] heeltexture;
    //public Texture[] heelnormal;
    
    
    //public ElementIdenity[] elements;
    //void Awake()
    //{
    //   // FileInfo[] fileInfo = levelDirectoryPath.GetFiles("*.*", SearchOption.AllDirectories);
    //    if (isLoadTextureFromResouse)
    //    {
    //        //texture = LoadTexturesFromResourse(textureFileName);
    //        //spritesArr = convertToSprites(texture);
    //        //Array.Clear(texture, 0, texture.Length);
    //        //texture = new Texture2D[0];
    //    }
    //    //if (isLoadNormalFromResouse)
    //    //{
    //    //    normal = LoadTexturesFromResourse(normaltextureFileName);
    //    //}

    //}
    //IEnumerator Start()
    //{

    //    var path = "file://" + Application.dataPath + "/../../TestConfig/Images/ImageSmall_01.png";
    //    var www = new WWW(path);
    //    yield return www;

    //    var wr = new WeakReference(www.texture);

    //    var unloader = Resources.UnloadUnusedAssets();
    //    while (!unloader.isDone)
    //    {
    //        yield return null;
    //    }

    //    //GC.Collect();

    //    var newTex = wr.Target;
    //    if (newTex == null)
    //        Debug.Log("GC collected");
    //    else
    //    {
    //        Debug.Log("Reference still here: " + newTex.GetType());

    //        if (newTex as UnityEngine.Object)
    //            Debug.Log((newTex as Texture).width);
    //        else
    //            Debug.Log("Texture was destroyed");
    //    }
    //}
    IEnumerator Start()
    {
        if (isLoadTextureFromResouse)
        {
          ////  texture = LoadTexturesFromResourse(textureFileName);
          ////  spritesArr = convertToSprites(texture);
          //////  unloadAll();
          ////  Array.Clear(texture, 0, texture.Length);

            //texture = LoadTexturesFromResourse(textureFileName);
            //unloadAll();
            //spritesArr = convertToSprites(texture);
            //Array.Clear(texture, 0, texture.Length);
            //texture = new Texture2D[0];
            spritesArr = LoadSpritesFromResourse(textureFileName);
            ////Array.Clear(texture, 0, texture.Length);
          //  texture = new Texture2D[0];
        }
        if (isLoadNormalFromResouse)
        {
            normal = LoadTexturesFromResourse(normaltextureFileName);
        }
        else
        {
          //  Debug.Log("load");
            setNamesfromSprites();
        }
        //Array.Clear(texture, 0, texture.Length);
        //texture = new Texture2D[0];

        var unloader = Resources.UnloadUnusedAssets();
        while (!unloader.isDone)
        {
            Finishload = true;
            yield return null;
        }
        if (PlayerPrefsX.GetColorArray("colorArr").Length > 0 && isColorBank)
        {
           
            ColorsBank = PlayerPrefsX.GetColorArray("colorArr");
        }
        if (isSetPrice)
        {
            priceArr = getPrices(namesArr);
        }
       

    }
    void unloadAll()
    {
        for (int i = 0; i < namesArr.Length; i++)
        {
            Resources.UnloadAsset(texture[i]);
        }
    }
    //void OnGUI()
    //{
    //    if (GUI.Button(new Rect(Screen.width / 2 - 100, Screen.height / 2 + 100, 100, 100), "Unload"))
    //    {
    //        Resources.UnloadUnusedAssets();
    //    }
    //}
    public void AddColor(Color _color)
    {
        //Color[] ColorsBankTemp= new Color[ColorsBank.Length+1];
        //ColorsBankTemp[0] = _color;
        //for (int i = 0; i < ColorsBank.Length; i++)
        //{
        //    ColorsBankTemp[i+1] = ColorsBank[i];
        //}
        //ColorsBank = ColorsBankTemp;

        Color[] ColorsBankTemp = new Color[ColorsBank.Length + 1];
        
        for (int i = 0; i < ColorsBank.Length; i++)
        {
            ColorsBankTemp[i] = ColorsBank[i];
        }
        ColorsBankTemp[ColorsBank.Length] = _color;
        ColorsBank = ColorsBankTemp;
    }
    public Sprite GetImage(Sprite[] ArrImages, int index)
    {
        return ArrImages[index];
    }
    public int getArrLength(Sprite[] ArrImages)
    {
        return ArrImages.Length;
    }
    public string getListContent(int index)
    {
        return contents[index];
    }

    public int getListLength()
    {
        return contents.Length;
    }
    public Texture2D[] LoadTextures(string folderName)
    {
        Debug.Log("PP");
        Texture2D[] Textures = Resources.LoadAll<Texture2D>(folderName);
        return Textures;
      //  imageContent = Sprite.Create(imagemat, new Rect(0, 0, imagemat.width, imagemat.height), new Vector2(0.5f, 0.5f), 1);
    }
    public Texture2D[] LoadTexturesFromResourse(string foldername)
    {
        //Debug.Log("PP");
        //Textures = Resources.LoadAll<Texture2D>("Textures");
        // string[] strArr = Directory.GetFiles(path).Length;
        //  GameObject[] strArr = Resources.LoadAll<Texture2D>("Textures");
        Texture2D[] TexturesTemp = Resources.LoadAll<Texture2D>(foldername);
        namesArr = new string[TexturesTemp.Length];
        //spriteArr = new Sprite[TexturesTemp.Length];
        for (int i = 0; i < TexturesTemp.Length; i++)
        {
            namesArr[i] = TexturesTemp[i].name.ToString();
           // spriteArr[i] = Sprite.Create(TexturesTemp[i], new Rect(0, 0, TexturesTemp[i].width, TexturesTemp[i].height), new Vector2(0.5f, 0.5f), 5);
        }
        return TexturesTemp;
    }
    public void setNamesfromSprites()
    {
        namesArr = new string[spritesArr.Length];
       
        //spriteArr = new Sprite[TexturesTemp.Length];
        for (int i = 0; i < spritesArr.Length; i++)
        {
            if (spritesArr[i] == null)
            {
                namesArr[i] = "Null#10#20";

            }
            else
            {
                namesArr[i] = spritesArr[i].name.ToString();
            }
           
            // spriteArr[i] = Sprite.Create(TexturesTemp[i], new Rect(0, 0, TexturesTemp[i].width, TexturesTemp[i].height), new Vector2(0.5f, 0.5f), 5);
        }
        //return _namesArr;
    }
    public Sprite[] LoadSpritesFromResourse(string foldername)
    {
        //Debug.Log("PP");
        //Textures = Resources.LoadAll<Texture2D>("Textures");
        // string[] strArr = Directory.GetFiles(path).Length;
        //  GameObject[] strArr = Resources.LoadAll<Texture2D>("Textures");
        Sprite[] spritesArr = Resources.LoadAll<Sprite>(foldername+"1");
        namesArr = new string[spritesArr.Length];
        //spriteArr = new Sprite[TexturesTemp.Length];
        for (int i = 0; i < spritesArr.Length; i++)
        {
            namesArr[i] = spritesArr[i].name.ToString();
            // spriteArr[i] = Sprite.Create(TexturesTemp[i], new Rect(0, 0, TexturesTemp[i].width, TexturesTemp[i].height), new Vector2(0.5f, 0.5f), 5);
        }
        return spritesArr;
    }
    public Sprite[] convertToSprites(Texture2D[] ArrOfTextures)
    {
        Sprite[] spriteArr = new Sprite[ArrOfTextures.Length];

        for (int i = 0; i < ArrOfTextures.Length; i++)
        {
            //namesArr[i] = TexturesTemp[i].name.ToString();
            spriteArr[i] = Sprite.Create(ArrOfTextures[i], new Rect(0, 0, ArrOfTextures[i].width, ArrOfTextures[i].height), new Vector2(0.5f, 0.5f), 0.005f);
        }

        return spriteArr;
    }
    public Texture2D LoadTextureFromResourse(string filename, string textureName)
    {
    //    if (textureName != null)
    //    {
            return Resources.Load<Texture2D>(filename + "/" + textureName);
        //}
        //else
        //{
        //    return null;
        //}
        
    }
    //public int getArrLength(Sprite[] ArrImages)
    //{
    //    return ArrImages.Length;
    //}
    public Texture2D NormalMapGen(Texture2D source, float strength, float distortion)
    {
        Texture2D normalTex=null;
        //strength = 1f;
        //distortion = 15;
        //strength = Mathf.Clamp(strength, 0.0F, 55.0F);
        StartCoroutine(CreatNormal(source, (status) =>
        {
           // print(status.ToString());
             normalTex= (Texture2D)status;
            
        }
        ));
     //  return null;
       return normalTex;
    }
    IEnumerator CreatNormal(Texture2D source, System.Action<Texture2D> callback)
    {



        float strength = 1f;
        float distortion = 10;
        strength = Mathf.Clamp(strength, 0.0F, 55.0F);
        Texture2D normalTexture;
        float xLeft;
        float xRight;
        float yUp;
        float yDown;
        float yDelta;
        float xDelta;
        normalTexture = new Texture2D(source.width, source.height, TextureFormat.ARGB32, true);
        for (int y = 0; y < normalTexture.height; y++)
        {
            for (int x = 0; x < normalTexture.width; x++)
            {
                xLeft = source.GetPixel(x - 1, y).grayscale * strength;
                xRight = source.GetPixel(x + 1, y).grayscale * strength;
                yUp = source.GetPixel(x, y - 1).grayscale * strength;
                yDown = source.GetPixel(x, y + 1).grayscale * strength;
                xDelta = ((xLeft - (xRight)) + 1) * 0.5f;
                yDelta = ((yUp - yDown) + 1) * 0.5f;
                normalTexture.SetPixel(x, y, new Color(Mathf.Clamp01(sCurve(xDelta, distortion)), Mathf.Clamp01(sCurve(yDelta, distortion)), 1, Mathf.Clamp01(sCurve(yDelta, distortion))));
                //normalTexture.SetPixel(x, y, new Color(xDelta, yDelta, 1.0f, yDelta));

            }


        }
        normalTexture.Apply();
       // yield return new WaitForSeconds(0);
        callback(normalTexture);
        yield return new WaitForEndOfFrame();
        string normalPath = Application.persistentDataPath + "/" + source.name + ".png";
       // string normalPath = Application.persistentDataPath + "/" + source.name + ".jpg";
        //   Debug.Log(screenShotPath);

        //Code for exporting the image to assets folder 
         System.IO.File.WriteAllBytes(normalPath, normalTexture.EncodeToPNG());
        //System.IO.File.WriteAllBytes(normalPath, normalTexture.EncodeToJPG());
        callback(normalTexture);
       // yield return null;
        
    }


    public static float sCurve(float x, float distortion)
    {
        return 1f / (1f + Mathf.Exp(-Mathf.Lerp(5, 10, distortion) * (x - 0.5f)));
    }

    public static void SetupMaterialWithBlendMode(Material material, int blendMode)
    {
        switch (blendMode)
        {
            case 0://BlendMode.Opaque:
                material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                material.SetInt("_ZWrite", 1);
                material.DisableKeyword("_ALPHATEST_ON");
                material.DisableKeyword("_ALPHABLEND_ON");
                material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                material.renderQueue = -1;
                break;
            case 1://BlendMode.Cutout:
                material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                material.SetInt("_ZWrite", 1);
                material.EnableKeyword("_ALPHATEST_ON");
                material.DisableKeyword("_ALPHABLEND_ON");
                material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                material.renderQueue = 2450;
                break;
            case 2://BlendMode.Fade:
                material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                material.SetInt("_ZWrite", 0);
                material.DisableKeyword("_ALPHATEST_ON");
                material.EnableKeyword("_ALPHABLEND_ON");
                material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                material.renderQueue = 3000;
                break;
            case 3://BlendMode.Transparent:
                material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                material.SetInt("_ZWrite", 0);
                material.DisableKeyword("_ALPHATEST_ON");
                material.DisableKeyword("_ALPHABLEND_ON");
                material.EnableKeyword("_ALPHAPREMULTIPLY_ON");
                material.renderQueue = 3000;
                break;
        }
    }

//    IEnumerator LoadNormalImage(Texture2D selectTexture)
//    {
//        string normalPath = Application.persistentDataPath + selectTexture.name + ".png";
//#if UNITY_IPHONE || UNITY_ANDROID

//        Debug.Log("mobile");
//        WWW w = new WWW("file://" + normalPath);
//        yield return w;

//#endif
//        //if (w.error == null)
//        //{
//        //    normalTexture = w.texture;
//        //}
//        //else
//        //{
//        //    normalTexture = materialSlider.Bank.NormalMapGen(selectedTexteure, 1, 1); //print(w.error);
//        //}
//    }

    public int[] getPrices(string[] _names)
    {
      //  Debug.Log("ss");
        int[] Prices=new int[_names.Length];
        for (int i = 0; i < _names.Length; i++)
        {
         

                var names = _names[i].Split('#');
                if (names.Length > 1)
                {
                //    Debug.Log(names[0] + " " + names[1]);
                    Prices[i] = int.Parse(names[1]);
                }
                else
                {
                   // Debug.Log(names[0]);
                    Prices[i] = 30;
                }
                
          
        }
        

        return Prices;
    }
}

