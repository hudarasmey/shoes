﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
//using CodeStage.AntiCheat.ObscuredTypes;

public class UnlockElement : MonoBehaviour
{

    public ScrollCellTest cell;
    public int cellIndex;
    public string elementName;
    public bool OldMenu;
    public int Level = 1;
    public string LevelName = " ";
    public int chapterNumber;
    public GameObject lockImage;
    public GameObject ScoreImage;
   // public Text PriceText;
    public TextMeshProUGUI PriceTextTP;
    public SelectedMenuType menuType;
    public GameObject unlockWindow,unlocksureWindow;
    public bool isLocked;
    public bool notPriced;
    public ShopMan shop;

    void Start()
    {
        shop = GameObject.FindGameObjectWithTag("UnlockWin").GetComponent<ShopMan>();
        //unlockWindow = GameObject.FindGameObjectWithTag("UnlockWin");
        unlockWindow = shop.window;
        //if (!notPriced)
        //{
            CheckIfLocked();
        //}
        
    }
    
    public void CheckIfLocked()
{
     //   shop.loadingImage.sprite = cell.imageContent;
        cell = gameObject.GetComponent<ScrollCellTest>();
       
        unlockWindow = shop.window;
       // unlocksureWindow = GameObject.FindGameObjectWithTag("SureUnlockWin");
     //   cell.
       // Debug.Log(unlockWindow.name);
        menuType = cell.menuType;
        if (menuType == SelectedMenuType.Texture)
        {

            if (!notPriced)
            {
                if (cell.imageContent != null)
                {
                    elementName = cell.imageContent.name;
                }



                cellIndex = cell.index;
                // Debug.Log((Level).ToString() + " " + chapterNumber.ToString() + " " + ObscuredPrefs.GetInt("chapter" + "-" + chapterNumber.ToString() + "level" + "-" + (Level).ToString(), -1));
                //ObscuredPrefs.SetInt("LevelName-", SilverFlex);
                if (cellIndex > 4)
                {

                    // int level = ObscuredPrefs.GetInt(LevelName + "-" + (Level - 1).ToString(), -1);
                    //  int level = ObscuredPrefs.GetInt("chapter" + "-" + chapterNumber.ToString() + "level" + "-" + (Level).ToString(), -1);
                    int _cellIndex = PlayerPrefs.GetInt("element" + "-" + elementName.ToString() + "cell" + "-" + (cellIndex).ToString(), -1);


                    if (_cellIndex != -1)
                    {


                        //Debug.Log(Level + " Unlocked " + level);
                        isLocked = false;
                        //GetComponent<Button>().interactable = true;
                        if (lockImage != null)
                            lockImage.SetActive(false);
                        //if (ScoreImage != null)
                        //    ScoreImage.SetActive(true);



                    }
                    else
                    {

                        //Debug.Log(Level + " locked " + level);
                        isLocked = true;
                        //GetComponent<Button>().interactable = false;
                        if (lockImage != null)
                            lockImage.SetActive(true);
                        //if (PriceText != null)
                        //    PriceText.text = cell.price.ToString();// "50";
                        if (PriceTextTP != null)
                            PriceTextTP.text = cell.price.ToString();// "50";


                    }
                }
                else
                {

                    //Debug.Log(Level + " Unlocked " + level);
                    isLocked = false;
                    GetComponent<Button>().interactable = true;
                    if (lockImage != null)
                        lockImage.SetActive(false);
                    //if (PriceText != null)
                    //    PriceText.text = 50.ToString();
                    if (PriceTextTP != null)
                        PriceTextTP.text = 50.ToString();// "50";

                }
            }
            else
            {
                isLocked = false;
                if (lockImage != null)
                    lockImage.SetActive(false);
            }
            
        }
        else
        {
            //Debug.Log(Level + " Unlocked " + level);
            isLocked = false;
            //GetComponent<Button>().interactable = true;
            if (lockImage != null)
                lockImage.SetActive(false);
            //if (levelText != null)
            //    levelText.text = 50.ToString();
        }

}

    //public void CheckIfLocked()
    //{
    //    Debug.Log((Level).ToString() + " " + chapterNumber.ToString() + " " + PlayerPrefs.GetInt("chapter" + "-" + chapterNumber.ToString() + "level" + "-" + (Level).ToString(), -1));
    //    //ObscuredPrefs.SetInt("LevelName-", SilverFlex);
    //    if (Level > 1)
    //    {

    //        // int level = ObscuredPrefs.GetInt(LevelName + "-" + (Level - 1).ToString(), -1);
    //        int level = PlayerPrefs.GetInt("chapter" + "-" + chapterNumber.ToString() + "level" + "-" + (Level).ToString(), -1);


    //        if (level != -1)
    //        {
    //            if (OldMenu)
    //            {
    //                //Debug.Log(Level + " Unlocked " + level);
    //                GetComponent<Button>().interactable = true;
    //                if (lockImage != null)
    //                    lockImage.SetActive(false);
    //                if (ScoreImage != null)
    //                    ScoreImage.SetActive(true);
    //            }
    //            else
    //            {
    //                GetComponent<Button>().interactable = true;
    //                if (lockImage != null)
    //                    lockImage.SetActive(false);
    //                gameObject.GetComponent<Image>().enabled = true;
    //                levelText.gameObject.SetActive(true);
    //                //   gameObject.GetComponent<Image>().sprite = levelSp;
    //            }

    //        }
    //        else
    //        {
    //            if (OldMenu)
    //            {
    //                //Debug.Log(Level + " locked " + level);
    //                GetComponent<Button>().interactable = false;
    //                if (lockImage != null)
    //                    lockImage.SetActive(true);
    //                if (ScoreImage != null)
    //                    ScoreImage.SetActive(false);
    //            }
    //            else
    //            {
    //                GetComponent<Button>().interactable = false;
    //                if (lockImage != null)
    //                    lockImage.SetActive(true);
    //                gameObject.GetComponent<Image>().enabled = false;
    //                levelText.gameObject.SetActive(false);
    //                //gameObject.GetComponent<Image>().sprite = LockSp;

    //            }
    //        }
    //    }
    //    else
    //    {
    //        if (chapterNumber == 0)
    //        {
    //            if (lockImage != null)
    //                lockImage.SetActive(false);
    //            if (ScoreImage != null)
    //                ScoreImage.SetActive(true);
    //        }

    //    }
    //}
    public void lockTextuerEnable()
    {
        if (lockImage != null)
        {
            lockImage.SetActive(!GetComponent<Button>().interactable);
        }
        else
        {
            Debug.LogError("null");
        }
    }
    public void unlockWindowSc()
    {
       // shop.window = GameObject.FindGameObjectWithTag("UnlockWin");
        //if (GameManager.Instance.Currency >= int.Parse(PriceTextTP.text))
        //{

            if (shop.window != null)
            {

                //shop.window.GetComponent<ShopMan>().targetElement = this;
                //shop.window.GetComponent<ShopMan>().window.SetActive(true);
               // shop.targetElement = this;
                shop.window.SetActive(true);
            if (GameManager.Instance.Currency >= int.Parse(PriceTextTP.text))
            {
                shop.coinBtn.interactable = true;
                shop.coinBtn2.interactable = true;

            }
            else
            {
                shop.coinBtn.interactable = false;
                shop.coinBtn2.interactable = false;
            }

            if (cell.imageContent != null)
                {
                    shop.image.sprite = cell.imageContent;
                    shop.image2.sprite = cell.imageContent;  
                }
            //if (shop.window != null)
            //    {
                    shop.windowCoinsText.text = PriceTextTP.text + " Coins";
                    shop.windowCoinsText2.text = PriceTextTP.text + " Coins";
                //}
                
                // unlockWindow.SetActive(true);
            }
            else
            {
                Debug.LogError("null");
            }
        //}
        //else
        //{
        //    shop.coinBtn.interactable = false;

            shop.targetElement = this;
        //   // shop.shopWindow.SetActive(true);
        //}
    }
    public void unlockElementFn()
    {
        isLocked = false;
        PlayerPrefs.SetInt("element" + "-" + elementName.ToString() + "cell" + "-" + (cellIndex).ToString(), 1);
        CheckIfLocked();
        //cell.clickit();
    }
    public void revokAction()
    {
       
        Debug.Log("klsdksldks");
        cell.clickitBack();
    }
}



    //public bool OldMenu;
    //public int Level = 1;
    //public string LevelName = " ";
    //public int chapterNumber;
    //public GameObject lockImage;
    //public GameObject ScoreImage;
    //public GameObject levelText;

    //void Start()
    //{
    //    // Debug.Log((Level).ToString() + " " + chapterNumber.ToString() + " " + ObscuredPrefs.GetInt("chapter" + "-" + chapterNumber.ToString() + "level" + "-" + (Level).ToString(), -1));
    //    //ObscuredPrefs.SetInt("LevelName-", SilverFlex);
    //    if (Level > 1)
    //    {

    //        // int level = ObscuredPrefs.GetInt(LevelName + "-" + (Level - 1).ToString(), -1);
    //      //  int level = ObscuredPrefs.GetInt("chapter" + "-" + chapterNumber.ToString() + "level" + "-" + (Level).ToString(), -1);
    //        int level = PlayerPrefs.GetInt("chapter" + "-" + chapterNumber.ToString() + "level" + "-" + (Level).ToString(), -1);


    //        if (level != -1)
    //        {
    //            if (OldMenu)
    //            {

    //                //Debug.Log(Level + " Unlocked " + level);
    //                GetComponent<Button>().interactable = true;
    //                if (lockImage != null)
    //                    lockImage.SetActive(false);
    //                if (ScoreImage != null)
    //                    ScoreImage.SetActive(true);
    //            }
    //            else
    //            {
    //                GetComponent<Button>().interactable = true;
    //                if (lockImage != null)
    //                    lockImage.SetActive(false);
    //                gameObject.GetComponent<Image>().enabled = true;
    //                levelText.SetActive(true);

    //                // gameObject.GetComponent<Image>().sprite = levelSp;
    //            }


    //        }
    //        else
    //        {
    //            if (OldMenu)
    //            {
    //                //Debug.Log(Level + " locked " + level);
    //                GetComponent<Button>().interactable = false;
    //                if (lockImage != null)
    //                    lockImage.SetActive(true);
    //                if (ScoreImage != null)
    //                    ScoreImage.SetActive(false);
    //            }
    //            else
    //            {
    //                GetComponent<Button>().interactable = false;
    //                if (lockImage != null)
    //                    lockImage.SetActive(true);
    //                gameObject.GetComponent<Image>().enabled = false;
    //                levelText.SetActive(false);
    //                // gameObject.GetComponent<Image>().sprite = LockSp;
    //            }
    //        }
    //    }
    //    else
    //    {
    //        if (chapterNumber == 0)
    //        {
    //            if (lockImage != null)
    //            {
    //                lockImage.SetActive(false);
    //            }

    //        }
    //        else
    //        {
    //            //int level = ObscuredPrefs.GetInt("chapter" + "-" + chapterNumber.ToString() + "level" + "-" + (Level).ToString(), -1);
    //            int level = PlayerPrefs.GetInt("chapter" + "-" + chapterNumber.ToString() + "level" + "-" + (Level).ToString(), -1);


    //            if (level != -1)
    //            {
    //                if (OldMenu)
    //                {
    //                    //Debug.Log(Level + " Unlocked " + level);
    //                    GetComponent<Button>().interactable = true;
    //                    if (lockImage != null)
    //                        lockImage.SetActive(false);
    //                    if (ScoreImage != null)
    //                        ScoreImage.SetActive(true);
    //                }
    //                else
    //                {
    //                    GetComponent<Button>().interactable = true;
    //                    if (lockImage != null)
    //                        lockImage.SetActive(false);
    //                    gameObject.GetComponent<Image>().enabled = true;
    //                    levelText.SetActive(true);

    //                }


    //            }
    //            else
    //            {
    //                if (OldMenu)
    //                {
    //                    //Debug.Log(Level + " locked " + level);
    //                    GetComponent<Button>().interactable = false;
    //                    if (lockImage != null)
    //                        lockImage.SetActive(true);
    //                    if (ScoreImage != null)
    //                        ScoreImage.SetActive(false);
    //                }
    //                else
    //                {
    //                    GetComponent<Button>().interactable = false;
    //                    if (lockImage != null)
    //                        lockImage.SetActive(true);
    //                    gameObject.GetComponent<Image>().enabled = false;
    //                    levelText.SetActive(false);

    //                }
    //            }
    //        }

    //    }

    //}
    //public void CheckIfLocked()
    //{
    //    Debug.Log((Level).ToString() + " " + chapterNumber.ToString() + " " + PlayerPrefs.GetInt("chapter" + "-" + chapterNumber.ToString() + "level" + "-" + (Level).ToString(), -1));
    //    //ObscuredPrefs.SetInt("LevelName-", SilverFlex);
    //    if (Level > 1)
    //    {

    //        // int level = ObscuredPrefs.GetInt(LevelName + "-" + (Level - 1).ToString(), -1);
    //        int level = PlayerPrefs.GetInt("chapter" + "-" + chapterNumber.ToString() + "level" + "-" + (Level).ToString(), -1);


    //        if (level != -1)
    //        {
    //            if (OldMenu)
    //            {
    //                //Debug.Log(Level + " Unlocked " + level);
    //                GetComponent<Button>().interactable = true;
    //                if (lockImage != null)
    //                    lockImage.SetActive(false);
    //                if (ScoreImage != null)
    //                    ScoreImage.SetActive(true);
    //            }
    //            else
    //            {
    //                GetComponent<Button>().interactable = true;
    //                if (lockImage != null)
    //                    lockImage.SetActive(false);
    //                gameObject.GetComponent<Image>().enabled = true;
    //                levelText.SetActive(true);
    //                //   gameObject.GetComponent<Image>().sprite = levelSp;
    //            }

    //        }
    //        else
    //        {
    //            if (OldMenu)
    //            {
    //                //Debug.Log(Level + " locked " + level);
    //                GetComponent<Button>().interactable = false;
    //                if (lockImage != null)
    //                    lockImage.SetActive(true);
    //                if (ScoreImage != null)
    //                    ScoreImage.SetActive(false);
    //            }
    //            else
    //            {
    //                GetComponent<Button>().interactable = false;
    //                if (lockImage != null)
    //                    lockImage.SetActive(true);
    //                gameObject.GetComponent<Image>().enabled = false;
    //                levelText.SetActive(false);
    //                //gameObject.GetComponent<Image>().sprite = LockSp;

    //            }
    //        }
    //    }
    //    else
    //    {
    //        if (chapterNumber == 0)
    //        {
    //            if (lockImage != null)
    //                lockImage.SetActive(false);
    //            if (ScoreImage != null)
    //                ScoreImage.SetActive(true);
    //        }

    //    }
    //}
    //public void lockTextuerEnable()
    //{
    //    if (lockImage != null)
    //    {
    //        lockImage.SetActive(!GetComponent<Button>().interactable);
    //    }
    //    else
    //    {
    //        Debug.LogError("null");
    //    }
    //}
