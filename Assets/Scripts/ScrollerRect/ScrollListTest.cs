﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public enum SelectedMenuType { Texture, Color, Save, Select }
public enum TagCollection { none, SaveInfoBank, MatColorTexInfoBank, MatBWTexInfoBank, ColorInfoBank, AccsInfoBank, coolBank, flowerBank, foodBank, leatherBank, FaceModelBank, FrontFaceModelBank, BackFaceModelBank, HeelModelBank, PatternSelectBank, MatParamBank}
public class ScrollListTest : UIList<ScrollCellTest> {

//	public InputField numCellsInput;

    public bool IsInst;
    public bool IsVerticalList;
    public SelectedMenuType menuType = SelectedMenuType.Texture;
    private Sprite texture2d;

    public Sprite Texture2d
    {
        get { return texture2d; }
        set { texture2d = value; }
    }
    public ScrollRect myScrollRect;
    public int numOfCells;
    //public Scrollbar newScrollBar;
    public TagCollection targetBank;
    public InfoBank Bank;
    public int selectedIndex = -1;
    public Image selectedSprite;
    public bool isUpdate;
    public bool IsAction;
    public Slider tileValueSlider;
    public Texture2D checker;
    public int tempIndex = -1;
    int TmpNumCell;
    public Image RightImg, LeftImg;
    public bool istented;
    public int selectedMenuIndex;
    public float tempHorz;
    public int savedIndex,tempClickedIndex;
    public bool isParchaseWin;
    MaterialMenusManager materialMenusManager;

   // public Vector2 cellSize;//= new Vector2(100,100);
    public bool deleteAction;
    void Awake()
    {
        tempClickedIndex = -1;
        savedIndex = 0;
    //    Debug.Log("JFJFJ");
        if (Bank ==null)
        {
            switch (targetBank)
            {

                case TagCollection.none:

                    break;
                case TagCollection.SaveInfoBank:
                    Bank = GameObject.FindGameObjectWithTag("SaveInfoBank").GetComponent<InfoBank>();
                    break;
                case TagCollection.MatColorTexInfoBank:
                    Bank = GameObject.FindGameObjectWithTag("MatColorTexInfoBank").GetComponent<InfoBank>();
                    break;
                case TagCollection.MatBWTexInfoBank:
                    Bank = GameObject.FindGameObjectWithTag("MatBWTexInfoBank").GetComponent<InfoBank>();
                    break;
                case TagCollection.ColorInfoBank:
                    Bank = GameObject.FindGameObjectWithTag("ColorInfoBank").GetComponent<InfoBank>();
                    break;
                case TagCollection.AccsInfoBank:
                    Bank = GameObject.FindGameObjectWithTag("AccsInfoBank").GetComponent<InfoBank>();
                    break;
                case TagCollection.coolBank:
                    Bank = GameObject.FindGameObjectWithTag("coolBank").GetComponent<InfoBank>();
                    break;
                case TagCollection.flowerBank:
                    Bank = GameObject.FindGameObjectWithTag("flowerBank").GetComponent<InfoBank>();
                    break;
                case TagCollection.foodBank:
                    Bank = GameObject.FindGameObjectWithTag("foodBank").GetComponent<InfoBank>();
                    break;
                case TagCollection.leatherBank:
                    Bank = GameObject.FindGameObjectWithTag("leatherBank").GetComponent<InfoBank>();
                    break;
                case TagCollection.FaceModelBank:
                    Bank = GameObject.FindGameObjectWithTag("FaceModelBank").GetComponent<InfoBank>();
                    break;
                case TagCollection.FrontFaceModelBank:
                    Bank = GameObject.FindGameObjectWithTag("FrontFaceModelBank").GetComponent<InfoBank>();
                    break;
                case TagCollection.BackFaceModelBank:
                    Bank = GameObject.FindGameObjectWithTag("BackFaceModelBank").GetComponent<InfoBank>();
                    break;
                case TagCollection.HeelModelBank:
                    Bank = GameObject.FindGameObjectWithTag("HeelModelBank").GetComponent<InfoBank>();
                    break;
                case TagCollection.PatternSelectBank:
                    Bank = GameObject.FindGameObjectWithTag("PatternSelectBank").GetComponent<InfoBank>();
                    break;
                default:
                    break;
            }
        }
        
    }
    void OnDisable()
    {
        clearMarker(selectedIndex);
      //  Debug.Log("skdsodks"+selectedIndex);
       // selectedIndex = -1;
    }
    void OnEnable()
    {
        //tempClickedIndex = 0;
        //savedIndex = 0;
        //isAwake();
        if (istented)
        {
            sliderSmoke();
        }
        else
        {
            if (RightImg != null)
            {
                RightImg.gameObject.SetActive(false);
            }
            if (LeftImg != null)
            {
                LeftImg.gameObject.SetActive(false);
            }
        }
        if (isUpdate == true)
        {
            //gameObject.GetComponent<Image>().enabled = false;
        }
        
     //   Debug.Log("PFDKFD");
        switch (menuType)
        {
            case SelectedMenuType.Texture:
            case SelectedMenuType.Select:
                numOfCells = Bank.spritesArr.Length;
                //
                if (TmpNumCell != numOfCells)
                {
                  
                   
                    OnCreateList(numOfCells);
                    TmpNumCell = numOfCells;
                }
                
                //  numOfCells = Bank.spritesArr.Length;
                break;
            case SelectedMenuType.Color:

                numOfCells = Bank.ColorsBank.Length;
                if (TmpNumCell != numOfCells)
                {
                    OnCreateList(numOfCells);
                    TmpNumCell = numOfCells;
                }
                
                if (selectedIndex >= 0)
                {
                    cells[selectedIndex].SetMarker(true);
                }
                
                break;
            case SelectedMenuType.Save:
             //   Debug.Log("PFDKFD");
                numOfCells = Bank.SavedModel.Length;
              //  Bank.spritesArr = new Sprite[Bank.SavedModel.Length];
                OnCreateList(numOfCells);


                break;
            default:
                break;
        }

        //selectedIndex = tempIndex;
        
        //cell.SetMarker(true);
        //cell.relatedList.clearMarker(selectedIndex);
    }
    //void OnDisable()
    //{
    //    gameObject.GetComponent<Image>().enabled = false;
    //}
    void Start()
    {

        
        isUpdate = true;
         OnCreateList(numOfCells);

         resetScroller();
         if (selectedSprite != null)
         {
             setBtnSprit(0); 
         }
         
        //myScrollRect.horizontalNormalizedPosition = 0f;
        // grid.cellSize = cellSize;

        // PlayerPrefs.DeleteAll();
         //if (isUpdate == true)
         //{
         //    gameObject.GetComponent<Image>().enabled = false;
         //}
    }
    void sliderSmoke()
    {
    //    if (myScrollRect.horizontalNormalizedPosition < 0)
    //{
    //    myScrollRect.horizontalNormalizedPosition = tempHorz = 0;
    //}
        if (IsVerticalList)
        {

            if (tempHorz != myScrollRect.verticalNormalizedPosition)
            {


                RightImg.color = new Color(RightImg.color.r, RightImg.color.b, RightImg.color.g, 1 - myScrollRect.verticalNormalizedPosition);
                LeftImg.color = new Color(LeftImg.color.r, LeftImg.color.b, LeftImg.color.g, myScrollRect.verticalNormalizedPosition);
                tempHorz = myScrollRect.verticalNormalizedPosition;
                if (tempHorz <= 0.1f)
                {
                    LeftImg.enabled = false;
                }
                else
                {
                    LeftImg.enabled = true;
                }
                if (tempHorz >= 0.9f)
                {
                    RightImg.enabled = false;
                }
                else
                {
                    RightImg.enabled = true;
                }
            }
        }
        else
        {

            if (tempHorz != myScrollRect.horizontalNormalizedPosition)
            {


                RightImg.color = new Color(RightImg.color.r, RightImg.color.b, RightImg.color.g, 1 - myScrollRect.horizontalNormalizedPosition);
                LeftImg.color = new Color(LeftImg.color.r, LeftImg.color.b, LeftImg.color.g, myScrollRect.horizontalNormalizedPosition);
                tempHorz = myScrollRect.horizontalNormalizedPosition;
                if (tempHorz <= 0.1f)
                {
                    LeftImg.enabled = false;
                }
                else
                {
                    LeftImg.enabled = true;
                }
                if (tempHorz >= 0.9f)
                {
                    RightImg.enabled = false;
                }
                else
                {
                    RightImg.enabled = true;
                }
            }
        }
        
    }
    public void Update()
    {
        if (istented)
        {
            sliderSmoke();
        }
        else
        {
            if (RightImg !=null)
            {
                RightImg.gameObject.SetActive(false);
            }
            if (LeftImg!=null)
	        {
                LeftImg.gameObject.SetActive(false);
	        }
        }
        
        //myScrollRect.horizontalNormalizedPosition;

        if (Input.GetKeyDown(KeyCode.G))
        {
           //nCreateList(numOfCells);
            resetScroller();
            Debug.Log(gameObject.transform.parent.gameObject.name);
        }
        //Debug.Log("myScrollRect.horizontalNormalizedPosition" + myScrollRect.horizontalNormalizedPosition);
        if (isUpdate && numOfCells > 0)// && myScrollRect.horizontalNormalizedPosition != 0)
        {
           // OnCreateList(numOfCells);
            isUpdate = false;
            //Change the current horizontal scroll position.
            if (IsVerticalList)
            {
                myScrollRect.verticalNormalizedPosition = 1f;
            }
            else
            {
                myScrollRect.horizontalNormalizedPosition = 0f;
            }
            //myScrollRect.verticalNormalizedPosition = 1f;
            //gameObject.GetComponent<Image>().enabled = true;
        }
      
        
    }
	#region implemented abstract members of UIList
    public void setBtnSprit( int index)
    {
       selectedSprite.sprite=cells[index].imageContent;
    }
	public override int NumberOfCells ()
	{
        switch (menuType)
        {
            case SelectedMenuType.Texture:
            case SelectedMenuType.Select:
                numOfCells = Bank.spritesArr.Length;
                break;
            case SelectedMenuType.Color:
                numOfCells = Bank.ColorsBank.Length;
                break;
            case SelectedMenuType.Save:
                numOfCells = Bank.SavedModel.Length;
                //Bank.spritesArr = new Sprite[Bank.SavedModel.Length];
                break;
            default:
                break;
        }
        
        return numOfCells;
        //int numCells = 0;
        //int.TryParse(numCellsInput.text, out numCells);
        //return numCells;
	}
    public void MarkAgain(int index)
    {
        for (int x = 0; x < cells.Count; x++)
        {
            if (index== x)
            {
                cells[x].SetMarker(true);
            }
            else
            {
                cells[x].SetMarker(false);
            }
            
        }
    }
   
	public override void UpdateCell (int index, ScrollCellTest cell)
	{
        
        //cell.imageContent.sprite = texture2d;
       
      //////////  myScrollRect.verticalNormalizedPosition = 1f;
        //cell.SetSprite(texture2d);
      //  cell.SetSprite(Bank.Images[index]);//////////////pppp
    //    cell.SetSpriteit();
        switch (menuType)
        {
            case SelectedMenuType.Save:
              //  Debug.Log(index);
                cell.SetTexture(Bank.spritesArr[index]);
                //cell.cellLabel.text = "";
                if (cell.DateTime !=null)
                {
                    cell.DateTime.text = Bank.dateTime[index]; 
                }
                cell.index = index;
                cell.relatedList = this;
                //if (IsInst)
                //{
                //    if (index == 0)
                //    {
                //        //cell.clickit();
                //        cell.relatedList.selectedIndex = index;
                //        cell.SetMarker(true);
                //        cell.relatedList.clearMarker(index);
                //        IsInst = false;

                //    }

                //}
                break;
            case SelectedMenuType.Texture:
            case SelectedMenuType.Select:
                cell.SetTexture(Bank.spritesArr[index]);
                //cell.cellLabel.text = "";
                cell.index = index;
                cell.relatedList = this;
                //if (IsInst)
                //{
                //    if (index == 0)
                //    {
                //        //cell.clickit();
                //        cell.relatedList.selectedIndex = index;
                //        cell.SetMarker(true);
                //        cell.relatedList.clearMarker(index);
                //        IsInst = false;

                //    }

                //}
                break;
            case SelectedMenuType.Color:
                cell.SetSpriteColor(Bank.ColorsBank[index]);
                cell.cellLabel.text = ColorToHex(Bank.ColorsBank[index]);
                cell.index = index;
                cell.relatedList = this;
                //ColorUtility.ToHtmlStringRGB( myColor )
                //if (IsInst)
                //{
                //    if (index == 0)
                //    {
                //       // Debug.Log("ffDF");
                //        //cell.clickit();
                //        cell.relatedList.selectedIndex = index;
                //        cell.SetMarker(true);
                //        cell.relatedList.clearMarker(index);
                //        IsInst = false;

                //    }

                //}
                break;
            default:
                break;
        }
        
     
        //cell.imageContent = Bank.Images[index];
		//cell.cellLabel.text = "Cell: " + index.ToString();
        
        //
       // isUpdate = true;

       
    }
    public override void UpdateCellPos()
    {
      //  myScrollRect.verticalNormalizedPosition = 1f;
    }
	#endregion

	public void OnCreateList(int numOfCell)
	{
		int cells = 0;

		//int.TryParse(numCellsInput.text, out cells);
        cells = numOfCell;
        
		if (cells > 0)
		{
			// Delete the old list
			ClearAllCells();
			
			Refresh();

		}

		
	}
    public void clearMarker(int index)
    {
        if (index != tempIndex)
        {
            if (tempIndex == -1)
            {
                tempIndex = index;

            }
            else
            {
               // Debug.Log("index" + tempIndex);
                //Instantiate(checker,Vector3
                cells[tempIndex].SetMarker(false);
                tempIndex = index;
                
            }
        }
        
           
        

    }

    public void changeEvent()
    {
        isUpdate = true;
        OnCreateList(numOfCells);
    }
    public void TempChangeEvent()
    {
        //isUpdate = true;
        OnCreateList(numOfCells);
    }
    //public Sprite AddSprite(Texture2D tex)
    //{
    //    Texture2D _texture = tex;
    //    Sprite newSprite = Sprite.Create(_texture, new Rect(0f, 0f, _texture.width, _texture.height), new Vector2(0.5f, 0.5f), 128f);
    //    GameObject sprGameObj = new GameObject();
    //    sprGameObj.name = "something";
    //    sprGameObj.AddComponent<SpriteRenderer>();
    //    SpriteRenderer sprRenderer = sprGameObj.GetComponent<SpriteRenderer>();
    //    sprRenderer.sprite = newSprite;
    //    return sprGameObj;
    //}
    string ColorToHex(Color32 color)
    {
        string hex ="#"+color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
        return hex;
    }
    public void resetScroller()
    {
        if (IsInst)
        {
            selectedIndex = 0;
            tempIndex = 0;
            MarkAgain(selectedIndex);
           
        }
        else
        {
            selectedIndex = -1;
            tempIndex = -1;
            MarkAgain(selectedIndex);
        }
        //if (cells.Count>0)
        //{
        //    cells[0].clickit();
        //}
        
    }
    public void resetScroller1()
    {
        //int temp;
        //temp = selectedIndex;
        //selectedIndex = -1;
        //MarkAgain(selectedIndex);
        //selectedIndex = temp;
      //  Debug.Log(selectedIndex);
        if (selectedIndex >0)
        {
            cells[selectedIndex].SetMarker(false);
        }
       
        
    }
    
}
