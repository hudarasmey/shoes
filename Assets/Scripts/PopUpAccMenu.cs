﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PopUpAccMenu : MonoBehaviour {

    public Camera myCamera;
    public Decal DecalSc;
    public UILineRenderer uiLineRender;
    public Image buttonImage;

    public GameObject targetPoint;
    public Image targetImage;
    public Canvas canvas;
   // public bool isVisiable;
    public float xOffset;
    public bool isRefreshButtonMatreial;
    Vector2 startImagePos;
    public Vector2 tempButtonPos;
    public Texture2D texture;
    public Color _color;
    //public bool tempPool;
    // Use this for initialization
    void Start()
    {
       // tempButtonPos = new Vector2(buttonImage.gameObject.transform.parent.GetComponent<Image>().transform.position.x, buttonImage.gameObject.transform.parent.GetComponent<Image>().transform.position.y);
        //uiLineRender.GetComponent<UILineRenderer>().Points = new Vector2[3];
        //////uiLineRender.Points = new Vector2[2];
        //isVisiable = false;
       // startImagePos = new Vector2(buttonImage.gameObject.transform.parent.GetComponent<Image>().transform.position.x, buttonImage.gameObject.transform.parent.GetComponent<Image>().transform.position.y);
    }

    // Update is called once per frame
    void Update()
    {
        //if (uiLineRender.Points[0].x != buttonImage.gameObject.GetComponent<Image>().rectTransform.localPosition.x || uiLineRender.Points[0].y != buttonImage.gameObject.GetComponent<Image>().rectTransform.localPosition.y)
        //{

        //Debug.Log("sdsD");
        ////uiLineRender.Points[0] = buttonImage.gameObject.GetComponent<Image>().rectTransform.localPosition;
        // uiLineRender.Points[1] = new Vector2(uiLineRender.Points[1].x, buttonImage.rectTransform.localPosition.y);
        //Vector2 temp = WorldToCanvasPoint(canvas, targetPoint.transform.position, null);
        //uiLineRender.Points[1] = new Vector2(temp.x, buttonImage.gameObject.transform.parent.GetComponent<Image>().rectTransform.localPosition.y);
        ////uiLineRender.refresh();
        //Vector2 temp2 = WorldToCanvasPoint(canvas, targetPoint.transform.position, null);
        //uiLineRender.Points[1] = new Vector2(buttonImage.rectTransform.localPosition.x+ xOffset, buttonImage.rectTransform.localPosition.y);

        //}

        //if (targetImage.transform.position.x - Screen.width / 2 < 0)
        //{
        //    if (buttonImage.gameObject.transform.parent.GetComponent<Image>().transform.position.x != tempButtonPos.x - (Screen.width / 2))
        //    {
        //        // buttonImage.gameObject.transform.parent.GetComponent<Image>().transform.position = new Vector2(tempButtonPos.x - (Screen.width / 2), buttonImage.gameObject.transform.parent.GetComponent<Image>().transform.position.y);

        //        buttonImage.gameObject.transform.parent.GetComponent<Image>().transform.position = new Vector2(tempButtonPos.x - (Screen.width / 2), buttonImage.gameObject.transform.parent.GetComponent<Image>().transform.position.y);
        //        // Debug.Log(gameObject.transform.name + " a2al" + (targetImage.transform.position.x - Screen.width / 2).ToString());
        //    }




        //}
        //else
        //{
        //    if (buttonImage.gameObject.transform.parent.GetComponent<Image>().transform.position.x != tempButtonPos.x)
        //    {
        //        buttonImage.gameObject.transform.parent.GetComponent<Image>().transform.position = new Vector2(tempButtonPos.x, buttonImage.gameObject.transform.parent.GetComponent<Image>().transform.position.y);
        //        //Debug.Log(gameObject.transform.name + " aktar" + (targetImage.transform.position.x - Screen.width / 2).ToString());
        //    }


        //}
        if (DecalSc.selectedItemIndex == -1)
        {
            if (targetPoint != null)
            {
                targetPoint = null;
            }
        }
        else
        {

            if (DecalSc.accessoriesList.Count > 0)
            {
                targetPoint = DecalSc.accessoriesList[DecalSc.selectedItemIndex].transform.gameObject;//.position;
            }
            if (targetPoint != null)
            {
                Vector3 worldToScreen = myCamera.WorldToScreenPoint(targetPoint.transform.position);
            }
        }
            
            //Debug.Log(worldToScreen + " " + Screen.width / 2 + " " + Screen.height / 2);
            ////if (uiLineRender.Points[1].x != worldToScreen.x || uiLineRender.Points[1].y != worldToScreen.y)
            ////{
            ////    Vector2 temp= WorldToCanvasPoint(canvas, targetPoint.transform.position, null);
            ////    uiLineRender.Points[1] = new Vector2(temp.x - xOffset, temp.y-xOffset);// new Vector2(-Screen.width/2+worldToScreen.x,-Screen.height/2+worldToScreen.y);
            ////    uiLineRender.refresh();
            ////}
           // targetImage.transform.position = worldToScreen;
           
            //Vector2 tempd = WorldToCanvasPoint(canvas, targetPoint.transform.position, null);
            //uiLineRender.Points[1] = new Vector2(tempd.x, buttonImage.gameObject.transform.parent.GetComponent<Image>().rectTransform.localPosition.y);
            ////uiLineRender.refresh();
        


        //  uiLineRender.Points[1] = targetPoint;
        //uiLineRender.Points[0]
    }
    //static public Vector2 WorldToCanvasPoint(this Canvas canvas, Vector3 worldPosition, Camera camera = null)
    //{
    //    if (camera == null)
    //        camera = Camera.main;
    //    var screenPosition = camera.WorldToScreenPoint(worldPosition);
    //    var scaler = canvas.GetComponentInParent<CanvasScaler>();
    //    var guiScale = 1.0f;
    //    if (Mathf.Approximately(scaler.matchWidthOrHeight, 0.0f))
    //        guiScale = scaler.referenceResolution.x / (float)Screen.width;

    //    else if (Mathf.Approximately(scaler.matchWidthOrHeight, 1.0f))
    //        guiScale = scaler.referenceResolution.y / (float)Screen.height;
    //    return new Vector2(
    //        (screenPosition.x - (Screen.width * 0.5f)) * guiScale,
    //        (screenPosition.y - (Screen.height * 0.5f)) * guiScale);
    //}
    public void VisabiltyAction(bool _bool)
    {

        buttonImage.transform.parent.gameObject.SetActive(_bool);
        uiLineRender.gameObject.SetActive(_bool);
     //   targetImage.gameObject.SetActive(_bool);

    }

    public void SetTexture(Texture2D imagemat, Color color)
    {
       // Debug.Log("OOO");
        Sprite imageContent;
        imageContent = Sprite.Create(imagemat, new Rect(0, 0, imagemat.width, imagemat.height), new Vector2(0.5f, 0.5f), 1);
        buttonImage.GetComponent<Image>().sprite = imageContent;
        buttonImage.GetComponent<Image>().color = color;
        // return imageContent;
    }
}
