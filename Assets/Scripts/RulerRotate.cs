﻿using UnityEngine;
using System.Collections;

public class RulerRotate : MonoBehaviour {
    float Ysensitivity=10;
    public GameObject camera;
    public GameObject ruler;
    public TextMesh rulerText;
    public GameObject emoji;
    public Texture v1, v2;
    public float maxX, minX;
    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        transform.rotation = Quaternion.LookRotation(new Vector3( camera.transform.position.x,camera.transform.position.y*0,camera.transform.position.z))* Quaternion.Euler(0, -180, 0);
       // ClampRotation(float minAngle, float maxAngle, float clampAroundAngle = 0)
       // float mouseY = Input.GetAxis("Mouse Y") * Ysensitivity;
       // transform.localEulerAngles.x = YourMathf.ClampAngle(transform.localEulerAngles.x,minX,maxX);
       // transform.Rotate(0, mouseY, 0);


       ruler.transform.localPosition=new Vector3(Mathf.Sign(ruler.transform.localPosition.x)* moveruler(minX, maxX),0,0);

        if (camera.transform.eulerAngles.y < 270 && camera.transform.eulerAngles.y > 90) 
            {
               // Debug.Log("R");
                if (Mathf.Sign(ruler.transform.localPosition.x) == -1)
                {
                    ruler.transform.localPosition = new Vector3(ruler.transform.localPosition.x*-1 , ruler.transform.localPosition.y, ruler.transform.localPosition.z);
                    ruler.transform.localScale = new Vector3(ruler.transform.localScale.x * -1, ruler.transform.localScale.y, ruler.transform.localScale.z);
                   //rulerText.anchor = TextAnchor.MiddleLeft;
                   // rulerText.anchor = TextAnchor.LowerLeft;
                    ruler.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", v1);
                    rulerText.anchor = TextAnchor.LowerCenter;
                    rulerText.transform.localPosition = new Vector3(-rulerText.transform.localPosition.x, rulerText.transform.localPosition.y, rulerText.transform.localPosition.z);
                    emoji.transform.localPosition = new Vector3(-emoji.transform.localPosition.x, emoji.transform.localPosition.y, emoji.transform.localPosition.z);
                }
               
                
                
            }
        
            else if ( (camera.transform.eulerAngles.y < 90 && camera.transform.eulerAngles.y >=0)|| (camera.transform.eulerAngles.y > 270 && camera.transform.eulerAngles.y < 360))
            {


                if (Mathf.Sign(ruler.transform.localPosition.x) != -1)
                {
                  //  Debug.Log("L");
                    ruler.transform.localPosition = new Vector3(-ruler.transform.localPosition.x , ruler.transform.localPosition.y, ruler.transform.localPosition.z);
                    ruler.transform.localScale = new Vector3(ruler.transform.localScale.x * -1, ruler.transform.localScale.y, ruler.transform.localScale.z);
                    //rulerText.anchor = TextAnchor.LowerCenter;
                  //
                    ruler.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", v2);
                    rulerText.anchor = TextAnchor.LowerCenter;
                    rulerText.transform.localPosition = new Vector3(-rulerText.transform.localPosition.x, rulerText.transform.localPosition.y, rulerText.transform.localPosition.z);
                    emoji.transform.localPosition = new Vector3(-emoji.transform.localPosition.x, emoji.transform.localPosition.y, emoji.transform.localPosition.z);
                 //   rulerText.anchor = TextAnchor.LowerRight;
                }
                
               
                
              
            }
       
	}
    float moveruler(float minXMove, float maxXMove)
    {
        if (ruler.GetComponent<zzVerticalRuler>().scrollervalue < 0.5f)
        {
            //float h = (0.5f-(ruler.GetComponent<zzVerticalRuler>().scrollervalue / 2)) * (maxXMove - minXMove)/2;
            float h = ((maxXMove - minXMove) * (0.5f - ruler.GetComponent<zzVerticalRuler>().scrollervalue)) / 0.5f;
            //Debug.Log(h);
            return maxXMove-h ;
        }
        else
        {
            return  maxXMove;
        }
    }
    void ClampRotation(float minAngle, float maxAngle, float clampAroundAngle = 0)
    {
        //clampAroundAngle is the angle you want the clamp to originate from
        //For example a value of 90, with a min=-45 and max=45, will let the angle go 45 degrees away from 90

        //Adjust to make 0 be right side up
        clampAroundAngle += 180;

        //Get the angle of the z axis and rotate it up side down
        float z = transform.rotation.eulerAngles.z - clampAroundAngle;

        z = WrapAngle(z);

        //Move range to [-180, 180]
        z -= 180;

        //Clamp to desired range
        z = Mathf.Clamp(z, minAngle, maxAngle);

        //Move range back to [0, 360]
        z += 180;

        //Set the angle back to the transform and rotate it back to right side up
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, z + clampAroundAngle);
    }

    //Make sure angle is within 0,360 range
    float WrapAngle(float angle)
    {
        //If its negative rotate until its positive
        while (angle < 0)
            angle += 360;

        //If its to positive rotate until within range
        return Mathf.Repeat(angle, 360);
    }
}
