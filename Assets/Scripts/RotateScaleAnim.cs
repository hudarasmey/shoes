﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateScaleAnim : MonoBehaviour {
    public bool rotateClockwise;
    public float speed=1;
	// Use this for initialization
	void Start () {
		
	}
    Transform trans; //giving Transform a reference

    void Awake() //When game starts...
    {
        trans = GetComponent<Transform>(); //return a component to our "trans:
    }

    void Update() //every frame...
    {
        if (rotateClockwise) //check if the player is pressing the left arrow key...
        {
            trans.Rotate(0, 0, speed *-1*Time.deltaTime); //if the player is, then rotate counterclockwise in the x axis    
        }
        else 
        {
            trans.Rotate(0, 0, speed*1* Time.deltaTime); //if he is, then rotate clockwise in the x axis
        }
    }
}
