﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public enum AminationStatus { forward, back };
public class GUIAnimation : MonoBehaviour {
    public AnimationCurve MoveCurve;
    public bool isTranslateAnim = true;
    public AnimationType _AnimationType;
    public AminationStatus status = AminationStatus.forward;
    public bool start = true, isReverse = true;
    public float timeOfTravel=.4f; //time after object reach a target place 
    
     float currentTime=0; // actual floting time 
     float normalizedValue;
    public bool UsePosValue;
     public float xShift, yShift;
     public float delayTime;
     public Vector3 startPosition, endPosition;

     //getting reference to this compon
    //Declare RectTransform in script
     RectTransform rectTransform;
    RectTransform faceButton;
    
    //The new position of your button
    Vector3 newPos = new Vector3(-42, 0, 0);
    //Reference value used for the Smoothdamp method
    private Vector3 buttonVelocity = Vector3.zero;


    public bool isPopAnim = true;
    public Image TargetPopImage;
    public Image blingImage;
    //Smooth time
    private float smoothTime = 0.5f;

    static GameObject target;
    static float _currentScale = InitScale, currentScale;
    
    const float InitScale = 1f;
    const int FramesCount = 10;
    private Coroutine PopCor;
    static bool _upScale = true;
    public bool _isBling = true;
    public float minBling= 2f, maxBling= 3.5f, minTimeDelay=0, maxTimeDelay=2.5f;
    void Awake()
    {
        start = true;
       // Debug.Log("kdk");
        rectTransform = GetComponent<RectTransform>();
        endPosition = rectTransform.anchoredPosition;
        if (!UsePosValue)
        {

        rectTransform.anchoredPosition = startPosition = new Vector2(rectTransform.anchoredPosition.x + (Screen.width*xShift), rectTransform.anchoredPosition.y + (Screen.height* yShift));

        }
        else
        {
            rectTransform.anchoredPosition = startPosition;
        }
       
        //startPosition = GetComponent<RectTransform>().localPosition;
        //Get the RectTransform component
        //faceButton = GetComponent<RectTransform>();
    }
    void Start()
    {
      //  rectTransform.anchoredPosition = startPosition = new Vector2(rectTransform.anchoredPosition.x + (Screen.width * xShift), rectTransform.anchoredPosition.y + (Screen.height * yShift));
       // start = true;
        //if ( (status == AminationStatus.forward || (status == AminationStatus.back && isReverse)))
        //{

        //    StartCoroutine(LerpObject(delayTime));

        //}
       // rectTransform = GetComponent<RectTransform>(); 
    }

    void Update()
    {
        if (!_isBling)
        {
            _isBling = true;
            StartCoroutine(repeatBling(3,6f));
        }
        //Update the localPosition towards the newPos
        if (start && (status == AminationStatus.forward || (status == AminationStatus.back && isReverse)))
        {
            start = false;
          
                StartCoroutine(LerpObject(delayTime));
           
        }
       // faceButton.localPosition = Vector3.SmoothDamp(gameObject.transform.localPosition, newPos, ref buttonVelocity, smoothTime);
    }

    IEnumerator LerpObject(float delay)
    {
        yield return new WaitForSeconds(delay);
        while (currentTime <= timeOfTravel)
        {
            currentTime += Time.deltaTime;
           
            normalizedValue = currentTime / timeOfTravel; // we normalize our time
            float time = 0;
            switch (_AnimationType)
            {
                case AnimationType.Linear:
                    time = normalizedValue;
                    switch (status)
                    {


                        case AminationStatus.forward:

                            rectTransform.anchoredPosition = Vector3.Lerp(startPosition, endPosition, time);
                            break;
                        case AminationStatus.back:
                            rectTransform.anchoredPosition = Vector3.Lerp(endPosition, startPosition, time);
                            break;
                        default:
                            break;
                    }
                    break;
                case AnimationType.Curve:
                    time = MoveCurve.Evaluate(normalizedValue);
                    switch (status)
                    {


                        case AminationStatus.forward:

                            rectTransform.anchoredPosition = Vector3.Lerp(startPosition, endPosition, time);
                            break;
                        case AminationStatus.back:
                            rectTransform.anchoredPosition = Vector3.Lerp(endPosition, startPosition, time);
                            break;
                        default:
                            break;
                    }
                    
                    break;
                case AnimationType.Swing:


                    time = Mathf.Sin(normalizedValue * Mathf.PI / 2) * (1 + Mathf.Sin(normalizedValue * Mathf.PI) * 0.1f);
                    switch (status)
                    {
                        case AminationStatus.forward:

                            rectTransform.anchoredPosition = Vector3.LerpUnclamped(startPosition, endPosition, time);
                            break;
                        case AminationStatus.back:
                            rectTransform.anchoredPosition = Vector3.LerpUnclamped(endPosition, startPosition, time);
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
            
           
           
            
            yield return null;
        }
        //if (isReverse)
        //{
        //    if (status == AminationStatus.forward)
        //    {
        //        status = AminationStatus.back;
        //    }
        //    else
        //    {
        //        status = AminationStatus.forward;
        //    }
        //}
       
       // start = false;
        currentTime = 0;
    }

    void scale()
    {
        //txt.rectTransform.localScale = new Vector3(newScale, 1.0f, 1.0f);//This works

        //txt.rectTransform.localScale.Set(newScale, 1.0f, 1.0f); //This DOES not. Despicable bug
        Vector3 ff = Vector3.SmoothDamp(gameObject.transform.localPosition, newPos, ref buttonVelocity, smoothTime);
        rectTransform.localScale.Set(ff.x,ff.y,ff.z) ;
    }
    void OnDisable()
    {
        _isBling = false;
       // Debug.Log("ff");
        rectTransform.anchoredPosition = startPosition;
        start = true;
    }
    void OnEnable()
    {
        StartCoroutine(repeatBling(minTimeDelay, maxTimeDelay));
    }
    public void Deactive()
    {
        StartCoroutine(LerpObject(delayTime));
    }
    /// <summary>
    ///PopAnim
    /// </summary>
    /// <param name="animTime"></param>
    /// <param name="To_Sprite"></param>
    /// <returns></returns>
    public  IEnumerator pulse(float animTime, Sprite To_Sprite, Sprite Back_To_Sprite)
    {
        if (TargetPopImage.gameObject.activeSelf == true)
        {

            Sprite ko = To_Sprite;
            float TargetScale = 1.2f;
            float AnimationTimeSeconds = animTime;
            float _deltaTime = AnimationTimeSeconds / FramesCount;
            float _dx = (TargetScale - InitScale) / FramesCount;
            //while (true)
            //{
            while (_upScale)
            {
                _currentScale += _dx;
                if (_currentScale > TargetScale)
                {
                    _upScale = false;
                    _currentScale = TargetScale;
                }
                TargetPopImage.transform.localScale = Vector3.one * _currentScale;
                yield return new WaitForSeconds(_deltaTime);
            }
            if (ko != null)
            {
                SwipeTexture(TargetPopImage, ko);
                if (Back_To_Sprite != null)
                {
                    SwipeTexture(gameObject.GetComponent<Image>(), Back_To_Sprite);
                }

            }

            while (!_upScale)
            {
                _currentScale -= _dx;
                if (_currentScale < InitScale)
                {
                    _upScale = true;
                    _currentScale = InitScale;
                }
                TargetPopImage.transform.localScale = Vector3.one * _currentScale;
                yield return new WaitForSeconds(_deltaTime);
            }
        }
        //}
    }
    //private void Start()
    //{
    //    StartCoroutine(Breath());
    //}
    public void PulseAnim(GameObject obj, float animTime, Sprite To_Sprite, Sprite back_To_Sprite)
    {

        target = obj;
        //Debug.Log("obj" + obj.name + "   " + target.activeSelf);
        if (gameObject.transform.parent.transform.parent.transform.parent.gameObject.activeSelf == true)
        {
            PopCor = StartCoroutine(pulse(animTime, To_Sprite, back_To_Sprite));
        }
        else
        {
            SwipeTexture(gameObject.GetComponent<Image>(), back_To_Sprite);
            TargetPopImage.sprite = To_Sprite;
           // SwipeTexture(TargetPopImage, ko);
        }
        
    }
    public  void SwipeTexture(Image fromSprite, Sprite To_Sprite) // Sprite from_Sprite,
    {
        fromSprite.sprite = To_Sprite;
    }
    public void PulseAnimCancel(GameObject obj, float animTime, Sprite from_Sprite,Sprite To_Sprite)
    {

        if (PopCor != null)
        {
            StopCoroutine(PopCor);
            
        }

       TargetPopImage.sprite = To_Sprite;
        TargetPopImage.transform.localScale = Vector3.one;
    }

    public IEnumerator Bling(float animTime)
    {
        if (blingImage != null)
        {
            _isBling = true;
            currentScale = 0;
            blingImage.transform.localScale = Vector3.zero;
            float initScale = 0;
            float TargetScale = Random.Range(minBling, maxBling);
            float AnimationTimeSeconds = animTime;
            float _deltaTime = AnimationTimeSeconds / 10;
            float _dx = (TargetScale - initScale) / 10;
            //while (true)
            //{
            while (_isBling)
            {
               // Debug.Log(_currentScale);
                currentScale += _dx;
                if (currentScale > TargetScale)
                {
                    currentScale = 0;
                    _isBling = false;
                     initScale = 0;
                    blingImage.transform.localScale = Vector3.zero;
                    break;
                    //_currentScale = TargetScale;
                }
                blingImage.transform.localScale = Vector3.one * currentScale;
                yield return new WaitForSeconds(_deltaTime);
            }
        }else
        {
            _isBling = true;
        }
      
            //  StartCoroutine( Bling(0.1f));
        }
    public IEnumerator repeatBling(float min,float max)
    {
        yield return new WaitForSeconds(Random.Range(min, max));
        StartCoroutine(Bling(0.2f));
    }
}
