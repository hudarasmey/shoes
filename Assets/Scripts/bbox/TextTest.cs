﻿using UnityEngine;
using System.Collections;

public class TextTest : MonoBehaviour
{

    public Font myFont;
    public Material myFontMaterial;

    // Use this for initialization
    void Start()
    {
        // Create new object to display 3d text
        GameObject myTextObject = new GameObject("myText");
        myTextObject.AddComponent<TextMesh>();
        //myTextObject.AddComponent("MeshRenderer");
        //myTextObject.renderer.material.color = Color.black;

        // Get components
        TextMesh textMeshComponent = myTextObject.GetComponent(typeof(TextMesh)) as TextMesh;
        MeshRenderer meshRendererComponent = myTextObject.GetComponent(typeof(MeshRenderer)) as MeshRenderer;

        // Set font of TextMesh component (it works according to the inspector)
        textMeshComponent.font = myFont;

        // Create an array of materials for the MeshRenderer component (it works according to the inspector)
        meshRendererComponent.materials = new Material[1];

        // Set the text string of the TextMesh component (it works according to the inspector)
        textMeshComponent.text = "Testing";

        // Test that we do indeed have a fontMaterial that is not null
        Debug.Log("own fontMaterial name: " + myFontMaterial.name);
        // prints out "own fontMaterial name: font material"

        Debug.Log("MeshRenderer Material name before assignment: " + meshRendererComponent.materials[0].name);
        // prints out "MeshRenderer Material name before assignment: Default-Diffuse (Instance)"

        // Set the material of the MeshRenderer component (IT DOERSN'T DO ANYTHING according to the inspector)
      //  meshRendererComponent.materials[0] = myFontMaterial;
        meshRendererComponent.material = myFontMaterial;

        Debug.Log("MeshRenderer Material name after assignment: " + meshRendererComponent.materials[0].name);
        // prints out "MeshRenderer Material name after assignment: Default-Diffuse (Instance)"
    }

    // Update is called once per frame
    void Update()
    {

    }
}