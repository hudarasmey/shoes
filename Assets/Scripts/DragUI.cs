﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class DragUI : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public bool mouseDown = false;
    public Vector3 startMousePos;
    public Vector3 startPos;
    public bool restrictX;
    public bool restrictY;
    public float fakeX;
    public float fakeY;
    public float myWidth;
    public float myHeight;

    public RectTransform ParentRT;
    public RectTransform MyRect;


    private bool isTouchingDevice;

    private Vector3 lastInputWorldPos;
    private Vector3 currentInputWorldPos;
    private Vector3 deltaInputWorldPos;
    public RectTransform start, end;
    public bool actionActivation;
    public Vector3 targetScale;
    Vector3 startScale;
    public Vector3 ss;
    public float var;
    public float scaleTempX, scaleTempY;
    public float tempXPos;
    void Awake()
    {


        switch (Application.platform)
        {
            case RuntimePlatform.WindowsEditor:
                isTouchingDevice = false;
                break;
            case RuntimePlatform.Android:
                isTouchingDevice = true;
                break;
        }
    }

    void Start()
    {
        MyRect.position = start.position;
        startScale = MyRect.localScale;
        ss = gameObject.transform.localScale - targetScale;

        myWidth = (MyRect.rect.width + 5) / 2;
        myHeight = (MyRect.rect.height + 5) / 2;
    }


    public void OnPointerDown(PointerEventData ped)
    {
        mouseDown = true;
        startPos = transform.position;
        startMousePos = Input.mousePosition;

    }

    public void OnPointerUp(PointerEventData ped)
    {
        mouseDown = false;
    }

    void storeFingerPosition()
    {
        if (Input.GetTouch(0).phase == TouchPhase.Began)
        {
           
            lastInputWorldPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
        }
        else if (Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            currentInputWorldPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            deltaInputWorldPos = new Vector3(currentInputWorldPos.x - lastInputWorldPos.x, 0.0f, 0.0f);
            //foreach (ListBox listbox in listBoxes)
            //    listbox.updatePosition(deltaInputWorldPos);

            lastInputWorldPos = currentInputWorldPos;
        }
        else if (Input.GetTouch(0).phase == TouchPhase.Ended) {
            //inUp();
        }
        // setSlidingEffect();
    }
    void storeMousePosition()
    {
        if (Input.GetMouseButtonDown(0))
        {
            
            lastInputWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
        else if (Input.GetMouseButton(0))
        {
            
            currentInputWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            deltaInputWorldPos = new Vector3(currentInputWorldPos.x - lastInputWorldPos.x, 0.0f, 0.0f);
            //foreach (ListBox listbox in listBoxes)
            //    listbox.updatePosition(deltaInputWorldPos);

            lastInputWorldPos = currentInputWorldPos;
        }
        else if (Input.GetMouseButtonUp(0))
        {
           
            //inUp();
            //setSlidingEffect();
        }
    }
    void inUp()
    {
        if (MyRect.position.x != end.position.x || MyRect.position.x != start.position.x)
        {
            float dis =end.position.x- start.position.x  ;
          //  Debug.Log("fff" + dis);
            tempXPos = MyRect.position.x;
            if (MyRect.position.x - start.position.x <= dis / 2)
            {
                float StartSpeed = 35;
                tempXPos = floatTo(tempXPos, start.position.x, Time.deltaTime * StartSpeed * 10);
            }
            else
            {
                float StartSpeed = 35;
                tempXPos = floatTo(tempXPos, end.position.x, Time.deltaTime * StartSpeed * 10);
            }
            MyRect.position = new Vector2(tempXPos,start.position.y);//start.position.y);
            scaleTempX = ((MyRect.position.x - start.position.x) * ss.x) / (end.position.x - start.position.x);
            scaleTempY = ((MyRect.position.x - start.position.x) * ss.y) / (end.position.x - start.position.x);
            MyRect.localScale = new Vector3(startScale.x - scaleTempX, startScale.y - scaleTempY, MyRect.localScale.z);
        }
        
       // transform.position = floatTo(transform.position, tempHealth, Time.deltaTime * StartSpeed * 10);
    }
    void Update()
    {
        if (!mouseDown)
        {
            
            inUp();
            //setSlidingEffect();
        }
        if (MyRect.position.x >= end.position.x)
        {
            actionActivation = true;
        }
        
        if (mouseDown && MyRect.position.x >= start.position.x && MyRect.position.x <= end.position.x && !actionActivation)
        {
            actionActivation = false;

            if (!isTouchingDevice)
                storeMousePosition();
            else
                storeFingerPosition();


            transform.position += deltaInputWorldPos;

            scaleTempX = ((MyRect.position.x - start.position.x) * ss.x) / (end.position.x - start.position.x);
            scaleTempY = ((MyRect.position.x - start.position.x) * ss.y) / (end.position.x - start.position.x);
            MyRect.localScale = new Vector3(startScale.x - scaleTempX, startScale.y - scaleTempY, MyRect.localScale.z);
            //Vector3 currentPos = Input.mousePosition;
            ////Vector3 currentPos =new Vector3( Input.mousePosition.x,0,0);
            //Vector3 diff = currentPos - startMousePos;
            //Vector3 pos = startPos + diff;
            // transform.position = pos;

            //if (transform.localPosition.x < 0 - ((ParentRT.rect.width / 2) - myWidth) || transform.localPosition.x > ((ParentRT.rect.width / 2) - myWidth))
            //    restrictX = true;
            //else
            //    restrictX = false;

            //if (transform.localPosition.y < 0 - ((ParentRT.rect.height / 2) - myHeight) || transform.localPosition.y > ((ParentRT.rect.height / 2) - myHeight))
            //    restrictY = true;
            //else
            //    restrictY = false;

            //if (restrictX)
            //{
            //    if (transform.localPosition.x < 0)
            //        fakeX = 0 - (ParentRT.rect.width / 2) + myWidth;
            //    else
            //        fakeX = (ParentRT.rect.width / 2) - myWidth;

            //    Vector3 xpos = new Vector3(fakeX, transform.localPosition.y, 0.0f);
            //    transform.localPosition = xpos;
            //}

            //if (restrictY)
            //{
            //    if (transform.localPosition.y < 0)
            //        fakeY = 0 - (ParentRT.rect.height / 2) + myHeight;
            //    else
            //        fakeY = (ParentRT.rect.height / 2) - myHeight;

            //    Vector3 ypos = new Vector3(transform.localPosition.x, fakeY, 0.0f);
            //    transform.localPosition = ypos;
            //}

        }
        else
        {

            if (MyRect.position.x <= start.position.x)
            {
                MyRect.position = start.position;
            }
            if (MyRect.position.x >= end.position.x)
            {
                MyRect.position = end.position;
            }
        }
    }
    float floatTo(float var, float to, float speed)
    {


        if (var != to)
        {
            //camera.IsActive = true;
            //camera.startMove();
            var = Mathf.MoveTowards(var, to, Time.deltaTime * speed);
            // return false;
        }


        return var;

    }
}
//    private bool mouseDown = false;
//    private Vector3 startMousePos;
//    private Vector3 startPos;
//    private bool restrictX;
//    private bool restrictY;
//    private float fakeX;
//    private float fakeY;
//    private float myWidth;
//    private float myHeight;

//    public RectTransform ParentRT;
//    public RectTransform MyRect;

//    void Start()
//    {
//        myWidth = (MyRect.rect.width + 5) / 2;
//        myHeight = (MyRect.rect.height + 5) / 2;
//    }


//    public void OnPointerDown(PointerEventData ped)
//    {
//        mouseDown = true;
//        startPos = transform.position;
//        startMousePos = Input.mousePosition;
//    }

//    public void OnPointerUp(PointerEventData ped)
//    {
//        mouseDown = false;
//    }


//    void Update()
//    {
//        if (mouseDown)
//        {
//            Vector3 currentPos = Input.mousePosition;
//            Vector3 diff = currentPos - startMousePos;
//            Vector3 pos = startPos + diff;
//            transform.position = pos;

//            if (transform.localPosition.x < 0 - ((ParentRT.rect.width / 2) - myWidth) || transform.localPosition.x > ((ParentRT.rect.width / 2) - myWidth))
//                restrictX = true;
//            else
//                restrictX = false;

//            if (transform.localPosition.y < 0 - ((ParentRT.rect.height / 2) - myHeight) || transform.localPosition.y > ((ParentRT.rect.height / 2) - myHeight))
//                restrictY = true;
//            else
//                restrictY = false;

//            if (restrictX)
//            {
//                if (transform.localPosition.x < 0)
//                    fakeX = 0 - (ParentRT.rect.width / 2) + myWidth;
//                else
//                    fakeX = (ParentRT.rect.width / 2) - myWidth;

//                Vector3 xpos = new Vector3(fakeX, transform.localPosition.y, 0.0f);
//                transform.localPosition = xpos;
//            }

//            if (restrictY)
//            {
//                if (transform.localPosition.y < 0)
//                    fakeY = 0 - (ParentRT.rect.height / 2) + myHeight;
//                else
//                    fakeY = (ParentRT.rect.height / 2) - myHeight;

//                Vector3 ypos = new Vector3(transform.localPosition.x, fakeY, 0.0f);
//                transform.localPosition = ypos;
//            }

//        }
//    }
//}