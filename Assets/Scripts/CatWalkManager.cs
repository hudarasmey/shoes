﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CatWalkManager : MonoBehaviour {
    public GameObject Shose,ShoseClone,cloneLabel;
    public GameObject accessoriesObjs;
    public Transform org_objTransform;
    public Vector3 Org_Pos; 
       Quaternion Org_Rot;
    public int indexOfTransform = 0;
    public Transform[] left_objTransform, right_objTransform;
    public modelControl1 modelcontrol;
    public GameObject mainScene, DarkScene;
    public Camera cam;
   // public SkinnedMeshRenderer _skinMesh;
	// Use this for initialization
	void Start () {
	
	}
    void OnEnable()
    {
        Org_Pos = Shose.transform.localPosition;
        Org_Rot = Shose.transform.localRotation;
        mainScene.SetActive(false);
        DarkScene.SetActive(true);
       // org_objTransform = Shose.transform;
        if (ShoseClone == null)
        {
            InstNewItem(Shose, right_objTransform[indexOfTransform]);
        }
        
    }
    void OnDisable()
    {
        //mainScene.SetActive(true);
        //DarkScene.SetActive(false);
        if (modelcontrol.mainIndex != 7)
        {
            if (ShoseClone != null)
            {
                Destroy(ShoseClone);
            }
            if (Shose != null)
            {

                    //Shose.SetActive(false);
                    //Shose.transform.localPosition = Org_Pos;
                    //Shose.transform.localRotation = Org_Rot;
                    //Shose.SetActive(true);

            }
        }
       
    }
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.O))
        {
            InstNewItem(Shose, right_objTransform[indexOfTransform]);
            //FlipThenScaleSkinnedMesh(_objTransform, _skinMesh, new Vector3(1, 1, -1));
         //  InstNewItem(Shose, _objTransform);
        }

	}

    public void InstNewItem(GameObject Obj, Transform Objtrans)//,List<GameObject> ListOfObj, GameObject parentObj) // inst new hero
    {

        // AccItem acc = new AccItem();

       // AccItem GameObject instObjnewItem = Instantiate(ItemBase, position, rotate) as AccItem;
        //GameObject instObj = new GameObject();
        Obj.SetActive(false);
        GameObject instObj = Instantiate(Obj, Vector3.zero, Quaternion.identity) as GameObject;
        //GameObject instObj = Instantiate(Obj, Objtrans) as GameObject;
        //instObj.transform.localPosition = Objtrans.position;
        //instObj.transform.localRotation = Objtrans.rotation;// Quaternion.Euler(Objtrans.rotation);

        instObj.transform.localScale = new Vector3(instObj.transform.localScale.x, instObj.transform.localScale.y, instObj.transform.localScale.z * -1);
        ShoseClone = instObj;
        cloneLabel = ShoseClone.GetComponent<Shose>().labelObj;
        ShoseClone.GetComponent<Shose>().labelObj.transform.localPosition = new Vector3(-1.55508f, 1.430001f, -8.34554f);
        cloneLabel.transform.eulerAngles = new Vector3(0f, -5.338f, 0f);
        ShoseClone.GetComponent<Shose>().labelObj.transform.localScale = new Vector3(1, 1, -1);
        changePosition(instObj, right_objTransform, indexOfTransform);

        
        //0.05524689 ,0.384798,-8.739882 rr -0.129 4.51 0.359 ss 1 1 -1
        //ListOfObj.Add(instObj);
        changePosition(Obj, left_objTransform, indexOfTransform);
        Obj.SetActive(true);
        instObj.SetActive(true);
        //instObj.transform.parent = parentObj.transform;

    }
    // _objTransform: the transform of the main parent game object
    // _skinMesh: the skinned mesh renderer component of the main parent game object
    // _scale: the scale to apply arbitrarily lets imagine that, new Vector3(-1,1,1) is being pasted in
    public void changePosition(GameObject obj,Transform[] _Transform, int index)
    {
        obj.transform.localPosition = new Vector3(_Transform[index].position.x, Shose.transform.localPosition.y, _Transform[index].position.z);// left_objTransform[index].position; _Transform[index].position;
        obj.transform.localRotation = _Transform[index].rotation;// Quaternion.Euler(Objtrans.rotation);
      //  obj.transform.localScale = new Vector3(_Transform[index].transform.localScale.x, _Transform[index].transform.localScale.y, _Transform[index].transform.localScale.z );
    }
    public void changeDisplay(int index)
    {
        Shose.transform.localPosition = new Vector3(left_objTransform[index].position.x, Shose.transform.localPosition.y, left_objTransform[index].position.z);// left_objTransform[index].position;
        Shose.transform.localRotation = left_objTransform[index].rotation;
        ShoseClone.transform.localPosition = new Vector3(right_objTransform[index].position.x, ShoseClone.transform.localPosition.y, right_objTransform[index].position.z);                     // right_objTransform[index].position;


        ShoseClone.transform.localRotation = right_objTransform[index].rotation;
    }
    void FlipThenScaleSkinnedMesh(Transform _objTransform, SkinnedMeshRenderer _skinMesh, Vector3 _scale)
    {
        var newMesh = GameObject.Instantiate(_skinMesh.sharedMesh) as Mesh;

        //get the vertices. this creates a copy of the verts
        var localVerts = newMesh.vertices;

        //apply the scale to each vert on the correct axis
        for (int i = 0; i < localVerts.Length; ++i)
        {
            //localVerts[i].Scale(_scale);

            //I have been trying this also
            localVerts[i] =  _objTransform.TransformPoint(localVerts[i]);

        }

        foreach (Transform b in _skinMesh.bones)
        {
            b.localScale = _scale;
        }


        //depending on if it is an even or odd scaling toggle the winding order of the triangles
        //and store them back into the meshes triangles. A solution to changing the winding
        //order has already been posted in another thread


        newMesh.vertices = localVerts;
        _objTransform.localScale = Vector3.one;

    }
}
