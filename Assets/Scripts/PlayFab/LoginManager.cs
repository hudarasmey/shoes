﻿//using Facebook.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using System;

public class LoginManager : MonoBehaviour {

    // Use this for initialization
    public GameObject Loading;
    public GameObject LoginSelect;


    PlayfabManager playfab;
    FacebookManager facebook;
    int loginValue;
    bool isConnected;
    private const bool allowCarrierDataNetwork = false;
    private const string pingAddress = "8.8.8.8"; // Google Public DNS server
    private const float waitingTime = 2.0f;

    private Ping ping;
    private float pingStartTime;
    public bool isfinishLoadPF, isLoadOldValue;
    
    // Use this for initialization
    void Start()
    {
        isConnected = false;
        //PlayerPrefs.DeleteAll();
        facebook = FindObjectOfType<FacebookManager>();
        Debug.Log("lll"+checkConnectivity());
        playfab = PlayfabManager.Instance;
        if (checkConnectivity() == false)
        {
            loginValue = PlayerPrefs.GetInt("login", 0);
          Debug.Log(loginValue);
          Loading.SetActive(true);
            InternetIsNotAvailable();
            if (loginValue == 2)
            {
                Debug.Log("POP");
                // loadSavedPic()
              //  facebook.loadSavedPic();

            }
            else if (loginValue == 1)
            {
               // facebook.FirstName = "User#"+facebook.UserID.ToString().Substring(facebook.UserID.ToString().Length - 4, 4);
                StartCoroutine(loadingScreen(.5f));
            }
            int fristTimeRun = PlayerPrefs.GetInt("firstTimeGameStart", -1);
            if (fristTimeRun == -1)
            {
                //Debug.Log(result.InfoResultPayload.UserVirtualCurrency["SC"]));
                
                    GameManager.Instance.Currency = 1000;
            
            }
            return;
        }
        else
        {
            InternetAvailable();
            StartCoroutine(CheckForConnection());
            //StartCoroutine(checkInternetConnection((Connected)=>{isConnected=true;}));
                //(isConnected) => {

            //    loginFn();
            //    ping = new Ping(pingAddress);
            //    pingStartTime = Time.time;
            //}
          //  ));

            //if (!isConnected)
            //{
                //loginValue = PlayerPrefs.GetInt("login", 0);
                //Debug.Log(loginValue);
                //Loading.SetActive(true);
            //}
            //else
            //{
            //loginFn();
            //ping = new Ping(pingAddress);
            //pingStartTime = Time.time;    
            //}
        }
      
    }
    public void Update()
    {
       //Debug.Log(isfinishLoadPF + "  " + loginValue);

        if (isfinishLoadPF)
        {
            loginValue = PlayerPrefs.GetInt("login", 0);
            if (loginValue == 2)
            {
                //if (facebook.loadImage)
                //{
                //    Debug.Log("da5al FB :(");
                //    isfinishLoadPF = false;
                //    facebook.loadImage = false;
                //    SceneManager.LoadScene("Game Scene");
                //    //StartCoroutine(AsyncLoadingScreen());
                //    //SceneManager.LoadScene("Game Scene");
                //}
               
            }
            else if (loginValue == 1)
            {
             //  Debug.Log("da5al Device :(");
               SceneManager.LoadScene("Game Scene");
               // StartCoroutine(AsyncLoadingScreen());
                //SceneManager.LoadScene("Game Scene");
                isfinishLoadPF = false;
            }
            else if (loginValue == 0 && checkConnectivity() == false)
            {
                SceneManager.LoadScene("Game Scene");
               // StartCoroutine(AsyncLoadingScreen());
                //SceneManager.LoadScene("Game Scene", LoadSceneMode.Single);
                isfinishLoadPF = false;
            }
       //     Debug.Log("Game Scene"+facebook.LoadOldInfo);
           
            
        }
        else if (loginValue == 0 && checkConnectivity() == false)
        {
            
            StartCoroutine(loadingScreen(0f));
            //SceneManager.LoadScene("Game Scene", LoadSceneMode.Single);
            isfinishLoadPF = false;
        }
        //if (facebook.LoadOldInfo)
        //{
        //    facebook.LoadOldInfo = false;
        //    StartCoroutine(loadingScreen(0f));
        //}
        //if (ping != null)
        //{
        //    bool stopCheck = true;
        //    if (ping.isDone)
        //    {
        //        if (ping.time >= 0)
        //            InternetAvailable();
        //        else
        //            InternetIsNotAvailable();
        //    }
        //    else if (Time.time - pingStartTime < waitingTime)
        //        stopCheck = false;
        //    else
        //        InternetIsNotAvailable();
        //    if (stopCheck)
        //        ping = null;
        //}
    }
    private void FacebookManager_OnLoginCall(bool success)
    {
        Debug.Log("facebook callback " + success);
        //if (success)
        //{
        //   // PlayerPrefs.SetInt("login", 2);
        //    playfab.LoginWithFaceBook(AccessToken.CurrentAccessToken.TokenString);
        //}
        //else
        //{
        //   // facebook.FBLogin();
        //}
    }

    public void LoginWithDevice()
    {
        PlayerPrefs.SetInt("login", 1);

        playfab.LoginUsingDevice();
        Loading.SetActive(true);
        LoginSelect.SetActive(false);
        StartCoroutine(loadingScreen(9));
    }

    void LoggedInAndReady(bool foundDisplayName)
    {
        isfinishLoadPF = true;
      // SceneManager.LoadScene("Game Scene");
       // Application.LoadLevel("GamePlay");
    }

    public void LoginWithFaceBook()
    {


     //   FacebookManager.Instance.InitFB();
        Loading.SetActive(true);
        LoginSelect.SetActive(false);
    }
    void loginFn()
    {
        loginValue = PlayerPrefs.GetInt("login", 0);
        Debug.Log(loginValue + "   ");
        switch (loginValue)
        {
            case 0:
                //LoginSelect.SetActive(true);
                Debug.Log("LoginWithDevice");
                LoginWithDevice();
                break;
            case 1:
                LoginWithDevice();
                break;
            case 2:
                LoginWithFaceBook();
                break;
            default:
                break;
        }

        playfab.OnPlayerLoggedIn = LoggedInAndReady;

        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        //facebook = FacebookManager.Instance;
        //FacebookManager.OnLoginCall += FacebookManager_OnLoginCall; 
    }
    bool checkConnectivity()
    {
        bool internetPossiblyAvailable;
//#if UNITY_EDITOR
//        if (NetworkMessageInfo.player.ipAddress.ToString() != "127.0.0.1")
//        {
//            internetPossiblyAvailable = true;
//        }
//        else
//        {
//            internetPossiblyAvailable = false;
//        }
//#endif
#if (UNITY_IPHONE || UNITY_ANDROID)
        //bool internetPossiblyAvailable;
        switch (Application.internetReachability)
        {
            case NetworkReachability.ReachableViaLocalAreaNetwork:
                
                internetPossiblyAvailable = true;
                break;
            case NetworkReachability.ReachableViaCarrierDataNetwork:
                internetPossiblyAvailable = allowCarrierDataNetwork;
                break;
            default:
                internetPossiblyAvailable = false;
                break;
        }
#endif

        return internetPossiblyAvailable;
    }
    private void InternetIsNotAvailable()
    {
        Debug.Log("No Internet :(");
    }

    private void InternetAvailable()
    {
        Debug.Log("Internet is available! ;)");
    }

    IEnumerator loadingScreen(float sec)
    {
        Loading.SetActive(true);
        yield return new WaitForSeconds(sec);
        // LoggedInAndReady(true);
        // SceneManager.LoadScene("Game Scene");
        SceneManager.LoadScene("Game Scene");
      //  StartCoroutine(AsyncLoadingScreen());
    }
    IEnumerator AsyncLoadingScreen()
    {
        //Debug.Log("Loading completejjjjjjjjjjjjjjjjjjjjjjjjjjjjjj");
        AsyncOperation async = Application.LoadLevelAsync("Game Scene");
        yield return async;
       // Debug.Log("Loading complete");
    }
    //IEnumerator checkInternetConnection(Action<bool> action)
    //{
    //    WWW www = new WWW("http://google.com");
    //    yield return www;
    //    if (www.error != null)
    //    {
    //        action(false);
    //    }
    //    else
    //    {
    //        action(true);
    //    }
    //}

    IEnumerator CheckForConnection()
    {
        //Ping png = new Ping("139.130.4.5");
        //Ping png = new Ping("8.8.8.8");
        //float startTime = Time.time;
        //while (Time.time < startTime + 5.0f)
        //{
           yield return new WaitForSeconds(0);
        //}
        //Debug.Log("png.isDone " + png.isDone);
        if (checkConnectivity())
        {
            print("Connected!");
            //if (!isConnected)
            //{
            //loginValue = PlayerPrefs.GetInt("login", 0);
            //Debug.Log(loginValue);
            //Loading.SetActive(true);
            //}
            //else
            //{
            loginFn();
            //ping = new Ping(pingAddress);
            //pingStartTime = Time.time;  
        }
        else
        {
            print("Not Connected!");
            loginValue = PlayerPrefs.GetInt("login", 0);
            Debug.Log(loginValue);
            Loading.SetActive(true);
            if (loginValue == 0)
            {
                loginFn();
                Debug.Log("POPjjjj");
               

            }
            else if (loginValue == 2)
            {
                Debug.Log("POP");
                // loadSavedPic()
               // facebook.loadSavedPic();

            }
            else if (loginValue == 1)
            {
                // facebook.FirstName = "User#"+facebook.UserID.ToString().Substring(facebook.UserID.ToString().Length - 4, 4);
                StartCoroutine(loadingScreen(0f));
            }
        }
    }
    //StartCoroutine(checkInternetConnection((isConnected)=>{}));
}



 //public void Start()
 //   {
 //       bool internetPossiblyAvailable;
 //       switch (Application.internetReachability)
 //       {
 //           case NetworkReachability.ReachableViaLocalAreaNetwork:
 //               internetPossiblyAvailable = true;
 //               break;
 //           case NetworkReachability.ReachableViaCarrierDataNetwork:
 //               internetPossiblyAvailable = allowCarrierDataNetwork;
 //               break;
 //           default:
 //               internetPossiblyAvailable = false;
 //               break;
 //       }
 //       if (!internetPossiblyAvailable)
 //       {
 //           InternetIsNotAvailable();
 //           return;
 //       }
 //       ping = new Ping(pingAddress);
 //       pingStartTime = Time.time;
 //   }

 //   public void Update()
 //   {
 //       if (ping != null)
 //       {
 //           bool stopCheck = true;
 //           if (ping.isDone)
 //           {
 //               if (ping.time >= 0)
 //                   InternetAvailable();
 //               else
 //                   InternetIsNotAvailable();
 //           }
 //           else if (Time.time - pingStartTime < waitingTime)
 //               stopCheck = false;
 //           else
 //               InternetIsNotAvailable();
 //           if (stopCheck)
 //               ping = null;
 //       }
 //   }

 //   private void InternetIsNotAvailable()
 //   {
 //       Debug.Log("No Internet :(");
 //   }

 //   private void InternetAvailable()
 //   {
 //       Debug.Log("Internet is available! ;)");
 //   }