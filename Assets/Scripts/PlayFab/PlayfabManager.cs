﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using System;
using UnityEngine.SceneManagement;

public class PlayfabManager : MonoBehaviour
{
    public delegate void FieldReturned(string value);
    public delegate void ListReturned(Dictionary<string, UserDataRecord> data);
    public delegate void OnPlayfabdReturnCatalogue(List<CatalogItem> data);
    public delegate void OnDisplayNameChanged(bool successful);

    public delegate void PlayerLogedIn(bool newlyCreated);
    public PlayerLogedIn OnPlayerLoggedIn;

    public static PlayfabManager Instance;
    public static string UserName;
    public bool closeSave;
    // Use this for initialization
    void Awake()
    {
        if (!Instance)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
    }

    public void LoginUsingDevice()
    {
        LoginWithCustomIDRequest request = new LoginWithCustomIDRequest()
        {
            CustomId = SystemInfo.deviceUniqueIdentifier,
            CreateAccount = true,

            InfoRequestParameters = new GetPlayerCombinedInfoRequestParams()
            {
                GetUserData = true,
                GetPlayerStatistics = true,
                GetUserVirtualCurrency = true,
                GetUserAccountInfo = true,
                GetUserInventory = true,
                GetTitleData = true
            }
        };
        PlayFabClientAPI.LoginWithCustomID(request, OnLoginSuccess, OnLoginFailure);
    }

    public void LoginWithFaceBook(string accessToken)
    {
        LoginWithFacebookRequest request = new LoginWithFacebookRequest()
        {
            AccessToken = accessToken,
            CreateAccount = true,
            InfoRequestParameters = new GetPlayerCombinedInfoRequestParams()
            {
                GetUserData = true,
                GetPlayerStatistics = true,
                GetUserVirtualCurrency = true,
                GetUserAccountInfo = true,
                GetUserInventory = true,
                GetTitleData = true
            }
        };

        PlayFabClientAPI.LoginWithFacebook(request, OnLoginSuccess, OnLoginFailure);
    }

    private void OnLoginSuccess(LoginResult result)
    {
        Debug.Log("Loged In");
        GetAccountInfo();
        if (result.NewlyCreated)
        {
            //setd
        }
        // GameManager.Instance.Currency = 500;
        //foreach (var item in result.InfoResultPayload.UserData)
        //{
        //if (item.Key == "XP")
        //    GameManager.Instance.Currency.Experience = int.Parse(item.Value.Value);
        //else if (item.Key.Contains("CurrQuest"))
        //{
        //    GameManager.Instance.CurrentQuestData.Add(
        //        JsonUtility.FromJson<QuestData>(item.Value.Value));
        //}
        //else if (item.Key.Contains("CurrGig"))
        //{
        //    GameManager.Instance.CurrentGigsData.Add(
        //        JsonUtility.FromJson<GigData>(item.Value.Value));
        //}
        //else if (item.Key.Contains("CurrAction"))
        //{
        //    GameManager.Instance.CurrentActionsData.Add(
        //      JsonUtility.FromJson<ActionData>(item.Value.Value));
        //}
        //}


        //foreach (var item in result.InfoResultPayload.UserInventory)
        //{

        //}

        checkcoins(result);

        //GameManager.Instance.Currency.Dollars = result.InfoResultPayload.UserVirtualCurrency["SC"];
        //GameManager.Instance.Currency.Glam = result.InfoResultPayload.UserVirtualCurrency["HC"];
        GameManager.Instance.UserName = result.InfoResultPayload.AccountInfo.TitleInfo.DisplayName;

        if (OnPlayerLoggedIn != null)
            OnPlayerLoggedIn(result.NewlyCreated);
    }
    void checkcoins(LoginResult result)
    {
        int fristTimeRun = PlayerPrefs.GetInt("firstTimeGameStart", -1);
        //Debug.Log(GameManager.Instance.Currency + "   " + result.InfoResultPayload.UserVirtualCurrency["SC"] + "  " + fristTimeRun);
        //  Debug.Log(fristTimeRun);
        if (fristTimeRun != -1)
        {
            if (GameManager.Instance.Currency != result.InfoResultPayload.UserVirtualCurrency["SC"])
            {
                if (GameManager.Instance.Currency > result.InfoResultPayload.UserVirtualCurrency["SC"])
                {
                    if (PlayfabManager.Instance)
                        PlayfabManager.Instance.AddCurrency("SC", GameManager.Instance.Currency - result.InfoResultPayload.UserVirtualCurrency["SC"]);
                }
                else if (GameManager.Instance.Currency < result.InfoResultPayload.UserVirtualCurrency["SC"])
                {
                    if (PlayfabManager.Instance)
                        PlayfabManager.Instance.SubtractCurrency("SC", result.InfoResultPayload.UserVirtualCurrency["SC"] - GameManager.Instance.Currency);
                }

            }
            else
            {
                GameManager.Instance.Currency = result.InfoResultPayload.UserVirtualCurrency["SC"];

            }
        }
        else
        {
            //Debug.Log(result.InfoResultPayload.UserVirtualCurrency["SC"]));
            if (1000 != result.InfoResultPayload.UserVirtualCurrency["SC"])
            {
                if (1000 > result.InfoResultPayload.UserVirtualCurrency["SC"])
                {
                    if (PlayfabManager.Instance)
                        PlayfabManager.Instance.AddCurrency("SC", 1000 - result.InfoResultPayload.UserVirtualCurrency["SC"]);


                }
                else if (1000 < result.InfoResultPayload.UserVirtualCurrency["SC"])
                {
                    if (PlayfabManager.Instance)
                        PlayfabManager.Instance.SubtractCurrency("SC", result.InfoResultPayload.UserVirtualCurrency["SC"] - 1000);
                }
                GameManager.Instance.Currency = 1000;
            }
            else
            {
                GameManager.Instance.Currency = 1000;
            }
        }
    }
   
    internal void ResetUserData()
    {
        GetUserDataRequest request = new GetUserDataRequest();

        PlayFabClientAPI.GetUserData(request, result =>
        {
            List<string> keys = new List<string>();

            foreach (var item in result.Data)
            {
                keys.Add(item.Key);
            }

            print("removed " + keys.Count + " data");

            RemoveUserData(keys,true);

        }, error => { });


        //throw new NotImplementedException();
    }

    //public void SaveCurrentQuestData(QuestData data)
    //{
    //    SetUserData("CurrQuest-" + data.QuestID, JsonUtility.ToJson(data));
    //}

    //public void SaveCurrentGigData(GigData data)
    //{
    //    SetUserData("CurrGig-" + data.gigID, JsonUtility.ToJson(data));
    //}

    //public void SaveCurrentAction(ActionData data)
    //{
    //    SetUserData("CurrAction-" + data.actionName + "-" + data.GigId, JsonUtility.ToJson(data));
    //}



    public void RemoveQuestData(string questID)
    {
        RemoveUserData("CurrQuest-" + questID);
    }
    public void RemoveGigData(string gigID)
    {
        RemoveUserData("CurrGig-" + gigID);
    }

    //public void RemoveActionData(ActionData data)
    //{
    //    RemoveUserData("CurrAction-" + data.actionName + "-" + data.GigId);
    //}

    //public void RemoveActionData(List<ActionData> data)
    //{
    //    List<string> keys = new List<string>();

    //    foreach (var item in data)
    //    {
    //        keys.Add("CurrAction-" + item.actionName + "-" + item.GigId);
    //        print(keys[keys.Count - 1]);
    //    }

    //    RemoveUserData(keys,false);
    //}

    public void RemoveUserData(string key)
    {


        List<string> dataToRemove = new List<string>() { { key } };

        UpdateUserDataRequest request = new UpdateUserDataRequest() { KeysToRemove = dataToRemove };
        PlayFabClientAPI.UpdateUserData(request, result =>
        {
            Debug.Log("Data Removed");
        },
        error =>
        {
            Debug.Log("Error removing data");
        });
    }

    public void RemoveUserData(List<string> keys,bool isReseting)
    {
        UpdateUserDataRequest request = new UpdateUserDataRequest() { KeysToRemove = keys };
        PlayFabClientAPI.UpdateUserData(request, result =>
        {
            Debug.Log("Data Removed");
            if (isReseting)
              SceneManager.LoadScene("SplashScreen");
            //Application.LoadLevel("SplashScreen");
        },
        error =>
        {
            Debug.Log("Error removing data");
        });
    }

    private void OnLoginFailure(PlayFabError error)
    {
        Debug.LogWarning("Something went wrong with your first API call.  :(");
        Debug.LogError("Here's some debug information:");
        Debug.LogError(error.GenerateErrorReport());
        Debug.LogError(error.ErrorMessage);
    }

    public void UpdateStatistics(List<StatisticUpdate> data)
    {
        if (!PlayFabClientAPI.IsClientLoggedIn())
        {
            Debug.LogWarning("Not logged in to playfab, can't update statistics.");
            return;
        }
        UpdatePlayerStatisticsRequest request = new UpdatePlayerStatisticsRequest()
        {
            Statistics = data
        };

        PlayFabClientAPI.UpdatePlayerStatistics(request, (result) =>
        {
            Debug.Log("User statistics Updated");

        }, (error) =>
        {
            Debug.Log(error.ErrorMessage);
        }
        );
    }


    public void GetUserData(string key, FieldReturned onData)
    {
        if (!PlayFabClientAPI.IsClientLoggedIn())
        {
            Debug.LogWarning("Not logged in to playfab, can't get data.");
            return;
        }
        List<string> thekey = new List<string>();
        thekey.Add(key);

        GetUserDataRequest request = new GetUserDataRequest()
        {
            Keys = thekey
        };

        PlayFabClientAPI.GetUserData(request, (result) =>
        {
            Debug.Log("Successfully updated user data");

            if (onData != null)
            {
                if (result.Data.ContainsKey(key))
                    onData(result.Data[key].Value);
                else
                    onData("");
            }

        }, (error) =>
        {
            Debug.Log(error.ErrorDetails);
        });
    }

    public void GetUserDatas(List<string> keys, ListReturned onData)
    {

        if (!PlayFabClientAPI.IsClientLoggedIn())
        {
            Debug.LogWarning("Not logged in to playfab, can't get data.");
            return;
        }
        GetUserDataRequest request = new GetUserDataRequest()
        {
            Keys = keys
        };

        PlayFabClientAPI.GetUserData(request, (result) =>
        {
            Debug.Log("Successfully updated user data");
            if (onData != null)
                onData(result.Data);

        }, (error) =>
        {
            Debug.Log(error.ErrorDetails);
        });
    }

    public void SetUserData(string key, object data)
    {
        if (closeSave)
            return;

        if (!PlayFabClientAPI.IsClientLoggedIn())
        {
            Debug.LogWarning("Not logged in to playfab, can't set data.");
            return;
        }
        UpdateUserDataRequest request = new UpdateUserDataRequest()
        {
            Data = new Dictionary<string, string>(){
      {key, data.ToString()}
    }
        };

        PlayFabClientAPI.UpdateUserData(request, (result) =>
        {
            Debug.Log("Successfully updated user data");
        }, (error) =>
        {
            Debug.Log(error.ErrorDetails);
        });
    }

    public void AddCurrency(string currencyCode, int amount)
    {
        if (amount > 0)
        {
            AddUserVirtualCurrencyRequest VCRequest = new AddUserVirtualCurrencyRequest()
            {  
                VirtualCurrency = currencyCode,
                Amount = amount
            };

            PlayFabClientAPI.AddUserVirtualCurrency(VCRequest, (result) =>
            {
                Debug.Log("Successfully added Currency" );
            }, (error) =>
            {
                Debug.Log("failed " + error.ErrorMessage);
            });
        }
    }

    public void SubtractCurrency(string currencyCode, int amount)
    {
        SubtractUserVirtualCurrencyRequest VCRequest = new SubtractUserVirtualCurrencyRequest()
        {
            VirtualCurrency = currencyCode,
            Amount = amount
        };

        PlayFabClientAPI.SubtractUserVirtualCurrency(VCRequest, (result) =>
        {
            Debug.Log("Successfully subtracted Currency");

        }, (error) =>
        {
            Debug.Log(error.ErrorMessage);
        });
    }

    public void GetItemsIncatalogue(string catalogeVersion, OnPlayfabdReturnCatalogue onCatalogueReturned = null)
    {
        GetCatalogItemsRequest request = new GetCatalogItemsRequest()
        {
            CatalogVersion = catalogeVersion
        };

        PlayFabClientAPI.GetCatalogItems(request, result =>
        {
            // result.Catalog;
            if (onCatalogueReturned != null)
            {
                onCatalogueReturned(result.Catalog);
            }
        }, error =>
        {
            Debug.Log(error.ErrorMessage);
        });
    }


    public void SetDisplayName(string name, OnDisplayNameChanged changed)
    {
        PlayFabClientAPI.UpdateUserTitleDisplayName(
            new UpdateUserTitleDisplayNameRequest() { DisplayName = name },
            (result) =>
            {
                Debug.Log("User Name Set");

                if (changed != null)
                    changed(true);
            },
            (error) =>
            {
                Debug.Log(error.ErrorMessage);
                if (changed != null)
                    changed(false);
            });
    }

    public void LinkCurrentAccountToGoogle(string accessToken)
    {

        LinkGoogleAccountRequest request = new LinkGoogleAccountRequest()
        {
            ServerAuthCode = accessToken,
            ForceLink = true
        };

        PlayFabClientAPI.LinkGoogleAccount(request, result =>
        { Debug.Log("linked to google"); }, error =>
        { Debug.Log(error.ErrorMessage); });
    }

    public void LinkCurrentAccountToGameCenter(string ID)
    {
        LinkGameCenterAccountRequest request = new LinkGameCenterAccountRequest()
        {
            GameCenterId = ID,
            ForceLink = true
        };

        PlayFabClientAPI.LinkGameCenterAccount(request, result =>
        { Debug.Log("linked to google"); }, error =>
        { Debug.Log(error.ErrorMessage); });
    }

    public void LinkCurrentAccountToFacebook(string accessToken)
    {
        if (!PlayFabClientAPI.IsClientLoggedIn())
        {
            Debug.LogWarning("Not logged in to playfab, can't link with facebook.");
            return;
        }
        LinkFacebookAccountRequest Linkrequest = new LinkFacebookAccountRequest()
        {
            AccessToken = accessToken,
            ForceLink = true
        };

        PlayFabClientAPI.LinkFacebookAccount(Linkrequest, (result) =>
        {
            Debug.Log("Linked to facebook");

        }, (linkerror) =>
        {
            Debug.Log("error while linking");
            Debug.Log(linkerror.ErrorDetails);
        });
    }
    public static void GetAccountInfo()
    {
        GetAccountInfoRequest request = new GetAccountInfoRequest();
       // UserName = request.PlayFabId ;
       // Debug.Log("request.Username" + request.PlayFabId);
        PlayFabClientAPI.GetAccountInfo(request, OnGetUsernameResult, OnGetUsernameError);
        
        //GetAccountInfoRequest request = new GetAccountInfoRequest();
        //PlayFabClientAPI.GetAccountInfo(request, OnGetAccountInfoSuccess, OnPlayFabCallbackError);
    }
    public static void OnGetUsernameResult(GetAccountInfoResult result)
    {
        UserName = result.AccountInfo.PlayFabId;
        //Debug.Log("request.Username" + result.AccountInfo.PlayFabId);
        PlayerPrefs.SetString("playerID", UserName);
        //print("username:"+ UserName);
    }
    public static void OnGetUsernameError(PlayFabError result)
    {
        print("username notfound");
    }
    //void OnGetAccountInfoSuccess(GetAccountInfoResult result)
    //{
    //    PlayFabLoginCalls.LoggedInUserInfo = result.AccountInfo;
    //    if (OnPlayfabCallbackSuccess != null)
    //    {
    //        OnPlayfabCallbackSuccess("", PlayFabAPIMethods.GetAccountInfo);
    //    }
    //}
}

