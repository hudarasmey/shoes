﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
//using WebTools.Currencies.Behaviours;
//using WebTools.Currencies.Datas;

public class GameManager : MonoBehaviour// : Singleton<GameManager>
{
    public static GameManager Instance;
    public string UserName;

  //  private int currency;
    public int Currency
    {
        get { return PlayerPrefs.GetInt("Currency"); }
        set { PlayerPrefs.SetInt("Currency", value); } //coins = value;
    }
    //public Currency Currency;
  
    //public List<QuestData> CurrentQuestData;
    //public List<GigData> CurrentGigsData;
    //public List<ActionData> CurrentActionsData;

    //to be determine

    public int Followers;

    public int Class { get { return Followers; } }

    bool insideGame;



    void Awake()
    {
        if (!Instance )
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);

    }

    void Start()
    {
        //// PlayfabManager.GetAccountInfo();
        ////CurrentQuestID = new List<string>();
        //if (PlayerPrefs.GetString("playerID")!= UserName)//&& UserName != null
        //{
        //    PlayerPrefs.SetString("playerID", UserName);
        //}
        //Debug.Log(PlayerPrefs.GetString("playerID"));
    }

    public void GameStarted()
    {
        //Currency.CaluculateMotivation();

        //PanelCurrenciesBehaviour.Instance.UpdateCurrencyPanel(Currency);
        insideGame = true;
    }

    void Update()
    {
        UserName = PlayfabManager.UserName;
        //if (Currency.CaluculateMotivation() && insideGame)
        //{
        //    PanelCurrenciesBehaviour.Instance.UpdateCurrencyPanel(Currency);
        //}
        if (Input.GetKeyDown(KeyCode.X))
        {
            PlayerPrefs.DeleteAll();
            ResetGame();
            Debug.Log(GameManager.Instance.Currency);
        }
        
    }

    public void Destroy()
    {
        Destroy(gameObject);
    }

    public void AddSoftCurrency(int value)
    {
        Currency += value;

      //  PanelCurrenciesBehaviour.Instance.UpdateCurrencyPanel(Currency);
        //UIController.Instance.UpdateSoftCurrency(Currency.Dollars);
        if (PlayfabManager.Instance)
            PlayfabManager.Instance.AddCurrency("SC", value);
    }

    //public void AddHardCurrency(int value)
    //{
    //    Currency.Glam += value;

    //    PanelCurrenciesBehaviour.Instance.UpdateCurrencyPanel(Currency);
    //    //UIController.Instance.UpdateHardCurrency(Currency.Glam);
    //    if (PlayfabManager.Instance)
    //        PlayfabManager.Instance.AddCurrency("HC", value);
    //}

    //public void SubtractSoftCurrency(int value)
    //{
    //    Currency.Dollars += value;

    //    PanelCurrenciesBehaviour.Instance.UpdateCurrencyPanel(Currency);
    //    //UIController.Instance.UpdateSoftCurrency(Currency.Dollars);
    //    if (PlayfabManager.Instance)
    //        PlayfabManager.Instance.SubtractCurrency("SC", value);
    //}

    //public void SubtractHardCurrency(int value)
    //{
    //    Currency.Glam += value;

    //    PanelCurrenciesBehaviour.Instance.UpdateCurrencyPanel(Currency);
    //    //UIController.Instance.UpdateHardCurrency(Currency.Glam);
    //    if (PlayfabManager.Instance)
    //        PlayfabManager.Instance.SubtractCurrency("HC", value);
    //}

    //public bool IncrementEnergy(int value)
    //{
    //    //Energy += value;
    //    bool isIncremented = Currency.IncrementMotivation(value);

    //    PanelCurrenciesBehaviour.Instance.UpdateCurrencyPanel(Currency);
    //    return isIncremented;
    //}

    public void ResetGame()
    {
        PlayerPrefs.DeleteAll();

        //CurrentQuestData.Clear();
        //CurrentGigsData.Clear();
        //CurrentActionsData.Clear();

        if (PlayfabManager.Instance)
            PlayfabManager.Instance.ResetUserData();
    }

    //public void IncrementExperience(int value)
    //{
    //    Currency.Experience += value;

    //    PanelCurrenciesBehaviour.Instance.UpdateCurrencyPanel(Currency);
    //    //UIController.Instance.UpdateLevel(Currency.Experience);
    //}
}