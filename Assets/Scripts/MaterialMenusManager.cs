﻿using UnityEngine;
using System.Collections;

public class MaterialMenusManager : MonoBehaviour {
    public enum MaterialMenuType { faceIn, faceOut, baseUp, baseSide, baseButton, heel, faceBack }
    public MaterialMenuType MenuType;
    public SubMenuSc mainMaterialMenu;
    public string[] materialFolder;
    public int[] index;
    public Material[] targetMat;
    public ObjMenu[] ObjIs;
    public int[] ColorSelectedIndex;
    public Color[] color;
    public Vector2[] TilesValue;
    public float[] TilesSliderValue;
    public int[] TextureSelectedIndex;
    public int[] SelectedPattrernMenuIndex;
    public int[] PatternSelectedIndex;
    public Texture2D[] Texture;
    public Texture2D[] Normals;
    public Sprite[] Sprites;
    public Vector4[] MatParam;
    public int TempIndexMenu;
    public MaterialSelected[] materialSelected;
    public float[] colorHorizontalNormalizedPosition;
    public float[] colorTexHorizontalNormalizedPosition;
    public float[] BWTexHorizontalNormalizedPosition;
    public Texture2D defaultTex;
    public Texture2D defaultNor;
    public Sprite defaultSprite;
    public int[] materialType;
    public Texture2D fabricBump;
    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.G))
        {
            ResetAllMaterialData();
        }
        if (TempIndexMenu != mainMaterialMenu.targetMaterialIndex)
        {
            setMenuPar(mainMaterialMenu.targetMaterialIndex);
            TempIndexMenu = mainMaterialMenu.targetMaterialIndex;
        }
	}
    void selectMenuType()
    {
        switch (MenuType)
        {
            case MaterialMenuType.faceIn:

                break;
            case MaterialMenuType.faceOut:

                break;
            case MaterialMenuType.baseUp:

                break;
            case MaterialMenuType.baseSide:

                break;
            case MaterialMenuType.baseButton:

                break;
            case MaterialMenuType.heel:

                break;
            case MaterialMenuType.faceBack:

                break;
            default:
                break;
        }
    }
   public void clearIntArr(int[] Arr, int content)
    {
        for (int i = 0; i < Arr.Length; i++)
        {
            Arr[i] = content;
        }
    }

    void setMenuPar(int _index)
    {
      //  Debug.Log("dkkd");
        mainMaterialMenu.materialIndex=index[_index];
        mainMaterialMenu.TargetMaterial=targetMat[_index];
        mainMaterialMenu.ObjIs=ObjIs[_index];   ///////// jkjhkjhjkhkjh
        mainMaterialMenu.tempColor=color[_index];
        mainMaterialMenu.tileValue = TilesValue[_index];
       // mainMaterialMenu.materialSlider.GetComponent<ScrollListTest>().tileValueSlider.value = TilesValue[_index].x;
        mainMaterialMenu.selectedTexteure=Texture[_index];
        mainMaterialMenu.colorListPicker.GetComponent<ScrollListTest>().selectedIndex = ColorSelectedIndex[_index];
        mainMaterialMenu.colorListPicker.GetComponent<ScrollListTest>().MarkAgain(ColorSelectedIndex[_index]);
        mainMaterialMenu.materialSlider.selectedIndex = TextureSelectedIndex[_index];
        mainMaterialMenu.materialSlider.MarkAgain(TextureSelectedIndex[_index]);
      //  Debug.Log(mainMaterialMenu.PatternList.selectedIndex);
        mainMaterialMenu.PatternList.selectedIndex = PatternSelectedIndex[_index];
       // Debug.Log(mainMaterialMenu.PatternList.selectedIndex);
        mainMaterialMenu.PatternList.MarkAgain(PatternSelectedIndex[_index]);
        mainMaterialMenu.MaterialPara= MatParam[_index];
        mainMaterialMenu.selectedSprite = Sprites[_index];
        mainMaterialMenu.normalTexture = Normals[_index];
        mainMaterialMenu.MatSelect=materialSelected[_index];
        mainMaterialMenu.materialSlider.tileValueSlider.value = mainMaterialMenu.PatternList.tileValueSlider.value= TilesSliderValue[_index] ;
        mainMaterialMenu.matType=(MaterialType) materialType[_index];
    }
    public void ResetAllMaterialData()
    {
     //  Debug.Log("Dd");
        mainMaterialMenu.materialSlider.selectedIndex = 0;
        mainMaterialMenu.materialSlider.tempIndex = 0;
        mainMaterialMenu.colorListPicker.GetComponent<ScrollListTest>().selectedIndex = mainMaterialMenu.colorListPicker.GetComponent<ScrollListTest>().tempIndex = 0;
        ColorSelectedIndex= clearintArr(ColorSelectedIndex,-1);
        color=clearColorArr(color,Color.white);
       TilesValue= clearVector2eArr(TilesValue,new Vector2(2f,2f));
        TilesSliderValue=clearFloateArr(TilesSliderValue, 0);
        Texture=clearTextureArr(Texture, defaultTex);
        TextureSelectedIndex=clearintArr(TextureSelectedIndex, -1);
        SelectedPattrernMenuIndex=clearintArr(SelectedPattrernMenuIndex, 0);
        PatternSelectedIndex=clearintArr(PatternSelectedIndex, -1);
        Normals=clearTextureArr(Normals, defaultTex);
        MatParam=clearVector4eArr(MatParam,new Vector4(0,0,0,0));
       Sprites=clearSpriteeArr(Sprites, defaultSprite);
       TempIndexMenu = 0;
       materialSelected=clearMaterialSelectedeArr(materialSelected, MaterialSelected.texture);
       colorHorizontalNormalizedPosition = clearFloateArr(colorHorizontalNormalizedPosition, 0);
       colorTexHorizontalNormalizedPosition = clearFloateArr(colorTexHorizontalNormalizedPosition, 1);
       BWTexHorizontalNormalizedPosition = clearFloateArr(BWTexHorizontalNormalizedPosition, 1);
      materialType=clearintArr(materialType, 0);
       resetMaterialContent();
            
    }
    void resetMaterialContent()
    {
        for (int i = 0; i < targetMat.Length; i++)
        {
        targetMat[i].SetTexture("_MainTex", defaultTex);
        targetMat[i].SetTexture("_BumpMap", defaultNor);
      //  targetMat[i].SetFloat("_BumpScale", _BumpScale);
        targetMat[i].SetTexture("_OcclusionMap", defaultTex);

        }
    }
    int[] clearintArr(int[] Arr, int value)
    {
        int[] Temp = new int[Arr.Length];
        for (int i = 0; i < Arr.Length; i++)
        {
            Temp[i] = value;
        }
        return Temp; 
    }
    Color[] clearColorArr(Color[] Arr, Color value)
    {
        Color[] Temp = new Color[Arr.Length];
        for (int i = 0; i < Arr.Length; i++)
        {
            Temp[i] = value;
        }
        return Temp;
    }
    Texture2D[] clearTextureArr(Texture2D[] Arr, Texture2D value)
    {
        Texture2D[] Temp = new Texture2D[Arr.Length];
        for (int i = 0; i < Arr.Length; i++)
        {
            Temp[i] = value;
        }
        return Temp;
    }
    Vector2[] clearVector2eArr(Vector2[] Arr, Vector2 value)
    {
        Vector2[] Temp = new Vector2[Arr.Length];
        for (int i = 0; i < Arr.Length; i++)
        {
            Temp[i] = value;
        }
        return Temp;
    }
    Vector4[] clearVector4eArr(Vector4[] Arr, Vector4 value)
    {
        Vector4[] Temp = new Vector4[Arr.Length];
        for (int i = 0; i < Arr.Length; i++)
        {
            Temp[i] = value;
        }
        return Temp;
    }
    float[] clearFloateArr(float[] Arr, float value)
    {
        float[] Temp = new float[Arr.Length];
        for (int i = 0; i < Arr.Length; i++)
        {
            Temp[i] = value;
        }
        return Temp;
    }
    Sprite[] clearSpriteeArr(Sprite[] Arr, Sprite value)
    {
        Sprite[] Temp = new Sprite[Arr.Length];
        for (int i = 0; i < Arr.Length; i++)
        {
            Temp[i] = value;
        }
        return Temp;
    }
    MaterialSelected[] clearMaterialSelectedeArr(MaterialSelected[] Arr, MaterialSelected value)
    {
        MaterialSelected[] Temp = new MaterialSelected[Arr.Length];
        for (int i = 0; i < Arr.Length; i++)
        {
            Temp[i] = value;
        }
        return Temp;
    }

}
