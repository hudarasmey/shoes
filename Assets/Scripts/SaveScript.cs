﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;


public class MaterialParamters
{
    // public int faceIndex, heelIndex, frontFaceIndex, backFaceIndex;

    //Material parm
    public Texture2D _MainTex, _BumpMap, _OcclusionMap, _DetailNormalMap;

    public Color _Color = Color.white;

    public float _BumpScale = 1, _OcclusionStrength = 1, _Metallic, _Glossiness;//,_TileValue=1;

    public Vector2 _TileValue;
}
public class ModelParamters
{
    public int faceIndex, heelIndex, frontFaceIndex, backFaceIndex;
    public float baseHeightFront, heelThickness, facethick;
}
public class SaveScript : MonoBehaviour
{
    public int faceIndex, heelIndex, frontFaceIndex, backFaceIndex;

    // //Material parm
    // public Texture2D _MainTex, _BumpMap, _OcclusionMap, _DetailNormalMap;

    // public Color _Color = Color.white;

    // public float _BumpScale = 1, _OcclusionStrength = 1, _Metallic, _Glossiness;//,_TileValue=1;

    // public Vector2 _TileValue;
    //// public InfoBank heelBank, FaceBank, frontFaceBank, backFaceBank;
    // public Material Mo;
    public ObjectSelectionSubMenu ObjSelectManager;
    public modelControl1 modelManager;
    public ScrollListTest SaveSlider;
    public InfoBank Bank, acessBank;
    public GameObject saveBtn, backBtn;
    public string ModelName, selectedName;
    public int savedShoesindex;
    public Decal AccessManager;
    public GameObject tempAccess;
    public Camera ScreenshotCam;
    public Texture2D normal;
    //    public Texture2D _MainTex, _BumpMap, _OcclusionMap, _DetailNormalMap;
    #region screenshotVar
    public Rect windowRect;
    //  public int  rectXPos, rectYPos, rectWidth, rectHeight;
    public Texture2D img;
    public GameObject obj;
    public Image bb;
    public int indextexture;
    public MaterialMenusManager MaterialManager;
    #endregion screenshotVar
    public ProgressBar _ProgressBar;
    GameObject SoundManagereObj;
    SoundManagerClass SoundManScript;
    void Awake()
    {
        SoundManagereObj = GameObject.FindGameObjectWithTag("SoundManager");
        SoundManScript = (SoundManagerClass)SoundManagereObj.GetComponent("SoundManagerClass");
        //Bank = GameObject.FindGameObjectWithTag("SaveInfoBank").GetComponent<InfoBank>();
        //acessBank = GameObject.FindGameObjectWithTag("AccsInfoBank").GetComponent<InfoBank>();
        
    }


    void Start()
    {
     //   Bank = GameObject.FindGameObjectWithTag("SaveInfoBank").GetComponent<InfoBank>();
      //  acessBank = GameObject.FindGameObjectWithTag("AccsInfoBank").GetComponent<InfoBank>();
        windowRect = ScreenshotCam.rect;
        //if (Input.GetKeyDown(KeyCode.W))
        //{
        //  PlayerPrefs.DeleteAll();
        //}
        StartCoroutine(resetSlider());
        FirstIntGame();
        //if (PlayerPrefsX.GetColorArray("saveModelsArr").Length > 0)
        //{
        //    Bank.ColorsBank = PlayerPrefsX.GetColorArray("saveModelsArr");
        //  //  saSlider.IsInst = true;

        //}


    }

    // Update is called once per frame
    void Update()
    {
        //string dateAndTimeVar = System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
        //print(dateAndTimeVar);
        if (SaveSlider.gameObject.activeSelf && SaveSlider.selectedIndex >= 0 && SaveSlider.IsAction)
        {
            if (SaveSlider.deleteAction)
            {
                Debug.Log("deleate" + SaveSlider.selectedIndex);
                SaveSlider.deleteAction = false;
                SaveSlider.IsAction = false;
                deleteFromList(Bank, SaveSlider.selectedIndex);
            }
            else
            {
                //   Debug.Log(Bank.SavedModel[SaveSlider.selectedIndex]);

                LoadAllMatFn(Bank.SavedModel[SaveSlider.selectedIndex], modelManager.shose);
                LoadModelParams(Bank.SavedModel[SaveSlider.selectedIndex], modelManager);
                LoadAcclist(Bank.SavedModel[SaveSlider.selectedIndex]);
                SaveSlider.IsAction = false;

                
                modelManager.GoToMenu(3);
                
               // modelManager.mainIndexTemp = modelManager.mainIndex = 3;
                if (SaveSlider.GetComponent<ScrollListTest>().selectedIndex >= 0 && SaveSlider.Bank.SavedModel.Length > 0)
                {
                    //Debug.Log(SaveSlider.GetComponent<ScrollListTest>().selectedIndex);
                    //Debug.Log("LLLL" + SaveSlider.GetComponent<ScrollListTest>().selectedIndex + "  ");
                    selectedName = SaveSlider.GetComponent<ScrollListTest>().Bank.SavedModel[SaveSlider.GetComponent<ScrollListTest>().selectedIndex];
                }
                modelManager.MenuIndexNum(3);
                //_ProgressBar.sss(0);
            }
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            // SaveMatParams("KOKO", Mo);
            //LoadTextureOfSlider(Bank);
            SaveAcclist("op");
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.SetInt("firstTimeGameStart", -1);
            PlayerPrefs.DeleteKey("saveModelsArr");
            Debug.Log("OOO");
            PlayerPrefs.DeleteKey("saveModelsArr");
            PlayerPrefs.SetInt("firstTimeGameStart", 1);
            Debug.Log("DLDLD");
            savedShoesindex = 0;
            PlayerPrefs.SetInt("savedShoesindex", savedShoesindex);
            Debug.Log(PlayerPrefs.GetInt("savedShoesindex"));
            // LoadAcclist("op");
        }
        //if (Input.GetKeyDown(KeyCode.L))
        //{
        //    Debug.Log("Load");
        //    LoadAllMatFn("LPLP", modelManager.shose);
        //    LoadModelParams("lolo", modelManager);
        //   // MaterialParamters MatPar = LoadMatParams("KOKO", Mo);
        //   //_MainTex= MatPar._MainTex;

        //   // //PlayerPrefsX.SetInt(materialName+"_BumpMap", _material.GetTexture("_BumpMap").name);
        //   //_OcclusionMap=MatPar._OcclusionMap;

        //   //_BumpMap = MatPar._BumpMap;
        //   //_DetailNormalMap = MatPar._DetailNormalMap;
        //   //_BumpScale= MatPar._BumpScale;
        //   //_Metallic= MatPar._Metallic;
        //   //_Glossiness= MatPar._Glossiness;
        //   //_Color = MatPar._Color;
        //   //_TileValue= MatPar._TileValue;

        //}
        if (Input.GetKeyDown(KeyCode.V))
            PlayerPrefs.DeleteKey("firstTimeGameStart");

        //if (Input.GetKeyDown(KeyCode.B))
        //    SaveBtn();
        //if (tempColor.r != colorPicker.CurrentColor.r && tempColor.g != colorPicker.CurrentColor.g && tempColor.b != colorPicker.CurrentColor.b)
        //{


        //if (Input.GetKeyDown(KeyCode.C))
        //{
        //    PlayerPrefsX.SetColorArray("saveModelsArr", new Color[0]);
        //}


    }


    # region SaveLoadMaterial
    public void SaveMatParams(string materialName, Material _material, int index)
    {
        if (_material.GetTexture("_MainTex") != null)
        {
            PlayerPrefs.SetString(materialName + "_MainTex", MaterialManager.materialFolder[index] + "/" + _material.GetTexture("_MainTex").name);
        }
        //PlayerPrefsX.SetInt(materialName+"_BumpMap", _material.GetTexture("_BumpMap").name);
        if (_material.GetTexture("_OcclusionMap") != null)
        {
            PlayerPrefs.SetString(materialName + "_OcclusionMap", MaterialManager.materialFolder[index] + "/" + _material.GetTexture("_OcclusionMap").name);
        }
        if (_material.GetTexture("_DetailNormalMap") != null)
        {
            Debug.Log(_material.GetTexture("_DetailNormalMap"));
            PlayerPrefs.SetString(materialName + "_DetailNormalMap", MaterialManager.materialFolder[index] + "/" + _material.GetTexture("_DetailNormalMap").name);
        }
        PlayerPrefs.SetInt(materialName + "_MaterialType", MaterialManager.materialType[index]);

        // PlayerPrefs.SetFloat(materialName+"_BumpScale", _material.GetFloat("_BumpScale"));
        PlayerPrefs.SetFloat(materialName + "_Metallic", _material.GetFloat("_Metallic"));
        PlayerPrefs.SetFloat(materialName + "_Glossiness", _material.GetFloat("_Glossiness"));
        PlayerPrefsX.SetColor(materialName + "_Color", _material.GetColor("_Color"));
        PlayerPrefsX.SetVector2(materialName + "_Tile", _material.GetTextureScale("_MainTex"));
        //    Debug.Log(materialName+"_Tile"+PlayerPrefsX.GetVector2(materialName + "_Tile", new Vector2(3, 3)));
    }
    public IEnumerator LoadMatParams(string materialName, Material _material)
    {
        MaterialParamters MatPar = new MaterialParamters();
        MatPar._MainTex = Resources.Load<Texture2D>(PlayerPrefs.GetString(materialName + "_MainTex"));//"Pattern" + "/" +
        yield return new WaitForEndOfFrame();
        string normalPath = "";
        //Debug.Log(MatPar._MainTex != null);
        //Debug.Log(materialName  + PlayerPrefs.GetString(materialName + "_MainTex"));
        if (PlayerPrefs.GetInt(materialName + "_MaterialType") != 1)
        {
            if (MatPar._MainTex != null)
            {

                // MatPar._BumpMap = NormalMapGen(MatPar._MainTex, 1, 15);
                // StartCoroutine(LoadNormalImage(MatPar._MainTex, MatPar));
                Debug.Log("mobe" + normalPath);
                normalPath = Application.persistentDataPath + "/" + MatPar._MainTex.name + ".png";
#if UNITY_IPHONE || UNITY_ANDROID
                Debug.Log("mobile" + normalPath);
                // WWW w = new WWW("file://'" + screenShotPath);
                WWW w = new WWW("file:///" + normalPath);
                yield return w;
                if (w.error == null)
                {
                    //  yield return new WaitForSeconds(2);
                    MatPar._BumpMap = w.texture;
                    Debug.Log("Load" + MatPar._BumpMap.name);
                    // Debug.Log("screenShotPathK" + normalTexture.name);
                }
                else
                {
                    MatPar._BumpMap = NormalMapGen(MatPar._OcclusionMap, 1, 15);

                    //normalTexture = materialSlider.Bank.NormalMapGen(selectedTexteure, 1, 1); //print(w.error);
                    Debug.Log("screenShotPathCreat");
                }
                // yield return new WaitForSeconds(6);
                //StartCoroutine(settextureit());
                // MatPar._BumpMap = normalTexture;
#endif
            }

        }
        else
        {
            MatPar._BumpMap = MaterialManager.fabricBump;
        }

        MatPar._OcclusionMap = Resources.Load<Texture2D>(PlayerPrefs.GetString(materialName + "_OcclusionMap"));
        if (PlayerPrefs.GetInt(materialName + "_MaterialType") != 1)
        {
            if (MatPar._OcclusionMap)
            {
                //  MatPar._BumpMap = NormalMapGen(MatPar._OcclusionMap, 1, 15);
                // StartCoroutine(LoadNormalImage(MatPar._OcclusionMap, MatPar));
                // StartCoroutine(LoadNormalImage(selectedTexteure));
                //       Texture2D normalTexture;
                normalPath = Application.persistentDataPath + "/" + MatPar._OcclusionMap.name + ".png";
#if UNITY_IPHONE || UNITY_ANDROID
                Debug.Log("mobile" + normalPath);
                // WWW w = new WWW("file://'" + screenShotPath);
                WWW w = new WWW("file:///" + normalPath);
                yield return w;
                if (w.error == null)
                {
                    //  yield return new WaitForSeconds(2);
                    MatPar._BumpMap = w.texture;

                    Debug.Log("screenShotPathK" + w.texture.name);
                }
                else
                {
                    MatPar._BumpMap = NormalMapGen(MatPar._OcclusionMap, 1, 15);

                    //normalTexture = materialSlider.Bank.NormalMapGen(selectedTexteure, 1, 1); //print(w.error);
                    Debug.Log("screenShotPathCreat");
                }
                // yield return new WaitForSeconds(6);
                //StartCoroutine(settextureit());
                // MatPar._BumpMap = normalTexture;
#endif

            }
        }
        else
        {
            MatPar._BumpMap = MaterialManager.fabricBump;
        }



        //PlayerPrefsX.SetInt(materialName+"_BumpMap", _material.GetTexture("_BumpMap").name);

        // MatPar._DetailNormalMap = Resources.Load<Texture2D>("DetailNormalMap" + "/" + PlayerPrefs.GetString(materialName + "_DetailNormalMap"));



        MatPar._BumpScale = PlayerPrefs.GetFloat(materialName + "_BumpScale");
        MatPar._Metallic = PlayerPrefs.GetFloat(materialName + "_Metallic");
        MatPar._Glossiness = PlayerPrefs.GetFloat(materialName + "_Glossiness");
        MatPar._Color = PlayerPrefsX.GetColor(materialName + "_Color");
        MatPar._TileValue = PlayerPrefsX.GetVector2(materialName + "_Tile", new Vector2(3, 3));
        normal = MatPar._BumpMap;
        yield return new WaitForEndOfFrame();
        //   Debug.Log(materialName + "_Tile" + PlayerPrefsX.GetVector2(materialName + "_Tile", new Vector2(3, 3)));
        SetValueToMat(_material, MatPar);
        // return MatPar;

    }
    IEnumerator SetValueToMatIe(Material targetMat, MaterialParamters MatPar)
    {
        yield return null;
    }

    void SetValueToMat(Material targetMat, MaterialParamters MatPar)
    {
        targetMat.SetTextureScale("_MainTex", MatPar._TileValue);
        targetMat.SetTextureScale("_BumpMap", MatPar._TileValue);
        targetMat.SetColor("_Color", MatPar._Color);
        targetMat.SetTexture("_MainTex", MatPar._MainTex);
        targetMat.SetTexture("_BumpMap", MatPar._BumpMap);
        targetMat.SetFloat("_BumpScale", MatPar._BumpScale);
        targetMat.SetTexture("_OcclusionMap", MatPar._OcclusionMap);
        targetMat.SetTexture("_DetailNormalMap", MatPar._DetailNormalMap);
        targetMat.SetFloat("_Metallic", MatPar._Metallic);
        targetMat.SetFloat("_Glossiness", MatPar._Glossiness);
        //targetMat.SetFloat("_OcclusionStrength", _OcclusionStrength);
    }


    public void SaveAllMatFn(string materialName, Shose shoes)
    {
        SaveMatParams(materialName + "faceIn", shoes.faceIn, 1);
        SaveMatParams(materialName + "faceOut", shoes.faceOut, 0);
        SaveMatParams(materialName + "baseUp", shoes.baseUp, 2);
        SaveMatParams(materialName + "baseMiddle", shoes.baseMiddle, 3);
        SaveMatParams(materialName + "baseButton", shoes.baseButton, 4);
        SaveMatParams(materialName + "heelIn", shoes.heelIn, 4);
        SaveMatParams(materialName + "heelOut", shoes.heelOut, 5);
        SaveMatParams(materialName + "BackFaceOut", shoes.BackFaceOut, 6);

    }
    public void LoadAllMatFn(string materialName, Shose shoes)
    {
        StartCoroutine(LoadMatParams(materialName + "faceIn", shoes.faceIn));
        StartCoroutine(LoadMatParams(materialName + "faceOut", shoes.faceOut));
        StartCoroutine(LoadMatParams(materialName + "baseUp", shoes.baseUp));
        StartCoroutine(LoadMatParams(materialName + "baseMiddle", shoes.baseMiddle));
        StartCoroutine(LoadMatParams(materialName + "baseButton", shoes.baseButton));
        StartCoroutine(LoadMatParams(materialName + "heelIn", shoes.heelIn));
        StartCoroutine(LoadMatParams(materialName + "heelOut", shoes.heelOut));
        StartCoroutine(LoadMatParams(materialName + "BackFaceOut", shoes.BackFaceOut));

    }
    # endregion SaveLoadMaterial


    # region GenerateNormal
    IEnumerator LoadNormalImage(Texture2D selectTexture, MaterialParamters MatPart)
    {

        //normalTexture = null;
        // SpinningLoadingBar.SetActive(true);
        //StartCoroutine(StartLoading());
        Texture2D normalTexture;
        string normalPath = Application.persistentDataPath + "/" + selectTexture.name + ".png";
#if UNITY_IPHONE || UNITY_ANDROID
        Debug.Log("mobile" + normalPath);
        // WWW w = new WWW("file://'" + screenShotPath);
        WWW w = new WWW("file:///" + normalPath);
        yield return w;
        if (w.error == null)
        {
            //  yield return new WaitForSeconds(2);
            normalTexture = w.texture;

            // Debug.Log("screenShotPathK" + normalTexture.name);
        }
        else
        {
            normalTexture = NormalMapGen(selectTexture, 1, 15);

            //normalTexture = materialSlider.Bank.NormalMapGen(selectedTexteure, 1, 1); //print(w.error);
            Debug.Log("screenShotPathCreat");
        }
        // yield return new WaitForSeconds(6);
        //StartCoroutine(settextureit());
        MatPart._BumpMap = normalTexture;
#endif


    }


    public Texture2D NormalMapGen(Texture2D source, float strength, float distortion)
    {
        strength = 1f;
        distortion = 15;
        strength = Mathf.Clamp(strength, 0.0F, 55.0F);
        Texture2D normalTexture;
        float xLeft;
        float xRight;
        float yUp;
        float yDown;
        float yDelta;
        float xDelta;
        normalTexture = new Texture2D(source.width, source.height, TextureFormat.ARGB32, true);
        for (int y = 0; y < normalTexture.height; y++)
        {
            for (int x = 0; x < normalTexture.width; x++)
            {
                xLeft = source.GetPixel(x - 1, y).grayscale * strength;
                xRight = source.GetPixel(x + 1, y).grayscale * strength;
                yUp = source.GetPixel(x, y - 1).grayscale * strength;
                yDown = source.GetPixel(x, y + 1).grayscale * strength;
                xDelta = ((xLeft - (xRight)) + 1) * 0.5f;
                yDelta = ((yUp - yDown) + 1) * 0.5f;
                normalTexture.SetPixel(x, y, new Color(Mathf.Clamp01(sCurve(xDelta, distortion)), Mathf.Clamp01(sCurve(yDelta, distortion)), 1, Mathf.Clamp01(sCurve(yDelta, distortion))));
                //normalTexture.SetPixel(x,y,new Color(xDelta,yDelta,1.0f,yDelta));

            }


        }
        normalTexture.Apply();
        //Code for exporting the image to assets folder 
        //System.IO.File.WriteAllBytes("Assets/Normals/"+source.name+".png", normalTexture.EncodeToPNG()); 
        return normalTexture;
    }
    public static float sCurve(float x, float distortion)
    {
        return 1f / (1f + Mathf.Exp(-Mathf.Lerp(5, 10, distortion) * (x - 0.5f)));
    }
    #endregion GenerateNormal


    #region SaveLoadModelInfo
    public void SaveModelParams(string DesignName, modelControl1 ModelManager)
    {

        PlayerPrefs.SetFloat(DesignName + "baseHeightFront", ModelManager.baseHeightFront.value);
        PlayerPrefs.SetFloat(DesignName + "heelThickness", ModelManager.heelThicknessDump.value);
        PlayerPrefs.SetFloat(DesignName + "facethick", ModelManager.facethick.value);
        PlayerPrefs.SetInt(DesignName + "ChooseTypeIndex", ModelManager.ChooseTypeIndex);

        //PlayerPrefs.SetInt(DesignName + "faceIndex",ModelManager.faceIndex);
        //PlayerPrefs.SetInt(DesignName + "heelIndex", ModelManager.heelIndex);
        //PlayerPrefs.SetInt(DesignName + "frontFaceIndex",ModelManager.frontFaceIndex);
        //PlayerPrefs.SetInt(DesignName + "backFaceIndex", ModelManager.backFaceIndex);
        //PlayerPrefs.SetInt(DesignName + "baseIndex",ModelManager.baseIndex);
        // ObjSelectManager.FaceModelScroller.Bank.Models[0]//[ObjSelectManager.FaceModelScroller.selectedIndex];
        if (ObjSelectManager.FaceModelScroller.selectedIndex >= 0)
        {
            PlayerPrefs.SetString(DesignName + "faceIndex", ObjSelectManager.FaceModelScroller.Bank.Models[ObjSelectManager.FaceModelScroller.selectedIndex].name);
        }
        else
        {
            PlayerPrefs.SetString(DesignName + "faceIndex", ObjSelectManager.FaceModelScroller.Bank.Models[0].name);
        }
        if (ObjSelectManager.HeelModelScroller.selectedIndex >= 0)
        {
            PlayerPrefs.SetString(DesignName + "heelIndex", ObjSelectManager.HeelModelScroller.Bank.Models[ObjSelectManager.HeelModelScroller.selectedIndex].name);
        }
        else
        {
            PlayerPrefs.SetString(DesignName + "heelIndex", ObjSelectManager.HeelModelScroller.Bank.Models[0].name);
        }
        if (ObjSelectManager.FrontFModelScroller.selectedIndex >= 0)
        {
            PlayerPrefs.SetString(DesignName + "frontFaceIndex", ObjSelectManager.FrontFModelScroller.Bank.Models[ObjSelectManager.FrontFModelScroller.selectedIndex].name);
        }
        else
        {
            PlayerPrefs.SetString(DesignName + "frontFaceIndex", ObjSelectManager.FrontFModelScroller.Bank.Models[0].name);
        }
        if (ObjSelectManager.BackGModelScroller.selectedIndex >= 0)
        {
            PlayerPrefs.SetString(DesignName + "backFaceIndex", ObjSelectManager.BackGModelScroller.Bank.Models[ObjSelectManager.BackGModelScroller.selectedIndex].name);
        }
        else
        {
            PlayerPrefs.SetString(DesignName + "backFaceIndex", ObjSelectManager.BackGModelScroller.Bank.Models[0].name);
        }
        if (ObjSelectManager.BaseModelScroller.selectedIndex >= 0)
        {
            PlayerPrefs.SetString(DesignName + "baseIndex", ObjSelectManager.BaseModelScroller.Bank.Models[ObjSelectManager.BaseModelScroller.selectedIndex].name);
        }
        else
        {
            //  PlayerPrefs.SetString(DesignName + "baseIndex", ObjSelectManager.BaseModelScroller.Bank.Models[0].name);
        }


    }
    public void LoadModelParams(string DesignName, modelControl1 ModelManager)
    {
        ModelManager.ChooseType(PlayerPrefs.GetInt(DesignName + "ChooseTypeIndex"));
        ModelManager.baseHeightFront.value = PlayerPrefs.GetFloat(DesignName + "baseHeightFront");
        ModelManager.heelThicknessDump.value = PlayerPrefs.GetFloat(DesignName + "heelThickness");
        ModelManager.facethick.value = PlayerPrefs.GetFloat(DesignName + "facethick");

        //ChangeModel(heelBank,PlayerPrefs.GetInt(DesignName + "heelIndex"));
        //ChangeModel(frontFaceBank,PlayerPrefs.GetInt(DesignName + "frontFaceIndex"));
        //ChangeModel(backFaceBank,PlayerPrefs.GetInt(DesignName + "backFaceIndex"));
        ////ChangeModel(, PlayerPrefs.GetInt(DesignName + "baseIndex"));
        //ChangeModel(FaceBank,PlayerPrefs.GetInt(DesignName + "faceIndex"));


        //if (ObjSelectManager.FaceModelScroller.selectedIndex >= 0)
        //{
        //    ChangeModel(ObjSelectManager.FaceModelScroller.Bank, PlayerPrefs.GetString(DesignName + "faceIndex"));
        //}
        //if (ObjSelectManager.HeelModelScroller.selectedIndex >= 0)
        //{
        //    ChangeModel(ObjSelectManager.HeelModelScroller.Bank, PlayerPrefs.GetString(DesignName + "heelIndex"));
        //}
        //if (ObjSelectManager.FrontFModelScroller.selectedIndex >= 0)
        //{
        //    ChangeModel(ObjSelectManager.FrontFModelScroller.Bank, PlayerPrefs.GetString(DesignName + "frontFaceIndex"));
        //}

        //if (ObjSelectManager.BackGModelScroller.selectedIndex >= 0)
        //{
        //    ChangeModel(ObjSelectManager.BackGModelScroller.Bank, PlayerPrefs.GetString(DesignName + "backFaceIndex"));
        //}
        //if (ObjSelectManager.BaseModelScroller.selectedIndex >= 0)
        //{
        //    ChangeModel(ObjSelectManager.BaseModelScroller.Bank, PlayerPrefs.GetString(DesignName + "baseIndex"));
        //}
        ChangeModel(ObjSelectManager.HeelModelScroller.Bank, PlayerPrefs.GetString(DesignName + "heelIndex"));
        // Debug.Log(PlayerPrefs.GetString(DesignName + "heelIndex"));

        ChangeModel(ObjSelectManager.FrontFModelScroller.Bank, PlayerPrefs.GetString(DesignName + "frontFaceIndex"));
        // Debug.Log(PlayerPrefs.GetString(DesignName + "frontFaceIndex"));

        ChangeModel(ObjSelectManager.BackGModelScroller.Bank, PlayerPrefs.GetString(DesignName + "backFaceIndex"));
        //  Debug.Log(PlayerPrefs.GetString(DesignName + "backFaceIndex"));
        //ChangeModel(ObjSelectManager.BaseModelScroller.Bank, PlayerPrefs.GetString(DesignName + "baseIndex"));
        ChangeModel(ObjSelectManager.FaceModelScroller.Bank, PlayerPrefs.GetString(DesignName + "faceIndex"));
        //  Debug.Log(PlayerPrefs.GetString(DesignName + "faceIndex"));

    }

    void ChangeModel(InfoBank Bank, string objectName)//,int index)
    {
        if (Bank != null)
        {


            if (gameObject.activeSelf)//&& index >= 0 )
            {
                // int tempModelIndex = 0;
                for (int i = 0; i < Bank.Models.Length; i++)
                {
                    //if (i == index)
                    //{
                    //    ////if (Bank.modelIndex[i] == widgetIndex)
                    //    ////{
                    //    ////    baseModel.SetActive(false);

                    //    ////}
                    //    ////else
                    //    ////{
                    //    ////    baseModel.SetActive(true);
                    //    ////}

                    //    Bank.Models[i].GetComponent<SkinnedMeshRenderer>().enabled = true;
                    //}
                    //else
                    //{
                    //    Bank.Models[i].GetComponent<SkinnedMeshRenderer>().enabled = false;
                    //}
                    if (Bank.Models[i].name == objectName)
                    {
                        Bank.Models[i].GetComponent<SkinnedMeshRenderer>().enabled = true;
                        if (Bank.Models[i].GetComponent<EachObjControl>().subObj != null)
                        {
                            Bank.Models[i].GetComponent<EachObjControl>().subObj.GetComponent<SkinnedMeshRenderer>().enabled = true;
                        }
                    }
                    else
                    {
                        Bank.Models[i].GetComponent<SkinnedMeshRenderer>().enabled = false;
                        if (Bank.Models[i].GetComponent<EachObjControl>().subObj != null)
                        {
                            Bank.Models[i].GetComponent<EachObjControl>().subObj.GetComponent<SkinnedMeshRenderer>().enabled = false;
                        }
                    }

                }


            }

        }
    }

    #endregion SaveLoadModelInfo



    //private Texture2D ScaleTexture(Texture2D source,int targetWidth,int targetHeight) {
    //    Texture2D result=new Texture2D(targetWidth,targetHeight,source.format,true);
    //    Color[] rpixels=result.GetPixels(0);
    //    float incX=(1.0f / (float)targetWidth);
    //    float incY=(1.0f / (float)targetHeight); 
    //    for(int px=0; px<rpixels.Length; px++) { 
    //        rpixels[px] = source.GetPixelBilinear(incX*((float)px%targetWidth), incY*((float)Mathf.Floor(px/targetWidth))); 
    //    } 
    //    result.SetPixels(rpixels,0); 
    //    result.Apply(); 
    //    return result; 
    //}

    #region ListModelcontroll
    public void SaveBtn()
    {

        SoundManScript.PlaySound(SoundManScript.saveBotton_Clip);
        // Bank.(colorPicker.CurrentColor);
        string[] SavedModelTemp;
        if (SaveSlider.Bank != null)
        {
             SavedModelTemp = new string[SaveSlider.Bank.SavedModel.Length + 1];
        }
        else
        {
            SaveSlider.Bank =GameObject.FindGameObjectWithTag("SaveInfoBank").GetComponent<InfoBank>();
            SavedModelTemp = new string[SaveSlider.Bank.SavedModel.Length + 1];
        }
        
        for (int i = 0; i < SaveSlider.Bank.SavedModel.Length; i++)
        {
            SavedModelTemp[i] = SaveSlider.Bank.SavedModel[i];
        }

        // Debug.Log(PlayerPrefs.GetInt("savedShoesindex"));
        savedShoesindex = PlayerPrefs.GetInt("savedShoesindex") + 1;
        //  Debug.Log(savedShoesindex);

        SavedModelTemp[SaveSlider.Bank.SavedModel.Length] = "Shoes_" + savedShoesindex.ToString();
        SaveSlider.Bank.SavedModel = SavedModelTemp;
        PlayerPrefsX.SetStringArray("saveModelsArr", SaveSlider.Bank.SavedModel);
        PlayerPrefs.SetInt("savedShoesindex", savedShoesindex);
        StartCoroutine(TakeScreenShot(SavedModelTemp[SaveSlider.Bank.SavedModel.Length - 1]));
        SaveModelParams(SavedModelTemp[SaveSlider.Bank.SavedModel.Length - 1], modelManager);
        // Debug.Log(SavedModelTemp[SaveSlider.Bank.SavedModel.Length - 1]);
        PlayerPrefs.SetString("date time_" + SavedModelTemp[SaveSlider.Bank.SavedModel.Length - 1].ToString(), System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));

        SaveAllMatFn(SavedModelTemp[SaveSlider.Bank.SavedModel.Length - 1], modelManager.shose);
        SaveAcclist(SavedModelTemp[SaveSlider.Bank.SavedModel.Length - 1]);
        StartCoroutine(resetSlider());
        //  Debug.Log(PlayerPrefs.GetInt("savedShoesindex") + "  " + SaveSlider.Bank.SavedModel + "  " + savedShoesindex);
        StartCoroutine(GoBack());
    }
    IEnumerator GoBack()
    {
        
        yield return new WaitForEndOfFrame();

        yield return new WaitForSeconds(0.2f);
        _ProgressBar.resetProgressBar();
       // _ProgressBar.sss(1);
        modelManager.GoToMenu(1);
      //  modelManager.mainIndexTemp = modelManager.mainIndex = 1;
    }
    //Crlear all memory ???
    public void deleteFromList(InfoBank Bank, int index)
    {
        string[] temp = PlayerPrefsX.GetStringArray("saveModelsArr");
        string[] last;
        int tempIndex = 0;
        Debug.Log(index + " " + " " + temp.Length);
        if (temp.Length >= 1)
        {

            last = new string[temp.Length - 1];

            for (int i = 0; i < temp.Length; i++)
            {
                if (i == index)
                {

                }
                else
                {
                    Debug.Log(tempIndex + " " + i);
                    if (last.Length > 0)
                    {
                        last[tempIndex] = temp[i];
                        tempIndex += 1;

                    }


                }
            }
        }
        else
        {
            last = new string[0];
        }
        SaveSlider.GetComponent<ScrollListTest>().selectedIndex = -1;
        PlayerPrefsX.SetStringArray("saveModelsArr", last);
        Bank.SavedModel = PlayerPrefsX.GetStringArray("saveModelsArr");
        resetSlider();

    }
    public void FirstIntGame()
    {
        int fristTimeRun = PlayerPrefs.GetInt("firstTimeGameStart", -1);
        //Debug.Log(fristTimeRun);

        //  Debug.Log(fristTimeRun);
        if (fristTimeRun != -1)
        {
            savedShoesindex = PlayerPrefs.GetInt("savedShoesindex");
            // Debug.Log(savedShoesindex + "  " + PlayerPrefsX.GetStringArray("saveModelsArr").Length);
            if (PlayerPrefsX.GetStringArray("saveModelsArr").Length > 0)
            {
                SaveSlider.Bank.SavedModel = PlayerPrefsX.GetStringArray("saveModelsArr");
                // SaveSlider.Bank.spritesArr = new Sprite[SaveSlider.Bank.SavedModel.Length];
                //  saSlider.IsInst = true;

            }
            //  Debug.Log("DssLDLD  " + PlayerPrefsX.GetStringArray("saveModelsArr").Length);
            //if (fristTimeRun == 0)
            //{
            //    //firstTimeGameStart = 1;
            //    //PlayersNum = 50;

            //}
            //else if (fristTimeRun == 1)
            //{
            //}
        }
        else
        {
            // firstTimeGameStart=1;
            Debug.Log("OOO");
            PlayerPrefs.DeleteKey("saveModelsArr");
            PlayerPrefs.SetInt("firstTimeGameStart", 1);
            Debug.Log("DLDLD");
            savedShoesindex = 0;
            PlayerPrefs.SetInt("savedShoesindex", savedShoesindex);
            Debug.Log(PlayerPrefs.GetInt("savedShoesindex"));
            //string[] tempStringArr = new string[1];
            //tempStringArr[0] = "heel0";
            //PlayerPrefsX.SetStringArray("saveModelsArr", tempStringArr);
            //SaveSlider.Bank.SavedModel = PlayerPrefsX.GetStringArray("saveModelsArr");
            //SaveSlider.Bank.spritesArr = new Sprite[SaveSlider.Bank.SavedModel.Length];

            //Debug.Log("!");
            //PlayersNum = 50;

            //ObscuredPrefs.SetString("");

        }
    }
    public void getModelSaved(string selectedModel)
    {
        for (int i = 0; i < SaveSlider.Bank.SavedModel.Length; i++)
        {

            if (SaveSlider.Bank.SavedModel[i] == selectedModel)
            {
                //Debug.Log(Bank.ColorsBank[i] + "  " + obj.GetComponent<Renderer>().materials[materialIndex].color);

                ModelName = SaveSlider.Bank.SavedModel[i];//.GetComponent<Renderer>().materials[materialIndex].color;
                //    Debug.Log("OK" + "  " + _color);
            }
        }

        //startColor = obj.GetComponent<Renderer>().materials[materialIndex].color;
    }

    #endregion ListModelcontroll

    #region screenshotVar
    IEnumerator TakeScreenShot(string ScreenshotName)
    {
        GameObject canvas = GameObject.Find("Canvas").GetComponent<Canvas>().gameObject;
        GameObject canvas_1 = GameObject.Find("Canvas_1").GetComponent<Canvas>().gameObject;
        GameObject canvas_2 = GameObject.Find("Canvas_2").GetComponent<Canvas>().gameObject;
        

        canvas_1.SetActive(false);
        canvas_2.SetActive(false);
        canvas.SetActive(false);
        
        // float x,y;
        //x=y=Screen.width *0.3f;

        //  windowRect = new Rect(, Screen.width * .20f);

        int newx = Mathf.FloorToInt(windowRect.x * Screen.width);
        int newy = Mathf.FloorToInt(windowRect.y * Screen.height);
        //int neww = Mathf.FloorToInt(windowRect.width);
        //int newh = Mathf.FloorToInt(windowRect.height);
        int neww = Mathf.FloorToInt(Screen.width * windowRect.width);
        int newh = Mathf.FloorToInt(Screen.height * windowRect.height);
        Debug.Log(newx + " " + newy + " " + neww + " " + newh);
        //newx = Mathf.FloorToInt((Screen.width*.75f)-(windowRect.width/2));
        //newy = Mathf.FloorToInt((Screen.height / 2) - (windowRect.height / 2));
        //Texture2D screenshot = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, true);  
        Texture2D screenshot = new Texture2D(newh, newh, TextureFormat.RGB24, true);
        yield return new WaitForEndOfFrame();
        //screenshot.ReadPixels(new Rect(newx, Screen.height - newy - newh, neww, newh), 0, 0, false);  // old
        screenshot.ReadPixels(new Rect(newx + ((neww - newh) / 2), newy, newh, newh), 0, 0, false);  // old
        //Texture2D screenshot = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        //screenshot.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        //screenshot.ReadPixels(new Rect(0, 0, Screen.width, Screen.height-200), 0, 0);// rectangle will be captured
        screenshot.Apply();

        Texture2D newScreenshot = ScaleTexture(screenshot, 200, 200);    // NEW width and height
        // Texture2D newScreenshot = ScaleTexture(screenshot, neww / 2, newh / 2);    // NEW width and height
        byte[] bytes = newScreenshot.EncodeToPNG();

        string screenShotPath = Application.persistentDataPath + "/" + ScreenshotName + ".png";
        Debug.Log(screenShotPath);
        File.WriteAllBytes(screenShotPath, bytes);
        // nota = service.Uploadfile(bytes, Network.player.ipAddress);

        //  yield return nota;
        //yield return new WaitForEndOfFrame();
        Destroy(screenshot);
        yield return new WaitForEndOfFrame();
        canvas.SetActive(true);
        canvas_1.SetActive(true);
        canvas_2.SetActive(true);
        
        // yield return new WaitForSeconds(1);
    }
    void LoadTextureOfSlider(InfoBank _bank)
    {
        _bank.spritesArr = new Sprite[_bank.SavedModel.Length];
        Bank.texture = new Texture2D[_bank.SavedModel.Length];
        for (int i = 0; i < _bank.SavedModel.Length; i++)
        {
            //       Debug.Log(_bank.SavedModel[i]);
            StartCoroutine(LoadImage(_bank.SavedModel[i]));

            //while (img != null)
            //{
            //   // bb.sprite = convertToSprites(img);
            //    _bank.spritesArr[i] = convertToSprites(img);
            //}

            //_bank.spritesArr[i] = convertToSprites(img);
        }
    }
    void LoadAllDates(InfoBank _bank)
    {
        _bank.dateTime = new string[_bank.SavedModel.Length];
        for (int i = 0; i < _bank.SavedModel.Length; i++)
        {
            if (PlayerPrefs.GetString("date time_" + Bank.SavedModel[i], "N") != "N")
            {
                _bank.dateTime[i] = PlayerPrefs.GetString("date time_" + Bank.SavedModel[i]);
            }
            else
            {
                _bank.dateTime[i] = " N ";
            }

        }
    }
    //   
    //IEnumerator TakeScreenshot1(string ScreenshotName)
    //{
    //    yield return new WaitForEndOfFrame();

    //    var width = Screen.width;
    //    var height = Screen.height;
    //    var tex = new Texture2D(width, height, TextureFormat.RGB24, false);
    //    // Read screen contents into the texture
    //    tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
    //    tex.Apply();

    //    // save screen shot to device
    //    string screenShotPath = Application.persistentDataPath  + ScreenshotName + ".png";
    //    Texture2D newScreenshot = ScaleTexture(tex, rectWidth / 2, rectHeight / 2);
    //    byte[] screenshot = tex.EncodeToPNG(); //tex.EncodeToJPG();//
    //    File.WriteAllBytes(screenShotPath, screenshot);

    //    Debug.Log("KKKK");
    //}

    IEnumerator LoadImage(string ScreenshotName)
    {
        //  Debug.Log(ScreenshotName);
        string screenShotPath = Application.persistentDataPath + "/" + ScreenshotName + ".png";
        //   Debug.Log(ScreenshotName);
#if UNITY_IPHONE || UNITY_ANDROID
        //   Debug.Log("mobile");
        // WWW w = new WWW("file://'" + screenShotPath);
        WWW w = new WWW("file:///" + screenShotPath);
        yield return w;
        if (w.error == null)
        {

            Bank.spritesArr[indextexture] = convertToSprites(w.texture);
            indextexture += 1;
        }
        else
            print(w.error);
#endif
        //  return null;
    }
    public void SaveBtn1()
    {
        StartCoroutine(TakeScreenShot("ScreenshotName"));
    }
    IEnumerator resetSlider()
    {
        //StartCoroutine(LoadImage("ScreenshotName"));
        //obj.GetComponent<Renderer>().material.mainTexture = img;
        //bb.sprite = convertToSprites(img);
        yield return new WaitForEndOfFrame();
        indextexture = 0;
        LoadTextureOfSlider(Bank);
        LoadAllDates(Bank);
    }

    public Sprite convertToSprites(Texture2D texture)
    {
        Sprite spriteArr = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f), 1);

        return spriteArr;
    }

    private Texture2D ScaleTexture(Texture2D source, int targetWidth, int targetHeight)
    {
        Texture2D result = new Texture2D(targetWidth, targetHeight, source.format, true);
        Color[] rpixels = result.GetPixels(0);
        float incX = ((float)1 / source.width) * ((float)source.width / targetWidth);
        float incY = ((float)1 / source.height) * ((float)source.height / targetHeight);
        for (int px = 0; px < rpixels.Length; px++)
        {
            rpixels[px] = source.GetPixelBilinear(incX * ((float)px % targetWidth),
                                                  incY * ((float)Mathf.Floor(px / targetWidth)));
        }
        result.SetPixels(rpixels, 0);
        result.Apply();
        return result;
    }
    #endregion screenshotVar

    #region SaveAcc
    void SaveAcclist(string DesignName)
    {
        PlayerPrefs.SetInt(DesignName + "AccessCount", AccessManager.accessoriesList.Count);
        //AccessManager.accessoriesList
        for (int i = 0; i < AccessManager.accessoriesList.Count; i++)
        {
            PlayerPrefsX.SetVector3(DesignName + "AccessPos" + i.ToString(), AccessManager.accessoriesList[i].transform.position);
            PlayerPrefsX.SetQuaternion(DesignName + "AccessQuat" + i.ToString(), AccessManager.accessoriesList[i].transform.rotation);
            PlayerPrefsX.SetVector3(DesignName + "AccessScale" + i.ToString(), AccessManager.accessoriesList[i].transform.localScale);
            PlayerPrefs.SetString(DesignName + "AccessObj" + i.ToString(), AccessManager.accessoriesList[i].innerObj.name);
            PlayerPrefsX.SetColor(DesignName + "AccessColor" + i.ToString(), AccessManager.accessoriesList[i].color);
        }
    }
    void LoadAcclist(string DesignName)
    {

        int arraylength = PlayerPrefs.GetInt(DesignName + "AccessCount", AccessManager.accessoriesList.Count);
        // Debug.Log(arraylength + "  " + DesignName + "   " + PlayerPrefs.GetString(DesignName + "AccessObj" + 0));
        if (arraylength > 0)
        {

            for (int i = 0; i < arraylength; i++)
            {
                //AccessManager.InstNewItem(GameObject heroPrefab, Vector3 position,Quaternion rotate,  List<AccItem> List,GameObject parentObj)
                Vector3 tempPos = PlayerPrefsX.GetVector3(DesignName + "AccessPos" + i.ToString());
                Quaternion rotation = PlayerPrefsX.GetQuaternion(DesignName + "AccessQuat" + i.ToString());
                Vector3 scale = PlayerPrefsX.GetVector3(DesignName + "AccessScale" + i.ToString(), Vector3.one);
                AccessManager.color = PlayerPrefsX.GetColor(DesignName + "AccessColor" + i.ToString(), Color.white);
                //  Debug.Log(getmodel(PlayerPrefs.GetString(DesignName + "AccessObj" + i), acessBank).name);
                AccessManager.InstNewItem(getmodel(PlayerPrefs.GetString(DesignName + "AccessObj" + i.ToString()), acessBank), tempPos, rotation, scale, AccessManager.accessoriesList, AccessManager.ParentObject);

            }
            //AccessManager.unm
            MeshRenderer[] allChildren = AccessManager.accessoriesList[AccessManager.selectedItemIndex].GetComponentsInChildren<MeshRenderer>();
            //  Debug.Log("PPP");
            //gos2 = GetChildGameObjects(other);

            foreach (MeshRenderer child in allChildren)
            {
                if (child.gameObject.CompareTag("ItemMarker"))
                    child.enabled = false;
            }
        }

    }
    GameObject getmodel(string name, InfoBank bank)
    {
        GameObject obj = null;
        for (int i = 0; i < bank.Models.Length; i++)
        {
            // Debug.Log(bank.Models[i].name + "  " + name);
            if (bank.Models[i].name + "(Clone)" == name)
            {
                obj = bank.Models[i];
            }

        }
        return obj;

    }
    #endregion SaveAcc
}
//public ScrollListTest colorSlider;
//    public ColorPicker colorPicker;
//    public InfoBank Bank;
//    public GameObject saveBtn, customBtn,buttonCollection,backBtn;
//    public Color _color, tempColor;
//    public bool InCustomMode;
//   // public Color[] ts;
//    // Use this for initialization
//    void Start () {
//        if (PlayerPrefsX.GetColorArray("colorArr").Length>0)
//        {
//            Bank.ColorsBank = PlayerPrefsX.GetColorArray("colorArr");
//            colorSlider.IsInst = true;

//           // colorSlider.cells[0].clickit();
//           //colorSlider.changeEvent();
//            //colorSlider.isUpdate = true;
//        }

//        tempColor =Color.white;
//    }
//    public void getColorMaterial(GameObject obj, int materialIndex)
//    {
//        for (int i = 0; i < Bank.ColorsBank.Length; i++)
//        {

//            if (Bank.ColorsBank[i] == obj.GetComponent<Renderer>().materials[materialIndex].color)
//            {
//                //Debug.Log(Bank.ColorsBank[i] + "  " + obj.GetComponent<Renderer>().materials[materialIndex].color);

//                _color = Bank.ColorsBank[i];//.GetComponent<Renderer>().materials[materialIndex].color;
//                //    Debug.Log("OK" + "  " + _color);
//            }
//        }

//        //startColor = obj.GetComponent<Renderer>().materials[materialIndex].color;
//    }
//    //public void getColorMeshMaterial(GameObject obj)
//    //{
//    //    Debug.Log(obj.name);
//    //    startColor = obj.GetComponent<MeshRenderer>().sharedMaterial.color;
//    //}
//    // Update is called once per frame
//    void Update () {
//        if (tempColor.r != colorPicker.CurrentColor.r && tempColor.g != colorPicker.CurrentColor.g && tempColor.b != colorPicker.CurrentColor.b)
//        {

//            Debug.Log("OO" + tempColor + "  " + colorPicker.CurrentColor);
//        if (InCustomMode)
//        {
//            tempColor = colorPicker.CurrentColor;
//        }
//        else
//        {
//            if (colorSlider.GetComponent<ScrollListTest>().selectedIndex >= 0)
//            {
//            //    Debug.Log("LLLL" + colorSlider.GetComponent<ScrollListTest>().selectedIndex + "  ");
//                colorPicker.CurrentColor = colorSlider.GetComponent<ScrollListTest>().Bank.ColorsBank[colorSlider.GetComponent<ScrollListTest>().selectedIndex];
//            }

//        }

//        }

//        //ts=PlayerPrefsX.GetColorArray("colorArr");
//        //PlayerPrefsX.SetColorArray("colorArr", Bank.ColorsBank);
//        //ts = colorPicker.CurrentColor;
//        if (Input.GetKeyDown(KeyCode.C))
//        {
//            PlayerPrefsX.SetColorArray("colorArr", new Color[0]);
//        }
//        if (Input.GetKeyDown(KeyCode.M))
//        {
//            SaveColorBtn();
//        }
//    }
//    public void SaveColorBtn()
//    {
//        InCustomMode = false;
//        //colorPicker.gameObject.SetActive(false);
//        //colorSlider.gameObject.SetActive(true);
//        //saveBtn.gameObject.SetActive(true); customBtn.gameObject.SetActive(false);
//        Bank.AddColor(colorPicker.CurrentColor);

//        PlayerPrefsX.SetColorArray("colorArr", Bank.ColorsBank);
//        // colorSlider.isUpdate = true;
//        colorSlider.TempChangeEvent();
//        colorSlider.cells[Bank.ColorsBank.Length - 1].clickit();
//        colorSlider.myScrollRect.verticalNormalizedPosition = 0f;
//        backColorBtn();
//        //colorPicker.startColor = colorSlider.GetComponent<ScrollListTest>().Bank.ColorsBank[colorSlider.GetComponent<ScrollListTest>().selectedIndex];
//    }
//    public void CustomColorBtn()
//    {

//        colorPicker.startColor=colorPicker.CurrentColor = colorSlider.GetComponent<ScrollListTest>().Bank.ColorsBank[colorSlider.GetComponent<ScrollListTest>().selectedIndex];
//        colorPicker.refresh = true;
//        InCustomMode = true;
//        colorPicker.gameObject.SetActive(true);
//        backBtn.gameObject.SetActive(false);
//        if (buttonCollection!=null)
//        buttonCollection.SetActive(false);
//        colorSlider.transform.parent.gameObject.SetActive(false);
//        saveBtn.gameObject.SetActive(false); customBtn.gameObject.SetActive(true);

//    }
//    public void backColorBtn()
//    {
//        colorPicker.startColor =colorPicker.CurrentColor = colorSlider.GetComponent<ScrollListTest>().Bank.ColorsBank[colorSlider.GetComponent<ScrollListTest>().selectedIndex];
//        InCustomMode = false;
//        colorPicker.gameObject.SetActive(false);
//        backBtn.gameObject.SetActive(true);

//        if (buttonCollection != null)
//        buttonCollection.SetActive(true);
//        colorSlider.transform.parent.gameObject.SetActive(true);
//        saveBtn.gameObject.SetActive(true); customBtn.gameObject.SetActive(false);

//        colorPicker.refresh = true;
//}