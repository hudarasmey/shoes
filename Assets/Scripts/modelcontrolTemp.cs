﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class modelcontrolTemp : MonoBehaviour {

    public int menuIndex;
    public Button FaceBtn, HeelBtn, BaseBtn,colorBtn;
    public ScrollListTest materialSliderFace;
    public ScrollListTest materialSliderBase;
    public ScrollListTest materialSliderHeel;
    public ColorPicker colorPicker;
    public GameObject heelSilders, baseSliders, faceSliders;
   
    public Renderer renderer;
   
    public Shose shose;
    public Slider baseHeightFront, heelThickness, HeelHeight, faceNode, facethick;
    public WowCamera CameraRotation;
    public Texture2D DefaultTexture;
    int tempCase;
    
    public enum ObjMenu { Face, Base, Heel, none };
    public bool isColor;
    //public Slider tileValueSlider;
    public float tileValue;

    public MaterialMenu matMenu, tempMatMenu;
    public ObjMenu objType;

    //public GameObject[] respawns;
    //public Shader shader;
    //public int renderNum;
    // public ColorPicker picker;
    //public bool start;
    // public bool isONGUI;
    // public IsClicked[] isClickedBools;
    // Use this for initialization
    void Awake()
    {
        // isClickedBools = FindObjectsOfType<IsClicked>();
        //respawns = GameObject.FindGameObjectsWithTag("isClicked");
        //isClickedBools = new IsClicked[respawns.Length];
        //for (int i = 0; i < respawns.Length; i++)
        //{
        //    if (respawns[i].GetComponent<IsClicked>())
        //    {
        //        isClickedBools[i] = respawns[i].GetComponent<IsClicked>();
        //    }
        //}
    }
    void OnEnable()
    {

        ///   print("script was enabled");
        //picker.CurrentColor = renderer.material.color;
    }
    void Start()
    {
       
        objType = ObjMenu.none;
        matMenu = MaterialMenu.color;
        tempMatMenu = MaterialMenu.none;
    }
    // Update is called once per frame
    void Update()
    {
        switch (menuIndex)
        {
            case 1:  //face
                if (tempCase != 1)
                {
                    //tempMatMenu = MaterialMenu.none;
                    //changeActiveMenu(colorPicker, materialSliderFace);


                    objType = ObjMenu.Face;
                    colorPicker.getColorMaterial(shose.faceObj,0);
                    colorPicker.setColorMaterial();
                    //materialSliderFace.transform.parent.gameObject.SetActive(true);
                    //materialSliderBase.transform.parent.gameObject.SetActive(false);
                    //materialSliderHeel.transform.parent.gameObject.SetActive(false);

                    //materialSliderFace.gameObject.SetActive(true);
                    //materialSliderBase.gameObject.SetActive(false);
                    //materialSliderHeel.gameObject.SetActive(false);


                    //faceSliders.gameObject.SetActive(false);
                    //baseSliders.gameObject.SetActive(false);
                    //heelSilders.gameObject.SetActive(true);


                    //faceSliders.SetActive(true);
                    //baseSliders.SetActive(false);
                    //heelSilders.SetActive(false);

                   // matMenu = MaterialMenu.none;
                    tempCase = menuIndex;
                    menuIndex = 0;
                    objType = ObjMenu.Face;
                    changeActiveMenu(colorPicker, materialSliderFace);
                    

                }
                //if (tempMatMenu == matMenu)
                //{
                    
                //}
                //if (matMenu==M)
                //{
                //    isColor = false; 
                //}
                
                break;
            case 2:  //Base

                if (tempCase != 2)
                {
                    //tempMatMenu = MaterialMenu.none;
                    //changeActiveMenu(colorPicker, materialSliderFace);


                    objType = ObjMenu.Base;
                    colorPicker.getColorMaterial(shose.baseObj,0);
                    colorPicker.setColorMaterial();
                    //materialSliderFace.transform.parent.gameObject.SetActive(false);
                    //materialSliderBase.transform.parent.gameObject.SetActive(true);
                    //materialSliderHeel.transform.parent.gameObject.SetActive(false);

                    //faceSliders.SetActive(false);
                    //baseSliders.SetActive(true);
                    //heelSilders.SetActive(false);

                   // matMenu = MaterialMenu.none;
                    tempCase = menuIndex;
                    menuIndex = 0;
                    objType = ObjMenu.Base;
                    changeActiveMenu(colorPicker, materialSliderBase);
                }
                //if (tempMatMenu == matMenu)
                //{
                //    MatMenuFn( matMenu);
                //}
                break;
            case 3:  //heel
                if (tempCase != 3)
                {
                    //tempMatMenu = MaterialMenu.none;
                    //changeActiveMenu(colorPicker, materialSliderFace);


                    objType = ObjMenu.Heel;
                    colorPicker.getColorMaterial(shose.heelObj,0);
                    colorPicker.setColorMaterial();
                    //materialSliderFace.transform.parent.gameObject.SetActive(false);
                    //materialSliderBase.transform.parent.gameObject.SetActive(false);
                    //materialSliderHeel.transform.parent.gameObject.SetActive(true);

                    //faceSliders.SetActive(false);
                    //baseSliders.SetActive(false);
                    //heelSilders.SetActive(true);

                    //matMenu = MaterialMenu.none;
                    tempCase = menuIndex;
                    menuIndex = 0;
                    objType = ObjMenu.Heel;
                    changeActiveMenu(colorPicker, materialSliderHeel);
                    objType = ObjMenu.Heel;
                    changeActiveMenu(colorPicker, materialSliderHeel);
                }
                //if (tempMatMenu == matMenu)
                //{

                //    MatMenuFn( matMenu);
                //}
                break;
            default:
                break;
        }

        if (materialSliderFace.gameObject.activeSelf && materialSliderFace.selectedIndex >= 0 && materialSliderFace.IsAction)
        {

            //Texture X = materialSliderFace.cells[materialSliderFace.selectedIndex].imageContent.texture;
            Texture X = materialSliderFace.Bank.texture[materialSliderFace.selectedIndex];//.bank.texture;
          //  X.name = "Procedural Texture";
            X.wrapMode = TextureWrapMode.Repeat;
            //shose.faceObj.GetComponent<Renderer>().material.SetTextureScale("_MainTex", new Vector2(3, 3));
            ChangeTexture(shose.faceObj, X, null, 0.5f);
            //shose.faceObj.GetComponent<Renderer>().material.mainTexture = X;
            shose.faceObj.GetComponent<Renderer>().material.color = colorPicker.CurrentColor;
            //shose.faceObj.GetComponent<Renderer>().material.color = shose.colorWheelControl.Selection;

           

            materialSliderFace.IsAction = false;

            
        }
        if (materialSliderBase.gameObject.activeSelf && materialSliderBase.selectedIndex >= 0 && materialSliderBase.IsAction)
        {
           // Texture X = materialSliderBase.cells[materialSliderBase.selectedIndex].imageContent.texture;
            Texture X = materialSliderFace.Bank.texture[materialSliderBase.selectedIndex];//.imageContent.texture;
        //    X.name = "Procedural Texture";
            X.wrapMode = TextureWrapMode.Repeat;
            shose.baseObj.GetComponent<Renderer>().material.SetTextureScale("_MainTex", new Vector2(4, 4));
            shose.baseObj.GetComponent<Renderer>().material.mainTexture = X;
           // shose.baseObj.GetComponent<Renderer>().material.color = shose.colorWheelControl.Selection;
            shose.baseObj.GetComponent<Renderer>().material.color = colorPicker.CurrentColor;
            materialSliderBase.IsAction = false;

        }
        if (materialSliderHeel.gameObject.activeSelf && materialSliderHeel.selectedIndex >= 0 && materialSliderHeel.IsAction)
        {
          //  Texture X = materialSliderHeel.cells[materialSliderHeel.selectedIndex].imageContent.texture;
            Texture X = materialSliderFace.Bank.texture[materialSliderHeel.selectedIndex];// imageContent.texture;
            X.name = "Procedural Texture";
            X.wrapMode = TextureWrapMode.Repeat;
            shose.heelObj.GetComponent<Renderer>().material.SetTextureScale("_MainTex", new Vector2(4, 4));
            shose.heelObj.GetComponent<Renderer>().material.mainTexture = X;
          // shose.baseObj.GetComponent<Renderer>().material.color = shose.colorWheelControl.Selection;
            shose.baseObj.GetComponent<Renderer>().material.color = colorPicker.CurrentColor;
            materialSliderHeel.IsAction = false;


          
        }
       
        //shose.faceObj.GetComponent<Renderer>().material.color = shose.colorWheelControl.Selection;
        //shose.heelObj.GetComponent<Renderer>().material.color = shose.colorWheelControl.Selection;
        //shose.baseObj.GetComponent<Renderer>().material.color = shose.colorWheelControl.Selection;

        //if (heelHeight.value < 0.5f)
        //{
        //    shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(3, heelHeightFront.value * 100);
        //    shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(3, heelHeightFront.value * 100);
        //    shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(3, heelHeightFront.value * 100);
        //}
        //else
        //{
        //    shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, heelHeight.value * 100);
        //    shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, heelHeight.value * 100);
        //    shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, heelHeight.value * 100);
        //}
        if (baseHeightFront.value < 0.5f)
            {
               
                shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(3, 100-(baseHeightFront.value * 2 * 100));
                shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(3, 100-(baseHeightFront.value * 2 * 100));
                shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(3, 100-(baseHeightFront.value * 2 * 100));
                shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 0);
                shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 0);
                shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 0);
                //shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, 100 - (baseHeightFront.value * 2 * 100));
                //shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, 100 - (baseHeightFront.value * 2 * 100));
                //shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, 100 - (baseHeightFront.value * 2 * 100));

                //if (shose.faceObj.GetComponent<SkinnedMeshRenderer>().GetBlendShapeWeight(1) >= (baseHeightFront.value * 2 * 100))
                //{
                //    shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, (baseHeightFront.value * 2 * 100));
                //    shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, (baseHeightFront.value * 2 * 100));
                //    shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, (baseHeightFront.value * 2 * 100));
                //}
                //else
                //{

                //    shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, heelThickness.value * 100);
                //    shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, heelThickness.value * 100);
                //    shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, heelThickness.value * 100);
                //}
        
            }
        //else if(baseHeightFront.value == 0.5f)
        //{
        //     shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 0);
        //     shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 0);
        //     shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(3, 0);
        //     shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(3, 0);
        //}
        else if (baseHeightFront.value >0.5f)
        {
            shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(3, 0);
            shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(3, 0);
            shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(3, 0);


            shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, (baseHeightFront.value - 0.5f) * 2 * 100);
            shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, (baseHeightFront.value - 0.5f) * 2 * 100);
            shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, (baseHeightFront.value - 0.5f) * 2 * 100);
            //if (shose.faceObj.GetComponent<SkinnedMeshRenderer>().GetBlendShapeWeight(1) < (baseHeightFront.value * 2 * 100))
            //{
            //shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, heelThickness.value * 100);
            //shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, heelThickness.value * 100);
            //shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, heelThickness.value * 100);
            //}
           
        }


        if (shose.faceObj.GetComponent<SkinnedMeshRenderer>().GetBlendShapeWeight(1) >= (baseHeightFront.value * 2 * 100))
        {
            shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, (baseHeightFront.value * 2 * 100));
            shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, (baseHeightFront.value * 2 * 100));
            shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, (baseHeightFront.value * 2 * 100));
            heelThickness.value = (baseHeightFront.value * 2 );
               
                   
        }
        else
        {
            shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, heelThickness.value * 100);
            shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, heelThickness.value * 100);
            shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(1, heelThickness.value * 100);
        }
        shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(5, (facethick.value * 100)/2);
        shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(5, (facethick.value * 100)/2);
        shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(5, (facethick.value * 100)/2);

        //shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(2, heelHeight.value * 100);
        //shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(2, heelHeight.value * 100);
        //shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(2, heelHeight.value * 100);

        //shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(3, HeelHeight.value * 100);
        //shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(3, HeelHeight.value * 100);
        //shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(3, HeelHeight.value * 100);

        //shose.faceObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(6, faceNode.value * 100);
        //shose.baseObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(6, faceNode.value * 100);
        //shose.heelObj.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(6, faceNode.value * 100);
    
        ////////switch (objType)
        ////////{
        ////////    case ObjMenu.Face:
        ////////        //color
        ////////        shose.faceObj.GetComponent<Renderer>().material.color = colorPicker.CurrentColor;
        ////////        //material tile
        ////////        if (tileValue != materialSliderFace.tileValueSlider.value * 10)
        ////////        {
        ////////            tileValue = 1 + materialSliderFace.tileValueSlider.value * 9;
        ////////            shose.faceObj.GetComponent<Renderer>().material.SetTextureScale("_MainTex", new Vector2(tileValue, tileValue));
        ////////            //glow

        ////////            //float scaleX = Mathf.Cos(Time.time) * 0.5F + 1;
        ////////            //float scaleY = Mathf.Sin(Time.time) * 0.5F + 1;
        ////////            //shose.faceObj.GetComponent<Renderer>().material.mainTextureScale = new Vector2(scaleX, scaleY);
        ////////            //shose.faceObj.GetComponent<Renderer>().material.mainTextureScale = new Vector2(tileValue, tileValue);
        ////////        }
        ////////        //


        ////////      changeActiveMenu( colorPicker,materialSliderFace);
             
               
                
        ////////        break;
        ////////    case ObjMenu.Base:
        ////////        //color
        ////////        shose.baseObj.GetComponent<Renderer>().material.color = colorPicker.CurrentColor;
        ////////        //material tile
        ////////        if (tileValue != materialSliderBase.tileValueSlider.value * 10)
        ////////        {
        ////////            tileValue = 1 + materialSliderBase.tileValueSlider.value * 9;
        ////////            shose.baseObj.GetComponent<Renderer>().material.SetTextureScale("_MainTex", new Vector2(tileValue, tileValue));
        ////////            //glow

        ////////            //float scaleX = Mathf.Cos(Time.time) * 0.5F + 1;
        ////////            //float scaleY = Mathf.Sin(Time.time) * 0.5F + 1;
        ////////            //shose.faceObj.GetComponent<Renderer>().material.mainTextureScale = new Vector2(scaleX, scaleY);
        ////////            //shose.faceObj.GetComponent<Renderer>().material.mainTextureScale = new Vector2(tileValue, tileValue);
        ////////        }
        ////////           changeActiveMenu( colorPicker,materialSliderBase);
        ////////        break;
        ////////    case ObjMenu.Heel:
        ////////        //color
        ////////        shose.heelObj.GetComponent<Renderer>().material.color = colorPicker.CurrentColor;
        ////////        //material tile
        ////////        if (tileValue != materialSliderHeel.tileValueSlider.value * 10)
        ////////        {
        ////////            tileValue = 1 + materialSliderHeel.tileValueSlider.value * 9;
        ////////            shose.heelObj.GetComponent<Renderer>().material.SetTextureScale("_MainTex", new Vector2(tileValue, tileValue));
        ////////            //glow

        ////////            //float scaleX = Mathf.Cos(Time.time) * 0.5F + 1;
        ////////            //float scaleY = Mathf.Sin(Time.time) * 0.5F + 1;
        ////////            //shose.faceObj.GetComponent<Renderer>().material.mainTextureScale = new Vector2(scaleX, scaleY);
        ////////            //shose.faceObj.GetComponent<Renderer>().material.mainTextureScale = new Vector2(tileValue, tileValue);
        ////////        }
        ////////         changeActiveMenu( colorPicker,materialSliderHeel);
        ////////        break;
        ////////    case ObjMenu.none:
        ////////        break;
        ////////    default:
        ////////        break;
        ////////}
        

       
       
       
    }

    //public void MatMenuFn( MaterialMenu matMenu)
    //{
    //    switch (matMenu)
    //    {
    //        case MaterialMenu.color:
    //            tempMatMenu = MaterialMenu.color;
    //            ChangeTexture(shose.faceObj, DefaultTexture, null, 0);
    //            colorPicker.gameObject.SetActive(true);
    //            break;
    //        case MaterialMenu.texture:
    //            tempMatMenu = MaterialMenu.texture;
    //            break;
    //        case MaterialMenu.pattern:
    //            tempMatMenu=MaterialMenu.pattern;
    //            break;
    //        case MaterialMenu.none:
    //            break;
    //        default:
    //            break;
    //    }

    //}
    public void ChangeTexture(GameObject obj, Texture _MainTex, Texture2D _BumpMap, float _BumpScale)
    {
        SkinnedMeshRenderer renderer = obj.GetComponent<SkinnedMeshRenderer>();
        Material mat = renderer.material;
        mat.SetTexture("_MainTex", _MainTex);
        mat.SetTexture("_BumpMap", _BumpMap);
        mat.SetFloat("_BumpScale", _BumpScale);
    }

    public void ChangeBump(GameObject obj, Texture _BumpMap, float _BumpScale)
    {
        SkinnedMeshRenderer renderer = obj.GetComponent<SkinnedMeshRenderer>();
        Material mat = renderer.material;
       
        mat.SetTexture("_BumpMap", _BumpMap);
        mat.SetFloat("_BumpScale", _BumpScale);
    }
    public void ChangeSecondTexture(GameObject obj, Texture _DetailAlbedoMap, Texture _DetailNormalMap, float _DetailNormalMapScale)
    {
        SkinnedMeshRenderer renderer = obj.GetComponent<SkinnedMeshRenderer>();
        Material mat = renderer.material;
    
        mat.SetTexture("_DetailAlbedoMap", _DetailAlbedoMap);
        mat.SetTexture("_DetailNormalMap", _DetailNormalMap);
        mat.SetFloat("_DetailNormalMapScale", _DetailNormalMapScale);

       
    }

    public void ChangeTexture(GameObject obj, float _Glossiness, float _Metallic)
    {
        SkinnedMeshRenderer renderer = obj.GetComponent<SkinnedMeshRenderer>();
        Material mat = renderer.material;

        mat.SetFloat("_Glossiness", _Glossiness);
        mat.SetFloat("_Metallic", _Metallic);

    }


    //public void ChangeTexture(GameObject obj, Texture2D _MainTex, Texture2D _BumpMap, float _BumpScale)
    //{
    //    SkinnedMeshRenderer renderer = GetComponent<SkinnedMeshRenderer>();
    //    Material mat = renderer.material;
    //    mat.SetTexture("_MainTex", _MainTex);
    //    mat.SetTexture("_BumpMap", _BumpMap);
    //    mat.SetFloat("_BumpScale", _BumpScale);


    //    mat.SetTexture("_DetailAlbedoMap", _DetailAlbedoMap);
    //    mat.SetTexture("_DetailNormalMap", _DetailNormalMap);
    //    mat.SetFloat("_BumpScale", _BumpScale);
    //    mat.SetFloat("_Glossiness", _Glossiness);
    //    mat.SetFloat("_Metallic", _Metallic);
    //    mat.SetFloat("_DetailNormalMapScale", _DetailNormalMapScale);
    //}

    public void changeActive(MaterialMenu matMenu,GameObject Obj,int index)
    {
        
        //switch (matMenu)
        //{
        //    case MaterialMenu.color:
        //        tempMatMenu = MaterialMenu.color;

        //        Obj.gameObject.SetActive(true);
        //        break;
        //    case MaterialMenu.texture:
        //        tempMatMenu = MaterialMenu.texture;
        //        break;
        //    case MaterialMenu.pattern:
        //        tempMatMenu = MaterialMenu.pattern;
        //        break;
        //    case MaterialMenu.none:
        //        break;
        //    default:
        //        break;
        //}


    }
    public void changeActiveMenu(ColorPicker colorpicker, ScrollListTest materialSlider)
    {
       


        if (tempMatMenu != matMenu)
        {
            switch (matMenu)
            {
                case MaterialMenu.color:
                    Debug.Log("lllll");
                    tempMatMenu = MaterialMenu.color;
                    materialSlider.transform.parent.gameObject.SetActive(false);
                    colorpicker.gameObject.SetActive(true);
                    break;
                case MaterialMenu.texture:
                    tempMatMenu = MaterialMenu.texture;
                    materialSlider.transform.parent.gameObject.SetActive(true);
                    colorpicker.gameObject.SetActive(false);
                    break;
                case MaterialMenu.pattern:
                    tempMatMenu = MaterialMenu.pattern;
                    materialSlider.transform.parent.gameObject.SetActive(false);
                    colorpicker.gameObject.SetActive(false);
                    break;
                case MaterialMenu.none:
                    materialSlider.transform.parent.gameObject.SetActive(false);
                    colorpicker.gameObject.SetActive(false);
                    break;
                default:
                    break;
            }


        }
    }
    public void ButtonClick(int index)
    {
        menuIndex = index;
    }
    public void Menu(int type)
    {
        matMenu = (MaterialMenu)type;
    }
    public void Menu1(MaterialMenu type)
    {
        matMenu = type;
    }
}