﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;


public class WowCamera : MonoBehaviour//,IPointerClickHandler
{
    public Camera cam ,guiCam;
public bool IsActive;
public GameObject followerPiont;
public Transform target;
public Vector3 targetPos;
public float targetHeight = 1.7f, maxHeight=15;
public float distance = 5.0f;
public float offsetFromWall = 0.1f;
public float maxDistance = 20;
public float minDistance = .6f;
public float xSpeed = 200.0f;
public float ySpeed = 200.0f;
public float targetSpeed = 5.0f;
public int yMinLimit = -80;
public int yMaxLimit = 80;
public int zoomRate = 40;
public float rotationDampening = 3.0f;
public float zoomDampening = 5.0f;
public LayerMask collisionLayers = -1;
public float targetY,targetX;
public float Speed=10;
public float xDeg = 0.0f;
public float yDeg = 0.0f;
public float currentDistance;
    public float desiredDistance;
private float correctedDistance;
public float tempSpeed;
public bool finish,halfFinish;
public float increaseRatio = 0.051f;
//public float targetSpeed;
public bool start;

    //device

private bool isTouchingDevice;
private Vector3 lastInputWorldPos;
private Vector3 currentInputWorldPos;
public Vector3 deltaInputWorldPos;
public bool mouseDown;
public bool mouseD;
public bool isONGUI;
public bool isMoveTouch;

//public List<float> Distance;
public List<Vector3> cameraRotationAngle;
public List<Vector3> selectObjCamRotAngle;
UnityEngine.Touch touchZero;
UnityEngine.Touch touchOne;
public float perspectiveZoomSpeed = 0.5f;        // The rate of change of the field of view in perspective mode.
public float orthoZoomSpeed = 0.5f;        // The rate of change of the orthographic size in orthographic mode.
bool isPunch;
float factor;
public bool setObliq;
public float TempX,TempY,TempD;
public float d;
public float intHeight, currentHeight;
public bool upDownMove;
public bool accBool;
public Transform tarhget;
public float deafultAccDis=50;
float tempSide;
[SerializeField, Range(0.0f, 1.0f)]
private float _lerpRate;
float tempObliqX, tempObliqY, ObliqX, ObliqY;
bool startOblic;
public    bool  accessVis,stopcamera;
    void Awake()
{
    //Instance = this;

    switch (Application.platform)
    {
        case RuntimePlatform.WindowsEditor:
            isTouchingDevice = false;
            break;
        case RuntimePlatform.Android:
            isTouchingDevice = true;
            break;
    }
    intHeight = maxHeight;// targetPos.y;
}
/* Store the position of mouse when the player clicks the left mouse button.
 */
void storeMousePosition()
{
   // Debug.Log("mouse");
    if (Input.GetMouseButtonDown(0) )
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            
            mouseDown = true;
                if (accessVis)
                {
                    stopcamera = true;
                }
                else
                {
                    stopcamera = false;
                }
                Debug.Log(" Touch  " + accessVis + stopcamera);
                
                lastInputWorldPos = Input.mousePosition;
        }
        else
        {
          //  Debug.Log("Hit UI, Ignore Touch");
            //lastInputWorldPos = Input.mousePosition;
            deltaInputWorldPos = Vector3.zero;
        }
       // mouseDown = true;
        //lastInputWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //lastInputWorldPos = Camera.main.WorldToViewportPoint(Input.mousePosition);
        
        
    }
    else if (Input.GetMouseButton(0))
    {
        if (mouseDown && !stopcamera)
        {
            

            //  mouseDown = true;
            //currentInputWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //currentInputWorldPos = Camera.main.WorldToViewportPoint(Input.mousePosition);
            currentInputWorldPos = Input.mousePosition;
            deltaInputWorldPos = new Vector3(currentInputWorldPos.x - lastInputWorldPos.x, currentInputWorldPos.y - lastInputWorldPos.y, 0.0f);
            //foreach (ListBox listbox in listBoxes)
            //    listbox.updatePosition(deltaInputWorldPos);
            if (deltaInputWorldPos.x > 5 || deltaInputWorldPos.y > 5)
            {
               /////////////////////  Debug.Log("Moved");
                isMoveTouch = true;
            } 
            lastInputWorldPos = currentInputWorldPos;
         //   Debug.Log(deltaInputWorldPos +" "+ mouseDown);
            //  Debug.Log(currentInputWorldPos + "  " + deltaInputWorldPos);
            
        }
    }
    else if (Input.GetMouseButtonUp(0) )
    {
        mouseDown = false;
        deltaInputWorldPos = Vector3.zero;
       // setSlidingEffect();
    }
}

bool IsPointerOverGameObject(int fingerId)
{
    EventSystem eventSystem = EventSystem.current;
    return (eventSystem.IsPointerOverGameObject(fingerId));
       // && eventSystem.currentSelectedGameObject != null);
}
/* Store the position of touching on the mobile.
 */
void storeFingerPosition()
{
    int nbTouches = Input.touchCount;
    
    if (nbTouches > 0)
    {
        if (Input.GetTouch(0).phase == TouchPhase.Began)
        {

            if (Input.touchCount == 1)
            {
               // Debug.Log("Touch" + Input.touchCount);
                if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    if (IsPointerOverGameObject(Input.GetTouch(0).fingerId))
                    {
                        deltaInputWorldPos = Vector3.zero;
                        Debug.Log("Hit UI, Ignore Touch");
                    }
                    else
                    {
                        mouseDown = true;
                        lastInputWorldPos = Input.GetTouch(0).position;
                       // Debug.Log("Handle Touch");
                    }
                }
            }
            else if (Input.touchCount == 2)
                {

                    // Store both touches.
                    //touchZero = Input.GetTouch(0);
                    //touchOne = Input.GetTouch(1);

                    ////// Find the position in the previous frame of each touch.
                    //Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                    //Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                    //// Find the magnitude of the vector (the distance) between the touches in each frame.
                    //float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                    //float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

                    //// Find the difference in the distances between each frame.
                    //float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

                    //// Otherwise change the field of view based on the change in distance between the touches.
                    //GetComponent<Camera>().fieldOfView += deltaMagnitudeDiff * perspectiveZoomSpeed;

                    //// Clamp the field of view to make sure it's between 0 and 180.
                    //GetComponent<Camera>().fieldOfView = Mathf.Clamp(GetComponent<Camera>().fieldOfView, 0.1f, 179.9f);
                    //Debug.Log("punsh" + deltaMagnitudeDiff + "  " + touchZeroPrevPos + " " + touchOnePrevPos + " " + Input.touchCount);
                }

           // Debug.Log("begien");
            //if (!EventSystem.current.IsPointerOverGameObject())
            //{
            //    mouseDown = true;
            //    lastInputWorldPos = Input.GetTouch(0).position;
            //}
            // mouseD = true;
            // lastInputWorldPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);

        }
        else if (Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            
            if (mouseDown)
            {
                if (Input.touchCount == 1)
                {
                    if (isPunch == true)
                    {
                      //  deltaMagnitudeDiff = 0;
                        //Debug.Log("Touch" + Input.touchCount);
                        //if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
                        //{
                        //    if (IsPointerOverGameObject(Input.GetTouch(0).fingerId))
                        //    {
                        //        deltaInputWorldPos = Vector3.zero;
                        //        Debug.Log("Hit UI, Ignore Touch");
                        //    }
                        //    else
                        //    {
                        //        mouseDown = true;
                        //        lastInputWorldPos = Input.GetTouch(0).position;
                        //        Debug.Log("Handle Touch");
                        //    }
                        //}
                        ////deltaInputWorldPos = Vector3.zero;
                        ////lastInputWorldPos = Vector3.zero;
                        isPunch = false;
                    }
                    factor = 1;
                    // mouseDown = true;
                    // currentInputWorldPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
                    currentInputWorldPos = Input.GetTouch(0).position;
                    deltaInputWorldPos = new Vector3(currentInputWorldPos.x - lastInputWorldPos.x, currentInputWorldPos.y - lastInputWorldPos.y, 0.0f);
                    //    Debug.Log(deltaInputWorldPos);
                    //foreach (ListBox listbox in listBoxes)
                    //    listbox.updatePosition(deltaInputWorldPos);
                    // Debug.Log(currentInputWorldPos + "  " + deltaInputWorldPos);
                    lastInputWorldPos = currentInputWorldPos;
             //////       Debug.Log("touch " + deltaInputWorldPos + "  " + lastInputWorldPos);
                    if (deltaInputWorldPos.x > 5 || deltaInputWorldPos.y > 5)
                    {
                        isMoveTouch = true;
                        // Debug.Log("Moved");
                    } 
                }
                else if (Input.touchCount >= 2)
                {
                    isPunch = true;
                    factor = 0;
                    deltaInputWorldPos = Vector3.zero;
                    // Store both touches.
                    UnityEngine.Touch touchZero = Input.GetTouch(0);
                    UnityEngine.Touch touchOne = Input.GetTouch(1);

                    // Find the position in the previous frame of each touch.
                    Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                    Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                    // Find the magnitude of the vector (the distance) between the touches in each frame.
                    float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                    float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

                    // Find the difference in the distances between each frame.
                    float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
                    distance += deltaMagnitudeDiff * 0.5f;
                    distance = Mathf.Clamp(distance, 40f, 120f);
                   
                    // Otherwise change the field of view based on the change in distance between the touches.
                    //cam.fieldOfView += deltaMagnitudeDiff * perspectiveZoomSpeed;

                    // Clamp the field of view to make sure it's between 0 and 180.
                    //cam.fieldOfView = Mathf.Clamp(cam.fieldOfView, 0.1f, 179.9f);
         //////////           Debug.Log("punsh" + "  " + distance + " " + deltaMagnitudeDiff + "  " + touchZeroPrevPos + " " + touchOnePrevPos + " " + Input.touchCount);
                }
             
            }
            
        }
        else if (Input.GetTouch(0).phase == TouchPhase.Stationary)
        {
            if (Input.touchCount == 1)
            {
                // Debug.Log("Stationary");
                deltaInputWorldPos = Vector3.zero;
            }
        }
        else if (Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            mouseDown = false;
            //  setSlidingEffect();
            deltaInputWorldPos = Vector3.zero;
        }
    }
    else
    {
    }
    
}
void Start ()
{
    if (setObliq)
    {
        SetObliqueness(0.25f, 0);
    }
    Vector3 angles = transform.eulerAngles;
    xDeg = angles.x;
    yDeg = angles.y;

    xDeg = 0;
    yDeg = 0;
    targetX = xDeg;
    targetY = yDeg;
    currentDistance = distance;
    desiredDistance = distance;
    correctedDistance = distance;
    //target.transform.position = targetPos;
    //target.GetComponent<TargetController>().targetPos = targetPos;
    if (GetComponent<Rigidbody>())
        GetComponent<Rigidbody>().freezeRotation = true;

   // startMove();
   // start = true;
   // IsActive = true;
    //IsActive = true;
}

public void OnClickRight(int Index, List<Vector3> _cameraRotationAngle)
{
    
    // targetPos = Obj[Index].targetTransform.position;
    tempSpeed = 0;
    targetX = _cameraRotationAngle[Index].x;
    targetY = _cameraRotationAngle[Index].y;
    distance = _cameraRotationAngle[Index].z;// Distance[Index];
    TempX = xDeg;
    TempY = yDeg;
    TempD = currentDistance;// Distance[Index];
   // mouseDown = true;
    IsActive = true;
    
    startMove();

}
public void SetObliqueness(float horizObl, float vertObl)
{
    ObliqX=horizObl;
    ObliqY = vertObl;
    //if (tempObliqX != horizObl)
    //{
    
    ////Matrix4x4 mat = Camera.main.projectionMatrix;
    ////mat[0, 2] = horizObl;
    ////mat[1, 2] = vertObl;
    ////Camera.main.projectionMatrix = mat;
    //    //while (tempObliqX != horizObl)
    //    //{
            
    //        Matrix4x4 mat = gameObject.GetComponent<Camera>().projectionMatrix;
    //    //    if (tempObliqX < horizObl)
    //    //    {
    //    //        mat[0, 2] = horizObl + Time.deltaTime * 2f;
    //    //    }
    //    //    else
    //    //    {
    //    //        mat[0, 2] = horizObl - Time.deltaTime * 2f;
    //    //    }
    //        mat[0, 2] = horizObl;
    //        mat[1, 2] = vertObl;
    //        gameObject.GetComponent<Camera>().projectionMatrix = mat;
    //        if (guiCam != null)
    //        {
    //            guiCam.GetComponent<Camera>().projectionMatrix = mat;
    //        }
            
    //    //}
    //    tempObliqX = horizObl;
    //    tempObliqY = vertObl;

    //}
    
}
void Update()
{
    
   // Debug.Log((d * 10) / 100 + "   " + upDownMove+"  "+currentHeight);
    //else
    //{

    //}
        //if (!isONGUI)
   // if (kk)

    if (tempObliqX != ObliqX)
    {

        //Matrix4x4 mat = Camera.main.projectionMatrix;
        //mat[0, 2] = horizObl;
        //mat[1, 2] = vertObl;
        //Camera.main.projectionMatrix = mat;
        //while (tempObliqX != horizObl)
        //{

        Matrix4x4 mat = gameObject.GetComponent<Camera>().projectionMatrix;
        //    if (tempObliqX < horizObl)
        //    {
        //        mat[0, 2] = horizObl + Time.deltaTime * 2f;
        //    }
        //    else
        //    {
        //        mat[0, 2] = horizObl - Time.deltaTime * 2f;
        //    }
        
        mat[0, 2] =tempObliqX= Mathf.Lerp(tempObliqX, ObliqX,  0.3f);
        //Debug.Log(tempObliqX);
        mat[1, 2] = ObliqY;
        gameObject.GetComponent<Camera>().projectionMatrix = mat;
        if (guiCam != null)
        {
            guiCam.GetComponent<Camera>().projectionMatrix = mat;
        }

        //}


    }
    
    

        if (!isTouchingDevice)
            storeMousePosition();
        else
            storeFingerPosition();


        //if (!EventSystem.current.IsPointerOverGameObject())
        //{
        //    mouseDown = true;
        //    if (mouseDown)
        //    {
        //        mouseD = false;
        //    }
            

        //}
        //else
        //{
        //    mouseDown = false;
        //}



       
        if (IsActive)
        {


            //target.GetComponent<TargetController>().targetPos = targetPos;
            //target.GetComponent<TargetController>().incSpeed = targetSpeed;
            //Move the Player with left & right button press together
           // intHeight = 10;
            if (start)
            {
                if (xDeg != targetX || yDeg != targetY || distance != currentDistance )//|| targetPos != new Vector3(targetPos.x, 13, targetPos.z))
                {
                    // Debug.Log("KKK");
                    //finish = true;
                    finish = false;
                    halfFinish = false;
                    xDeg = NumberTo(xDeg, targetX, Speed);
                    yDeg = NumberTo(yDeg, targetY, Speed);
                    if (targetX <= 0)
                    {
                        float th = NumberTo(targetPos.y, 14f, Speed * 1.2f);
                        targetPos = new Vector3(targetPos.x, th, targetPos.z);
                    }
                    else
                    {
                        targetPos = new Vector3(targetPos.x, maxHeight, targetPos.z);
                    }
                   
                   // Debug.Log("shalf" + Mathf.Abs((targetX + (TempX - targetX)/ 2 ))+ " " + Mathf.Abs(xDeg));
                    
                    desiredDistance = NumberTo(desiredDistance, distance, Speed/2 );
                    SideRotation();
                    //if (Mathf.Abs(xDeg) >= Mathf.Abs((targetX + (TempX - targetX) / 2)))//&& Mathf.Abs(yDeg) >= Mathf.Abs(targetY / 2) && Mathf.Abs(desiredDistance) >= Mathf.Abs(currentDistance / 2))
                    //{
                    //    //Debug.Log("half");
                    //    halfFinish = true;
                    //}
                }
                else
                {
                    start = false;
                    finish = true;
                    IsActive = false;


                    //if (followerPiont)
                    //{
                    //    followerPiont.transform.position = gameObject.transform.position;
                    //}


                    //IsActive = false;
                }
            }

        }
        else
        {

            if (mouseDown)
            {

                if (Input.GetMouseButton(0))
                {
                    
                    //float xDeg_start = xDeg;
                    //float targetxDeg= xDeg + (deltaInputWorldPos.x * xSpeed * 0.02f);
                    xDeg += deltaInputWorldPos.x * xSpeed * 0.02f;
                    yDeg -= deltaInputWorldPos.y * ySpeed * 0.02f;
                   // xDeg += Mathf.Lerp((deltaInputWorldPos.x * xSpeed * 0.02f), 0, _lerpRate);
                    //_xRotation = Mathf.Lerp(_xRotation, 0, _lerpRate);
                    //_yYRotation = Mathf.Lerp(_yYRotation, 0, _lerpRate);
                   // xDeg += Mathf.Lerp((deltaInputWorldPos.x * xSpeed ), 0, _lerpRate); //Mathf.LerpAngle(xDeg_start, targetxDeg, 55f * Time.deltaTime);
                    //xDeg = Mathf.SmoothDampAngle(xDeg_start, targetxDeg, ref yVelocity, 0.01f);
                    //Debug.Log("PPPPPPP" + xDeg+"  " + xDeg_start + "  " + targetxDeg);
                    // Debug.Log("PPPPPPP" + xDeg + "  " + yDeg);
                }
              
            }
            if (isPunch || desiredDistance != distance)
            {
                if (isPunch)
                {
                    desiredDistance = distance;

                }
                else
                {
                    desiredDistance = NumberTo(desiredDistance, distance, (Speed ));
                }
               
            }
            SideRotation();
            //if (Input.GetMouseButton(1) && Input.GetMouseButton(0))
            //{
            //    Debug.Log("PsP");
            //    float targetRotationAngle = target.eulerAngles.y;
            //    float currentRotationAngle = transform.eulerAngles.y;

            //    xDeg = Mathf.LerpAngle(currentRotationAngle, targetRotationAngle, rotationDampening * Time.deltaTime);
            //    //target.transform.Rotate(0, Input.GetAxis("Mouse X") * xSpeed * 0.02f, 0);
            //    //xDeg += Input.GetAxis("Mouse X") * targetSpeed * 0.02f;
            //    target.transform.Rotate(0, Input.GetAxis("Mouse X") * xSpeed * 0.02f, 0);
            //   // xDeg += Input.GetAxis("Mouse X") * targetSpeed * 0.02f;
            //    // target.transform.Translate(Vector3.forward * targetSpeed * Time.deltaTime);
            //    xDeg += Input.GetAxis("Mouse X") * xSpeed * 0.02f;
            //    yDeg -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;
            //}
        }
       // 
    //else { 
    //    //kk = false; 
    //}
  //if(Input.GetMouseButton(1)&&Input.GetMouseButton(0))
  //{
      
  //float targetRotationAngle = target.eulerAngles.y;
  //float currentRotationAngle = transform.eulerAngles.y;
  //xDeg = Mathf.LerpAngle (currentRotationAngle, targetRotationAngle, rotationDampening *Time.deltaTime);             
  //target.transform.Rotate(0,Input.GetAxis ("Mouse X") * xSpeed  * 0.02f,0);
  //xDeg += Input.GetAxis ("Mouse X") * targetSpeed * 0.02f;
  //target.transform.Translate(Vector3.forward * targetSpeed * Time.deltaTime);
  //}
}
public void startMove()
{
    start = true;
    finish = false;
    tempSpeed = 0;
}
float NumberTo(float var, float to, float speed)
{
   
        if (var != to)
        
        {
            if (tempSpeed<=Speed)
            {
                tempSpeed += increaseRatio;
            }
           // Debug.Log(Mathf.Exp(speed * Time.deltaTime));
           // var = ExpEase.Out(var, to, - speed);
            var = Mathf.MoveTowards(var, to, (Mathf.Abs(to - var) * Time.deltaTime + 0.17f) * Mathf.Exp(tempSpeed * Time.deltaTime));
            //var = Mathf.MoveTowards(var, to, speed * Time.deltaTime);
        }
        //else
        //{

        //  //  finish = true;
        //}
   

    return var;
}
 
void LateUpdate ()
{

    
    //0nsform.rotation = rot;
    //if (!IsActive)
    //{
       // Debug.Log("PPPPPPP");
        //  Debug.Log(distance + " " + currentDistance+"  ");
        Vector3 vTargetOffset;

        // Don't do anything if target is not defined
        if (!target)
            return;
        
        //  // If either mouse buttons are down, let the mouse govern camera position
        //  if (Input.GetMouseButton(0))
        //  {
        //      xDeg += Input.GetAxis ("Mouse X") * xSpeed * 0.02f;
        //      yDeg -= Input.GetAxis ("Mouse Y") * ySpeed * 0.02f;
        //  }
        ////Reset the camera angle and Rotate the Target Around the World!
        //else if (Input.GetMouseButton(1))
        //{
        //float targetRotationAngle = target.eulerAngles.y;
        //float currentRotationAngle = transform.eulerAngles.y;
        //xDeg = Mathf.LerpAngle (currentRotationAngle, targetRotationAngle, rotationDampening * Time.deltaTime);    
        //target.transform.Rotate(0,Input.GetAxis ("Mouse X") * xSpeed * 0.02f,0);
        //xDeg += Input.GetAxis ("Mouse X") * xSpeed * 0.02f;
        //}


        //  // otherwise, ease behind the target if any of the directional keys are pressed
        //  else if (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0)
        //  {
        //      float targetRotationAngle = target.eulerAngles.y;
        //      float currentRotationAngle = transform.eulerAngles.y;
        //      xDeg = Mathf.LerpAngle (currentRotationAngle, targetRotationAngle, rotationDampening * Time.deltaTime);
        //  }
 //   Debug.Log("yDeg=" + yDeg);
        Quaternion rot;
        if (accBool && tarhget!=null)
        {
            //find the vector pointing from our position to the target
          //  Vector3 _direction = (new Vector3(targetPos.x, tarhget.position.y, targetPos.z) - tarhget.position).normalized;
            Vector3 _direction = (new Vector3(targetPos.x, tarhget.position.y, targetPos.z) - tarhget.position).normalized;
            //create the rotation we need to be in to look at the target
            Quaternion _lookRotation = Quaternion.LookRotation(_direction);
            
             //    float angle = Vector3.RotateTowards(relativePos, Vector3.forward);
           // Debug.Log(_lookRotation);
            if (new Quaternion(Mathf.Abs(transform.rotation.x), Mathf.Abs(transform.rotation.y), Mathf.Abs(transform.rotation.z),Mathf.Abs( transform.rotation.w)) != new Quaternion(Mathf.Abs(_lookRotation.x), Mathf.Abs(_lookRotation.y), Mathf.Abs(_lookRotation.z), Mathf.Abs(_lookRotation.w)))
            {
                
                transform.rotation = Quaternion.Lerp(transform.rotation, _lookRotation, Time.deltaTime * 2f);
               // Debug.Log("kodfkod" + transform.rotation + "  " + _lookRotation);
                if (tarhget.position.y < maxHeight)
                {
                    targetPos = Vector3.Lerp(targetPos, new Vector3(targetPos.x, tarhget.position.y, targetPos.z), Time.deltaTime * 2f);
                }
                else
                {
                    targetPos = Vector3.Lerp(targetPos, new Vector3(targetPos.x, maxHeight, targetPos.z), Time.deltaTime * 2f);
                }
                distance = deafultAccDis;
                transform.position = targetPos - (transform.rotation * Vector3.forward * desiredDistance);//* NumberTo(desiredDistance, deafultAccDis, Speed)
               // SideRotation();
            }
            else
            {
             //   Debug.Log("xxx" + transform.rotation + "  " + _lookRotation);
                //transform.localEulerAngles.x
             //   desiredDistance=currentDistance = distance = deafultAccDis;
                xDeg = transform.localEulerAngles.y;
                yDeg = transform.localEulerAngles.x;
                accBool = false;
            }
        //    Debug.Log(transform.localEulerAngles);
        }
        else
        {
            if (yDeg >= yMinLimit && upDownMove == false)
            {
                yDeg = ClampAngle(yDeg, yMinLimit, yMaxLimit);
            }
            else
            {
                d = (desiredDistance * 100) / (maxDistance - minDistance);
                //Debug.Log(d);
                currentHeight = targetPos.y + (0.1f * yDeg);
                if (intHeight >= currentHeight && currentHeight >= (d * 10) / 100) //ratio of distance
                {


                    if (yDeg < yMinLimit)
                    {

                        targetPos = new Vector3(targetPos.x, currentHeight, targetPos.z);
                        // yDeg = yMinLimit;
                        yDeg = 0;
                        upDownMove = true;
                    }
                    else if (yDeg < yMaxLimit)
                    {
                        targetPos = new Vector3(targetPos.x, currentHeight, targetPos.z);
                        upDownMove = true;
                        yDeg = 0;

                    }



                }
                else
                {
                    if (currentHeight < (d * 10) / 100)
                    {
                        targetPos = new Vector3(targetPos.x, (d * 10) / 100, targetPos.z);
                        yDeg = 0;
                    }
                    else
                    {
                        if (yDeg > yMinLimit)
                        {
                            upDownMove = false;
                        }
                        else
                        {
                            yDeg = 0;
                        }
                    }

                }



            }



            // set camera rotation
            if (xDeg < -360)
                xDeg += 360;
            if (xDeg > 360)
                xDeg -= 360;


            rot = Quaternion.Euler(yDeg, xDeg, 0);
            //  Quaternion rotation = Quaternion.Euler(yDeg, xDeg, 0);
            //rotation.eulerAngles = new Vector3(yDeg, xDeg, 0);
            // calculate the desired distance
            ////// desiredDistance -= Input.GetAxis ("Mouse ScrollWheel") * Time.deltaTime * zoomRate * Mathf.Abs (desiredDistance);
            desiredDistance = Mathf.Clamp(desiredDistance, minDistance, maxDistance);
            correctedDistance = desiredDistance;

            // calculate desired camera position
            vTargetOffset = new Vector3(0, -targetHeight, 0);
            Vector3 position = target.position - (rot * Vector3.forward * desiredDistance + vTargetOffset);
           
            // check for collision using the true target's desired registration point as set by user using height
            //RaycastHit collisionHit;
            //        Vector3 trueTargetPosition = new Vector3(target.position.x, target.position.y + targetHeight, target.position.z);
            Vector3 trueTargetPosition = new Vector3(target.position.x, target.position.y, target.position.z);
          //  Debug.DrawRay(trueTargetPosition, position - trueTargetPosition);
            // if there was a collision, correct the camera position and calculate the corrected distance
            //  bool isCorrected = false;
            //if (Physics.Linecast (trueTargetPosition, position, out collisionHit, collisionLayers.value))
            //{
            //    // calculate the distance from the original estimated position to the collision location,
            //    // subtracting out a safety "offset" distance from the object we hit.  The offset will help
            //    // keep the camera from being right on top of the surface we hit, which usually shows up as
            //    // the surface geometry getting partially clipped by the camera's front clipping plane.
            //    correctedDistance = Vector3.Distance (trueTargetPosition, collisionHit.point) - offsetFromWall;
            //    isCorrected = true;
            //}

            // For smoothing, lerp distance only if either distance wasn't corrected, or correctedDistance is more than currentDistance
            //currentDistance = !isCorrected || correctedDistance > currentDistance ? Mathf.Lerp (currentDistance, correctedDistance, Time.deltaTime * zoomDampening) : correctedDistance;
            // currentDistance = correctedDistance > currentDistance ? Mathf.Lerp(currentDistance, correctedDistance, Time.deltaTime * zoomDampening) : correctedDistance;
            currentDistance = desiredDistance;
            // keep within legal limits
            currentDistance = Mathf.Clamp(currentDistance, minDistance, maxDistance);

            // recalculate position based on the new currentDistance
            //position = trueTargetPosition - (rotation * Vector3.forward * currentDistance);// + vTargetOffset);
            position = targetPos - (rot * Vector3.forward * currentDistance);// + vTargetOffset);
            //transform.rotation = Quaternion.Slerp(transform.rotation, rot, .05f); 
            transform.rotation = rot;
           // transform.position = Vector3.Slerp(transform.position, position, 0.8f);
            transform.position = position;
        }
       
    //}

}
 
float ClampAngle (float angle, float min, float max)
{
    if (angle < -360)
        angle += 360;
    if (angle > 360)
        angle -= 360;

    
    return Mathf.Clamp (angle, min, max);
}


public void startOne(float Tx,float Ty,float dis)
{
    //targetX = Tx;
    //targetY = Ty;

    //desiredDistance = dis;
    //startMove();
    //IsActive = true;

}
public void SideRotation()
{
    float range = 0;

    //if (cam.transform.eulerAngles.y != tempSide)
    //{

    if (distance <55 && distance >= 40)
    {
        //Debug.Log((distance - 50) * 10);
        range=10-((distance - 40) * 10)/15;
        if (cam.transform.eulerAngles.y < 270 && cam.transform.eulerAngles.y > 90)
        {
            // Debug.Log("R" + cam.transform.eulerAngles.y + " " + (cam.transform.eulerAngles.y - 90));

            if ((cam.transform.eulerAngles.y < 180 && cam.transform.eulerAngles.y >= 90))
            {
                //  Debug.Log("1R" + cam.transform.eulerAngles.y + " " + (180-cam.transform.eulerAngles.y ));
                targetPos = new Vector3((-((180 - cam.transform.eulerAngles.y) * range) / 90), targetPos.y, targetPos.z);
            }
            else
            {
                //  Debug.Log("2R" + cam.transform.eulerAngles.y + " " + ( (cam.transform.eulerAngles.y - 180)));
                targetPos = new Vector3(((((cam.transform.eulerAngles.y - 180)) * range) / 90), targetPos.y, targetPos.z);
            }


        }

        else if ((cam.transform.eulerAngles.y < 90 && cam.transform.eulerAngles.y >= 0) || (cam.transform.eulerAngles.y > 270 && cam.transform.eulerAngles.y < 360))
        {
            if ((cam.transform.eulerAngles.y < 90 && cam.transform.eulerAngles.y >= 0))
            {
                //Debug.Log("1L" + cam.transform.eulerAngles.y + " " + (cam.transform.eulerAngles.y - 0));
                targetPos = new Vector3((-(cam.transform.eulerAngles.y * range) / 90), targetPos.y, targetPos.z);
            }
            else
            {
                //Debug.Log("2L" + cam.transform.eulerAngles.y + " " + (90-(cam.transform.eulerAngles.y - 270) ));
                targetPos = new Vector3((((90 - (cam.transform.eulerAngles.y - 270)) * range) / 90), targetPos.y, targetPos.z);
            }







        }
    }
    else if (distance < 40)
    {
        range= 10;
        if (cam.transform.eulerAngles.y < 270 && cam.transform.eulerAngles.y > 90)
        {
            // Debug.Log("R" + cam.transform.eulerAngles.y + " " + (cam.transform.eulerAngles.y - 90));

            if ((cam.transform.eulerAngles.y < 180 && cam.transform.eulerAngles.y >= 90))
            {
                //  Debug.Log("1R" + cam.transform.eulerAngles.y + " " + (180-cam.transform.eulerAngles.y ));
                targetPos = new Vector3((-((180 - cam.transform.eulerAngles.y) * range) / 90), targetPos.y, targetPos.z);
            }
            else
            {
                //  Debug.Log("2R" + cam.transform.eulerAngles.y + " " + ( (cam.transform.eulerAngles.y - 180)));
                targetPos = new Vector3(((((cam.transform.eulerAngles.y - 180)) * range) / 90), targetPos.y, targetPos.z);
            }


        }

        else if ((cam.transform.eulerAngles.y < 90 && cam.transform.eulerAngles.y >= 0) || (cam.transform.eulerAngles.y > 270 && cam.transform.eulerAngles.y < 360))
        {
            if ((cam.transform.eulerAngles.y < 90 && cam.transform.eulerAngles.y >= 0))
            {
                //Debug.Log("1L" + cam.transform.eulerAngles.y + " " + (cam.transform.eulerAngles.y - 0));
                targetPos = new Vector3((-(cam.transform.eulerAngles.y * range) / 90), targetPos.y, targetPos.z);
            }
            else
            {
                //Debug.Log("2L" + cam.transform.eulerAngles.y + " " + (90-(cam.transform.eulerAngles.y - 270) ));
                targetPos = new Vector3((((90 - (cam.transform.eulerAngles.y - 270)) * range) / 90), targetPos.y, targetPos.z);
            }







        }
    }
    else
    {
        targetPos = new Vector3(0, targetPos.y, targetPos.z);
    }
    
    
    //tempSide = cam.transform.eulerAngles.y;
    //}
}



//public void OnPointerClick(PointerEventData eventData)
//{
//    kk = true;
//  //  throw new System.NotImplementedException();
//}

//void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
//{
//    throw new System.NotImplementedException();
//}
}


//    //get the rotationsigns

    

//    var forward = transform.TransformDirection(Vector3.up);

//    var forward2 = target.transform.TransformDirection(Vector3.up);

//     if (Vector3.Dot(forward,forward2) < 0)

//            xsign = -1; 

//            else

//            xsign =1;

    

    

//    for (var touch : Touch in Input.touches) {

//    if (touch.phase == TouchPhase.Moved) {

//        x += xsign * touch.deltaPosition.x * xSpeed *0.02;

//        y -= touch.deltaPosition.y * ySpeed *0.02;

        

        

//          y = ClampAngle (y, yMinLimit, yMaxLimit);      

//        var rotation = Quaternion.Euler(y, x, 0);

//        var position = rotation * Vector3(0.0, 0.0, -distance) + target.position;

        

//        transform.rotation = rotation;

//        transform.position = position;

//    }

//    }

//}
//function ClampAngle (angle :float,  min:float,  max:float)
//{
//    if (angle < -360)
//        angle += 360;
//    if (angle > 360)
//        angle -= 360;
//    return Mathf.Clamp (angle, min, max);
//}