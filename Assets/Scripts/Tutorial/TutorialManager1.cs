﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class TutorialSenario
{
    public int senarioID;
    public GameObject senarioObj;
    public GameObject[] tutorialPages;
    public bool[] isCompleted;

}
public class TutorialManager1 : MonoBehaviour {
    public TutorialSenario[] senarios;
    public int currentSenario, currentTutPage;
	// Use this for initialization
	void Start () {
        deactivateSenario(0);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void StartSenario(int senarioIndex, int PageIndex)
    {
        for (int i = 0; i < senarios.Length; i++)
        {
            if (i == senarioIndex)
            {
                senarios[i].senarioObj.SetActive(true);
                if (senarios[i].isCompleted[PageIndex] == false)
                {
                    senarios[i].tutorialPages[PageIndex].SetActive(true);
                    
                }

                currentSenario = i;
                currentTutPage = PageIndex;
                
            }
            else
            {
                if (senarios[i].senarioObj !=null)
                {
                    senarios[i].senarioObj.SetActive(false);
                }
                
            }
            
        }
    }
    public void deactivatePage(int senarioIndex, int PageIndex)
    {
        if (senarios[senarioIndex].isCompleted[PageIndex] == false)
        {

            senarios[senarioIndex].tutorialPages[PageIndex].SetActive(false);
            senarios[senarioIndex].isCompleted[PageIndex] = true;
        }
    }
    public void RevertTutPage(int senarioIndex, int PageIndex)
    {
        if (senarios[senarioIndex].isCompleted[PageIndex] ==false)
        {
            senarios[senarioIndex].tutorialPages[PageIndex].SetActive(false);

        }
        currentSenario = senarioIndex;
        currentTutPage = PageIndex-1;
       
       // senarios[senarioIndex].isCompleted[PageIndex] = false;
    }
    public void NextTutPage(int senarioIndex, int PageIndex)
    {
        deactivatePage(senarioIndex, PageIndex);
        if (PageIndex + 1 <= senarios[senarioIndex].tutorialPages.Length && senarios[senarioIndex].isCompleted[PageIndex+1] == false)
        {
            StartSenario(senarioIndex, PageIndex + 1);
        }
        else
        {
            currentTutPage = PageIndex + 1;
        }
        
        
        //if (senarios[senarioIndex].isCompleted[PageIndex+1] == false)
        //{
        //    senarios[senarioIndex].tutorialPages[PageIndex+1].SetActive(true);
        //    currentSenario = senarioIndex;
        //    currentTutPage = PageIndex+1;
        //}

        // senarios[senarioIndex].isCompleted[PageIndex] = false;
    }
    public void deactivateSenario(int index)
    {
        for (int i = 0; i < senarios[index].tutorialPages.Length; i++)
        {
            senarios[index].tutorialPages[i].SetActive(false);
        }
        if (senarios[index].senarioObj != null)
        {
            senarios[index].senarioObj.SetActive(false);
        }
        senarios[index].isCompleted = new bool[senarios[index].tutorialPages.Length];
    }
}
