﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialButton : MonoBehaviour {

    public TutorialManager1 tutorialManager;
    
    public bool  isActivateTutorial;
    public int activateSenarioNum;
    public int ActTutorialPageNum;
    public bool isDeactivateTutorial;
    public int deactivateSenarioNum;
    public int  DeactTutorialPageNum;
    public bool isBack, isNext;
	// Use this for initialization
	void Start () {
        tutorialManager = GameObject.FindObjectOfType<TutorialManager1>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void ClickButton()
    {
        if (tutorialManager != null)
        {
            if (isActivateTutorial)
            {
                tutorialManager.StartSenario(activateSenarioNum, ActTutorialPageNum);
            }
            if (isDeactivateTutorial)
            {
                tutorialManager.deactivatePage(deactivateSenarioNum, DeactTutorialPageNum);
            }
            if (isBack)
            {
                tutorialManager.RevertTutPage(tutorialManager.currentSenario, tutorialManager.currentTutPage);
            }
            if (isNext)
            {
                tutorialManager.NextTutPage(tutorialManager.currentSenario, tutorialManager.currentTutPage);
            }
        }
        
    }
   

}
