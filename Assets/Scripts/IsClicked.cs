﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class IsClicked :MonoBehaviour, IPointerDownHandler, IPointerUpHandler {
    public bool mouseDown;
    public void OnPointerDown(PointerEventData ped)
    {
        mouseDown = true;
        //startPos = transform.position;
        //startMousePos = Input.mousePosition;

    }

    public void OnPointerUp(PointerEventData ped)
    {
        mouseDown = false;
    }
	
}
