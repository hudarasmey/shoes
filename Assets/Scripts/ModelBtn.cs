﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ModelBtn : MonoBehaviour {
    public Image buttonImage;
    public ScrollListTest Lst;
    
	// Use this for initialization
	void OnEnable () {
      //  Debug.Log(Lst.selectedIndex);
        setBtnSprite(Lst, buttonImage);
	}
	
	// Update is called once per frame
    void setBtnSprite(ScrollListTest lst, Image image)
    {
        if (lst.selectedIndex >= 0)
        {

            image.sprite = lst.Bank.spritesArr[lst.selectedIndex];

        }
        else
        {
            image.sprite = lst.Bank.spritesArr[0];
        }
    }
}
