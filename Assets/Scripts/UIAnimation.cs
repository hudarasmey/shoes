﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UIAnimation : MonoBehaviour {

    public bool  AutoStart, Started, active;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
public enum TransformType { Postion, Scale, Rotation, Color, Fill }

[Serializable]
public class TransformElement
{
    public TransformType Type;
    public AnimationType AnimationType;
    public Vector4 StartValue;
    public Vector4 FinishValue;
    public float StartAfter;
    public float Duration;
    public bool Reverse;
    public float ReverseAfter;
    public bool Finished;
}

