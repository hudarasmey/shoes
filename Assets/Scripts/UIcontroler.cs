﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIcontroler : MonoBehaviour {
    public ListPositionCtrl mainSlider;
    public ListPositionCtrl FaceSubSlider;
    public ListPositionCtrl BaseSubSlider;
    public ListPositionCtrl HealSubSlider;
    public ColorPickerTester colorPicker;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        switch (mainSlider.CenteredObjName)
        {       

            case "Face":
                
          
                if (!FaceSubSlider.gameObject.activeSelf)
	            {
                    Debug.Log("face");
                    FaceSubSlider.gameObject.SetActive(true);
	            }
                if (BaseSubSlider.gameObject.activeSelf)
                {
                    BaseSubSlider.gameObject.SetActive(false);
                }
                if (HealSubSlider.gameObject.activeSelf)
                {
                    HealSubSlider.gameObject.SetActive(false);
                }
                FaceSubSliderSubMenuFn();
                break;
            case "Base":
                
                if (FaceSubSlider.gameObject.activeSelf)
	            {
                    FaceSubSlider.gameObject.SetActive(false);
	            }
                if (!BaseSubSlider.gameObject.activeSelf)
                {
                    Debug.Log("base");
                    BaseSubSlider.gameObject.SetActive(true);
                }
                if (HealSubSlider.gameObject.activeSelf)
                {
                    HealSubSlider.gameObject.SetActive(false);
                }
                break;
            case "Heal":
                
                if (FaceSubSlider.gameObject.activeSelf)
	            {
                    FaceSubSlider.gameObject.SetActive(false);
	            }
                if (BaseSubSlider.gameObject.activeSelf)
                {
                    BaseSubSlider.gameObject.SetActive(false);
                }
                if (!HealSubSlider.gameObject.activeSelf)
                {
                    Debug.Log("heal");
                    HealSubSlider.gameObject.SetActive(true);
                }
                break;
            default:
                break;
        }
	}
    void FaceSubSliderSubMenuFn()
    {
        Debug.Log(FaceSubSlider.CenteredObjName);
        switch (FaceSubSlider.CenteredObjName)
        {

            case "Morph":


                //if (!FaceSubSlider.gameObject.activeSelf)
                //{
                //    Debug.Log("face");
                //    FaceSubSlider.gameObject.SetActive(true);
                //}
                if (colorPicker.gameObject.activeSelf)
                {
                    colorPicker.gameObject.SetActive(false);
                }
                //if (HealSubSlider.gameObject.activeSelf)
                //{
                //    HealSubSlider.gameObject.SetActive(false);
                //}

                break;
            case "Color":

                //if (FaceSubSlider.gameObject.activeSelf)
                //{
                //    FaceSubSlider.gameObject.SetActive(false);
                //}
                if (!colorPicker.gameObject.activeSelf)
                {
                    Debug.Log("colorPicker");
                    colorPicker.gameObject.SetActive(true);
                }
                //if (HealSubSlider.gameObject.activeSelf)
                //{
                //    HealSubSlider.gameObject.SetActive(false);
                //}
                break;
            case "Material":

                //if (FaceSubSlider.gameObject.activeSelf)
                //{
                //    FaceSubSlider.gameObject.SetActive(false);
                //}
                if (colorPicker.gameObject.activeSelf)
                {
                    colorPicker.gameObject.SetActive(false);
                }
                //if (!HealSubSlider.gameObject.activeSelf)
                //{
                //    Debug.Log("heal");
                //    HealSubSlider.gameObject.SetActive(true);
                //}
                break;
            default:
                break;
        }
    }
}
