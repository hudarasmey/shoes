﻿using UnityEngine;
using System.Collections;
[SerializeField]
public class MaterialParam {

  public Texture _MainTex, _BumpMap, _OcclusionMap, _DetailNormalMap;

  public Color _Color = Color.white;

  public float _BumpScale = 1, _OcclusionStrength = 1, _Metallic, _Glossiness;//,_TileValue=1;

  public Vector2 _TileValue, _TileBumpValue;
	
    public void GetMatParams(Material _material)
    {
        _MainTex=_material.GetTexture("_MainTex");
        _BumpMap = _material.GetTexture("_BumpMap");
        //_BumpScale = _material.GetFloat("_BumpScale");
        _OcclusionMap = _material.GetTexture("_OcclusionMap");


        //mat.SetTexture("_DetailAlbedoMap", _DetailAlbedoMap);
        //mat.SetTexture("_DetailNormalMap", _DetailNormalMap);
        //mat.SetFloat("_DetailNormalMapScale", _DetailNormalMapScale);


        _Metallic = _material.GetFloat("_Metallic");
        _Glossiness = _material.GetFloat("_Glossiness");
        _OcclusionStrength = _material.GetFloat("_OcclusionStrength");
        _Color = _material.GetColor("_Color");
        _TileValue = _material.GetTextureScale("_MainTex");

    }
    public void SetMatParams(Material _material)
    {
        _material.SetTextureScale("_MainTex", new Vector2(_TileValue.x, _TileValue.y));
        _material.SetTextureScale("_BumpMap", new Vector2(_TileBumpValue.x, _TileBumpValue.y));
        _material.SetTextureScale("_OcclusionMap", new Vector2(_TileValue.x, _TileValue.y));
        _material.SetColor("_Color", _Color);
        _material.SetTexture("_MainTex", _MainTex);
        _material.SetTexture("_BumpMap", _BumpMap);
        //_material.SetFloat("_BumpScale", _BumpScale);
        _material.SetTexture("_OcclusionMap", _OcclusionMap);
        _material.SetTexture("_DetailNormalMap", _DetailNormalMap);


        //mat.SetTexture("_DetailAlbedoMap", _DetailAlbedoMap);
        //mat.SetTexture("_DetailNormalMap", _DetailNormalMap);
        //mat.SetFloat("_DetailNormalMapScale", _DetailNormalMapScale);


        _material.SetFloat("_Metallic", _Metallic);
        _material.SetFloat("_Glossiness", _Glossiness);
        _material.SetFloat("_OcclusionStrength", _OcclusionStrength);
        
     //   _material.SetColor("_Color", _Color);
    }
    public void MaterialParamter(Material _material, Vector4 Param)
    {

        //_material.SetFloat("_Metallic", Param.x);
        //_material.SetFloat("_Glossiness", Param.y);
        //_material.SetFloat("_OcclusionStrength", Param.z);
        _Metallic= Param.x;
        _Glossiness= Param.y;
        _OcclusionStrength= Param.z;

    }
    ////////////

    public void ChangeTexture(Texture mainTex, Texture bumpMap, Texture occlusionMap,Texture detailNormalMap, Color color,Vector2 tileValue)
    {

        if (mainTex != null)
        {
            _MainTex = mainTex;
        }
       
     //   _MainTex = mainTex;
        _BumpMap=bumpMap;
        _OcclusionMap = occlusionMap;
        _DetailNormalMap = detailNormalMap;
        _Color = color;
        _TileValue = tileValue;

    }
    public void SetColor(Color color)
    {
        _Color = color;
    }
    public void setTileValue(Vector2 value)
    {
        _TileValue = value;
    }

    public void SaveMatParams(string Name,Material _material)
    {
        
        _MainTex=_material.GetTexture("_MainTex");
        _BumpMap = _material.GetTexture("_BumpMap");
        _BumpScale = _material.GetFloat("_BumpScale");
        _OcclusionMap = _material.GetTexture("_OcclusionMap");


        //mat.SetTexture("_DetailAlbedoMap", _DetailAlbedoMap);
        //mat.SetTexture("_DetailNormalMap", _DetailNormalMap);
        //mat.SetFloat("_DetailNormalMapScale", _DetailNormalMapScale);


        _Metallic = _material.GetFloat("_Metallic");
        _Glossiness = _material.GetFloat("_Glossiness");
        _OcclusionStrength = _material.GetFloat("_OcclusionStrength");
        _Color = _material.GetColor("_Color");
        _TileValue = _material.GetTextureScale("_MainTex");
    }
    
}
// mat.DisableKeyword("_MainTex");
//mat.EnableKeyword("_MainTex");
