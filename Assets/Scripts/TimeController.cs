﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using CodeStage.AntiCheat.ObscuredTypes;
using TMPro;

public class TimeController : MonoBehaviour
{
    
   // public Text Timer;
    public TextMeshProUGUI Timer;
    public Button ChestBtn;

    public int minutesToWait;
    public DateTime currentDate;
    public DateTime endOfTimer;
    TimeSpan difference;
    public GameObject rewardWindow,timerCollection,buttonGroup;
    public ShopMan shop;
    public int plusCoins;
    public TextMeshProUGUI coins, pluscoins;


    long temp;
    GameObject SoundManagereObj;
    SoundManagerClass SoundManScript;
    void Awake()
    {
        SoundManagereObj = GameObject.FindGameObjectWithTag("SoundManager");
        SoundManScript = (SoundManagerClass)SoundManagereObj.GetComponent("SoundManagerClass");
    }
    void Start()
    {
        currentDate = System.DateTime.Now;

        if (ObscuredPrefs.GetString("LastTime", "-1") == "-1")
        {
            print("ssds");

            SaveTime();
        }
        else
        {
            getTime();

        }
        //Grab the old time from the player prefs as a long
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            SaveTime();
        }
        //Store the current time when it starts
        currentDate = System.DateTime.Now;

        //Use the Subtract method and store the result as a timespan variable
        difference = endOfTimer.Subtract(currentDate);

        //DateTime Timer = DateTime.FromBinary(15*60).Subtract(difference);

        if (difference.TotalMinutes >= 0)
        {
            Timer.text = string.Format("{0:D2}:{1:D2}:{2:D2}", difference.Hours, difference.Minutes, difference.Seconds); //difference.Hours + ":" + difference.Minutes + ":" + difference.Seconds;
            if (ChestBtn.interactable == true)
            {
                
                ChestBtn.GetComponent<UIAnim>().enabled = false;
                ChestBtn.transform.localScale = Vector3.one;
                ChestBtn.transform.localPosition = new Vector3(-26.607f, 15, 0);
                ChestBtn.interactable = false;
                timerCollection.SetActive(true);
            }
        }
        else
        {
            Timer.text = "00:00:00";
            if (ChestBtn.interactable == false)
            {
                ChestBtn.GetComponent<UIAnim>().enabled = true;
                ChestBtn.interactable = true;
                
            }
            timerCollection.SetActive(false);
        }
        //print("Difference: " + difference.Hours + ":" + difference.Minutes + ":" + difference.Seconds + " " + difference.TotalMinutes);

        if (difference.TotalMinutes < 0)
        {
            //    Debug.Log("kolo");
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            //   OnApplicationQuit();
            ResetTimer();
        }
    }
    void CheckTime()
    {
        //Store the current time when it starts
        currentDate = System.DateTime.Now;
        //Convert the old time from binary to a DataTime variable
        DateTime oldDate = DateTime.FromBinary(temp);
        print("oldDate: " + oldDate);
        //Use the Subtract method and store the result as a timespan variable
        difference = currentDate.Subtract(endOfTimer);

        print("Difference: " + difference + "  " + difference.TotalMinutes);

        if (difference.TotalMinutes > 15)
        {
            Debug.Log("kolo");
        }
    }
    void getTime()
    {
        temp = Convert.ToInt64(ObscuredPrefs.GetString("LastTime"));
        endOfTimer = DateTime.FromBinary(temp).AddMinutes(minutesToWait);

    }
    void SaveTime()
    {
        ObscuredPrefs.SetString("LastTime", System.DateTime.Now.ToBinary().ToString());
    }
    void ResetTimer()
    {
        //Savee the current system time as a string in the player prefs class
        ObscuredPrefs.SetString("LastTime", System.DateTime.Now.ToBinary().ToString());
        getTime();
    }
    //void OnApplicationQuit()
    //{
    //    //Savee the current system time as a string in the player prefs class
    //    ObscuredPrefs.SetString("LastTime", System.DateTime.Now.ToBinary().ToString());

    //    //print("Saving this date to prefs: " + System.DateTime.Now);
    //}
    public void BtnClick()
    {
        SoundManScript.PlaySound(SoundManScript.present_Clip);
        ResetTimer();
        StartCoroutine(rewardChest());
        //Add present to player
    }
    IEnumerator rewardChest()
    {
        Debug.Log("reward");
        buttonGroup.SetActive(false);
        coins.text = GameManager.Instance.Currency.ToString();
        rewardWindow.SetActive(true);
       // yield return new WaitForSeconds(0.1f);
        int randCoins= UnityEngine.Random.Range(10, 50);
        //plusCoins =UnityEngine.Random.Range(10,50);
        int start = 0;
        float duration = 0.7f;
        for (float timer = 0; timer < duration; timer += Time.deltaTime)
        {
            float progress = timer / duration;
            plusCoins = (int)Mathf.Lerp(start, randCoins, progress);
            pluscoins.text = "+" + plusCoins.ToString();
            yield return null;
        }
      //  score = target;

        
        yield return new WaitForSeconds(0.5f);
        int coinLast = GameManager.Instance.Currency;
        coins.text = GameManager.Instance.Currency.ToString();
        yield return new WaitForSeconds(0.5f);
        start = GameManager.Instance.Currency;
        int target = randCoins + start;
        shop.AddCoinsx(plusCoins);
        for (float timer = 0; timer < duration; timer += Time.deltaTime)
        {
            float progress = timer / duration;
            
            plusCoins = (int)Mathf.Lerp(randCoins, 0, progress);
            pluscoins.text = "+"+plusCoins.ToString();
            coinLast = (int)Mathf.Lerp(start, target, progress);
            coins.text = coinLast.ToString();
            yield return null;
        }
        pluscoins.text = "+" + "0";

        yield return new WaitForSeconds(0.1f);
        coins.text = GameManager.Instance.Currency.ToString();
        yield return new WaitForSeconds(1);
        rewardWindow.SetActive(false);
        buttonGroup.SetActive(true);
    }

}
