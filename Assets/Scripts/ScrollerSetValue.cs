﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollerSetValue : MonoBehaviour {
    public float value;
    public Slider tragetScroller, reftarget, refreshtarget, refreshtarget2;
	// Use this for initialization
    public void setValueFor()
    {
        if (reftarget != null)
        {
            tragetScroller.value = reftarget.value;
        }
        else
        {
            tragetScroller.value = value;
            if (refreshtarget != null)
            {
                refreshtarget.value = refreshtarget.value-0.001f;
                refreshtarget2.value = refreshtarget2.value + 0.001f;
            }
        }
    }

}
