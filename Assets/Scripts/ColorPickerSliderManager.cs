﻿using UnityEngine;
using System.Collections;

public class ColorPickerSliderManager : MonoBehaviour
{
    public ScrollListTest colorSlider;
    public ColorPicker colorPicker;
    public InfoBank Bank;
    public GameObject saveBtn, customBtn, buttonCollection, backBtn,effectSlider,tileSlider;
    public Color _color, tempColor;
    public bool InCustomMode, isOpenWindow;
    public ShopMan shop;
    public int colorPrice = 20;
    public GameObject TextlistSilder;

    GameObject SoundManagereObj;
    SoundManagerClass SoundManScript;
    void Awake()
    {
        //colorSlider.selectedIndex = 0;
        //colorSlider.tempIndex = 0;
        Bank = GameObject.FindGameObjectWithTag("ColorInfoBank").GetComponent<InfoBank>();
        SoundManagereObj = GameObject.FindGameObjectWithTag("SoundManager");
        SoundManScript = (SoundManagerClass)SoundManagereObj.GetComponent("SoundManagerClass");
    }
    // Use this for initialization
    void Start()
    {
        shop = GameObject.FindGameObjectWithTag("UnlockWin").GetComponent<ShopMan>();
        if (PlayerPrefsX.GetColorArray("colorArr").Length == 0)
        {
            PlayerPrefsX.SetColorArray("colorArr", Bank.ColorsBank);
        }
        else if (PlayerPrefsX.GetColorArray("colorArr").Length != Bank.ColorsBank.Length)
        {
            Bank.ColorsBank = PlayerPrefsX.GetColorArray("colorArr");
            colorSlider.IsInst = true;

            // colorSlider.cells[0].clickit();
            //colorSlider.changeEvent();
            //colorSlider.isUpdate = true;
        }

        tempColor = _color;// Color.white;.
        //if (colorSlider.IsVerticalList)
        //{
        //    colorSlider.myScrollRect.verticalNormalizedPosition = 1f;
        //}
        //else
        //{
        //    colorSlider.myScrollRect.horizontalNormalizedPosition = 0f;
        //}
    }
    public void getColorMaterial(GameObject obj, int materialIndex)
    {
        for (int i = 0; i < Bank.ColorsBank.Length; i++)
        {

            if (Bank.ColorsBank[i] == obj.GetComponent<Renderer>().materials[materialIndex].color)
            {
                //Debug.Log(Bank.ColorsBank[i] + "  " + obj.GetComponent<Renderer>().materials[materialIndex].color);

                _color = Bank.ColorsBank[i];//.GetComponent<Renderer>().materials[materialIndex].color;
                //    Debug.Log("OK" + "  " + _color);
            }
        }

        //startColor = obj.GetComponent<Renderer>().materials[materialIndex].color;
    }
    //public void getColorMeshMaterial(GameObject obj)
    //{
    //    Debug.Log(obj.name);
    //    startColor = obj.GetComponent<MeshRenderer>().sharedMaterial.color;
    //}
    // Update is called once per frame
    void Update()
    {
        // Debug.Log("olorSlider" + colorSlider.IsAction); 
        //if ((tempColor.r != colorPicker.CurrentColor.r || tempColor.g != colorPicker.CurrentColor.g || tempColor.b != colorPicker.CurrentColor.b))
        //{

        /////////////////////////////         Debug.Log("OO" + tempColor + "  " + colorPicker.CurrentColor);
        if (InCustomMode)
        {
            tempColor = colorPicker.CurrentColor;

        }
        else
        {
            // Debug.Log("eorSlider" + colorSlider.GetComponent<ScrollListTest>().selectedIndex + "  " + colorSlider.IsAction);
            if (colorSlider.GetComponent<ScrollListTest>().selectedIndex >= 0 && colorSlider.IsAction)
            {
                //  colorSlider.IsAction = false;
                //    Debug.Log("LLLL" + colorSlider.GetComponent<ScrollListTest>().selectedIndex + "  ");
                colorPicker.CurrentColor = colorSlider.GetComponent<ScrollListTest>().Bank.ColorsBank[colorSlider.GetComponent<ScrollListTest>().selectedIndex];
                //     Debug.Log("orSlider");
            }

        }

        //}
        //else
        //{
        //    Debug.Log("orSl");
        //    colorSlider.IsAction = false;
        //}

        //ts=PlayerPrefsX.GetColorArray("colorArr");
        //PlayerPrefsX.SetColorArray("colorArr", Bank.ColorsBank);
        //ts = colorPicker.CurrentColor;
        //if (Input.GetKeyDown(KeyCode.C))
        //{
        //    PlayerPrefsX.SetColorArray("colorArr", new Color[0]);
        //}
        //if (Input.GetKeyDown(KeyCode.M))
        //{
        //    SaveColorBtn();
        //}
    }
    public void SaveColorBtn()
    {

        if (shop != null)
        {
            if (GameManager.Instance.Currency >= colorPrice)
            {
                SoundManScript.PlaySound(SoundManScript.Botton_Clip);
                if (colorSlider.IsVerticalList)
                {
                    colorSlider.myScrollRect.verticalNormalizedPosition = 0f;
                }
                else
                {
                    colorSlider.myScrollRect.horizontalNormalizedPosition = 1f;
                }

                InCustomMode = false;
                //colorPicker.gameObject.SetActive(false);
                //colorSlider.gameObject.SetActive(true);
                //saveBtn.gameObject.SetActive(true); customBtn.gameObject.SetActive(false);
                Bank.AddColor(colorPicker.CurrentColor);

                PlayerPrefsX.SetColorArray("colorArr", Bank.ColorsBank);
                // colorSlider.isUpdate = true;
                colorSlider.TempChangeEvent();
                colorSlider.cells[Bank.ColorsBank.Length - 1].clickit();
                ///////////decrease coins 
                backColorBtn();

            }
            else
            {
                SoundManScript.PlaySound(SoundManScript.lockedListBotton_Clip);
                InCustomMode = false;
                //colorSlider.myScrollRect.horizontalNormalizedPosition = 1f;
                shop.shopWindow.SetActive(true);
                //colorPicker.gameObject.SetActive(false);
                //backBtn.gameObject.SetActive(true);

                //if (buttonCollection != null)
                //    buttonCollection.SetActive(true);
                //colorSlider.transform.parent.gameObject.SetActive(true);
                //saveBtn.gameObject.SetActive(true); customBtn.gameObject.SetActive(false);
                //  backColorBtn();
            }

        }
        //colorPicker.startColor = colorSlider.GetComponent<ScrollListTest>().Bank.ColorsBank[colorSlider.GetComponent<ScrollListTest>().selectedIndex];
    }
    public void CustomColorBtn()
    {
        SoundManScript.PlaySound(SoundManScript.Botton_Clip);
        //////// to be selected//////////ne3mel if
        if (colorSlider.GetComponent<ScrollListTest>().selectedIndex >= 0)
        {
            colorPicker.startColor = colorPicker.CurrentColor = colorSlider.GetComponent<ScrollListTest>().Bank.ColorsBank[colorSlider.GetComponent<ScrollListTest>().selectedIndex];
        }
        else
        {
            colorPicker.startColor = Color.white;
        }

        colorPicker.refresh = true;
        isOpenWindow = true;
        InCustomMode = true;
        colorPicker.gameObject.SetActive(true);
        backBtn.gameObject.SetActive(false);

       // tileSlider.gameObject.SetActive(false);
        if (buttonCollection != null)
            buttonCollection.SetActive(false);
        colorSlider.transform.parent.gameObject.SetActive(false);
        TextlistSilder.gameObject.SetActive(false);
        if (effectSlider != null)
        {
            effectSlider.gameObject.SetActive(false);
        }
        if (tileSlider != null)
        {
            tileSlider.gameObject.SetActive(false);
        }
        saveBtn.gameObject.SetActive(false); customBtn.gameObject.SetActive(true);

    }
    public void backColorBtn()
    {


        SoundManScript.PlaySound(SoundManScript.Botton_Clip);
        colorPicker.gameObject.SetActive(false);
        backBtn.gameObject.SetActive(true);



        if (buttonCollection != null)
        {
            buttonCollection.SetActive(true);
        }
        colorSlider.transform.parent.gameObject.SetActive(true);
        saveBtn.gameObject.SetActive(true);
        customBtn.gameObject.SetActive(false);
        TextlistSilder.gameObject.SetActive(true);
        if (effectSlider != null)
        {
            effectSlider.gameObject.SetActive(true);
        }
        if (tileSlider != null)
        {
            tileSlider.gameObject.SetActive(true);
        }
        if (colorSlider.GetComponent<ScrollListTest>().selectedIndex >= 0)
        {

            //tempColor = colorPicker.startColor = colorPicker.CurrentColor = colorSlider.GetComponent<ScrollListTest>().Bank.ColorsBank[colorSlider.GetComponent<ScrollListTest>().selectedIndex];
            tempColor = colorPicker.CurrentColor = colorSlider.GetComponent<ScrollListTest>().Bank.ColorsBank[colorSlider.GetComponent<ScrollListTest>().selectedIndex];
            //     Debug.Log(tempColor);
        }
        //else
        //{
        //    tempColor = colorPicker.startColor = colorPicker.CurrentColor = colorSlider.GetComponent<ScrollListTest>().Bank.ColorsBank[0];
        //}
        isOpenWindow = false;
        colorPicker.refresh = true;
        InCustomMode = false;
        // InCustomMode = false;
    }
}
