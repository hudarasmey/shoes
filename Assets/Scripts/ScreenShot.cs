﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;

public class ScreenShot : MonoBehaviour {
    public Rect windowRect;
    public Camera ScreenshotCam;
    public string ScreenshotName;
    public int PicNumber=1;
	// Use this for initialization
	void Start () {
        //windowRect = ScreenshotCam.rect;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.G))
        {
            StartCoroutine(TakeScreenShot(ScreenshotName + PicNumber.ToString()));
            PicNumber += 1;
        }
        
	}
    IEnumerator TakeScreenShot(string ScreenshotName)
    {
       // GameObject canvas = GameObject.Find("Canvas").GetComponent<Canvas>().gameObject;
       // canvas.SetActive(false);
        // float x,y;
        //x=y=Screen.width *0.3f;


        //  windowRect = new Rect(, Screen.width * .20f);
        int newx = Mathf.FloorToInt(windowRect.x * Screen.width);
        int newy = Mathf.FloorToInt(windowRect.y * Screen.height);
        //int neww = Mathf.FloorToInt(windowRect.width);
        //int newh = Mathf.FloorToInt(windowRect.height);
        int neww = Mathf.FloorToInt(Screen.width * windowRect.width);
        int newh = Mathf.FloorToInt(Screen.height * windowRect.height);
        Debug.Log(newx + " " + newy + " " + neww + " " + newh);
        //newx = Mathf.FloorToInt((Screen.width*.75f)-(windowRect.width/2));
        //newy = Mathf.FloorToInt((Screen.height / 2) - (windowRect.height / 2));
     //   Texture2D screenshot = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, true);
        Texture2D screenshot = new Texture2D(newh, newh, TextureFormat.RGB24, true);
        yield return new WaitForEndOfFrame();
        //screenshot.ReadPixels(new Rect(newx, Screen.height - newy - newh, neww, newh), 0, 0, false);  // old
        screenshot.ReadPixels(new Rect(newx + ((neww - newh) / 2), newy, newh, newh), 0, 0, false);  // old
        //Texture2D screenshot = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        //screenshot.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        //screenshot.ReadPixels(new Rect(0, 0, Screen.width, Screen.height-200), 0, 0);// rectangle will be captured
        screenshot.Apply();
       
       // Texture2D newScreenshot = ScaleTexture(screenshot, 200, 200);
        Texture2D newScreenshot = screenshot;
        // NEW width and height
        // Texture2D newScreenshot = ScaleTexture(screenshot, neww / 2, newh / 2);    // NEW width and height
        byte[] bytes = newScreenshot.EncodeToPNG();

        string screenShotPath = Application.persistentDataPath + "/" + ScreenshotName + ".png";
        Debug.Log(screenShotPath);
        File.WriteAllBytes(screenShotPath, bytes);
        // nota = service.Uploadfile(bytes, Network.player.ipAddress);

        //  yield return nota;
        //yield return new WaitForEndOfFrame();
        Destroy(screenshot);
        yield return new WaitForEndOfFrame();
        //canvas.SetActive(true);
        // yield return new WaitForSeconds(1);
    }
}
