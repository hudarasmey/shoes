﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShakeUI : MonoBehaviour {
    public GameObject Image;
    public Image imageRenderer;

    public float shakeTime = 0.1f; // duration of shake and red color

    public float shakeRange = 20f; // shake range change be changed from inspector,
                                   //keep it mind that max it can go is half in either direction

    // Use this for initialization
    void Start()
    {
        //by saving the renderer you will not need to use GetComponent<> again and again
        imageRenderer = Image.GetComponent<Image>();
    }
    private void OnEnable()
    {
       // StartCoroutine(Shake());
        StartCoroutine(EnemyShake());
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
           // 
           // StartCoroutine(Shake());
            StartCoroutine(EnemyShake());
        }

    }

    private IEnumerator Shake()
    {
        Color originalColor = imageRenderer.color;
        WaitForSeconds wait = new WaitForSeconds(shakeTime);
        imageRenderer.color = new Color32(255,255, 0, 255); //adjust color to your needs
        yield return wait;
        imageRenderer.color = originalColor;
    }

    private IEnumerator EnemyShake()
    {

        float elapsed = 0.0f;
        Quaternion originalRotation = Image.transform.rotation;
        Vector3 originalPos = Image.transform.localPosition;
        while (elapsed < shakeTime)
        {

            elapsed += Time.deltaTime;
            float z = Random.value * shakeRange - (shakeRange / 2);
           // Image.transform.eulerAngles = new Vector3(originalRotation.x, originalRotation.y, originalRotation.z + (z/2));
            Image.transform.localPosition = new Vector3(originalPos.x+z, originalPos.y, originalPos.z );
            yield return null;
        }

       // Image.transform.rotation = originalRotation;
        Image.transform.localPosition = originalPos;
    }
}
