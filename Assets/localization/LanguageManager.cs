﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Xml;
using ArabicSupport;
using System;
using TMPro;
public class LanguageManager : MonoBehaviour
{
    public enum languageType { English, Arabic, German, French };
    public languageType Language = languageType.English;

    [Serializable]

    public struct LanguageFont
    {
        public string name;
        //  public Font font;
        public TMP_FontAsset font;
        public Font normalFont;
        public Material[] fontMaterials;
    }

    public LanguageFont[] fonts;

    private Hashtable Strings;
    //public string path, fileName;
    public TextAsset LanguageFile;

    public static LanguageManager Instance;

    public string CurrentLanguage
    {
        get { return PlayerPrefs.GetString("Language", Application.systemLanguage.ToString()); }
        set { PlayerPrefs.SetString("Language", value); }
    }
    GameObject SoundManagereObj;
    SoundManagerClass SoundManScript;

    void Awake()
    {

        SoundManagereObj = GameObject.FindGameObjectWithTag("SoundManager");
        SoundManScript = (SoundManagerClass)SoundManagereObj.GetComponent("SoundManagerClass");
        // Debug.Log(CurrentLanguage+"  "+ Application.systemLanguage);
        if (!Instance)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
        switch (CurrentLanguage)
        {
            case "English":
                Language = languageType.English;
                break;
            case "Arabic":
                Language = languageType.Arabic;
                break;
            case "German":
                Language = languageType.German;
                break;
            case "French":
                Language = languageType.French;
                break;
            default:
                Language = languageType.English;
                break;

        }

        //if (Application.platform == RuntimePlatform.OSXEditor)
        //    fileName = "file://" + Application.streamingAssetsPath + "/" + path;
        //else
        //    fileName = Application.streamingAssetsPath + "/" + path;

        setLanguage(CurrentLanguage);
    }
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Language.ToString() != CurrentLanguage)
        {
            // Debug.Log(Language.ToString() + "  " + CurrentLanguage);
            CurrentLanguage = Language.ToString();
            setLanguage(CurrentLanguage);
        }
    }

    public void setLanguage(string language)
    {
        SoundManScript.PlaySound(SoundManScript.Botton_Clip);
        CurrentLanguage = language;

        XmlDocument xml = new XmlDocument();
        // xml.Load(Application.dataPath + path);
        //TextAsset textAsset = (TextAsset)Resources.Load("FileNameWhitoutFileExtention", typeof(TextAsset));
        //XmlDocument xmldoc = new XmlDocument();
        //xmldoc.LoadXml(textAsset.text);
        // Debug.Log(Application.dataPath + path);
        // xml.Load(Application.dataPath + path);

        xml.LoadXml(LanguageFile.text);

        Strings = new Hashtable();
        var element = xml.DocumentElement[language];
        if (element != null)
        {
            var elemEnum = element.GetEnumerator();
            while (elemEnum.MoveNext())
            {
                if (elemEnum.Current is XmlElement)
                {
                    XmlElement xmlItem = (XmlElement)elemEnum.Current;
                    Strings.Add(xmlItem.GetAttribute("name"), xmlItem.InnerText);
                }
            }
        }
        else
        {
            Debug.LogError("The specified language does not exist: " + language);
        }
    }

    public string getString(string name)
    {
        if (!Strings.ContainsKey(name))
        {
            // Debug.LogError("The specified string does not exist: " + name);

            return "";
        }

        string text = (string)Strings[name];

        if (CurrentLanguage == "Arabic")
            text = ArabicFixer.Fix(text, false, false);

        return text;
    }

    public TMP_FontAsset GetFont()
    {
        for (int i = 0; i < fonts.Length; i++)
        {
            if (fonts[i].name == CurrentLanguage)
                return fonts[i].font;
        }

        return null;
    }
    public Font GetNormalFont()
    {
        for (int i = 0; i < fonts.Length; i++)
        {
            if (fonts[i].name == CurrentLanguage)
                return fonts[i].normalFont;
        }

        return null;
    }
    public Material GetMaterialFn(int index)
    {

        for (int i = 0; i < fonts.Length; i++)
        {
            if (fonts[i].name == CurrentLanguage)
            {
                Material mat = fonts[i].fontMaterials[index];
                return mat;
            }
               
        }

        return null;
       
        
    }
    public void SetLanguageType(string _Language)
    {
        //  Debug.Log(_Language);
        switch (_Language)
        {
            case "English":
                Language = languageType.English;
                break;
            case "Arabic":
                Language = languageType.Arabic;
                break;
            case "German":
                Language = languageType.German;
                break;
            case "French":
                Language = languageType.French;
                break;
            default:
                break;
        }
    }

}
