﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using ArabicSupport;
public class LangSetter : MonoBehaviour
{

    // Use this for initialization
    string prevLanguage;
    string tempLang;
    bool started;
    public TextMeshProUGUI text;
    public Text textNormal;
    public int MaterialNumber = 0;
    public bool testCon = false;
    //public TMP_FontAsset jj;
    void Start()
    {
        SetText();
        started = true;
       
    }

    public void SetText()
    {
//        Debug.Log("kodkfod");
        //Text text = GetComponent<Text>();
        text = GetComponent<TextMeshProUGUI>();
        if (text != null)
        {
            text.text = LanguageManager.Instance.getString(name);
            //Debug.Log(LanguageManager.Instance.GetFont().name);
            text.font = LanguageManager.Instance.GetFont();
            text.fontMaterial=  LanguageManager.Instance.GetMaterialFn(MaterialNumber);
            //text.material = LanguageManager.Instance.GetMaterialFn(MaterialNumber);
        }
        else
        {
            textNormal = GetComponent<Text>();

            textNormal.text = LanguageManager.Instance.getString(name);
            Debug.Log(LanguageManager.Instance.GetNormalFont().name);
            textNormal.font = LanguageManager.Instance.GetNormalFont();
        }
        


        prevLanguage = LanguageManager.Instance.CurrentLanguage;
        tempLang = LanguageManager.Instance.CurrentLanguage;
    }
    // Update is called once per frame
    void Update()
    {

        if (testCon && Input.anyKeyDown)
        {
            //string k = textNormal.text;
            //k = ArabicFixer.Fix(k, false, false);
            //textNormal.text = "";

            //Debug.Log(k);
            //SetText();
        }
        if (LanguageManager.Instance.Language.ToString() != tempLang )
        {
            
               // Debug.Log("7ala2");
                SetText();
           
            
        }
    }

    void OnEnable()
    {
        if (started && prevLanguage != LanguageManager.Instance.CurrentLanguage)
            SetText();
    }
}
