﻿/* 
 * UnityAdsController.cs
 * https://github.com/yasuyuki-kamata/UnityAdsController
 * 
 * 
 * if the errors are caused in Editor, please check the following things
 *  - Missing "UnityEngine.Advertisements"
 *    -> Turning ON the UnityAds Service from Services Window.
 *    -> Or, import UnityAds SDK from AssetStore.
 * 
 * もしUnityエディタでエラーがでる場合は、下記のことをご確認ください
 *  - "UnityEngine.Advertisements"がみつからないといわれたとき
 *    -> ServiceウィンドウからAdsサービスをONにしてください
 *    -> または、アセットストアからUnityAdsのSDKをインポートしてください
 */
using System;
using UnityEngine;
using System.Collections;
using System.Security.Policy;
using UnityEngine.Advertisements;
using UnityEngine.Events;
using UnityEngine.UI;


public enum zones { rewardedVideo = 1, video = 2 };
public class UnityAdsController : MonoBehaviour
{
    //public static UnityAdsController Instance;
    [SerializeField]
    public zones zoneID = zones.video;
	//string zoneID = "rewardedVideo";
	[SerializeField]
    string gameID_iOS = "1531684";
	[SerializeField]
    string gameID_Android = "1531683";

	[Header("OnFinished Callback")]
	public UnityEvent OnFinishedAds;
	[Header("OnSkipped Callback")]
	public UnityEvent OnSkippedAds;
	[Header("OnFailed Callback")]
	public UnityEvent OnFailedAds;
    public bool finish = false;
    public static UnityAdsController instance;

    //void Awake()
    //{
    //    if (!instance)
    //    {
    //        instance = this;
    //        DontDestroyOnLoad(gameObject);
    //    }
    //    else
    //    {
    //        Destroy(gameObject);
    //    }
    //}

    void Update()
    {
       // Debug.Log(testMode);
           // UnityAdsEditor: Initialize("GameID", False);
   // UnityEditor.Advertisements.UnityAdsEditor:EditorOnLoad()
        //Debug.Log("Unity  Ads rewardedVideo: " + Advertisement.IsReady("rewardedVideo"));
        //Debug.Log("Unity  Ads Video: " + Advertisement.IsReady("video"));
    }

	void Start ()
	{
      
    
		if (Advertisement.isSupported && !Advertisement.isInitialized) {
			#if UNITY_ANDROID
            Advertisement.Initialize(gameID_Android);
            //Debug.Log("Unity Ads initialized: " + Advertisement.isInitialized);
            //Debug.Log("Unity Ads is supported: " + Advertisement.isSupported);
            //Debug.Log("Unity  Ads test mode enabled: " + Advertisement.IsReady("video"));
			#elif UNITY_IOS
			Advertisement.Initialize(gameID_iOS,testMode);
#endif
        }
	}
    private IEnumerator WaitForAdEditor()
    {
        float currentTimescale = Time.timeScale;
        Time.timeScale = 0f;
        AudioListener.pause = true;

        yield return null;

        while (Advertisement.isShowing)
        {
            yield return null;
        }

        AudioListener.pause = false;
        if (currentTimescale > 0f)
        {
            Time.timeScale = currentTimescale;
        }
        else
        {
            Time.timeScale = 1f;
        }
    }
    public void ShowUnityAds(int zone)
    {
#if UNITY_ANDROID || UNITY_IOS
        if (Advertisement.IsReady(((zones)zone).ToString()))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show(((zones)zone).ToString(), options);
        }
#endif
    }
    public void ShowRewardedFuelVideoAd()
    {
        ShowVideoAd(FuelRewardCallBack, "rewardedVideo");
    }
    private void FuelRewardCallBack(ShowResult showResult)
    {

        var fuelBtn = GameObject.Find("MoreFuelRewardAdBtn").GetComponent<Button>();
        
        switch (showResult)
        {
            case ShowResult.Finished:
                Debug.Log("Player finished watching the video ad and is being rewarded with extra fuel.");
                //GameManager.instance.extraFuel = 10f;
                //fuelBtn.transform.GetChild(0).GetComponent<Text>().text = "Video finished";
                //fuelBtn.transform.GetComponent<Button>().interactable = false;
                finish = true;
                //fuelBtn.enabled = false;
                break;

            case ShowResult.Skipped:
                Debug.Log("Player skipped watching the video ad, no reward.");
                //fuelBtn.GetComponent<Button>();
                //fuelBtn.transform.GetChild(0).GetComponent<Text>().text = " you skipped the ad";
                //fuelBtn.transform.GetComponent<Button>().interactable = false;
                //fuelBtn.enabled = false;
                finish = false;
                break;

            case ShowResult.Failed:
                Debug.Log("video ad failed, no reward.");
                finish = false;
                break;
        }
        
    }
    public void ShowStandardVideoAd()
    {
        ShowVideoAd(null , "video");
    }
    public bool IsAdWithZoneIdReady(string zoneId)
    {
        return Advertisement.IsReady(zoneId);
    }
    public void ShowVideoAd(Action<ShowResult> adCallBackAction = null, string zone = "")
    {

        StartCoroutine(WaitForAdEditor());

        if (string.IsNullOrEmpty(zone))
        {
            zone = null;
        }

        var options = new ShowOptions();

        if (adCallBackAction == null)
        {
            options.resultCallback = DefaultAdCallBackHandler;
        }
        else
        {
            options.resultCallback = adCallBackAction;
        }

        if (Advertisement.IsReady(zone))
        {
          //  Debug.Log("Showing ad for zone: " + zone);
            Advertisement.Show(zone, options);
        }
        else
        {
            Debug.LogWarning("Ad was not ready. Zone: " + zone);
           // StartCoroutine(WaitForAdEditor());
        }
    }

    private void DefaultAdCallBackHandler(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Time.timeScale = 1f;
                break;

            case ShowResult.Failed:
                Time.timeScale = 1f;
                break;

            case ShowResult.Skipped:
                Time.timeScale = 1f;
                break;
        }
    }
	private void HandleShowResult(ShowResult result)
	{
		switch (result)
		{
		case ShowResult.Finished:
			Debug.Log ("The ad was successfully shown.");
			OnFinished ();
			break;
		case ShowResult.Skipped:
			Debug.Log ("The ad was skipped before reaching the end.");
			OnSkipped ();
			break;
		case ShowResult.Failed:
			Debug.LogError ("The ad failed to be shown.");
			OnFailed ();
			break;
		}
	}

	void OnFinished ()
	{
		// ここに動画視聴完了時の処理
		OnFinishedAds.Invoke();
	}

	void OnSkipped ()
	{
		// ここに動画をスキップしたときの処理
		OnSkippedAds.Invoke();
	}

	void OnFailed ()
	{
		// ここに動画視聴失敗時の処理
		OnFailedAds.Invoke();
	}
}