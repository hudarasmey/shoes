﻿using UnityEngine;
using System.Collections;

public class Share : MonoBehaviour
{
    public GameObject effectCanvas;
    private const string TITLE = "Title";
    private const string MESSAGE = "Message";
    GameObject SoundManagereObj;
    SoundManagerClass SoundManScript;
    void Awake()
    {
        SoundManagereObj = GameObject.FindGameObjectWithTag("SoundManager");
        SoundManScript = (SoundManagerClass)SoundManagereObj.GetComponent("SoundManagerClass");
    }
    public void shareScreenShot()
    {
        SoundManScript.PlaySound(SoundManScript.Botton_Clip);
        FrispGames.Social.ScreenshotSharer.Instance().canvas_3 = effectCanvas;
        StartCoroutine(FrispGames.Social.ScreenshotSharer.Instance().PostScreenshot(TITLE, MESSAGE));
    }
    //private IEnumerator TakeScreenshot()
    //{
    //    yield return new WaitForEndOfFrame();

    //    var width = Screen.width;
    //    var height = Screen.height;
    //    var tex = new Texture2D(width, height, TextureFormat.RGB24, false);

    //    // Read screen contents into the texture
    //    tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
    //    tex.Apply();
    //    byte[] screenshot = tex.EncodeToPNG();

    //    var wwwForm = new WWWForm();
    //    wwwForm.AddBinaryData("image", screenshot, "InteractiveConsole.png");
    //    wwwForm.AddField("message", "herp derp.  I did a thing!  Did I do this right?");
    //    FB.API("me/photos", HttpMethod.POST, this.HandleResult, wwwForm);
    //}
}
