using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SoundManagerClass : MonoBehaviour
{

    public Image onAudio,offAudio;
    public Image onFx, offFx;
    public AudioSource Bg_Source,click_Source;
    public AudioSource Scene_Source;
    public Slider BGsoundSlider, fxSoundSlider;
    float tempBGsoundSliderValue;
    public AudioClip Bg_Clip;
    public AudioClip Botton_Clip;
    public AudioClip ListBotton_Clip, ListSelectBotton_Clip, lockedListBotton_Clip, present_Clip, deletListBotton_Clip;
    public AudioClip saveBotton_Clip;
    public AudioClip yes_Clip;
    public AudioClip parchase_Clip;

    public bool isSoundOff = false, isFxOff = false;





    // Use this for initialization
    void Start()
    {
      soundIcon(onAudio, offAudio, "offAudio");
        soundIcon(onFx, offFx, "offFx");
        //Scene_Source.volume = 0.3f;
    }


    void Awake()
    {
        
        //soundIcon();
        //Debug.Log("Audio Sources are initialized");
        Bg_Source = this.gameObject.AddComponent<AudioSource>();
        Scene_Source = this.gameObject.AddComponent<AudioSource>();
        if (PlayerPrefs.GetInt("offAudio") == 0)
        {
            Bg_Source.volume = 0.5f;
            isSoundOff = false;
           // soundIcon(onAudio, offAudio);
        }
        else
        {
            Bg_Source.volume = 0;
            isSoundOff = true;
        }
        if (PlayerPrefs.GetInt("offFx") == 0)
        {
           
            Scene_Source.volume = 1f;
            isFxOff = false;
            // soundIcon(onAudio, offAudio);
        }
        else
        {
            Scene_Source.volume = 0;
            isFxOff = true;
        }
        if (Bg_Clip != null)
        {
            Bg_Source.clip = Bg_Clip;
            Bg_Source.Play();
            Bg_Source.loop = true;
        }


        Scene_Source.playOnAwake = false;

    }



    // Update is called once per frame
    void Update()
    {
        //if (!isSoundOff)
        //{
        //    if (BGsoundSlider.value <= 0)
        //    {
        //        Bg_Source.volume = 0;
        //        soundActivation(Bg_Source, BGsoundSlider);
        //        //isSoundOff = true;
        //    }
        //    else
        //    {
        //        Bg_Source.volume = BGsoundSlider.value;
        //    }
        //    //if (fxSoundSlider.value <=0)
        //    //{
        //    //   Scene_Source.volume = 0;
        //    //   soundActivation(Scene_Source, fxSoundSlider);
        //    //    //isSoundOff = true;
        //    //}
        //    //else
        //    //{
        //    //    Scene_Source.volume = fxSoundSlider.value;
        //    //}
        //}
        //else
        //{
        //    if (BGsoundSlider.value > 0 && tempBGsoundSliderValue != BGsoundSlider.value)
        //    {
        //        Bg_Source.volume = BGsoundSlider.value;
        //        PlayerPrefs.SetInt("SoundOff", 0);
        //        PlayerPrefs.SetFloat("SoundVolume", BGsoundSlider.value);
        //        soundIcon();
        //        isSoundOff=false;
        //    }
        //}
        
       

    }
    public void PlaySound(AudioClip clip)
    {
        if (!isFxOff && clip != null)
        {
            Scene_Source.volume = 0.1f;
           // Debug.Log("BOOOOOOOOOOOOOOM");
            Scene_Source.clip = clip;
            Scene_Source.Play();
        }

    }
    public void soundActive()
    {
        isSoundOff= soundActivation(Bg_Source, isSoundOff, onAudio, offAudio, "offAudio",0.5f);//, BGsoundSlider);


    }
    public void ClickSoundActive()
    {
        isFxOff = soundActivation(Scene_Source, isFxOff, onFx, offFx, "offFx",1);
        //, fxSoundSlider);
    }
    public bool soundActivation(AudioSource soundSource, bool SoundOff,Image onImage, Image offImage,string prefName, float volumeValue)//,Slider slide)
    {
        //if (soundSource.volume <= 0 && isSoundOff == false)
        //{

        //    if (isSoundOff == false)
        //    {

        //        PlayerPrefs.SetInt("SoundOff", 1);
        //        PlayerPrefs.SetFloat("SoundVolume", 0);
        //        isSoundOff = true;
        //    }
        //    else
        //    {

        //        PlayerPrefs.SetInt("SoundOff", 1);
        //        isSoundOff = false;
        //    }
        //    soundIcon();
        //}
        //else 
        //{
        //Debug.Log(SoundOff.ToString());
        SoundOff = !SoundOff;

        if (SoundOff == false)
        {
            soundSource.volume = volumeValue;
            //soundSource.volume = PlayerPrefs.GetFloat("SoundVolume");
            //  slide.value = PlayerPrefs.GetFloat("SoundVolume");
            PlayerPrefs.SetInt(prefName, 0);
            // isSoundOff = true;

        }
        else
        {
            soundSource.volume = 0;
            PlayerPrefs.SetFloat("SoundVolume", volumeValue);// slide.value);
                                                      //  slide.value = 0;
            PlayerPrefs.SetInt(prefName.ToString(), 1);
            //   isSoundOff = false;

        }
        soundIcon(onImage, offImage, prefName);
        return SoundOff;
        // soundIcon();
        //}



    }
     void soundIcon(Image onImage, Image offImage, string prefName)
    {
        //        PlayerPrefs.GetInt("SoundOff");
        //s     Debug.Log(PlayerPrefs.GetInt("SoundOff"));
        //Bg_Source =  gameObject.GetComponent<AudioSource>();
       // Debug.Log(prefName + "  " + PlayerPrefs.GetInt(prefName));
        if (PlayerPrefs.GetInt(prefName) == 0)
        {
            // BGsoundSlider.value = PlayerPrefs.GetFloat("SoundVolume");
            //Bg_Source.volume=
            
            offImage.gameObject.SetActive(false);
            onImage.gameObject.SetActive(true);
            PlaySound(Botton_Clip);
           // isSoundOff=false;
        }
        else
        {
            //BGsoundSlider.value = 0;
            offImage.gameObject.SetActive(true);
            onImage.gameObject.SetActive(false);
            // isSoundOff = true;
        }
    }
    //void kk(bool SoundOff, Slider slider, AudioSource scene_Source)
    //{
    //    if (!SoundOff)
    //    {
    //        if (slider.value <= 0)
    //        {
    //            scene_Source.volume = 0;
    //            soundActivation(scene_Source);//, slider);
    //            //isSoundOff = true;
    //        }
    //        else
    //        {
    //            scene_Source.volume = slider.value;
    //        }
    //        //if (fxSoundSlider.value <=0)
    //        //{
    //        //   Scene_Source.volume = 0;
    //        //   soundActivation(Scene_Source, fxSoundSlider);
    //        //    //isSoundOff = true;
    //        //}
    //        //else
    //        //{
    //        //    Scene_Source.volume = fxSoundSlider.value;
    //        //}
    //    }
    //    else
    //    {
    //        if (slider.value > 0 && tempBGsoundSliderValue != slider.value)
    //        {
    //            scene_Source.volume = slider.value;
    //            PlayerPrefs.SetInt("SoundOff", 0);
    //           // PlayerPrefs.SetFloat("SoundVolume", slider.value);
    //            soundIcon();
    //            isSoundOff = false;
    //        }
    //    }
    //}
    
}
/*
 make empty gameobject and make tage SoundManager and add to empty game object 
 put SoundManagerClass script in it 
 make below in game play
//var.s
GameObject SoundManagereObj ;
SoundManagerClass SoundManScript ;
// start
SoundManagereObj= GameObject.FindGameObjectWithTag("SoundManager");
SoundManScript = (SoundManagerClass) SoundManagereObj.GetComponent("SoundManagerClass");
// run sound update
 SoundManScript.Scene_Source.clip = clip;
        SoundManScript.Scene_Source.Play();
*/
