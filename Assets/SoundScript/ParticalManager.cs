﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticalManager : MonoBehaviour {
    public ParticleSystem p1 , p2;
   // public SkinnedMeshRenderer kkk;
   // public Mesh myMesh;
	// Use this for initialization
    public ObjectSelectionSubMenu objectMan; 
	void Start () {
      //  ParticleSystem ps = GetComponent<ParticleSystem>();
        //var sh = p1.shape;
        //sh.enabled = true;
        //sh.shapeType = ParticleSystemShapeType.SkinnedMeshRenderer;
        //sh.skinnedMeshRenderer = kkk;
	}
	
	// Update is called once per frame

    void Update()
    {
        //p1.shape.skinnedMeshRenderer = kkk;
        //p1.shapeType = ParticleSystemShapeType.Mesh;
    }
    public void runParticalOnce(ParticleSystem _partical)
    {
      
        _partical.Play();
    }
    public void runParticalOnce(ParticleSystem _partical, SkinnedMeshRenderer sMesh)
    {
        var sh = _partical.shape;
        sh.enabled = true;
        sh.shapeType = ParticleSystemShapeType.SkinnedMeshRenderer;
        sh.skinnedMeshRenderer = sMesh;
       _partical.Play();
    }
    public void runParticalMaterial(ObjMenu objIs)
    {
        switch (objIs)
        {
            case ObjMenu.Face:
                runParticalOnce(p1, objectMan.faceSkinnedMesh);
                break;
            case ObjMenu.Base:
                runParticalOnce(p1, objectMan.baseSkinnedMesh);
                break;
            case ObjMenu.Heel:
                runParticalOnce(p1, objectMan.heelSkinnedMesh);
                break;
            case ObjMenu.Widge:
                runParticalOnce(p1, objectMan.heelSkinnedMesh);
                break;
            case ObjMenu.FaceFront:
                runParticalOnce(p1, objectMan.frontFaceSkinnedMesh);
                break;
            case ObjMenu.FaceBack:
                runParticalOnce(p1, objectMan.backFaceSkinnedMesh);
                break;
            case ObjMenu.Cone:
                runParticalOnce(p1, objectMan.heelSkinnedMesh);
                break;
            case ObjMenu.none:
                break;
            default:
                break;
        }
    }

}
