﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIAnimationGroup : MonoBehaviour {
    public GUIAnimation[] group;
	// Use this for initialization
	void Start () {
        
      //  group = GameObject.FindObjectsOfType<GUIAnimation>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void DeactivateAll()
    {
        for (int i = 0; i < group.Length; i++)
        {
            group[i].Deactive();
        }

    }
}
