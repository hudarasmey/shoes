﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class RewardButton : MonoBehaviour {
    public UnityAdsController adsController;
    public GameObject Spinner;
    public ShopMan _ShopMan;
    public bool startVideo, ismed;
	// Use this for initialization
    void OnEnable()
    {
      //  transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Video";
    }
	void Start () {
        adsController = GameObject.FindGameObjectWithTag("Ads").GetComponent<UnityAdsController>();
        Spinner.SetActive(true);
        gameObject.transform.GetComponent<Button>().interactable = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (adsController.IsAdWithZoneIdReady("rewardedVideo"))
        {
            if (Spinner.activeSelf)
            {
                Spinner.SetActive(false);
            }
            if (gameObject.transform.GetComponent<Button>().interactable != true)
            {
                gameObject.transform.GetComponent<Button>().interactable = adsController.IsAdWithZoneIdReady("rewardedVideo");
            }
        }
        else
        {
            if (Spinner.activeSelf ==false)
            {
                Spinner.SetActive(true);
            }
            if ( gameObject.transform.GetComponent<Button>().interactable==true)
            {
                gameObject.transform.GetComponent<Button>().interactable = false;
            }
           
        }
        if (startVideo && adsController.finish)
        {
            adsController.finish = false;
            startVideo = false;
            _ShopMan.targetElement.unlockElementFn();
            _ShopMan.BuyByVideo(ismed);
        }
	}
    public void ClickVideoButton(bool isMed)
    {
        ismed = isMed;
        startVideo = true;
        adsController.ShowRewardedFuelVideoAd();
    }
}
