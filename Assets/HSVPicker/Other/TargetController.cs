﻿using UnityEngine;
using System.Collections;

public class TargetController : MonoBehaviour {

    public float Target = 1;
    public float speed = 1;
    public float incSpeed=1;
    float tempStart;
    bool finish;
    public Vector3 targetPos;
    public float tempSpeed;
    Vector3 tempTargetPos;
    // Use this for initialization
    void Start()
    {
        tempTargetPos = gameObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (tempTargetPos != targetPos)
        {
            finish = false;
            tempTargetPos = ToVector3(tempTargetPos, targetPos, speed);
            gameObject.transform.position = tempTargetPos;
        }
        else
        {
            tempSpeed = 0.001f;
            finish = true;
        }
        
        //if (tempStart != Target)
        //{
        //    finish = false;
        //    tempStart = NumberTo(tempStart, Target, speed);
        //}
        //else
        //{

        //    finish = true;
        //}

    }
    //float NumberTo(float var, float to, float speed)
    //{
    //    if (!finish)
    //    {
    //        if (var != to)
    //        {
    //           // var = Mathf.MoveTowards(var, to, (Mathf.Abs(to - var) * Time.deltaTime + 0.01f) * Mathf.Exp(speed * Time.deltaTime));
    //            var = Mathf.Lerp(var, to, 2);
    //        }
    //        //else
    //        //{

    //        //    finish = true;
    //        //}
    //    }

    //    return var;
    //}
    Vector3 ToVector3(Vector3 var, Vector3 to, float speed)
    {
        if (!finish)
        {
            if (var != to)
            {
                if (tempSpeed <= speed)
                {
                    tempSpeed += incSpeed/1000;
                }
               //var = Vector3.MoveTowards(var, to, (Vector3.Distance(to,var) * Time.deltaTime + 0.01f) * Mathf.Exp(speed * Time.deltaTime));
                var = Vector3.MoveTowards(var, to, Time.deltaTime * tempSpeed);
                // var = Mathf.MoveTowards(var, to, Time.deltaTime * speed);
            }
            //else
            //{

            //    finish = true;
            //}
        }

        return var;
    }
}
