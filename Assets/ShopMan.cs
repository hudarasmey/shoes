﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
public class ShopMan : MonoBehaviour
{
    public modelControl1 Manager;
    public GameObject window;
    public GameObject shopWindow,unlocksureWindow, unlocksureWindow2;
    public Image image,image2,loadingImage;
    public TextMeshProUGUI windowCoinsText, windowCoinsText2;
    public Button videoBtn, coinBtn, coinBtn2;
    public UnlockElement targetElement;
    public TextMeshProUGUI CoinsText;
    public modelControl1 SceneManager;
    public int tempMenuIndex;
    public ScrollRect storeList;
    //private int coins;
    //public int Coins
    //{
    //    get { return PlayerPrefs.GetInt("coins"); }
    //    set { coins = value; PlayerPrefs.SetInt("coins", value); } //coins = value;
    //}
    private int firstTimeGameStart;
    public int FirstTimeGameStart
    {
        get { return PlayerPrefs.GetInt("firstTimeGameStart", -1); }
        set { firstTimeGameStart = value; PlayerPrefs.SetInt("firstTimeGameStart", value); }
    }
    void OnEnable()
    {
        tempMenuIndex = SceneManager.mainIndex;
        storeList.horizontalNormalizedPosition = 0;
        unlocksureWindow.SetActive(false);
    }
    GameObject SoundManagereObj;
    SoundManagerClass SoundManScript;
    void Awake()
    {
        SoundManagereObj = GameObject.FindGameObjectWithTag("SoundManager");
        SoundManScript = (SoundManagerClass)SoundManagereObj.GetComponent("SoundManagerClass");
    }
    // Use this for initialization
    void Start()
    {
        unlocksureWindow2.SetActive(false);
        // Debug.Log(PlayerPrefs.GetInt("firstTimeGameStart"));
        if (FirstTimeGameStart != 1)
        {
            // GameManager.Instance.Currency = 500;
            FirstTimeGameStart = 1;
        }

        // CoinsText.text = "Coins " + GameManager.Instance.Currency.ToString();
        if (CoinsText != null)
        {
            CoinsText.text = GameManager.Instance.Currency.ToString();
        }
        
       

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            GameManager.Instance.Currency = 0;
        }
    }
    public void SubCoins(int _coins)
    {

        //coins = coins - _coins;
        int Temp = GameManager.Instance.Currency - _coins;
        GameManager.Instance.Currency = Temp;

        //  SetText(CoinsText, "Coins " + GameManager.Instance.Currency.ToString());
        SetText(CoinsText, GameManager.Instance.Currency.ToString());
        // Debug.Log(PlayerPrefs.GetInt("coins") + "  " + _coins);
    }
    public void AddCoins(int _coins)
    {
        SoundManScript.PlaySound(SoundManScript.Botton_Clip);
        GameManager.Instance.Currency = GameManager.Instance.Currency + _coins;
        //SetText(CoinsText, "Coins " + GameManager.Instance.Currency.ToString());
        SetText(CoinsText, GameManager.Instance.Currency.ToString());
        Close(shopWindow);
    }
    public void AddCoinsx(int _coins)
    {
        SoundManScript.PlaySound(SoundManScript.Botton_Clip);
        GameManager.Instance.Currency = GameManager.Instance.Currency + _coins;
        //SetText(CoinsText, "Coins " + GameManager.Instance.Currency.ToString());
        SetText(CoinsText, GameManager.Instance.Currency.ToString());
        
    }
    public void Close(GameObject Obj)
    {
        if (targetElement != null)
        {
            targetElement.revokAction();
        }
        
        SoundManScript.PlaySound(SoundManScript.Botton_Clip);
        Obj.SetActive(false);
        
        if (SceneManager.mainIndex == 9)
        {
            SceneManager.GoToMenu(tempMenuIndex);
        }


    }
    public void Close2(GameObject Obj)
    {
        
        SoundManScript.PlaySound(SoundManScript.Botton_Clip);
        Obj.SetActive(true);
        unlocksureWindow2.SetActive(false);
        if (SceneManager.mainIndex == 9)
        {
            SceneManager.GoToMenu(tempMenuIndex);
        }


    }
    public void CloseTemp(GameObject Obj)
    {
        Obj.SetActive(false);
        //  SceneManager.GoToMenu(tempMenuIndex);
        

    }
    public void buyByCoins(bool isMid)
    {
        //if (GameManager.Instance.Currency >= int.Parse(windowCoinsText.text))
        //{

        //}
        //else
        //{

            SoundManScript.PlaySound(SoundManScript.Botton_Clip);
            targetElement.unlockElementFn();
            // Close(window);
            SubCoins(int.Parse(targetElement.PriceTextTP.text));
            targetElement.cell.relatedList.savedIndex = targetElement.cell.relatedList.tempClickedIndex;
            window.SetActive(false);
            unlocksureWindow2.SetActive(false);
            if (SceneManager.mainIndex == 9)
            {
                SceneManager.GoToMenu(tempMenuIndex);
            }
            targetElement.cell.relatedList.isParchaseWin = false;
            unlocksureWindow.SetActive(false);
        //}
        if (isMid)
        {
            Manager.escapeClicked = true;
        }
        


    }
    public void BuyByVideo(bool isMid)
    {
        SoundManScript.PlaySound(SoundManScript.Botton_Clip);
        //Close(window);
        targetElement.cell.relatedList.savedIndex = targetElement.cell.relatedList.tempClickedIndex;
        targetElement.cell.relatedList.isParchaseWin = false;
        window.SetActive(false);
        unlocksureWindow2.SetActive(false);
        unlocksureWindow.SetActive(false);
        if (SceneManager.mainIndex == 9)
        {
            SceneManager.GoToMenu(tempMenuIndex);
        }
        if (isMid)
        {
            Manager.escapeClicked = true;
        }
    }
    void SetText(TextMeshProUGUI _text, string txt)
    {
        _text.text = txt;
    }
    public void Open(GameObject Obj)
    {
        Obj.SetActive(true);

    }
}
