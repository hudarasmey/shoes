﻿using UnityEngine;
using System.Collections;

public class NormalGeneration : MonoBehaviour {
    public Texture2D T1, T2;
   public Material G;
   public float distortion;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.L))
        {
          //  T2 =(Texture) NormalMapGen(T1, 1);
            //G.GetComponent<MeshRenderer>().material.SetTexture("_BumpMap", T2); 
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            //T2 = NormalMapGen(T1, 1);
           // T2 = G.GetComponent<Renderer>().materials[0].GetTexture("_BumpMap");//.SetTexture("_BumpMap", T2);
           T2 = NormalMapGen(T1,1f);
          // T2 = toNormalMap(T2);

            G.SetTexture("_BumpMap", T2);
        }

        if (true)
        {
            
        }
    //    n.SetPixel(x, y, new Color(Mathf.Clamp01(sCurve(xDelta, distortion)), Mathf.Clamp01(sCurve(yDelta, distortion)), 1f, 1.0f));
	}
    public static float sCurve(float x, float distortion)
    {
        return 1f / (1f + Mathf.Exp(-Mathf.Lerp(5, 10, distortion) * (x -0.5f)));
    }
    public  Texture2D toNormalMap(Texture2D t)
    {
        Texture2D t1 = new Texture2D(t.width, t.height, TextureFormat.ARGB32, true);
        //for (int x = 0; x < t.width; x++)
        //{
        //    for (int y = 0; y < t.height; y++)
        //    {
        //        t1.SetPixel(x, y, new Color(Mathf.Clamp01(sCurve(xDelta, distortion)), Mathf.Clamp01(sCurve(yDelta, distortion)), 1f, 1.0f));
        //    }
        //}
        //t1.Apply();
        Texture2D n = new Texture2D(t.width, t.height, TextureFormat.ARGB32, true);
        Color oldColor = new Color();
        Color newColor = new Color();

        for (int x = 0; x < t.width; x++)
        {
            for (int y = 0; y < t.height; y++)
            {
                
                oldColor = t.GetPixel(x, y);
                
                newColor.r = oldColor.g;
                newColor.b = oldColor.g;
                newColor.g = oldColor.g;
                newColor.a = oldColor.r;
               // n.SetPixel(x, y, newColor);
                
            }
        }

        n.Apply();

        return n;
    }
 
 
    private Texture2D NormalMapGen(Texture2D source, float strength) 
    { 

        strength=Mathf.Clamp(strength,0.0F,55.0F);
        Texture2D normalTexture;
            float xLeft;
        float xRight; 
        float yUp; 
        float yDown; 
        float yDelta; 
        float xDelta;
        normalTexture = new Texture2D (source.width, source.height, TextureFormat.ARGB32, true); 
        for (int y=0; y<normalTexture.height; y++) 
        {
                for (int x=0; x<normalTexture.width; x++) 
            {
                xLeft = source.GetPixel(x - 1, y).grayscale*strength;
                xRight = source.GetPixel(x + 1, y).grayscale*strength;
                yUp = source.GetPixel(x, y - 1).grayscale*strength;
                yDown = source.GetPixel(x, y + 1).grayscale*strength;
                xDelta = ((xLeft - (xRight)) + 1) *0.5f;
                yDelta = ((yUp - yDown) + 1) *0.5f;
                normalTexture.SetPixel(x, y, new Color(Mathf.Clamp01(sCurve(xDelta, distortion)), Mathf.Clamp01(sCurve(yDelta, distortion)), 1,Mathf.Clamp01(sCurve(yDelta, distortion))));
                //normalTexture.SetPixel(x,y,new Color(xDelta,yDelta,1.0f,yDelta));
            } 
        
        
   	}
        normalTexture.Apply(); 
        //Code for exporting the image to assets folder System.IO.File.WriteAllBytes( "Assets/NormalMap.png", normalTexture.EncodeToPNG()); 
        return normalTexture;
    }
}
