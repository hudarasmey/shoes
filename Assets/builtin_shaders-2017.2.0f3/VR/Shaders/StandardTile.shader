﻿ Shader "Custom/StandardTile" {
     Properties {
         _Color ("Color", Color) = (1,1,1,1)
         _MainTex ("Albedo (RGB)", 2D) = "white" {}
         _BumpMap ("Normal Map", 2D) = "bump" {}
         _Glossiness ("Smoothness", Range(0,1)) = 0.5
         _Metallic ("Metallic", Range(0,1)) = 0.0
         _Spec ("Specular Map", 2D) = "white" {}
         _OcclusionStrength ("Occlusion Intensity", Float) = 1.0
         _OcclusionMap ("Occlusion Map", 2D) = "white" {}
         _EmitCol ("Emission Color", Color) = (1,1,1,1)
         _EmitInt ("Emission Intensity", Float) = 0.0
         _EmissionMap ("Emission", 2D) = "white" {}
     }
     SubShader {
         Tags { "RenderType"="Opaque" }
         LOD 200
         Pass
		{
			ZWrite Off
			ZTest LEqual
			Cull Off
			}
         CGPROGRAM
         // Physically based Standard lighting model, and enable shadows on all light types
         #pragma surface surf Standard fullforwardshadows
 
         // Use shader model 3.0 target, to get nicer looking lighting
         #pragma target 3.0
 
         sampler2D _MainTex;
         sampler2D _BumpMap;
         sampler2D _Spec;
         sampler2D _OcclusionMap;
         sampler2D _EmissionMap;
 
         struct Input {
             float2 uv_MainTex;
             float2 uv_BumpMap;
             float2 uv_Spec;
             float2 uv_OcclusionMap;
             float2 uv_EmissionMap;
         };
 
         half _Glossiness;
         half _Metallic;
         half _OcclusionStrength;
         half _EmitInt;
         fixed4 _Color;
         fixed4 _EmitCol;
 
         void surf (Input IN, inout SurfaceOutputStandard o) {
             o.Normal = UnpackNormal (tex2D (_BumpMap, IN.uv_BumpMap));
             // Albedo comes from a texture tinted by color
             fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
             fixed occlusion = pow (tex2D (_OcclusionMap, IN.uv_OcclusionMap).r, _OcclusionStrength);
             c.rgb *= occlusion;
             o.Albedo = c.rgb;
             // Metallic and smoothness come from slider variables
             fixed4 spec = tex2D (_Spec, IN.uv_Spec);
             o.Metallic = _Metallic * spec.r;
             o.Smoothness = _Glossiness * spec.a;
             o.Emission = tex2D (_EmissionMap, IN.uv_EmissionMap).rgb * _EmitInt * _EmitCol;
             o.Alpha = c.a;
         }
         ENDCG
     }
     FallBack "Diffuse"
 }