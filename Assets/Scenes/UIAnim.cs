﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIAnim : MonoBehaviour {

    public Vector2 CanvasRef = new Vector2(800, 600);
    public bool getCurrentRes;
    public TransformElement[] Elements;
    public bool AutoStart, Started, LoopAnim;
    MaskableGraphic image;
    float time;
    public float publicDelay;

    bool active;
    public bool Active
    {
        get
        {
            return active;
        }
    }

    // Use this for initialization
    void Start()
    {
       
        if (getCurrentRes)
        {
            CanvasRef = new Vector2(Screen.width + (transform.position.x / 2), Screen.height + (transform.position.y / 2));
            //CanvasRef = new Vector2(Screen.width, Screen.height );
        }
        if (AutoStart)
        {
//print(Time.time);
            //    StartCoroutine(Play(publicDelay));
            Play(publicDelay);
            
        }
    }
  
    void Play(float wait)
    {
        ////Debug.Log(wait);
        // //print(Time.time);
        // yield return new WaitForSecondsRealtime(2);
        //print(Time.time);
        ////  yield return new WaitForSeconds(wait);
        Started = true;
        if (GetComponent<MaskableGraphic>())
            image = GetComponent<MaskableGraphic>();


        foreach (TransformElement item in Elements)
        {
            switch (item.Type)
            {
                case TransformType.Postion:
                     transform.localPosition = new Vector3(item.StartValue.x * CanvasRef.x, item.StartValue.y * CanvasRef.y, 0);
                    break;
                case TransformType.Scale:
                    transform.localScale = item.StartValue;
                    break;
                case TransformType.Rotation:
                    transform.rotation = Quaternion.Euler(item.StartValue);
                    break;
                case TransformType.Color:
                    if (image)
                        image.color = item.StartValue;
                    break;
                case TransformType.Fill:
                    if (image)
                        ((Image)image).fillAmount = item.StartValue.x;
                    break;
            }
            item.Finished = false;
        }

        time = 0;
        active = true;

    }

    public void Reverse()
    {
        foreach (TransformElement item in Elements)
        {
            item.Reverse = true;
            item.ReverseAfter = time;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!Started)
            return;
        //  Debug.Log(Started);
        active = false;
        time += Time.unscaledDeltaTime;

        foreach (TransformElement item in Elements)
        {
            switch (item.Type)
            {
                case TransformType.Postion:
                    Vector4 resultVal;
                    if (CalculateValue(item, out resultVal))
                    {
                        active = true;

                        Vector3 modVal = new Vector3(resultVal.x * CanvasRef.x, resultVal.y * CanvasRef.y, 0);
                        transform.localPosition = modVal;
                    }

                    break;
                case TransformType.Scale:
                    if (CalculateValue(item, out resultVal))
                    {
                        active = true;
                        transform.localScale = resultVal;
                    }

                    break;
                case TransformType.Rotation:
                    if (CalculateValue(item, out resultVal))
                    {
                        active = true;
                        transform.rotation = Quaternion.Euler(resultVal);
                    }

                    break;
                case TransformType.Color:
                    if (CalculateValue(item, out resultVal))
                    {
                        active = true;
                        image.color = resultVal;
                    }

                    break;
                case TransformType.Fill:
                    if (CalculateValue(item, out resultVal))
                    {
                        active = true;
                        ((Image)image).fillAmount = resultVal.x;
                    }

                    break;
            }
        }
        bool isItFinish = false;
        foreach (TransformElement item in Elements)
        {

            if (item.Finished)
            {
                isItFinish = item.Finished;
            }
            else
            {
                isItFinish = false;

                break;
            }


        }
        Started = !isItFinish;
        if (!Started)
        {
            //   Debug.Log("reset");
            resetValues();
            if (LoopAnim)
            {
                Play(0);
            }
        }
    }

    private bool CalculateValue(TransformElement item, out Vector4 returnValue)
    {
        returnValue = Vector3.zero;

        if (time > item.StartAfter)
        {
            if ((time - item.StartAfter) / item.Duration < 1)
            {
                float currTime = (time - item.StartAfter) / item.Duration;

                if (item.AnimationType == AnimationType.Curve)
                {
                    currTime = Mathf.Sin(currTime * Mathf.PI / 2);
                }
                else if (item.AnimationType == AnimationType.Swing)
                {
                    currTime = Mathf.Sin(currTime * Mathf.PI / 2) * (1 + Mathf.Sin(currTime * Mathf.PI) * 0.5f);
                }

                returnValue = Vector4.LerpUnclamped(item.StartValue, item.FinishValue, currTime);
            }
            else if (item.Reverse)
            {
                if (time < item.ReverseAfter)
                    returnValue = item.FinishValue;
                else
                {
                    float currTime = (time - item.ReverseAfter) / item.Duration;

                    if (item.AnimationType == AnimationType.Curve)
                    {
                        currTime = Mathf.Sin(currTime * Mathf.PI / 2);
                    }


                    if ((time - item.ReverseAfter) / item.Duration < 1)
                        returnValue = Vector4.Lerp(item.FinishValue, item.StartValue, currTime);
                    else
                    {
                        returnValue = item.StartValue;
                        if (!item.Finished)
                        {
                            item.Finished = true;
                            return true;
                        }
                        return false;
                    }
                }
            }
            else
            {
                returnValue = item.FinishValue;
                if (!item.Finished)
                {
                    item.Finished = true;
                    return true;
                }
                return false;
            }

            return true;
        }
        returnValue = item.StartValue;
        return false;
    }

    public void SaveValue(int Index)
    {
        if (Elements.Length == 0)
            return;

        if (GetComponent<MaskableGraphic>())
            image = GetComponent<MaskableGraphic>();

        switch (Elements[Elements.Length - 1].Type)
        {
            case TransformType.Postion:
                Vector3 currPosition = transform.localPosition;

                currPosition.x /= CanvasRef.x;
                currPosition.y /= CanvasRef.y;

                if (Index == 0)
                    Elements[Elements.Length - 1].StartValue = currPosition;
                else
                    Elements[Elements.Length - 1].FinishValue = currPosition;


                break;
            case TransformType.Scale:

                if (Index == 0)
                    Elements[Elements.Length - 1].StartValue = transform.localScale;
                else
                    Elements[Elements.Length - 1].FinishValue = transform.localScale;

                break;
            case TransformType.Rotation:
                if (Index == 0)
                    Elements[Elements.Length - 1].StartValue = transform.eulerAngles;
                else
                    Elements[Elements.Length - 1].FinishValue = transform.eulerAngles;

                break;
            case TransformType.Color:
                if (Index == 0)
                {
                    if (image)
                        Elements[Elements.Length - 1].StartValue = image.color;
                }
                else
                {
                    if (image)
                        Elements[Elements.Length - 1].FinishValue = image.color;
                }

                break;
        }
    }
    public void resetValues()
    {
        foreach (TransformElement item in Elements)
        {

            item.Finished = false;


        }
        active = false;
        AutoStart = false;
        Started = false;
    }

}
