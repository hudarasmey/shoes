2O                         UNITY_UI_ALPHACLIP    #ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	mediump vec4 _Color;
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec2 in_TEXCOORD0;
out mediump vec4 vs_COLOR0;
out mediump vec2 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
vec4 u_xlat1;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    u_xlat0 = in_COLOR0 * _Color;
    vs_COLOR0 = u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD1 = in_POSITION0;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec4 _SinTime;
uniform 	vec4 _CosTime;
uniform 	vec4 _ScreenParams;
uniform 	mediump vec4 _TextureSampleAdd;
uniform 	vec4 _ClipRect;
uniform 	float _Speed;
uniform 	float _Thickness;
uniform lowp sampler2D _MainTex;
in mediump vec4 vs_COLOR0;
in mediump vec2 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
mediump vec4 u_xlat16_0;
lowp vec4 u_xlat10_0;
vec3 u_xlat1;
mediump vec4 u_xlat16_1;
bool u_xlatb1;
vec4 u_xlat2;
mediump vec4 u_xlat16_2;
vec4 u_xlat3;
bvec4 u_xlatb3;
mediump float u_xlat16_4;
bool u_xlatb6;
float u_xlat11;
bvec2 u_xlatb11;
float u_xlat16;
void main()
{
    u_xlat10_0 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat16_0 = u_xlat10_0 + _TextureSampleAdd;
    u_xlat0 = u_xlat16_0 * vs_COLOR0;
    u_xlat1.xy = _ScreenParams.xy * vec2(0.5, 0.5) + vs_TEXCOORD1.xy;
    u_xlat1.xy = u_xlat1.xy / _ScreenParams.xy;
    u_xlat1.x = u_xlat1.x + 1.0;
    u_xlat1.x = (-_SinTime.w) * _Speed + u_xlat1.x;
    u_xlat1.z = u_xlat1.x + _Thickness;
    u_xlatb11.xy = greaterThanEqual(u_xlat1.zyzy, u_xlat1.yxyx).xy;
    u_xlatb11.x = u_xlatb11.x && u_xlatb11.y;
    u_xlat16 = (-u_xlat1.y) + u_xlat1.x;
    u_xlat2.x = (-u_xlat16) + 1.0;
    u_xlat16 = u_xlat16 + (-_Thickness);
    u_xlat16 = u_xlat16 + 1.0;
    u_xlat2.x = u_xlat2.x + (-_Thickness);
    u_xlat2 = u_xlat0 / u_xlat2.xxxx;
    u_xlat16_2 = (u_xlatb11.x) ? u_xlat2 : u_xlat0;
    u_xlat3 = u_xlat16_2 / vec4(u_xlat16);
    u_xlat11 = u_xlat1.x + (-_Thickness);
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(u_xlat1.x>=u_xlat1.y);
#else
    u_xlatb1 = u_xlat1.x>=u_xlat1.y;
#endif
#ifdef UNITY_ADRENO_ES3
    u_xlatb6 = !!(u_xlat1.y>=u_xlat11);
#else
    u_xlatb6 = u_xlat1.y>=u_xlat11;
#endif
    u_xlatb1 = u_xlatb1 && u_xlatb6;
    u_xlat16_1 = (bool(u_xlatb1)) ? u_xlat3 : u_xlat16_2;
#ifdef UNITY_ADRENO_ES3
    u_xlatb3.x = !!(0.0<_CosTime.w);
#else
    u_xlatb3.x = 0.0<_CosTime.w;
#endif
    u_xlat16_0 = (u_xlatb3.x) ? u_xlat16_1 : u_xlat0;
    u_xlatb3.xy = greaterThanEqual(vs_TEXCOORD1.xyxx, _ClipRect.xyxx).xy;
    u_xlatb3.zw = greaterThanEqual(_ClipRect.zzzw, vs_TEXCOORD1.xxxy).zw;
    u_xlat3 = mix(vec4(0.0, 0.0, 0.0, 0.0), vec4(1.0, 1.0, 1.0, 1.0), vec4(u_xlatb3));
    u_xlat3.xy = vec2(u_xlat3.z * u_xlat3.x, u_xlat3.w * u_xlat3.y);
    u_xlat3.x = u_xlat3.y * u_xlat3.x;
    u_xlat16_4 = u_xlat16_0.w * u_xlat3.x + -0.00100000005;
    u_xlat3.x = u_xlat16_0.w * u_xlat3.x;
    SV_Target0.xyz = u_xlat16_0.xyz;
    SV_Target0.w = u_xlat3.x;
#ifdef UNITY_ADRENO_ES3
    u_xlatb3.x = !!(u_xlat16_4<0.0);
#else
    u_xlatb3.x = u_xlat16_4<0.0;
#endif
    if((int(u_xlatb3.x) * int(0xffffffffu))!=0){discard;}
    return;
}

#endif
                           